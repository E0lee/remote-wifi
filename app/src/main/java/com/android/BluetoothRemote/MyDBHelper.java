package com.android.BluetoothRemote;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by lee on 2015/7/19.
 */
public class MyDBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "voiceRecWifi.db";	//資料庫名稱
    private static final int DATABASE_VERSION = 1;	//資料庫版本

    private SQLiteDatabase db;

    public MyDBHelper(Context context) {	//建構子
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = this.getWritableDatabase();
    }
    public SQLiteDatabase getDatabase(){
        return db;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        //建立config資料表，詳情請參考SQL語法
        db.execSQL(DBOperation.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //oldVersion=舊的資料庫版本；newVersion=新的資料庫版本
        db.execSQL("DROP TABLE IF EXISTS " + DBOperation.TABLE_NAME);	//刪除舊有的資料表
        onCreate(db);
    }

}
