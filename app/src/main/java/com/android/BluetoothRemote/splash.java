package com.android.BluetoothRemote;

import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import com.android.RemoteWiFi.R;

public class splash extends Activity{
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		
		
		Timer timer = new Timer(true);
        timer.schedule(new timerTask(), 1000, 250);
    }
 
    public class timerTask extends TimerTask{
 
        public void run()
        {
            Intent it = new Intent();
            it.setClass(splash.this, BluetoothChat.class);
            startActivity(it);
            this.cancel();
            splash.this.finish();
        }
    }
}