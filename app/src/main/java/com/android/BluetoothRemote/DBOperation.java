package com.android.BluetoothRemote;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by lee on 2015/7/19.
 */
public class DBOperation {
    // 表格名稱
    public static final String TABLE_NAME = "WIFIVoiceRecognizer";

    // 編號表格欄位名稱，固定不變
    public static final String KEY_ID = "_ID";

    // 其它表格欄位名稱
    public static final String VOICE_COLUMN = "voice";
    public static final String ACTION_COLUMN = "action";
    //public static final String CONTENT_COLUMN = "content";
    // 使用上面宣告的變數建立表格的SQL指令
    public static final String CREATE_TABLE =
            "create table " + TABLE_NAME + " (" +
                    KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    VOICE_COLUMN +" TEXT NOT NULL, "+
                    ACTION_COLUMN + " TEXT NOT NULL) ";
    // 資料庫物件
    private SQLiteDatabase db;
    private String TAG ="DBOperation";
    // 建構子
    MyDBHelper helper;
    DBOperation(Context context){
        helper = new MyDBHelper(context);
        db = helper.getDatabase();
    }
    // 關閉資料庫，一般的應用都不需要修改
    public void close() {
        db.close();
    }

    public boolean update(long id,int number,String name,String content){
        ContentValues cv = new ContentValues();
        cv.put(VOICE_COLUMN,number);
        cv.put(ACTION_COLUMN,name);

        String where = KEY_ID + "=" + id;
        return db.update(TABLE_NAME,cv,where,null)>0;
    }
    public boolean delete(long id){
        // 設定條件為編號，格式為「欄位名稱=資料」
        String where = KEY_ID + "=" + id;
        // 刪除指定編號資料並回傳刪除是否成功
        return db.delete(TABLE_NAME, where , null) > 0;
    }
    public long insert(String voice,String action){
        ContentValues cv = new ContentValues();
        cv.put(VOICE_COLUMN,voice);
        cv.put(ACTION_COLUMN,action);
        Log.d(TAG, "insert " + voice + " Action is " + action);
        return db.insert(TABLE_NAME,null,cv);

    }
    public void insertArray(String[] voice,String action){
        Log.d(TAG, "insert array" +voice.length);
        for (int i=0; i<voice.length;i++){
            if(getVoiceResult(voice[i])=="finalResultIsNull"){
                Log.d(TAG, "匯入");
                this.insert(voice[i],action);
            }
        }


    }
    public String getById(long id){
        String where = KEY_ID + "=" + id;
        Cursor result = db.query(
                TABLE_NAME, null, where, null, null, null, null, null);
        result.moveToFirst();
        return result.getString(1).toString();
    }
    public String getVoiceResult(String voice){
        String finalResult=null;
        String where = VOICE_COLUMN + "='" + voice+"'";
        Cursor cursor = db.query(
        TABLE_NAME, null, where, null, null, null, null, null);
        cursor.moveToFirst();
        if(cursor!=null && cursor.getCount()>0){
            return  cursor.getString(2).toString();
        }
        return  "finalResultIsNull";
    }
}
