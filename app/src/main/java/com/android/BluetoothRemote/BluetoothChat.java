/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.BluetoothRemote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.os.Messenger;
import android.speech.RecognizerIntent;
import android.widget.Button;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGestureListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar.LayoutParams;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.android.RemoteWiFi.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * This is the main Activity that displays the current chat session.
 */
public  class BluetoothChat extends Activity implements OnTouchListener, OnGestureListener, android.view.GestureDetector.OnGestureListener
 ,TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener, Handler.Callback{
    // Debugging
    private static final String TAG = "WifiChat";
    private static final boolean D = true;
    
    //menu
    private String changeMenu = "none";//YILINEDIT-voiceRecg改成字串
    protected static final int st = Menu.FIRST;
    protected static final int close = Menu.FIRST+1;
    protected static final int scan = Menu.FIRST+2;
    //YILINEDIT-voiceRecg
    protected static final int record = Menu.FIRST+3;
    protected static final int record_learning =Menu.FIRST+4;
    protected static final int record_learning_stop =Menu.FIRST+5;
  //YILINEDIT
    private SharedPreferences OPenCam;
    private static final String OpenCamData ="OPENCAMDATA";
    private static final String CAM = "CAM";


    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    //dialog
    private static final int DIALOG_MESSAGE = 1;
    private static final int DIALOG_MENU = 2;

    
    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int LAMP_PIC1 = 3;
    private static final int LAMP_PIC2 = 4;
    private static final int LAMP_PIC3 = 5;
    private static final int LAMP_PIC4 = 6;
    private static final int LAMP_PIC5 = 7;
    private static final int LAMP_PIC6 = 8;
    private static final int LAMP_PIC7 = 9;
    private static final int LAMP_PIC8 = 10;
    private static final int LAMP_PIC9 = 11;
    private static final int LAMP_PIC10 = 12;
    private static final int LAMP_PIC11 = 13;
    private static final int LAMP_PIC12 = 14;
    private static final int RECORD = 15;//YILINEDIT-voiceRecg
    private static final int RETRUN_MAIN =16;
    //YILINEDIT－情境
    public Handler handler_sendMacro = new Handler(this);
    private static final int SEND_MESSAGE_MACRO = 1;
    // Layout Views
    public TextView mTitle;
    public ListView mConversationView;
    private Spinner spinner,spinner2,spinner3;
    
    public Button mButton01,mButton02,mButton03,mButton04,mButton05,mButton06,mButton07,
    mButton08,mButton09,mButton10,mButton11,mButton12,mButton13,mButton14,mButton15,mButton16,
    mButton17,mButton18,mButton19,mButton20,mButton21,mButton22,mButton23,mButton24,mButton25
    ,mButton26,mButton27,mButton28,mButton29,mButton30,mButton31,mButton32,mButton33,mButton34
    ,mButton35,mButton36,mButton37,mButton38,mButton39,mButton40,mButton41,mButton42,mButton43
    ,mButton44,mButton45,mButton46,mButton47,mButton48,mButton49,mButton50,mButton51,mButton52
    ,mButton53,mButton54,mButton55,mButton56,mButton57,mButton58,mButton59,mButton60,mButton61
    ,mButton62,mButton63,mButton64,mButton65,mButton66,mButton67,mButton68,mButton69,mButton70
    ,mButton71,mButton72,mButton73,mButton74,mButton75,mButton76,mButton77,mButton78,mButton79
    ,mButton80,mButton81,mButton82,mButton83,mButton84,mButton85,mButton86,mButton87,mButton88
    ,mButton89,mButton90,mButton91,mButton92,mButton93,mButton94,mButton95,mButton96,mButton97
    ,mButton98,mButton99,mButton100,mButton101,mButton102,mButton103,mButton104,mButton105,mButton106
    ,mButton107,mButton108,mButton109,mButton110,mButton111,mButton112,mButton113,mButton114,mButton115
    ,mButton116,mButton117,mButton118,mButton119,mButton120,mButton121,mButton122,mButton123,mButton124
    ,mButton125,mButton126,mButton127,mButton128,mButton129,mButton130,mButton131,mButton132,mButton133
    ,mButton134,mButton135,mButton136,mButton137,mButton138,mButton139,mButton140,mButton141,mButton142
    ,mButton143,mButton144,mButton145,mButton146,mButton147,mButton148,mButton149,mButton150,mButton151
    ,mButton152,mButton153,mButton154,mButton155,mButton156,set00,set01,set02,custom1set,custom2set,custom3set,lampset,lampok,marco1,marco2,marco3
    ,marco4,marco5,marco6,marcoset1,marcoset2,marcoset3,marcoset4,marcoset5,marcoset6,curtainset,marcosetok
    ,marcoset1ok,marcoset2ok,marcoset3ok,marcoset4ok,marcoset5ok,marcoset6ok,curok,s1,s2,s3,s4,s5,opcs,s6,s7,s8,s9,s10,s11,s12
    ,s13,s14,s15,s16,s17,s18,s19,s20,s21,s22,s23,s24,s25,s26,s27,s28,s29,s30,home0,home1,home2,home3,home4,home5,home6,home7,home8,home9
    ,row1,row2,row3,row4,row5,row6,row7,f1,f2,f3,f4,f5,f6,firstset,firstok,airset,airok;
    
    public EditText meditText1,meditText2,meditText3,meditText4,meditText5,meditText6
    ,meditText7,meditText8,meditText9,meditText10,meditText11,meditText12,meditText13
    ,meditText14,meditText15,meditText16,meditText17,meditText18,meditText19,meditText20
    ,meditText21,meditText22,meditText23,meditText24,meditText25,meditText26,meditText27
    ,meditText28,meditText29,meditText30,meditText31,meditText32,meditText33,meditText34
    ,meditText35,meditText36,meditText37,meditText38,meditText39,meditText40,meditText41
    ,meditText42,meditText43,meditText44,meditText45,meditText46,meditText47,meditText48
    ,meditText49,meditText50,meditText51,meditText52,meditText53,meditText54,meditText55
    ,meditText56,meditText57,meditText58,meditText59,meditText60,meditText61,meditText62
    ,meditText63,lampeditText1,lampeditText2,lampeditText3,cur1edit,cur2edit,cur3edit
    ,cur4edit,cur5edit,cur6edit,cur7edit,cur8edit,cur9edit,cur10edit,cur11edit,cur12edit
    ,custom1edit,custom2edit,custom3edit,lampeditpic1,lampeditpic2,lampeditpic3,lampeditpic4,lampeditpic5,lampeditpic6
    ,lampeditpic7,lampeditpic8,lampeditpic9,lampeditpic10,lampeditpic11,lampeditpic12
    ,lampeditText4,lampeditText5,lampeditText6,lampeditText7,lampeditText8,lampeditText9,lampeditText10
    ,lampeditText11,lampeditText12,marcosetname,marcoset1name,marcoset2name,marcoset3name
    ,marcoset4name,marcoset5name,marcoset6name,marco1EditText,marcosetbtn,marco1setbtn,marco2setbtn,marco3setbtn
    ,marco4setbtn,marco5setbtn,marco6setbtn,f1edit,f2edit,f3edit,f4edit,f5edit,f6edit,ip,port,opencam,ip_gcm,port_gcm;//YILINEDIT
    
    public TextView cus1name,cus2name,cus3name,cur1name,cur2name,cur3name,cur4name,cur5name,cur6name
    ,cur7name,cur8name,cur9name,cur10name,cur11name,cur12name;
    
    public ImageView imagebtn1,imagebtn2,imagebtn3,imagebtn4,imagebtn5,imagebtn6,imagebtn7,imagebtn8,imagebtn9,imagebtn10,imagebtn11,imagebtn12;
    
    public TabWidget tabs;
    
    public ScrollView scrollView1;
    
    
    int count1,count2,count3,count4,count5,count6,count7,count8,count9,count10,count11,count12,count13,count14,count15,count16,count17,count18_
    ,count19,count20,count21,count22,count23,count24,count25,count26,count27,count28,count29,count30,count31,count32,count33,count34,count35,count36
    ,count37,count38,count39,count40,count41,count42,count43,count44,count45,count46,count47,count48,count49,count50,count51,count52,count53,count54
    ,count55,count56,count57,count58,count59,count60,count61,count62,count63,count64,count65,count66,count67,count68,count69,count70,count71,count72
    ,count73,count74,count75,count76,count77,count78,count79,count80,count81,count82,count83,count84,count85,count86,count87,count88,count89,count90
    ,count91,count92,count93,count94,count95,count96,count97,count98,count99,count100,count101,count102,count103,count104,count105,count106,count107
    ,count108,count109,count110,count111,count112,count113,count114,count115,count116,count117,count118,count119,count120,count121,count122,count123
    ,count124,count125,count126,count127,count128,count129,count130,count131,count132,count133,count134,count135,count136,count137,count138,count139
    ,count140,count141,count142,count143,count144,count145,count146,count147,count148,count149,count150,count151,count152 = 0;
    
    
    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Array adapter for the conversation thread
    public ArrayAdapter<String> mConversationArrayAdapter;
    // String buffer for outgoing messages
    private StringBuffer mOutStringBuffer;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the chat services
    private BluetoothChatService mChatService = null;

    private View view1, view2, view3, view4, view5,view6,view7,view8,view9,view10,view11;//需要滑动的页卡  
   //YILINEDIT-頁卡的編號
    private int page_macro=0,page_lamp=1,page_tv=2,page_air=3,page_custom1=4,page_custom2=5,page_custom3=6,page_ip_config=7,page_cam_config=8,page_about=9;
   private ViewPager viewPager;//viewpager
    private List<View> viewList;//把需要滑动的页卡添加到这个list中 
    private List<String> titleList;//viewpager的标题  
    private String[] lunch  = {"1", "2", "3", "4", "5", "6"};
    private ArrayAdapter<String> lunchList; 
    
    //Socket socketa = null;
	Socket socket = null;
    Socket socket_gcm = null;
    
    private SharedPreferences lamp;
    private static final String lampdata = "DATA";
    private static final String btn01name = "btn01name";
    private static final String btn02name = "btn02name";
    private static final String btn03name = "btn03name";
    private static final String btn04name = "btn04name";
    private static final String btn05name = "btn05name";
    private static final String btn06name = "btn06name";
    private static final String btn07name = "btn07name";
    private static final String btn08name = "btn08name";
    private static final String btn09name = "btn09name";
    private static final String btn10name = "btn10name";
    private static final String btn11name = "btn11name";
    private static final String btn12name = "btn12name";
    private static final String btn1pic = "btn1pic";
    private static final String btn2pic = "btn2pic";
    private static final String btn3pic = "btn3pic";
    private static final String btn4pic = "btn4pic";
    private static final String btn5pic = "btn5pic";
    private static final String btn6pic = "btn6pic";
    private static final String btn7pic = "btn7pic";
    private static final String btn8pic = "btn8pic";
    private static final String btn9pic = "btn9pic";
    private static final String btn10pic = "btn10pic";
    private static final String btn11pic = "btn11pic";
    private static final String btn12pic = "btn12pic";
    
    private SharedPreferences curtain;
    private static final String curtaindata = "DATA10";
    private static final String curtain1 = "curtain1";
    private static final String curtain2 = "curtain2";
    private static final String curtain3 = "curtain3";
    private static final String curtain4 = "curtain4";
    private static final String curtain5 = "curtain5";
    private static final String curtain6 = "curtain6";
    private static final String curtain7 = "curtain7";
    private static final String curtain8 = "curtain8";
    private static final String curtain9 = "curtain9";
    private static final String curtain10 = "curtain10";
    private static final String curtain11 = "curtain11";
    private static final String curtain12 = "curtain12";
    
    private SharedPreferences setting;
    private static final String custom1data = "DATA1";
    private static final String custom1name = "custom1name";
    private static final String btn54name = "btn54name";
    private static final String btn55name = "btn55name";
    private static final String btn56name = "btn56name";
    private static final String btn57name = "btn57name";
    private static final String btn58name = "btn58name";
    private static final String btn59name = "btn59name";
    private static final String btn60name = "btn60name";
    private static final String btn61name = "btn61name";
    private static final String btn62name = "btn62name";
    private static final String btn63name = "btn63name";
    private static final String btn64name = "btn64name";
    private static final String btn65name = "btn65name";
    private static final String btn66name = "btn66name";
    private static final String btn67name = "btn67name";
    private static final String btn68name = "btn68name";
    private static final String btn99name = "btn99name";
    private static final String btn100name = "btn100name";
    private static final String btn101name = "btn101name";
    private static final String btn102name = "btn102name";
    private static final String btn103name = "btn103name";
    private static final String btn104name = "btn104name";
    
    private SharedPreferences settingb;
    private static final String custom2data = "DATA2";
    private static final String custom2name = "custom2name";
    private static final String btn69name = "btn69name";
    private static final String btn70name = "btn70name";
    private static final String btn71name = "btn71name";
    private static final String btn72name = "btn72name";
    private static final String btn73name = "btn73name";
    private static final String btn74name = "btn74name";
    private static final String btn75name = "btn75name";
    private static final String btn76name = "btn76name";
    private static final String btn77name = "btn77name";
    private static final String btn78name = "btn78name";
    private static final String btn79name = "btn79name";
    private static final String btn80name = "btn80name";
    private static final String btn81name = "btn81name";
    private static final String btn82name = "btn82name";
    private static final String btn83name = "btn83name";
    private static final String btn105name = "btn105name";
    private static final String btn106name = "btn106name";
    private static final String btn107name = "btn107name";
    private static final String btn108name = "btn108name";
    private static final String btn109name = "btn109name";
    private static final String btn110name = "btn110name";
    
    private SharedPreferences setting2;
    private static final String custom3data = "DATA3";
    private static final String custom3name = "custom3name";
    private static final String btn84name = "btn84name";
    private static final String btn85name = "btn85name";
    private static final String btn86name = "btn86name";
    private static final String btn87name = "btn87name";
    private static final String btn88name = "btn88name";
    private static final String btn89name = "btn89name";
    private static final String btn90name = "btn90name";
    private static final String btn91name = "btn91name";
    private static final String btn92name = "btn92name";
    private static final String btn93name = "btn93name";
    private static final String btn94name = "btn94name";
    private static final String btn95name = "btn95name";
    private static final String btn96name = "btn96name";
    private static final String btn97name = "btn97name";
    private static final String btn98name = "btn98name";
    private static final String btn111name = "btn111name";
    private static final String btn112name = "btn112name";
    private static final String btn113name = "btn113name";
    private static final String btn114name = "btn114name";
    private static final String btn115name = "btn115name";
    private static final String btn116name = "btn116name";
    
    private SharedPreferences marcoset;
    private static final String marcosetdata = "DATA4";
    private static final String marconame = "marconame";
    private static final String marcobtn = "marcobtn";
    
    private SharedPreferences marco1set;
    private static final String marcoset1data = "DATA5";
    private static final String marco1name = "marco1name";
    private static final String marco1btn = "marco1btn";
    
    private SharedPreferences marco2set;
    private static final String marcoset2data = "DATA6";
    private static final String marco2name = "marco2name";
    private static final String marco2btn = "marco2btn";
    
    private SharedPreferences marco3set;
    private static final String marcoset3data = "DATA7";
    private static final String marco3name = "marco3name";
    private static final String marco3btn = "marco3btn";
    
    private SharedPreferences marco4set;
    private static final String marcoset4data = "DATA8";
    private static final String marco4name = "marco4name";
    private static final String marco4btn = "marco4btn";
    
    private SharedPreferences marco5set;
    private static final String marcoset5data = "DATA9";
    private static final String marco5name = "marco5name";
    private static final String marco5btn = "marco5btn";
  
    private SharedPreferences airseting;
    private static final String airdata = "DATA10";
    private static final String btn17name = "btn17name";
    private static final String btn18name = "btn18name";
    private static final String btn19name = "btn19name";
    private static final String btn20name = "btn20name";
    private static final String btn21name = "btn21name";
    private static final String btn22name = "btn22name";
    
    private SharedPreferences IPname;
    private static final String Ipdata = "IPDATA";
    private static final String IP = "IP";
    private static final String PORT = "PORT";
    private static final String IP_gcm = "IP_gcm";
    private static final String PORT_gcm = "PORT_gcm";
    private static final String gcmID = "gcn_ID";
    private static String token ="";
    private SharedPreferences slave;
    private static final String slaveiddata = "DATA11";
    private static final String slaveid = "slaveid";

    //YILINEDIT-voiceRecg
    private boolean isVoiceRecMode =false;
    private boolean isRecTypeLearning =false;
    private String recAction ="";
    private String[] recResult = new String[5];
  //!@#
    FloatService mService;
    private ServiceConnection mConnection =  new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("AppFloater", "Connected to service");
            mService = ((FloatService.FloatBinder) service).getService();
         
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("AppFloat", "Disconnected from service");
            
        }
    };  
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(D) Log.e(TAG, "+++ ON CREATE +++");

        // Set up the window layout
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
         setContentView(R.layout.main);

        viewPager = (ViewPager)this.findViewById(R.id.viewPager);
       // getLayoutInflater();
        LayoutInflater lf =  LayoutInflater.from(this);
        view1 = lf.inflate(R.layout.first, null);
  		view2 = lf.inflate(R.layout.lamp, null);
   		view3 = lf.inflate(R.layout.tv1, null);
  		view4 = lf.inflate(R.layout.air, null);
  		view5 = lf.inflate(R.layout.custom1, null);
  		view6 = lf.inflate(R.layout.custom2, null);
  		view7 = lf.inflate(R.layout.custom3, null);

  		view8 = lf.inflate(R.layout.activity_main, null);
  		//view9 = lf.inflate(R.layout.config, null);
  		view10 = lf.inflate(R.layout.about, null);

  		viewPager.setOffscreenPageLimit(10);

  		viewList = new ArrayList<View>();// 将要分页显示的View装入数组中

  		viewList.add(view1);
  		viewList.add(view2);
  		viewList.add(view3);
  		viewList.add(view4);
  		viewList.add(view5);
  		viewList.add(view6);
  		viewList.add(view7);
  		viewList.add(view8);
  		//viewList.add(view9);
  		viewList.add(view10);
  		//viewList.add(view11);

  		
  	//YILINEDIT
  		Intent intent = new Intent(BluetoothChat.this, FloatService.class);
	    bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
	    
  		PagerAdapter apdter = new PagerAdapter() {
  		    
  		   @Override
  		   public boolean isViewFromObject(View arg0, Object arg1) {
  		    // TODO Auto-generated method stub
  		    return arg0 == arg1;
  		   }
  		    
  		   public int getItemPosition(Object object) {  
  		    return POSITION_NONE;  
  		   } 
  		   
  		   @Override
  		   public int getCount() {
  		    // TODO Auto-generated method stub
  		    return viewList.size();
  		   }
  		   
  		   
  		 
  		   @Override
  		   public void destroyItem(ViewGroup container, int position,
  		     Object object) {
  		    // TODO Auto-generated method stub
  		    ((ViewPager)container).removeView(viewList.get(position));
  		   }
  		
  		   
  		   
  		   @Override
  		   public CharSequence getPageTitle(int position) {
  		     //TODO Auto-generated method stub
  			 
  		    return titleList.get(position);//這裡需回傳Title的名稱,position就是每個Page的index 
  		   }
  		
  		 
  		   @Override
  		   public Object instantiateItem(ViewGroup container, int position) {
  			 switch (position) {
  			 
  			 
  			// case 0 :
  				
  				 //break;
  				 
  				 
  			 case 1 :
  				f1 = (Button) findViewById(R.id.f1);
  				f2 = (Button) findViewById(R.id.f2);
  				f3 = (Button) findViewById(R.id.f3);
  				f4 = (Button) findViewById(R.id.f4);
  				f5 = (Button) findViewById(R.id.f5);
  				f6 = (Button) findViewById(R.id.f6);
  				home0 = (Button) findViewById(R.id.home0);
  				//findViewById(R.id.firstset).setOnClickListener(new ButtonListener());;
  				
  				f1.setOnClickListener(new ButtonListener());
  				f2.setOnClickListener(new ButtonListener());
  				f3.setOnClickListener(new ButtonListener());
  				f4.setOnClickListener(new ButtonListener());
  				f5.setOnClickListener(new ButtonListener());
  				f6.setOnClickListener(new ButtonListener());
  				home0.setOnClickListener(new ButtonListener());

                //YILINEDIT-情境設定
                f1.setOnLongClickListener(new ButtonLongListner());
                f2.setOnLongClickListener(new ButtonLongListner());
                f3.setOnLongClickListener(new ButtonLongListner());
                f4.setOnLongClickListener(new ButtonLongListner());
                f5.setOnLongClickListener(new ButtonLongListner());
                f6.setOnLongClickListener(new ButtonLongListner());

  				marcoset = getSharedPreferences(marcosetdata,0);
  				marco1set = getSharedPreferences(marcoset1data,0);
  				marco2set = getSharedPreferences(marcoset2data,0);
  				marco3set = getSharedPreferences(marcoset3data,0);
  				marco4set = getSharedPreferences(marcoset4data,0);
  				marco5set = getSharedPreferences(marcoset5data,0);
  				
  				f1.setText(marcoset.getString(marconame,""));
  				f2.setText(marco1set.getString(marco1name,""));
  				f3.setText(marco2set.getString(marco2name,""));
  				f4.setText(marco3set.getString(marco3name,""));
  				f5.setText(marco4set.getString(marco4name,""));
  				f6.setText(marco5set.getString(marco5name,""));
  				 break;
  			 
  			 case 2 :
  				lamp = getSharedPreferences(lampdata,0);
  				
  				imagebtn1 = (ImageView) findViewById(R.id.imagebtn1);
  				imagebtn2 = (ImageView) findViewById(R.id.imagebtn2);
  				imagebtn3 = (ImageView) findViewById(R.id.imagebtn3);
  				imagebtn4 = (ImageView) findViewById(R.id.imagebtn4);
  				imagebtn5 = (ImageView) findViewById(R.id.imagebtn5);
  				imagebtn6 = (ImageView) findViewById(R.id.imagebtn6);
  				imagebtn7 = (ImageView) findViewById(R.id.imagebtn7);
  				imagebtn8 = (ImageView) findViewById(R.id.imagebtn8);
  				imagebtn9 = (ImageView) findViewById(R.id.imagebtn9);
  				imagebtn10 = (ImageView) findViewById(R.id.imagebtn10);
  				imagebtn11 = (ImageView) findViewById(R.id.imagebtn11);
  				imagebtn12 = (ImageView) findViewById(R.id.imagebtn12);
  				
  				String stringuri1 = lamp.getString(btn1pic,"");
  				String stringuri2 = lamp.getString(btn2pic,"");
  				String stringuri3 = lamp.getString(btn3pic,"");
  				String stringuri4 = lamp.getString(btn4pic,"");
  				String stringuri5 = lamp.getString(btn5pic,"");
  				String stringuri6 = lamp.getString(btn6pic,"");
  				String stringuri7 = lamp.getString(btn7pic,"");
  				String stringuri8 = lamp.getString(btn8pic,"");
  				String stringuri9 = lamp.getString(btn9pic,"");
  				String stringuri10 = lamp.getString(btn10pic,"");
  				String stringuri11 = lamp.getString(btn11pic,"");
  				String stringuri12 = lamp.getString(btn12pic,"");
  				
  				if(stringuri1==null)
  				{
  				imagebtn1.setImageDrawable(null);
  				}else{
  				imagebtn1.setImageURI(Uri.parse(stringuri1));
  				Log.e("uri1", stringuri1);
  				}
  				
  				if(stringuri2==null)
  				{
  				imagebtn2.setImageDrawable(null);
  				}else{
  				imagebtn2.setImageURI(Uri.parse(stringuri2));
  				}
  				
  				if(stringuri3==null)
  				{
  				imagebtn3.setImageDrawable(null);
  				}else{
  				imagebtn3.setImageURI(Uri.parse(stringuri3));
  				}
  				
  				if(stringuri4==null)
  				{
  				imagebtn4.setImageDrawable(null);
  				}else{
  				imagebtn4.setImageURI(Uri.parse(stringuri4));
  				}
  				
  				if(stringuri5==null)
  				{
  					imagebtn5.setImageDrawable(null);
  				}else{
  				imagebtn5.setImageURI(Uri.parse(stringuri5));
  				}
  				
  				if(stringuri6==null)
  				{
  					imagebtn6.setImageDrawable(null);
  				}else{
  				imagebtn6.setImageURI(Uri.parse(stringuri6));
  				}
  				
  				if(stringuri7==null)
  				{
  					imagebtn7.setImageDrawable(null);
  				}else{
  				imagebtn7.setImageURI(Uri.parse(stringuri7));
  				}
  				
  				if(stringuri8==null)
  				{
  				imagebtn8.setImageDrawable(null);
  				}else{
  				imagebtn8.setImageURI(Uri.parse(stringuri8));
  				}
  				
  				if(stringuri9==null)
  				{
  				imagebtn9.setImageDrawable(null);
  				}else{
  				imagebtn9.setImageURI(Uri.parse(stringuri9));
  				}
  				
  				if(stringuri10==null)
  				{
  				imagebtn10.setImageDrawable(null);
  				}else{
  				imagebtn10.setImageURI(Uri.parse(stringuri10));
  				}
  				
  				if(stringuri11==null)
  				{
  				imagebtn11.setImageDrawable(null);
  				}else{
  				imagebtn11.setImageURI(Uri.parse(stringuri11));
  				}
  				
  				if(stringuri12==null)
  				{
  				imagebtn12.setImageDrawable(null);
  				}else{
  				imagebtn12.setImageURI(Uri.parse(stringuri12));
  				}
  				
  				
  				
  				
  				
  				mButton01 = (Button) findViewById(R.id.btn01);
  		        mButton02 = (Button) findViewById(R.id.btn02);
  		        mButton03 = (Button) findViewById(R.id.btn03);
  		        mButton04 = (Button) findViewById(R.id.btn04);
  		        mButton05 = (Button) findViewById(R.id.btn05);
  		        mButton06 = (Button) findViewById(R.id.btn06);
  		        mButton07 = (Button) findViewById(R.id.btn07);
  		        mButton08 = (Button) findViewById(R.id.btn08);
  		        mButton09 = (Button) findViewById(R.id.btn09);
  		        mButton10 = (Button) findViewById(R.id.btn10);
  		        mButton11 = (Button) findViewById(R.id.btn11);
  		        mButton12 = (Button) findViewById(R.id.btn12);
  		        home1 = (Button) findViewById(R.id.home1);
  		        
  		        
  		        findViewById(R.id.lampseting).setOnClickListener(new ButtonListener());
  		        mButton01.setOnClickListener(new ButtonListener());
  		        mButton02.setOnClickListener(new ButtonListener());
  		        mButton03.setOnClickListener(new ButtonListener());
  		        mButton04.setOnClickListener(new ButtonListener());
  		        mButton05.setOnClickListener(new ButtonListener());
  		        mButton06.setOnClickListener(new ButtonListener());
  		        mButton07.setOnClickListener(new ButtonListener());
  		        mButton08.setOnClickListener(new ButtonListener());
  		        mButton09.setOnClickListener(new ButtonListener());
  		        mButton10.setOnClickListener(new ButtonListener());
  		        mButton11.setOnClickListener(new ButtonListener());
  		        mButton12.setOnClickListener(new ButtonListener());
  		        home1.setOnClickListener(new ButtonListener());
  		        
  		        
  		        //lamp = getSharedPreferences(lampdata,0);
  		        mButton01.setText(lamp.getString(btn01name,""));
  		        mButton02.setText(lamp.getString(btn02name,""));
  		        mButton03.setText(lamp.getString(btn03name,""));
  		        mButton04.setText(lamp.getString(btn04name,""));
  		        mButton05.setText(lamp.getString(btn05name,""));
  		        mButton06.setText(lamp.getString(btn06name,""));
  		        mButton07.setText(lamp.getString(btn07name,""));
  		        mButton08.setText(lamp.getString(btn08name,""));
  		        mButton09.setText(lamp.getString(btn09name,""));
  		        mButton10.setText(lamp.getString(btn10name,""));
  		        mButton11.setText(lamp.getString(btn11name,""));
  		        mButton12.setText(lamp.getString(btn12name,""));
  		        
  		        break;
  		        
  			 case 3 :
  				mButton33 = (Button) findViewById(R.id.btn33);
//                mButton34 = (Button) findViewById(R.id.btn34);
                mButton35 = (Button) findViewById(R.id.btn35);
//                mButton36 = (Button) findViewById(R.id.btn36);
//                mButton37 = (Button) findViewById(R.id.btn37);
//                mButton38 = (Button) findViewById(R.id.btn38);
//                mButton39 = (Button) findViewById(R.id.btn39);
//                mButton40 = (Button) findViewById(R.id.btn40);
//                mButton41 = (Button) findViewById(R.id.btn41);
                mButton42 = (Button) findViewById(R.id.btn42);
                mButton43 = (Button) findViewById(R.id.btn43);
                mButton44 = (Button) findViewById(R.id.btn44);
                mButton45 = (Button) findViewById(R.id.btn45);
                mButton46 = (Button) findViewById(R.id.btn46);
                mButton47 = (Button) findViewById(R.id.btn47);
                mButton48 = (Button) findViewById(R.id.btn48);
                mButton49 = (Button) findViewById(R.id.btn49);
                mButton50 = (Button) findViewById(R.id.btn50);
                mButton51 = (Button) findViewById(R.id.btn51);
                mButton52 = (Button) findViewById(R.id.btn52);
                mButton53 = (Button) findViewById(R.id.btn53);
                mButton153 = (Button) findViewById(R.id.btn153);
                mButton154 = (Button) findViewById(R.id.btn154);
                mButton155 = (Button) findViewById(R.id.btn155);
                mButton156 = (Button) findViewById(R.id.btn156);
                home3 = (Button) findViewById(R.id.home3);
                
                mButton33.setOnClickListener(new ButtonListener());
//                mButton34.setOnClickListener(new ButtonListener());
                mButton35.setOnClickListener(new ButtonListener());
//                mButton36.setOnClickListener(new ButtonListener());
//                mButton37.setOnClickListener(new ButtonListener());
//                mButton38.setOnClickListener(new ButtonListener());
//                mButton39.setOnClickListener(new ButtonListener());
//                mButton40.setOnClickListener(new ButtonListener());
//                mButton41.setOnClickListener(new ButtonListener());
                mButton42.setOnClickListener(new ButtonListener());
                mButton43.setOnClickListener(new ButtonListener());
                mButton44.setOnClickListener(new ButtonListener());
                mButton45.setOnClickListener(new ButtonListener());
                mButton46.setOnClickListener(new ButtonListener());
                mButton47.setOnClickListener(new ButtonListener());
                mButton48.setOnClickListener(new ButtonListener());
                mButton49.setOnClickListener(new ButtonListener());
                mButton50.setOnClickListener(new ButtonListener());
                mButton51.setOnClickListener(new ButtonListener());
                mButton52.setOnClickListener(new ButtonListener());
                mButton53.setOnClickListener(new ButtonListener());
                mButton153.setOnClickListener(new ButtonListener());
                mButton154.setOnClickListener(new ButtonListener());
                mButton155.setOnClickListener(new ButtonListener());
                mButton156.setOnClickListener(new ButtonListener());
                home3.setOnClickListener(new ButtonListener());
                
                break;
                
  			 case 4 :
  				mButton13 = (Button) findViewById(R.id.btn13);
                mButton14 = (Button) findViewById(R.id.btn14);
                mButton15 = (Button) findViewById(R.id.btn15);
                mButton16 = (Button) findViewById(R.id.btn16);
                mButton17 = (Button) findViewById(R.id.btn17);
                mButton18 = (Button) findViewById(R.id.btn18);
                mButton19 = (Button) findViewById(R.id.btn19);
                mButton20 = (Button) findViewById(R.id.btn20);
                mButton21 = (Button) findViewById(R.id.btn21);
                mButton22 = (Button) findViewById(R.id.btn22);
                mButton23 = (Button) findViewById(R.id.btn23);
                mButton24 = (Button) findViewById(R.id.btn24);
//                mButton25 = (Button) findViewById(R.id.btn25);
//                mButton26 = (Button) findViewById(R.id.btn26);
//                mButton27 = (Button) findViewById(R.id.btn27);
//                mButton28 = (Button) findViewById(R.id.btn28);
//                mButton29 = (Button) findViewById(R.id.btn29);
//                mButton30 = (Button) findViewById(R.id.btn30);
//                mButton31 = (Button) findViewById(R.id.btn31);
//                mButton32 = (Button) findViewById(R.id.btn32);
                home2 = (Button) findViewById(R.id.home2);
                airset = (Button) findViewById(R.id.airset);
                
                mButton13.setOnClickListener(new ButtonListener());
                mButton14.setOnClickListener(new ButtonListener());
                mButton15.setOnClickListener(new ButtonListener());
                mButton16.setOnClickListener(new ButtonListener());
                mButton17.setOnClickListener(new ButtonListener());
                mButton18.setOnClickListener(new ButtonListener());
                mButton19.setOnClickListener(new ButtonListener());
                mButton20.setOnClickListener(new ButtonListener());
                mButton21.setOnClickListener(new ButtonListener());
                mButton22.setOnClickListener(new ButtonListener());
                mButton23.setOnClickListener(new ButtonListener());
                mButton24.setOnClickListener(new ButtonListener());
//                mButton25.setOnClickListener(new ButtonListener());
//                mButton26.setOnClickListener(new ButtonListener());
//                mButton27.setOnClickListener(new ButtonListener());
//                mButton28.setOnClickListener(new ButtonListener());
//                mButton29.setOnClickListener(new ButtonListener());
//                mButton30.setOnClickListener(new ButtonListener());
//                mButton31.setOnClickListener(new ButtonListener());
//                mButton32.setOnClickListener(new ButtonListener());
                home2.setOnClickListener(new ButtonListener());
                airset.setOnClickListener(new ButtonListener());
                
                airseting = getSharedPreferences(airdata,0);
                mButton17.setText(airseting.getString(btn17name,""));
  		        mButton18.setText(airseting.getString(btn18name,""));
  		        mButton19.setText(airseting.getString(btn19name,""));
  		        mButton20.setText(airseting.getString(btn20name,""));
  		        mButton21.setText(airseting.getString(btn21name,""));
  		        mButton22.setText(airseting.getString(btn22name,""));
                break; 
                
  			
                
  			 case 5 :
//   			mButton54 = (Button) findViewById(R.id.btn54);
//             	mButton55 = (Button) findViewById(R.id.btn55);
//             	mButton56 = (Button) findViewById(R.id.btn56);
//             	mButton57 = (Button) findViewById(R.id.btn57);
//             	mButton58 = (Button) findViewById(R.id.btn58);
//             	mButton59 = (Button) findViewById(R.id.btn59);
//             	mButton60 = (Button) findViewById(R.id.btn60);
//             	mButton61 = (Button) findViewById(R.id.btn61);
//             	mButton62 = (Button) findViewById(R.id.btn62);
             	mButton63 = (Button) findViewById(R.id.btn63);
             	mButton64 = (Button) findViewById(R.id.btn64);
             	mButton65 = (Button) findViewById(R.id.btn65);
             	mButton66 = (Button) findViewById(R.id.btn66);
             	mButton67 = (Button) findViewById(R.id.btn67);
             	mButton68 = (Button) findViewById(R.id.btn68);
//             	mButton99 = (Button) findViewById(R.id.btn99);
//             	mButton100 = (Button) findViewById(R.id.btn100);
//             	mButton101 = (Button) findViewById(R.id.btn101);
//             	mButton102 = (Button) findViewById(R.id.btn102);
//             	mButton103 = (Button) findViewById(R.id.btn103);
//             	mButton104 = (Button) findViewById(R.id.btn104);
             	custom1set = (Button) findViewById(R.id.custom1set);
             	home5 = (Button) findViewById(R.id.home5);
             	
//             	mButton54.setOnClickListener(new ButtonListener());
//             	mButton55.setOnClickListener(new ButtonListener());
//             	mButton56.setOnClickListener(new ButtonListener());
//             	mButton57.setOnClickListener(new ButtonListener());
//             	mButton58.setOnClickListener(new ButtonListener());
//             	mButton59.setOnClickListener(new ButtonListener());
//             	mButton60.setOnClickListener(new ButtonListener());
//             	mButton61.setOnClickListener(new ButtonListener());
//             	mButton62.setOnClickListener(new ButtonListener());
             	mButton63.setOnClickListener(new ButtonListener());
             	mButton64.setOnClickListener(new ButtonListener());
             	mButton65.setOnClickListener(new ButtonListener());
             	mButton66.setOnClickListener(new ButtonListener());
             	mButton67.setOnClickListener(new ButtonListener());
             	mButton68.setOnClickListener(new ButtonListener());
//             	mButton99.setOnClickListener(new ButtonListener());
//             	mButton100.setOnClickListener(new ButtonListener());
//             	mButton101.setOnClickListener(new ButtonListener());
//             	mButton102.setOnClickListener(new ButtonListener());
//             	mButton103.setOnClickListener(new ButtonListener());
//             	mButton104.setOnClickListener(new ButtonListener());
             	custom1set.setOnClickListener(new ButtonListener());
             	home5.setOnClickListener(new ButtonListener());
             	
             	
             	setting = getSharedPreferences(custom1data,0);
             	cus1name=(TextView)findViewById(R.id.cus1name);
             	cus1name.setText(setting.getString(custom1name,""));
//             	mButton54.setText(setting.getString(btn54name,""));
//             	mButton55.setText(setting.getString(btn55name,""));
//             	mButton56.setText(setting.getString(btn56name,""));
//             	mButton57.setText(setting.getString(btn57name,""));
//             	mButton58.setText(setting.getString(btn58name,""));
//             	mButton59.setText(setting.getString(btn59name,""));
//             	mButton60.setText(setting.getString(btn60name,""));
//             	mButton61.setText(setting.getString(btn61name,""));
//             	mButton62.setText(setting.getString(btn62name,""));
             	mButton63.setText(setting.getString(btn63name,""));
             	mButton64.setText(setting.getString(btn64name,""));
             	mButton65.setText(setting.getString(btn65name,""));
             	mButton66.setText(setting.getString(btn66name,""));
             	mButton67.setText(setting.getString(btn67name,""));
             	mButton68.setText(setting.getString(btn68name,""));
//             	mButton99.setText(setting.getString(btn99name,""));
//             	mButton100.setText(setting.getString(btn100name,""));
//             	mButton101.setText(setting.getString(btn101name,""));
//             	mButton102.setText(setting.getString(btn102name,""));
//             	mButton103.setText(setting.getString(btn103name,""));
//             	mButton104.setText(setting.getString(btn104name,""));
             	
             	
             	break;
             	
   			 case 6 :
//   			mButton69 = (Button) findViewById(R.id.btn69);
//             	mButton70 = (Button) findViewById(R.id.btn70);
//             	mButton71 = (Button) findViewById(R.id.btn71);
//             	mButton72 = (Button) findViewById(R.id.btn72);
//             	mButton73 = (Button) findViewById(R.id.btn73);
//             	mButton74 = (Button) findViewById(R.id.btn74);
//             	mButton75 = (Button) findViewById(R.id.btn75);
//             	mButton76 = (Button) findViewById(R.id.btn76);
//             	mButton77 = (Button) findViewById(R.id.btn77);
             	mButton78 = (Button) findViewById(R.id.btn78);
             	mButton79 = (Button) findViewById(R.id.btn79);
             	mButton80 = (Button) findViewById(R.id.btn80);
             	mButton81 = (Button) findViewById(R.id.btn81);
             	mButton82 = (Button) findViewById(R.id.btn82);
             	mButton83 = (Button) findViewById(R.id.btn83);
//             	mButton105 = (Button) findViewById(R.id.btn105);
//             	mButton106 = (Button) findViewById(R.id.btn106);
//             	mButton107 = (Button) findViewById(R.id.btn107);
//             	mButton108 = (Button) findViewById(R.id.btn108);
//             	mButton109 = (Button) findViewById(R.id.btn109);
//             	mButton110 = (Button) findViewById(R.id.btn110);
             	custom2set = (Button) findViewById(R.id.custom2set);
             	home6 = (Button) findViewById(R.id.home6);
             	
//             	mButton69.setOnClickListener(new ButtonListener());
//             	mButton70.setOnClickListener(new ButtonListener());
//             	mButton71.setOnClickListener(new ButtonListener());
//             	mButton72.setOnClickListener(new ButtonListener());
//             	mButton73.setOnClickListener(new ButtonListener());
//             	mButton74.setOnClickListener(new ButtonListener());
//             	mButton75.setOnClickListener(new ButtonListener());
//             	mButton76.setOnClickListener(new ButtonListener());
//             	mButton77.setOnClickListener(new ButtonListener());
             	mButton78.setOnClickListener(new ButtonListener());
             	mButton79.setOnClickListener(new ButtonListener());
             	mButton80.setOnClickListener(new ButtonListener());
             	mButton81.setOnClickListener(new ButtonListener());
             	mButton82.setOnClickListener(new ButtonListener());
             	mButton83.setOnClickListener(new ButtonListener());
//             	mButton105.setOnClickListener(new ButtonListener());
//             	mButton106.setOnClickListener(new ButtonListener());
//             	mButton107.setOnClickListener(new ButtonListener());
//             	mButton108.setOnClickListener(new ButtonListener());
//             	mButton109.setOnClickListener(new ButtonListener());
//             	mButton110.setOnClickListener(new ButtonListener());
             	custom2set.setOnClickListener(new ButtonListener());
             	home6.setOnClickListener(new ButtonListener());
             	
             	settingb = getSharedPreferences(custom2data,0);
             	cus2name=(TextView)findViewById(R.id.cus2name);
             	cus2name.setText(settingb.getString(custom2name,""));
//             	mButton69.setText(settingb.getString(btn69name,""));
//             	mButton70.setText(settingb.getString(btn70name,""));
//             	mButton71.setText(settingb.getString(btn71name,""));
//             	mButton72.setText(settingb.getString(btn72name,""));
//             	mButton73.setText(settingb.getString(btn73name,""));
//             	mButton74.setText(settingb.getString(btn74name,""));
//             	mButton75.setText(settingb.getString(btn75name,""));
//             	mButton76.setText(settingb.getString(btn76name,""));
//             	mButton77.setText(settingb.getString(btn77name,""));
             	mButton78.setText(settingb.getString(btn78name,""));
             	mButton79.setText(settingb.getString(btn79name,""));
             	mButton80.setText(settingb.getString(btn80name,""));
             	mButton81.setText(settingb.getString(btn81name,""));
             	mButton82.setText(settingb.getString(btn82name,""));
             	mButton83.setText(settingb.getString(btn83name,""));
//             	mButton105.setText(settingb.getString(btn105name,""));
//             	mButton106.setText(settingb.getString(btn106name,""));
//             	mButton107.setText(settingb.getString(btn107name,""));
//             	mButton108.setText(settingb.getString(btn108name,""));
//             	mButton109.setText(settingb.getString(btn109name,""));
//             	mButton110.setText(settingb.getString(btn110name,""));
//             	
             	break;
  			 
  			case 7 :
  				mButton93 = (Button) findViewById(R.id.btn93);
            	mButton94 = (Button) findViewById(R.id.btn94);
            	mButton95 = (Button) findViewById(R.id.btn95);
            	mButton96 = (Button) findViewById(R.id.btn96);
            	mButton97 = (Button) findViewById(R.id.btn97);
            	mButton98 = (Button) findViewById(R.id.btn98);
            	custom3set = (Button) findViewById(R.id.custom3set);
            	home7 = (Button) findViewById(R.id.home7);
            	
            	mButton93.setOnClickListener(new ButtonListener());
            	mButton94.setOnClickListener(new ButtonListener());
            	mButton95.setOnClickListener(new ButtonListener());
            	mButton96.setOnClickListener(new ButtonListener());
            	mButton97.setOnClickListener(new ButtonListener());
            	mButton98.setOnClickListener(new ButtonListener());
            	custom3set.setOnClickListener(new ButtonListener());
            	home7.setOnClickListener(new ButtonListener());
            	
            	setting2 = getSharedPreferences(custom3data,0);
            	cus3name=(TextView)findViewById(R.id.cus3name);
            	cus3name.setText(setting2.getString(custom3name,""));
            	mButton93.setText(setting2.getString(btn93name,""));
            	mButton94.setText(setting2.getString(btn94name,""));
            	mButton95.setText(setting2.getString(btn95name,""));
            	mButton96.setText(setting2.getString(btn96name,""));
            	mButton97.setText(setting2.getString(btn97name,""));
            	mButton98.setText(setting2.getString(btn98name,""));
            	
            	break;
            	

  			 /*case 8 :
  				 
  				marco1 = (Button) findViewById(R.id.marco1);
  				marco2 = (Button) findViewById(R.id.marco2);
  				marco3 = (Button) findViewById(R.id.marco3);
  				marco4 = (Button) findViewById(R.id.marco4);
  				marco5 = (Button) findViewById(R.id.marco5);
  				marco6 = (Button) findViewById(R.id.marco6);
  				home8 = (Button) findViewById(R.id.home8);
  				
  				marcoset1 = (Button) findViewById(R.id.marcoset1);
  				marcoset2 = (Button) findViewById(R.id.marcoset2);
  				marcoset3 = (Button) findViewById(R.id.marcoset3);
  				marcoset4 = (Button) findViewById(R.id.marcoset4);
  				marcoset5 = (Button) findViewById(R.id.marcoset5);
  				marcoset6 = (Button) findViewById(R.id.marcoset6);
  				
  				marco1.setOnClickListener(new ButtonListener());
  				marco2.setOnClickListener(new ButtonListener());
  				marco3.setOnClickListener(new ButtonListener());
  				marco4.setOnClickListener(new ButtonListener());
  				marco5.setOnClickListener(new ButtonListener());
  				marco6.setOnClickListener(new ButtonListener());
  				home8.setOnClickListener(new ButtonListener());
  				
  				marcoset1.setOnClickListener(new ButtonListener());
  				marcoset2.setOnClickListener(new ButtonListener());
  				marcoset3.setOnClickListener(new ButtonListener());
  				marcoset4.setOnClickListener(new ButtonListener());
  				marcoset5.setOnClickListener(new ButtonListener());
  				marcoset6.setOnClickListener(new ButtonListener());
  				
  				marcoset = getSharedPreferences(marcosetdata,0);
  				marco1set = getSharedPreferences(marcoset1data,0);
  				marco2set = getSharedPreferences(marcoset2data,0);
  				marco3set = getSharedPreferences(marcoset3data,0);
  				marco4set = getSharedPreferences(marcoset4data,0);
  				marco5set = getSharedPreferences(marcoset5data,0);
  				
  				marco1.setText(marcoset.getString(marconame,""));
  				marco2.setText(marco1set.getString(marco1name,""));
  				marco3.setText(marco2set.getString(marco2name,""));
  				marco4.setText(marco3set.getString(marco3name,""));
  				marco5.setText(marco4set.getString(marco4name,""));
  				marco6.setText(marco5set.getString(marco5name,""));
  				
  				 break;
  				 */
  			 case 8 :
  				findViewById(R.id.save).setOnClickListener(new ButtonListener());
   				findViewById(R.id.open).setOnClickListener(new ButtonListener());
   				//findViewById(R.id.send).setOnClickListener(new ButtonListener());
   				//findViewById(R.id.clear).setOnClickListener(new ButtonListener());
   				spinner = (Spinner)findViewById(R.id.mySpinner);
   				lunchList = new ArrayAdapter<String>(BluetoothChat.this,R.layout.myspinner, lunch);
   				lunchList.setDropDownViewResource(R.layout.myspinner);
   				spinner.setAdapter(lunchList);
   				
   				slave = getSharedPreferences(slaveiddata,0);
   				if (slave.getString(slaveid, "").contains("1")){
						spinner.setSelection(0);}
					else if (slave.getString(slaveid, "").contains("2")){
						spinner.setSelection(1);}
					else if (slave.getString(slaveid, "").contains("3")){
						spinner.setSelection(2);}
					else if (slave.getString(slaveid, "").contains("4")){
						spinner.setSelection(3);}
					else if (slave.getString(slaveid, "").contains("5")){
						spinner.setSelection(4);}
					else if (slave.getString(slaveid, "").contains("6")){
						spinner.setSelection(5);}
					else{
						spinner.setSelection(0);}
   				
   				spinner.setOnItemSelectedListener(new OnItemSelectedListener(){
   					
   					
					@Override
					public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
						// TODO Auto-generated method stub
						//Toast.makeText(MainActivity.this, "謔ｨ驕ｸ謫�+adapterView.getSelectedItem().toString(), Toast.LENGTH_LONG).show();
						
						slave = getSharedPreferences(slaveiddata,0);
						slave.edit()
				        	.putString(slaveid, adapterView.getSelectedItem().toString())
				        	.commit();
					
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub
						
					}});

   				    
   				
   				ip=(EditText) findViewById(R.id.ip);
   				port=(EditText) findViewById(R.id.port);
   				//sendContent=(EditText) findViewById(R.id.sendContent);
   				//recContent=(EditText) findViewById(R.id.recContent);
   				
   				IPname = getSharedPreferences(Ipdata,0);

                 findViewById(R.id.open_gcm).setOnClickListener(new ButtonListener());
                 ip_gcm=(EditText) findViewById(R.id.ip_gcm);
                 port_gcm=(EditText) findViewById(R.id.port_gcm);
                 readData(13);
   				//init();
                 //YILINEDIT把監視器設定合併至此
                 findViewById(R.id.saveOpenCam).setOnClickListener(new ButtonListener());
                 opencam =(EditText) findViewById(R.id.openCam);
                 OPenCam = getSharedPreferences(OpenCamData,0);
                 readData(14);
  				 break;
  				 
  				//YILINEDIT這頁先廢掉
   			case 9:
                /*findViewById(R.id.saveOpenCam).setOnClickListener(new ButtonListener());
                opencam =(EditText) findViewById(R.id.openCam);
                OPenCam = getSharedPreferences(OpenCamData,0);
                readData(14);*/
   				break;
             	
   			 
  			 }
  			 
  		    ((ViewPager)container).addView(viewList.get(position));
  		    return viewList.get(position);
  		   }

		private ContentResolver getContentResolver() {
			// TODO Auto-generated method stub
			return null;
		}
  		    
  		  };
  		  viewPager.setAdapter(apdter);
  		   
  		  viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {//逶｣閨ｽ逡ｶViewPager陲ｫ謾ｹ隶咳age譎�  		    
              @Override
              public void onPageSelected(int arg0) {
                  //YILINEDIT-voiceRecg
                  if (viewPager.getCurrentItem() == 1)
                      sendmessage("W+j?u");
                  if (isVoiceRecMode && viewPager.getCurrentItem() != 1) {
                      viewPager.setCurrentItem(page_lamp);
                      Toast.makeText(BluetoothChat.this, "請先解除語音學習模式", Toast.LENGTH_SHORT).show();
                      //Log.d(TAG,"move");
                  }
              }


              @Override
              public void onPageScrolled(int index, float arg1, int arg2) {
              }

              @Override
              public void onPageScrollStateChanged(int index) {
              }
          });
  		    

         getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);

        // Set up the custom title
        //mTitle = (TextView) findViewById(R.id.title_left_text);
        //mTitle.setText(R.string.app_name);
       //mTitle = (TextView) findViewById(R.id.title_right_text);
        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Bundle reply = msg.getData();
                token = msg.getData().getString("data");
                Log.d("TAG", "gcm" + token);
                IPname = getSharedPreferences(Ipdata,0);
                IPname.edit()
                        .putString(gcmID, token)
                        .commit();
            }
        };
        if (checkPlayServices()) {
            Log.d("TAG", "gcm" );
            // Start IntentService to register this application with GCM.
            Intent intent_gcm = new Intent(this, com.lee.gcm.RegistrationIntentService.class);
            intent.putExtra("messenger", new Messenger(handler));
            startService(intent);
            intent = new Intent(this, com.lee.gcm.RegistrationIntentService.class);
            startService(intent);
            //123
        }

 }
    
    
    
    @Override
    protected Dialog onCreateDialog(int id) {
     switch (id) {
     
 	case DIALOG_MENU:
 		AlertDialog.Builder builder = new AlertDialog.Builder(this);
 		LayoutInflater inflater = LayoutInflater.from(BluetoothChat.this);
 		View dialog_menu = inflater.inflate(R.layout.dialog,null);

 		builder.setView(dialog_menu);
 		
 		Button cancel = (Button)dialog_menu.findViewById(R.id.cancel);
 		cancel.setOnClickListener(new OnClickListener() {
 			
			@Override
			public void onClick(View v) {
				Runtime runtime = Runtime.getRuntime();
				try {
					runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
 		});
 			
 		
 		Button start1 = (Button)dialog_menu.findViewById(R.id.start1);
 		start1.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				viewPager.setCurrentItem(page_macro);
 				Runtime runtime = Runtime.getRuntime();
				try {
					runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
 			}
 		});
 		
 		Button start4 = (Button)dialog_menu.findViewById(R.id.start4);
 		start4.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				viewPager.setCurrentItem(page_lamp);
                //sendmessage("W+j?u");
 				Runtime runtime = Runtime.getRuntime();
				try {
                    runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
 			}
 		});
 		
 		Button start7 = (Button)dialog_menu.findViewById(R.id.start7);
 		start7.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				viewPager.setCurrentItem(page_air);
 				Runtime runtime = Runtime.getRuntime();
				try {
					runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
 			}
 		});
 		
 		
 		
 		Button start2 = (Button)dialog_menu.findViewById(R.id.start2);
 		start2.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				viewPager.setCurrentItem(page_tv);
 				Runtime runtime = Runtime.getRuntime();
				try {
					runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
 			}
 		});
 		
 		Button start5 = (Button)dialog_menu.findViewById(R.id.start5);
 		start5.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				viewPager.setCurrentItem(page_custom1);
 				Runtime runtime = Runtime.getRuntime();
				try {
					runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
 			}
 		});
 		
 		Button start8 = (Button)dialog_menu.findViewById(R.id.start8);
 		start8.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				viewPager.setCurrentItem(page_custom2);
 				Runtime runtime = Runtime.getRuntime();
				try {
					runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
 			}
 		});
 		
 		//YILINEDIT
 		Button start6 = (Button)dialog_menu.findViewById(R.id.start6);
 		start6.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				OPenCam = getSharedPreferences(OpenCamData,0);
 				String PackageNameToOpen=OPenCam.getString(CAM, "");
 				if ("".equals(PackageNameToOpen.trim())){
 					Log.e(TAG, "PackageNameToOpen is Empty");
 					Toast.makeText(BluetoothChat.this, "監視器App沒有設定", Toast.LENGTH_LONG).show();
 					viewPager.setCurrentItem(page_ip_config);//9
 					Runtime runtime = Runtime.getRuntime();
 					try {
 						runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
 					} catch (IOException e) {
 						// TODO Auto-generated catch block
 						e.printStackTrace();
 					}
 				} else{
 					try{
 						Log.e(TAG, "開啟"+OPenCam.getString(CAM, ""));
 						Intent intent = getPackageManager().getLaunchIntentForPackage(PackageNameToOpen);
 	 		       		startActivity(intent);
	 	 		       	//Toast.makeText(BluetoothChat.this, "�������, Toast.LENGTH_LONG).show();
	 		       		floatApp(0,0,0);
	 		       		//floatApp(300,200,1);
	 		       		//Toast.makeText(BluetoothChat.this, "��餈���", Toast.LENGTH_LONG).show();
 					}catch(RuntimeException e){
 						Log.e(TAG, "packageName�航炊");
 						Toast.makeText(BluetoothChat.this, "監視器App設定錯誤", Toast.LENGTH_LONG).show();
 						viewPager.setCurrentItem(page_ip_config);//9
 	 					Runtime runtime = Runtime.getRuntime();
 	 					try {
 	 						runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
 	 					} catch (IOException g) {
 	 						// TODO Auto-generated catch block
 	 						g.printStackTrace();
 	 					}
 						e.printStackTrace();
 					}
 				}
 				
 			}
 		});
 		
 		Button start3  = (Button)dialog_menu.findViewById(R.id.start3);
 		start3.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				viewPager.setCurrentItem(page_custom3);
 				Runtime runtime = Runtime.getRuntime();
				try {
					runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
 			}
 		});
 		
 		Button start9 = (Button)dialog_menu.findViewById(R.id.start9);
 		start9.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				viewPager.setCurrentItem(page_ip_config);
 				Runtime runtime = Runtime.getRuntime();
				try {
					runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
 			}
 		});
 		
 		AlertDialog dialog = builder.create();
 		dialog.show();
 		
     	}
	return null;
     
    }

    //YILINEDIT-voiceRecg
    private AlertDialog getVoiceAlertDialog(final String title, final String message,boolean islamp) {
        //產生一個Builder物件
        Builder builder = new AlertDialog.Builder(this);
        //設定Dialog的標題
        builder.setTitle("請選擇"+title+"的語音動作");
        //設定Dialog的內容
        // builder.setMessage("MS");

        if(islamp){
            builder.setItems(new String[] { "開", "關" },new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog,int which){
                    //當使用者點選對話框時，顯示使用者所點選的項目
                    Log.d(TAG,"which"+which);
                    switch (which){
                        case 0:
                            recAction=message+"4";
                            break;
                        case 1:
                            recAction=message+"5";
                            break;
                    }
                    Log.d(TAG,"recAction "+recAction);
                    voiceRec("請說出 "+title + (which==0?"開":"關")+"的動作(確認網路良好)");
                }
            });
        }else{
            builder.setItems(new String[] { "開始錄音" },new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog,int which){
                    //當使用者點選對話框時，顯示使用者所點選的項目
                    Log.d(TAG,"which"+which);
                    voiceRec(title);
                }
            });
        }


        //設定Negative按鈕資料
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //按下按鈕時顯示快顯
                //Toast.makeText(this, "您按下Cancel按鈕", Toast.LENGTH_SHORT).show();
            }
        });
        //利用Builder物件建立AlertDialog
        return builder.create();
    }

    private void floatApp(int x,int y ,int id) {
     	 mService.floatApp(x,y,id);
      }

   //YILINEDIT-longclick
   public class ButtonLongListner implements View.OnLongClickListener {

      @Override
      public boolean onLongClick(View v) {
         // TODO Auto-generated method stub
         Log.e(TAG,""+v.getId());
         switch(v.getId()){
            case R.id.f1:
               // TODO Auto-generated catch block
               setContentView(R.layout.marcoset);

               marcosetname = (EditText) findViewById(R.id.marcosetname);
               marcosetbtn = (EditText) findViewById(R.id.marcosetbtn);
               mButton01 = (Button) findViewById(R.id.btn01);
               mButton02 = (Button) findViewById(R.id.btn02);
               mButton03 = (Button) findViewById(R.id.btn03);
               mButton04 = (Button) findViewById(R.id.btn04);
               mButton05 = (Button) findViewById(R.id.btn05);
               mButton06 = (Button) findViewById(R.id.btn06);
               mButton07 = (Button) findViewById(R.id.btn07);
               mButton08 = (Button) findViewById(R.id.btn08);
               mButton09 = (Button) findViewById(R.id.btn09);
               mButton10 = (Button) findViewById(R.id.btn10);
               mButton11 = (Button) findViewById(R.id.btn11);
               mButton12 = (Button) findViewById(R.id.btn12);
               mButton13 = (Button) findViewById(R.id.btn13);
               mButton14 = (Button) findViewById(R.id.btn14);
               mButton15 = (Button) findViewById(R.id.btn15);
               mButton16 = (Button) findViewById(R.id.btn16);
               mButton17 = (Button) findViewById(R.id.btn17);
               mButton18 = (Button) findViewById(R.id.btn18);
               mButton19 = (Button) findViewById(R.id.btn19);
               mButton20 = (Button) findViewById(R.id.btn20);
               mButton21 = (Button) findViewById(R.id.btn21);
               mButton22 = (Button) findViewById(R.id.btn22);
               mButton23 = (Button) findViewById(R.id.btn23);
               mButton24 = (Button) findViewById(R.id.btn24);
//	    			mButton25 = (Button) findViewById(R.id.btn25);
//	    			mButton26 = (Button) findViewById(R.id.btn26);
//	    			mButton27 = (Button) findViewById(R.id.btn27);
//	    			mButton28 = (Button) findViewById(R.id.btn28);
//	    			mButton29 = (Button) findViewById(R.id.btn29);
//	    			mButton30 = (Button) findViewById(R.id.btn30);
//	    			mButton31 = (Button) findViewById(R.id.btn31);
//	    			mButton32 = (Button) findViewById(R.id.btn32);
               mButton33 = (Button) findViewById(R.id.btn33);
//	    			mButton34 = (Button) findViewById(R.id.btn34);
               mButton35 = (Button) findViewById(R.id.btn35);
//	    			mButton36 = (Button) findViewById(R.id.btn36);
//	    			mButton37 = (Button) findViewById(R.id.btn37);
//	    			mButton38 = (Button) findViewById(R.id.btn38);
//	    			mButton39 = (Button) findViewById(R.id.btn39);
//	    			mButton40 = (Button) findViewById(R.id.btn40);
//	    			mButton41 = (Button) findViewById(R.id.btn41);
               mButton42 = (Button) findViewById(R.id.btn42);
               mButton43 = (Button) findViewById(R.id.btn43);
               mButton44 = (Button) findViewById(R.id.btn44);
               mButton45 = (Button) findViewById(R.id.btn45);
               mButton46 = (Button) findViewById(R.id.btn46);
               mButton47 = (Button) findViewById(R.id.btn47);
               mButton48 = (Button) findViewById(R.id.btn48);
               mButton49 = (Button) findViewById(R.id.btn49);
               mButton50 = (Button) findViewById(R.id.btn50);
               mButton51 = (Button) findViewById(R.id.btn51);
               mButton52 = (Button) findViewById(R.id.btn52);
               mButton53 = (Button) findViewById(R.id.btn53);
//	    			mButton54 = (Button) findViewById(R.id.btn54);
//	    			mButton55 = (Button) findViewById(R.id.btn55);
//	    			mButton56 = (Button) findViewById(R.id.btn56);
//	    			mButton57 = (Button) findViewById(R.id.btn57);
//	    			mButton58 = (Button) findViewById(R.id.btn58);
//	    			mButton59 = (Button) findViewById(R.id.btn59);
//	    			mButton60 = (Button) findViewById(R.id.btn60);
//	    			mButton61 = (Button) findViewById(R.id.btn61);
//	    			mButton62 = (Button) findViewById(R.id.btn62);
               mButton63 = (Button) findViewById(R.id.btn63);
               mButton64 = (Button) findViewById(R.id.btn64);
               mButton65 = (Button) findViewById(R.id.btn65);
               mButton66 = (Button) findViewById(R.id.btn66);
               mButton67 = (Button) findViewById(R.id.btn67);
               mButton68 = (Button) findViewById(R.id.btn68);
//	    			mButton69 = (Button) findViewById(R.id.btn69);
//	    			mButton70 = (Button) findViewById(R.id.btn70);
//	    			mButton71 = (Button) findViewById(R.id.btn71);
//	    			mButton72 = (Button) findViewById(R.id.btn72);
//	    			mButton73 = (Button) findViewById(R.id.btn73);
//	    			mButton74 = (Button) findViewById(R.id.btn74);
//	    			mButton75 = (Button) findViewById(R.id.btn75);
//	    			mButton76 = (Button) findViewById(R.id.btn76);
//	    			mButton77 = (Button) findViewById(R.id.btn77);
               mButton78 = (Button) findViewById(R.id.btn78);
               mButton79 = (Button) findViewById(R.id.btn79);
               mButton80 = (Button) findViewById(R.id.btn80);
               mButton81 = (Button) findViewById(R.id.btn81);
               mButton82 = (Button) findViewById(R.id.btn82);
               mButton83 = (Button) findViewById(R.id.btn83);
               //mButton84 = (Button) findViewById(R.id.btn84);
               //mButton85 = (Button) findViewById(R.id.btn85);
               //mButton86 = (Button) findViewById(R.id.btn86);
               //mButton87 = (Button) findViewById(R.id.btn87);
               //mButton88 = (Button) findViewById(R.id.btn88);
               //mButton89 = (Button) findViewById(R.id.btn89);
               //mButton90 = (Button) findViewById(R.id.btn90);
               //mButton91 = (Button) findViewById(R.id.btn91);
               //mButton92 = (Button) findViewById(R.id.btn92);
               mButton93 = (Button) findViewById(R.id.btn93);
               mButton94 = (Button) findViewById(R.id.btn94);
               mButton95 = (Button) findViewById(R.id.btn95);
               mButton96 = (Button) findViewById(R.id.btn96);
               mButton97 = (Button) findViewById(R.id.btn97);
               mButton98 = (Button) findViewById(R.id.btn98);
//	    			mButton99 = (Button) findViewById(R.id.btn99);
//	    			mButton100 = (Button) findViewById(R.id.btn100);
//	    			mButton101 = (Button) findViewById(R.id.btn101);
//	    			mButton102 = (Button) findViewById(R.id.btn102);
//	    			mButton103 = (Button) findViewById(R.id.btn103);
//	    			mButton104 = (Button) findViewById(R.id.btn104);
//	    			mButton105 = (Button) findViewById(R.id.btn105);
//	    			mButton106 = (Button) findViewById(R.id.btn106);
//	    			mButton107 = (Button) findViewById(R.id.btn107);
//	    			mButton108 = (Button) findViewById(R.id.btn108);
//	    			mButton109 = (Button) findViewById(R.id.btn109);
//	    			mButton110 = (Button) findViewById(R.id.btn110);
//	    			mButton111 = (Button) findViewById(R.id.btn111);
//	    			mButton112 = (Button) findViewById(R.id.btn112);
               mButton153 = (Button) findViewById(R.id.btn153);
               mButton154 = (Button) findViewById(R.id.btn154);
               mButton155 = (Button) findViewById(R.id.btn155);
               mButton156 = (Button) findViewById(R.id.btn156);


               marcosetok = (Button) findViewById(R.id.marcosetok);
               s1 = (Button) findViewById(R.id.s1);
               s2 = (Button) findViewById(R.id.s2);
               s3 = (Button) findViewById(R.id.s3);
               s4 = (Button) findViewById(R.id.s4);
               s5 = (Button) findViewById(R.id.s5);

               //row1 = (Button) findViewById(R.id.row1);
               //row2 = (Button) findViewById(R.id.row2);
               //row3 = (Button) findViewById(R.id.row3);
               //row4 = (Button) findViewById(R.id.row4);
               //row5 = (Button) findViewById(R.id.row5);
               //row6 = (Button) findViewById(R.id.row6);
               //row7 = (Button) findViewById(R.id.row7);

               s1.setOnClickListener(new ButtonListener());
               s2.setOnClickListener(new ButtonListener());
               s3.setOnClickListener(new ButtonListener());
               s4.setOnClickListener(new ButtonListener());
               s5.setOnClickListener(new ButtonListener());

               //row1.setOnClickListener(new ButtonListener());
               //row2.setOnClickListener(new ButtonListener());
               //row3.setOnClickListener(new ButtonListener());
               //row4.setOnClickListener(new ButtonListener());
               //row5.setOnClickListener(new ButtonListener());
               //row6.setOnClickListener(new ButtonListener());
               //row7.setOnClickListener(new ButtonListener());


               marcosetok.setOnClickListener(new ButtonListener());

               lamp = getSharedPreferences(lampdata,0);
               mButton01.setText(lamp.getString(btn01name,""));
               mButton02.setText(lamp.getString(btn02name,""));
               mButton03.setText(lamp.getString(btn03name,""));
               mButton04.setText(lamp.getString(btn04name,""));
               mButton05.setText(lamp.getString(btn05name,""));
               mButton06.setText(lamp.getString(btn06name,""));
               mButton07.setText(lamp.getString(btn07name,""));
               mButton08.setText(lamp.getString(btn08name,""));
               mButton09.setText(lamp.getString(btn09name,""));
               mButton10.setText(lamp.getString(btn10name,""));
               mButton11.setText(lamp.getString(btn11name,""));
               mButton12.setText(lamp.getString(btn12name,""));

               setting = getSharedPreferences(custom1data,0);

               cus1name=(TextView)findViewById(R.id.cus1name);
               cus1name.setText(setting.getString(custom1name,""));
//	    			mButton54.setText(setting.getString(btn54name, ""));
//	    			mButton55.setText(setting.getString(btn55name, ""));
//	    			mButton56.setText(setting.getString(btn56name, ""));
//	    			mButton57.setText(setting.getString(btn57name, ""));
//	    			mButton58.setText(setting.getString(btn58name, ""));
//	    			mButton59.setText(setting.getString(btn59name, ""));
//	    			mButton60.setText(setting.getString(btn60name, ""));
//	    			mButton61.setText(setting.getString(btn61name, ""));
//	    			mButton62.setText(setting.getString(btn62name, ""));
               mButton63.setText(setting.getString(btn63name, ""));
               mButton64.setText(setting.getString(btn64name, ""));
               mButton65.setText(setting.getString(btn65name, ""));
               mButton66.setText(setting.getString(btn66name, ""));
               mButton67.setText(setting.getString(btn67name, ""));
               mButton68.setText(setting.getString(btn68name, ""));
//	    			mButton99.setText(setting.getString(btn99name, ""));
//	    			mButton100.setText(setting.getString(btn100name, ""));
//	    			mButton101.setText(setting.getString(btn101name, ""));
//	    			mButton102.setText(setting.getString(btn102name, ""));
//	    			mButton103.setText(setting.getString(btn103name, ""));
//	    			mButton104.setText(setting.getString(btn104name, ""));

               settingb = getSharedPreferences(custom2data,0);

               cus2name=(TextView)findViewById(R.id.cus2name);
               cus2name.setText(settingb.getString(custom2name,""));
//	    			 mButton69.setText(settingb.getString(btn69name, ""));
//	    			 mButton70.setText(settingb.getString(btn70name, ""));
//	    			 mButton71.setText(settingb.getString(btn71name, ""));
//	    			 mButton72.setText(settingb.getString(btn72name, ""));
//	    			 mButton73.setText(settingb.getString(btn73name, ""));
//	    			 mButton74.setText(settingb.getString(btn74name, ""));
//	    			 mButton75.setText(settingb.getString(btn75name, ""));
//	    			 mButton76.setText(settingb.getString(btn76name, ""));
//	    			 mButton77.setText(settingb.getString(btn77name, ""));
               mButton78.setText(settingb.getString(btn78name, ""));
               mButton79.setText(settingb.getString(btn79name, ""));
               mButton80.setText(settingb.getString(btn80name, ""));
               mButton81.setText(settingb.getString(btn81name, ""));
               mButton82.setText(settingb.getString(btn82name, ""));
               mButton83.setText(settingb.getString(btn83name, ""));
//	    			 mButton105.setText(settingb.getString(btn105name, ""));
//	    			 mButton106.setText(settingb.getString(btn106name, ""));
//	    			 mButton107.setText(settingb.getString(btn107name, ""));
//	    			 mButton108.setText(settingb.getString(btn108name, ""));
//	    			 mButton109.setText(settingb.getString(btn109name, ""));
//	    			 mButton110.setText(settingb.getString(btn110name, ""));

               setting2 = getSharedPreferences(custom3data,0);

               cus3name=(TextView)findViewById(R.id.cus3name);
               cus3name.setText(setting2.getString(custom3name,""));
               // mButton84.setText(setting2.getString(btn84name, ""));
               //mButton85.setText(setting2.getString(btn85name, ""));
               //mButton86.setText(setting2.getString(btn86name, ""));
               //mButton87.setText(setting2.getString(btn87name, ""));
               //mButton88.setText(setting2.getString(btn88name, ""));
               //mButton89.setText(setting2.getString(btn89name, ""));
               //mButton90.setText(setting2.getString(btn90name, ""));
               //mButton91.setText(setting2.getString(btn91name, ""));
               //mButton92.setText(setting2.getString(btn92name, ""));
               mButton93.setText(setting2.getString(btn93name, ""));
               mButton94.setText(setting2.getString(btn94name, ""));
               mButton95.setText(setting2.getString(btn95name, ""));
               mButton96.setText(setting2.getString(btn96name, ""));
               mButton97.setText(setting2.getString(btn97name, ""));
               mButton98.setText(setting2.getString(btn98name, ""));
               //mButton111.setText(setting2.getString(btn111name, ""));
               //mButton112.setText(setting2.getString(btn112name, ""));
               //mButton113.setText(setting2.getString(btn113name, ""));
               //mButton114.setText(setting2.getString(btn114name, ""));
               //mButton115.setText(setting2.getString(btn115name, ""));
               //mButton116.setText(setting2.getString(btn116name, ""));

               airseting = getSharedPreferences(airdata,0);
               mButton17.setText(airseting.getString(btn17name,""));
               mButton18.setText(airseting.getString(btn18name,""));
               mButton19.setText(airseting.getString(btn19name,""));
               mButton20.setText(airseting.getString(btn20name,""));
               mButton21.setText(airseting.getString(btn21name,""));
               mButton22.setText(airseting.getString(btn22name,""));


               lamp = getSharedPreferences(lampdata,0);

               imagebtn1 = (ImageView) findViewById(R.id.imagebtn1);
               imagebtn2 = (ImageView) findViewById(R.id.imagebtn2);
               imagebtn3 = (ImageView) findViewById(R.id.imagebtn3);
               imagebtn4 = (ImageView) findViewById(R.id.imagebtn4);
               imagebtn5 = (ImageView) findViewById(R.id.imagebtn5);
               imagebtn6 = (ImageView) findViewById(R.id.imagebtn6);
               imagebtn7 = (ImageView) findViewById(R.id.imagebtn7);
               imagebtn8 = (ImageView) findViewById(R.id.imagebtn8);
               imagebtn9 = (ImageView) findViewById(R.id.imagebtn9);
               imagebtn10 = (ImageView) findViewById(R.id.imagebtn10);
               imagebtn11 = (ImageView) findViewById(R.id.imagebtn11);
               imagebtn12 = (ImageView) findViewById(R.id.imagebtn12);

               String stringuri1 = lamp.getString(btn1pic,"");
               String stringuri2 = lamp.getString(btn2pic,"");
               String stringuri3 = lamp.getString(btn3pic,"");
               String stringuri4 = lamp.getString(btn4pic,"");
               String stringuri5 = lamp.getString(btn5pic,"");
               String stringuri6 = lamp.getString(btn6pic,"");
               String stringuri7 = lamp.getString(btn7pic,"");
               String stringuri8 = lamp.getString(btn8pic,"");
               String stringuri9 = lamp.getString(btn9pic,"");
               String stringuri10 = lamp.getString(btn10pic,"");
               String stringuri11 = lamp.getString(btn11pic,"");
               String stringuri12 = lamp.getString(btn12pic,"");

               if(stringuri1==null)
               {
                  imagebtn1.setImageDrawable(null);
               }else{
                  imagebtn1.setImageURI(Uri.parse(stringuri1));
                  Log.e("uri1", stringuri1);
               }

               if(stringuri2==null)
               {
                  imagebtn2.setImageDrawable(null);
               }else{
                  imagebtn2.setImageURI(Uri.parse(stringuri2));
               }

               if(stringuri3==null)
               {
                  imagebtn3.setImageDrawable(null);
               }else{
                  imagebtn3.setImageURI(Uri.parse(stringuri3));
               }

               if(stringuri4==null)
               {
                  imagebtn4.setImageDrawable(null);
               }else{
                  imagebtn4.setImageURI(Uri.parse(stringuri4));
               }

               if(stringuri5==null)
               {
                  imagebtn5.setImageDrawable(null);
               }else{
                  imagebtn5.setImageURI(Uri.parse(stringuri5));
               }

               if(stringuri6==null)
               {
                  imagebtn6.setImageDrawable(null);
               }else{
                  imagebtn6.setImageURI(Uri.parse(stringuri6));
               }

               if(stringuri7==null)
               {
                  imagebtn7.setImageDrawable(null);
               }else{
                  imagebtn7.setImageURI(Uri.parse(stringuri7));
               }

               if(stringuri8==null)
               {
                  imagebtn8.setImageDrawable(null);
               }else{
                  imagebtn8.setImageURI(Uri.parse(stringuri8));
               }

               if(stringuri9==null)
               {
                  imagebtn9.setImageDrawable(null);
               }else{
                  imagebtn9.setImageURI(Uri.parse(stringuri9));
               }

               if(stringuri10==null)
               {
                  imagebtn10.setImageDrawable(null);
               }else{
                  imagebtn10.setImageURI(Uri.parse(stringuri10));
               }

               if(stringuri11==null)
               {
                  imagebtn11.setImageDrawable(null);
               }else{
                  imagebtn11.setImageURI(Uri.parse(stringuri11));
               }

               if(stringuri12==null)
               {
                  imagebtn12.setImageDrawable(null);
               }else{
                  imagebtn12.setImageURI(Uri.parse(stringuri12));
               }

               readData(5);

               break;

            //marco2
            case R.id.f2:
               // TODO Auto-generated catch block
               setContentView(R.layout.marcoset1);

               marcoset1name = (EditText) findViewById(R.id.marcoset1name);
               marco1setbtn = (EditText) findViewById(R.id.marco1setbtn);
               mButton01 = (Button) findViewById(R.id.btn01);
               mButton02 = (Button) findViewById(R.id.btn02);
               mButton03 = (Button) findViewById(R.id.btn03);
               mButton04 = (Button) findViewById(R.id.btn04);
               mButton05 = (Button) findViewById(R.id.btn05);
               mButton06 = (Button) findViewById(R.id.btn06);
               mButton07 = (Button) findViewById(R.id.btn07);
               mButton08 = (Button) findViewById(R.id.btn08);
               mButton09 = (Button) findViewById(R.id.btn09);
               mButton10 = (Button) findViewById(R.id.btn10);
               mButton11 = (Button) findViewById(R.id.btn11);
               mButton12 = (Button) findViewById(R.id.btn12);
               mButton13 = (Button) findViewById(R.id.btn13);
               mButton14 = (Button) findViewById(R.id.btn14);
               mButton15 = (Button) findViewById(R.id.btn15);
               mButton16 = (Button) findViewById(R.id.btn16);
               mButton17 = (Button) findViewById(R.id.btn17);
               mButton18 = (Button) findViewById(R.id.btn18);
               mButton19 = (Button) findViewById(R.id.btn19);
               mButton20 = (Button) findViewById(R.id.btn20);
               mButton21 = (Button) findViewById(R.id.btn21);
               mButton22 = (Button) findViewById(R.id.btn22);
               mButton23 = (Button) findViewById(R.id.btn23);
               mButton24 = (Button) findViewById(R.id.btn24);
//				mButton25 = (Button) findViewById(R.id.btn25);
//				mButton26 = (Button) findViewById(R.id.btn26);
//				mButton27 = (Button) findViewById(R.id.btn27);
//				mButton28 = (Button) findViewById(R.id.btn28);
//				mButton29 = (Button) findViewById(R.id.btn29);
//				mButton30 = (Button) findViewById(R.id.btn30);
//				mButton31 = (Button) findViewById(R.id.btn31);
//				mButton32 = (Button) findViewById(R.id.btn32);
               mButton33 = (Button) findViewById(R.id.btn33);
//				mButton34 = (Button) findViewById(R.id.btn34);
               mButton35 = (Button) findViewById(R.id.btn35);
//				mButton36 = (Button) findViewById(R.id.btn36);
//				mButton37 = (Button) findViewById(R.id.btn37);
//				mButton38 = (Button) findViewById(R.id.btn38);
//				mButton39 = (Button) findViewById(R.id.btn39);
//				mButton40 = (Button) findViewById(R.id.btn40);
//				mButton41 = (Button) findViewById(R.id.btn41);
               mButton42 = (Button) findViewById(R.id.btn42);
               mButton43 = (Button) findViewById(R.id.btn43);
               mButton44 = (Button) findViewById(R.id.btn44);
               mButton45 = (Button) findViewById(R.id.btn45);
               mButton46 = (Button) findViewById(R.id.btn46);
               mButton47 = (Button) findViewById(R.id.btn47);
               mButton48 = (Button) findViewById(R.id.btn48);
               mButton49 = (Button) findViewById(R.id.btn49);
               mButton50 = (Button) findViewById(R.id.btn50);
               mButton51 = (Button) findViewById(R.id.btn51);
               mButton52 = (Button) findViewById(R.id.btn52);
               mButton53 = (Button) findViewById(R.id.btn53);
//				mButton54 = (Button) findViewById(R.id.btn54);
//				mButton55 = (Button) findViewById(R.id.btn55);
//				mButton56 = (Button) findViewById(R.id.btn56);
//				mButton57 = (Button) findViewById(R.id.btn57);
//				mButton58 = (Button) findViewById(R.id.btn58);
//				mButton59 = (Button) findViewById(R.id.btn59);
//				mButton60 = (Button) findViewById(R.id.btn60);
//				mButton61 = (Button) findViewById(R.id.btn61);
//				mButton62 = (Button) findViewById(R.id.btn62);
               mButton63 = (Button) findViewById(R.id.btn63);
               mButton64 = (Button) findViewById(R.id.btn64);
               mButton65 = (Button) findViewById(R.id.btn65);
               mButton66 = (Button) findViewById(R.id.btn66);
               mButton67 = (Button) findViewById(R.id.btn67);
               mButton68 = (Button) findViewById(R.id.btn68);
//				mButton69 = (Button) findViewById(R.id.btn69);
//				mButton70 = (Button) findViewById(R.id.btn70);
//				mButton71 = (Button) findViewById(R.id.btn71);
//				mButton72 = (Button) findViewById(R.id.btn72);
//				mButton73 = (Button) findViewById(R.id.btn73);
//				mButton74 = (Button) findViewById(R.id.btn74);
//				mButton75 = (Button) findViewById(R.id.btn75);
//				mButton76 = (Button) findViewById(R.id.btn76);
//				mButton77 = (Button) findViewById(R.id.btn77);
               mButton78 = (Button) findViewById(R.id.btn78);
               mButton79 = (Button) findViewById(R.id.btn79);
               mButton80 = (Button) findViewById(R.id.btn80);
               mButton81 = (Button) findViewById(R.id.btn81);
               mButton82 = (Button) findViewById(R.id.btn82);
               mButton83 = (Button) findViewById(R.id.btn83);
               //mButton84 = (Button) findViewById(R.id.btn84);
               //mButton85 = (Button) findViewById(R.id.btn85);
               //mButton86 = (Button) findViewById(R.id.btn86);
               //mButton87 = (Button) findViewById(R.id.btn87);
               //mButton88 = (Button) findViewById(R.id.btn88);
               //mButton89 = (Button) findViewById(R.id.btn89);
               //mButton90 = (Button) findViewById(R.id.btn90);
               //mButton91 = (Button) findViewById(R.id.btn91);
               //mButton92 = (Button) findViewById(R.id.btn92);
               mButton93 = (Button) findViewById(R.id.btn93);
               mButton94 = (Button) findViewById(R.id.btn94);
               mButton95 = (Button) findViewById(R.id.btn95);
               mButton96 = (Button) findViewById(R.id.btn96);
               mButton97 = (Button) findViewById(R.id.btn97);
               mButton98 = (Button) findViewById(R.id.btn98);
//				mButton99 = (Button) findViewById(R.id.btn99);
//				mButton100 = (Button) findViewById(R.id.btn100);
//				mButton101 = (Button) findViewById(R.id.btn101);
//				mButton102 = (Button) findViewById(R.id.btn102);
//				mButton103 = (Button) findViewById(R.id.btn103);
//				mButton104 = (Button) findViewById(R.id.btn104);
//				mButton105 = (Button) findViewById(R.id.btn105);
//				mButton106 = (Button) findViewById(R.id.btn106);
//				mButton107 = (Button) findViewById(R.id.btn107);
//				mButton108 = (Button) findViewById(R.id.btn108);
//				mButton109 = (Button) findViewById(R.id.btn109);
//				mButton110 = (Button) findViewById(R.id.btn110);
//				mButton111 = (Button) findViewById(R.id.btn111);
//				mButton112 = (Button) findViewById(R.id.btn112);
               mButton153 = (Button) findViewById(R.id.btn153);
               mButton154 = (Button) findViewById(R.id.btn154);
               mButton155 = (Button) findViewById(R.id.btn155);
               mButton156 = (Button) findViewById(R.id.btn156);


               s6 = (Button) findViewById(R.id.s6);
               s7 = (Button) findViewById(R.id.s7);
               s8 = (Button) findViewById(R.id.s8);
               s9 = (Button) findViewById(R.id.s9);
               s10 = (Button) findViewById(R.id.s10);

               s6.setOnClickListener(new ButtonListener());
               s7.setOnClickListener(new ButtonListener());
               s8.setOnClickListener(new ButtonListener());
               s9.setOnClickListener(new ButtonListener());
               s10.setOnClickListener(new ButtonListener());


               findViewById(R.id.marcoset1ok).setOnClickListener(new ButtonListener());

               lamp = getSharedPreferences(lampdata,0);
               mButton01.setText(lamp.getString(btn01name,""));
               mButton02.setText(lamp.getString(btn02name,""));
               mButton03.setText(lamp.getString(btn03name,""));
               mButton04.setText(lamp.getString(btn04name,""));
               mButton05.setText(lamp.getString(btn05name,""));
               mButton06.setText(lamp.getString(btn06name,""));
               mButton07.setText(lamp.getString(btn07name,""));
               mButton08.setText(lamp.getString(btn08name,""));
               mButton09.setText(lamp.getString(btn09name,""));
               mButton10.setText(lamp.getString(btn10name,""));
               mButton11.setText(lamp.getString(btn11name,""));
               mButton12.setText(lamp.getString(btn12name,""));

               setting = getSharedPreferences(custom1data,0);

               cus1name=(TextView)findViewById(R.id.cus1name);
               cus1name.setText(setting.getString(custom1name,""));
//				mButton54.setText(setting.getString(btn54name, ""));
//				mButton55.setText(setting.getString(btn55name, ""));
//				mButton56.setText(setting.getString(btn56name, ""));
//				mButton57.setText(setting.getString(btn57name, ""));
//				mButton58.setText(setting.getString(btn58name, ""));
//				mButton59.setText(setting.getString(btn59name, ""));
//				mButton60.setText(setting.getString(btn60name, ""));
//				mButton61.setText(setting.getString(btn61name, ""));
//				mButton62.setText(setting.getString(btn62name, ""));
               mButton63.setText(setting.getString(btn63name, ""));
               mButton64.setText(setting.getString(btn64name, ""));
               mButton65.setText(setting.getString(btn65name, ""));
               mButton66.setText(setting.getString(btn66name, ""));
               mButton67.setText(setting.getString(btn67name, ""));
               mButton68.setText(setting.getString(btn68name, ""));
//				mButton99.setText(setting.getString(btn99name, ""));
//				mButton100.setText(setting.getString(btn100name, ""));
//				mButton101.setText(setting.getString(btn101name, ""));
//				mButton102.setText(setting.getString(btn102name, ""));
//				mButton103.setText(setting.getString(btn103name, ""));
//				mButton104.setText(setting.getString(btn104name, ""));

               settingb = getSharedPreferences(custom2data,0);

               cus2name=(TextView)findViewById(R.id.cus2name);
               cus2name.setText(settingb.getString(custom2name,""));
//				 mButton69.setText(settingb.getString(btn69name, ""));
//				 mButton70.setText(settingb.getString(btn70name, ""));
//				 mButton71.setText(settingb.getString(btn71name, ""));
//				 mButton72.setText(settingb.getString(btn72name, ""));
//				 mButton73.setText(settingb.getString(btn73name, ""));
//				 mButton74.setText(settingb.getString(btn74name, ""));
//				 mButton75.setText(settingb.getString(btn75name, ""));
//				 mButton76.setText(settingb.getString(btn76name, ""));
//				 mButton77.setText(settingb.getString(btn77name, ""));
               mButton78.setText(settingb.getString(btn78name, ""));
               mButton79.setText(settingb.getString(btn79name, ""));
               mButton80.setText(settingb.getString(btn80name, ""));
               mButton81.setText(settingb.getString(btn81name, ""));
               mButton82.setText(settingb.getString(btn82name, ""));
               mButton83.setText(settingb.getString(btn83name, ""));
//				 mButton105.setText(settingb.getString(btn105name, ""));
//				 mButton106.setText(settingb.getString(btn106name, ""));
//				 mButton107.setText(settingb.getString(btn107name, ""));
//				 mButton108.setText(settingb.getString(btn108name, ""));
//				 mButton109.setText(settingb.getString(btn109name, ""));
//				 mButton110.setText(settingb.getString(btn110name, ""));

               setting2 = getSharedPreferences(custom3data,0);

               cus3name=(TextView)findViewById(R.id.cus3name);
               cus3name.setText(setting2.getString(custom3name,""));
               // mButton84.setText(setting2.getString(btn84name, ""));
               //mButton85.setText(setting2.getString(btn85name, ""));
               //mButton86.setText(setting2.getString(btn86name, ""));
               //mButton87.setText(setting2.getString(btn87name, ""));
               //mButton88.setText(setting2.getString(btn88name, ""));
               //mButton89.setText(setting2.getString(btn89name, ""));
               //mButton90.setText(setting2.getString(btn90name, ""));
               //mButton91.setText(setting2.getString(btn91name, ""));
               //mButton92.setText(setting2.getString(btn92name, ""));
               mButton93.setText(setting2.getString(btn93name, ""));
               mButton94.setText(setting2.getString(btn94name, ""));
               mButton95.setText(setting2.getString(btn95name, ""));
               mButton96.setText(setting2.getString(btn96name, ""));
               mButton97.setText(setting2.getString(btn97name, ""));
               mButton98.setText(setting2.getString(btn98name, ""));
               //mButton111.setText(setting2.getString(btn111name, ""));
               //mButton112.setText(setting2.getString(btn112name, ""));
               //mButton113.setText(setting2.getString(btn113name, ""));
               //mButton114.setText(setting2.getString(btn114name, ""));
               //mButton115.setText(setting2.getString(btn115name, ""));
               //mButton116.setText(setting2.getString(btn116name, ""));

               airseting = getSharedPreferences(airdata,0);
               mButton17.setText(airseting.getString(btn17name,""));
               mButton18.setText(airseting.getString(btn18name,""));
               mButton19.setText(airseting.getString(btn19name,""));
               mButton20.setText(airseting.getString(btn20name,""));
               mButton21.setText(airseting.getString(btn21name,""));
               mButton22.setText(airseting.getString(btn22name,""));


               lamp = getSharedPreferences(lampdata,0);
               imagebtn1 = (ImageView) findViewById(R.id.imagebtn1);
               imagebtn2 = (ImageView) findViewById(R.id.imagebtn2);
               imagebtn3 = (ImageView) findViewById(R.id.imagebtn3);
               imagebtn4 = (ImageView) findViewById(R.id.imagebtn4);
               imagebtn5 = (ImageView) findViewById(R.id.imagebtn5);
               imagebtn6 = (ImageView) findViewById(R.id.imagebtn6);
               imagebtn7 = (ImageView) findViewById(R.id.imagebtn7);
               imagebtn8 = (ImageView) findViewById(R.id.imagebtn8);
               imagebtn9 = (ImageView) findViewById(R.id.imagebtn9);
               imagebtn10 = (ImageView) findViewById(R.id.imagebtn10);
               imagebtn11 = (ImageView) findViewById(R.id.imagebtn11);
               imagebtn12 = (ImageView) findViewById(R.id.imagebtn12);



               if(lamp.getString(btn1pic,"")==null)
               {
                  imagebtn1.setImageDrawable(null);
               }else{
                  imagebtn1.setImageURI(Uri.parse(lamp.getString(btn1pic,"")));
                  Log.e("uri1", lamp.getString(btn1pic,""));
               }

               if(lamp.getString(btn2pic,"")==null)
               {
                  imagebtn2.setImageDrawable(null);
               }else{
                  imagebtn2.setImageURI(Uri.parse(lamp.getString(btn2pic,"")));
               }

               if(lamp.getString(btn3pic,"")==null)
               {
                  imagebtn3.setImageDrawable(null);
               }else{
                  imagebtn3.setImageURI(Uri.parse(lamp.getString(btn3pic,"")));
               }

               if(lamp.getString(btn4pic,"")==null)
               {
                  imagebtn4.setImageDrawable(null);
               }else{
                  imagebtn4.setImageURI(Uri.parse(lamp.getString(btn4pic,"")));
               }

               if(lamp.getString(btn5pic,"")==null)
               {
                  imagebtn5.setImageDrawable(null);
               }else{
                  imagebtn5.setImageURI(Uri.parse(lamp.getString(btn5pic,"")));
               }

               if(lamp.getString(btn6pic,"")==null)
               {
                  imagebtn6.setImageDrawable(null);
               }else{
                  imagebtn6.setImageURI(Uri.parse(lamp.getString(btn6pic,"")));
               }

               if(lamp.getString(btn7pic,"")==null)
               {
                  imagebtn7.setImageDrawable(null);
               }else{
                  imagebtn7.setImageURI(Uri.parse(lamp.getString(btn7pic,"")));
               }

               if(lamp.getString(btn8pic,"")==null)
               {
                  imagebtn8.setImageDrawable(null);
               }else{
                  imagebtn8.setImageURI(Uri.parse(lamp.getString(btn8pic,"")));
               }

               if(lamp.getString(btn9pic,"")==null)
               {
                  imagebtn9.setImageDrawable(null);
               }else{
                  imagebtn9.setImageURI(Uri.parse(lamp.getString(btn9pic,"")));
               }

               if(lamp.getString(btn10pic,"")==null)
               {
                  imagebtn10.setImageDrawable(null);
               }else{
                  imagebtn10.setImageURI(Uri.parse(lamp.getString(btn10pic,"")));
               }

               if(lamp.getString(btn11pic,"")==null)
               {
                  imagebtn11.setImageDrawable(null);
               }else{
                  imagebtn11.setImageURI(Uri.parse(lamp.getString(btn11pic,"")));
               }

               if(lamp.getString(btn12pic,"")==null)
               {
                  imagebtn12.setImageDrawable(null);
               }else{
                  imagebtn12.setImageURI(Uri.parse(lamp.getString(btn12pic,"")));
               }

               readData(6);
               break;

            //marco3
            case R.id.f3:
               // TODO Auto-generated catch block
               setContentView(R.layout.marcoset2);

               marcoset2name = (EditText) findViewById(R.id.marcoset2name);
               marco2setbtn = (EditText) findViewById(R.id.marco2setbtn);
               mButton01 = (Button) findViewById(R.id.btn01);
               mButton02 = (Button) findViewById(R.id.btn02);
               mButton03 = (Button) findViewById(R.id.btn03);
               mButton04 = (Button) findViewById(R.id.btn04);
               mButton05 = (Button) findViewById(R.id.btn05);
               mButton06 = (Button) findViewById(R.id.btn06);
               mButton07 = (Button) findViewById(R.id.btn07);
               mButton08 = (Button) findViewById(R.id.btn08);
               mButton09 = (Button) findViewById(R.id.btn09);
               mButton10 = (Button) findViewById(R.id.btn10);
               mButton11 = (Button) findViewById(R.id.btn11);
               mButton12 = (Button) findViewById(R.id.btn12);
               mButton13 = (Button) findViewById(R.id.btn13);
               mButton14 = (Button) findViewById(R.id.btn14);
               mButton15 = (Button) findViewById(R.id.btn15);
               mButton16 = (Button) findViewById(R.id.btn16);
               mButton17 = (Button) findViewById(R.id.btn17);
               mButton18 = (Button) findViewById(R.id.btn18);
               mButton19 = (Button) findViewById(R.id.btn19);
               mButton20 = (Button) findViewById(R.id.btn20);
               mButton21 = (Button) findViewById(R.id.btn21);
               mButton22 = (Button) findViewById(R.id.btn22);
               mButton23 = (Button) findViewById(R.id.btn23);
               mButton24 = (Button) findViewById(R.id.btn24);
//				mButton25 = (Button) findViewById(R.id.btn25);
//				mButton26 = (Button) findViewById(R.id.btn26);
//				mButton27 = (Button) findViewById(R.id.btn27);
//				mButton28 = (Button) findViewById(R.id.btn28);
//				mButton29 = (Button) findViewById(R.id.btn29);
//				mButton30 = (Button) findViewById(R.id.btn30);
//				mButton31 = (Button) findViewById(R.id.btn31);
//				mButton32 = (Button) findViewById(R.id.btn32);
               mButton33 = (Button) findViewById(R.id.btn33);
//				mButton34 = (Button) findViewById(R.id.btn34);
               mButton35 = (Button) findViewById(R.id.btn35);
//				mButton36 = (Button) findViewById(R.id.btn36);
//				mButton37 = (Button) findViewById(R.id.btn37);
//				mButton38 = (Button) findViewById(R.id.btn38);
//				mButton39 = (Button) findViewById(R.id.btn39);
//				mButton40 = (Button) findViewById(R.id.btn40);
//				mButton41 = (Button) findViewById(R.id.btn41);
               mButton42 = (Button) findViewById(R.id.btn42);
               mButton43 = (Button) findViewById(R.id.btn43);
               mButton44 = (Button) findViewById(R.id.btn44);
               mButton45 = (Button) findViewById(R.id.btn45);
               mButton46 = (Button) findViewById(R.id.btn46);
               mButton47 = (Button) findViewById(R.id.btn47);
               mButton48 = (Button) findViewById(R.id.btn48);
               mButton49 = (Button) findViewById(R.id.btn49);
               mButton50 = (Button) findViewById(R.id.btn50);
               mButton51 = (Button) findViewById(R.id.btn51);
               mButton52 = (Button) findViewById(R.id.btn52);
               mButton53 = (Button) findViewById(R.id.btn53);
//				mButton54 = (Button) findViewById(R.id.btn54);
//				mButton55 = (Button) findViewById(R.id.btn55);
//				mButton56 = (Button) findViewById(R.id.btn56);
//				mButton57 = (Button) findViewById(R.id.btn57);
//				mButton58 = (Button) findViewById(R.id.btn58);
//				mButton59 = (Button) findViewById(R.id.btn59);
//				mButton60 = (Button) findViewById(R.id.btn60);
//				mButton61 = (Button) findViewById(R.id.btn61);
//				mButton62 = (Button) findViewById(R.id.btn62);
               mButton63 = (Button) findViewById(R.id.btn63);
               mButton64 = (Button) findViewById(R.id.btn64);
               mButton65 = (Button) findViewById(R.id.btn65);
               mButton66 = (Button) findViewById(R.id.btn66);
               mButton67 = (Button) findViewById(R.id.btn67);
               mButton68 = (Button) findViewById(R.id.btn68);
//				mButton69 = (Button) findViewById(R.id.btn69);
//				mButton70 = (Button) findViewById(R.id.btn70);
//				mButton71 = (Button) findViewById(R.id.btn71);
//				mButton72 = (Button) findViewById(R.id.btn72);
//				mButton73 = (Button) findViewById(R.id.btn73);
//				mButton74 = (Button) findViewById(R.id.btn74);
//				mButton75 = (Button) findViewById(R.id.btn75);
//				mButton76 = (Button) findViewById(R.id.btn76);
//				mButton77 = (Button) findViewById(R.id.btn77);
               mButton78 = (Button) findViewById(R.id.btn78);
               mButton79 = (Button) findViewById(R.id.btn79);
               mButton80 = (Button) findViewById(R.id.btn80);
               mButton81 = (Button) findViewById(R.id.btn81);
               mButton82 = (Button) findViewById(R.id.btn82);
               mButton83 = (Button) findViewById(R.id.btn83);
               //mButton84 = (Button) findViewById(R.id.btn84);
               //mButton85 = (Button) findViewById(R.id.btn85);
               //mButton86 = (Button) findViewById(R.id.btn86);
               //mButton87 = (Button) findViewById(R.id.btn87);
               //mButton88 = (Button) findViewById(R.id.btn88);
               //mButton89 = (Button) findViewById(R.id.btn89);
               //mButton90 = (Button) findViewById(R.id.btn90);
               //mButton91 = (Button) findViewById(R.id.btn91);
               //mButton92 = (Button) findViewById(R.id.btn92);
               mButton93 = (Button) findViewById(R.id.btn93);
               mButton94 = (Button) findViewById(R.id.btn94);
               mButton95 = (Button) findViewById(R.id.btn95);
               mButton96 = (Button) findViewById(R.id.btn96);
               mButton97 = (Button) findViewById(R.id.btn97);
               mButton98 = (Button) findViewById(R.id.btn98);
//				mButton99 = (Button) findViewById(R.id.btn99);
//				mButton100 = (Button) findViewById(R.id.btn100);
//				mButton101 = (Button) findViewById(R.id.btn101);
//				mButton102 = (Button) findViewById(R.id.btn102);
//				mButton103 = (Button) findViewById(R.id.btn103);
//				mButton104 = (Button) findViewById(R.id.btn104);
//				mButton105 = (Button) findViewById(R.id.btn105);
//				mButton106 = (Button) findViewById(R.id.btn106);
//				mButton107 = (Button) findViewById(R.id.btn107);
//				mButton108 = (Button) findViewById(R.id.btn108);
//				mButton109 = (Button) findViewById(R.id.btn109);
//				mButton110 = (Button) findViewById(R.id.btn110);
//				mButton111 = (Button) findViewById(R.id.btn111);
//				mButton112 = (Button) findViewById(R.id.btn112);
               mButton153 = (Button) findViewById(R.id.btn153);
               mButton154 = (Button) findViewById(R.id.btn154);
               mButton155 = (Button) findViewById(R.id.btn155);
               mButton156 = (Button) findViewById(R.id.btn156);


               findViewById(R.id.marcoset2ok).setOnClickListener(new ButtonListener());

               findViewById(R.id.s11).setOnClickListener(new ButtonListener());
               findViewById(R.id.s12).setOnClickListener(new ButtonListener());
               findViewById(R.id.s13).setOnClickListener(new ButtonListener());
               findViewById(R.id.s14).setOnClickListener(new ButtonListener());
               findViewById(R.id.s15).setOnClickListener(new ButtonListener());


               lamp = getSharedPreferences(lampdata,0);
               mButton01.setText(lamp.getString(btn01name,""));
               mButton02.setText(lamp.getString(btn02name,""));
               mButton03.setText(lamp.getString(btn03name,""));
               mButton04.setText(lamp.getString(btn04name,""));
               mButton05.setText(lamp.getString(btn05name,""));
               mButton06.setText(lamp.getString(btn06name,""));
               mButton07.setText(lamp.getString(btn07name,""));
               mButton08.setText(lamp.getString(btn08name,""));
               mButton09.setText(lamp.getString(btn09name,""));
               mButton10.setText(lamp.getString(btn10name,""));
               mButton11.setText(lamp.getString(btn11name,""));
               mButton12.setText(lamp.getString(btn12name,""));

               setting = getSharedPreferences(custom1data,0);

               cus1name=(TextView)findViewById(R.id.cus1name);
               cus1name.setText(setting.getString(custom1name,""));
//				mButton54.setText(setting.getString(btn54name, ""));
//				mButton55.setText(setting.getString(btn55name, ""));
//				mButton56.setText(setting.getString(btn56name, ""));
//				mButton57.setText(setting.getString(btn57name, ""));
//				mButton58.setText(setting.getString(btn58name, ""));
//				mButton59.setText(setting.getString(btn59name, ""));
//				mButton60.setText(setting.getString(btn60name, ""));
//				mButton61.setText(setting.getString(btn61name, ""));
//				mButton62.setText(setting.getString(btn62name, ""));
               mButton63.setText(setting.getString(btn63name, ""));
               mButton64.setText(setting.getString(btn64name, ""));
               mButton65.setText(setting.getString(btn65name, ""));
               mButton66.setText(setting.getString(btn66name, ""));
               mButton67.setText(setting.getString(btn67name, ""));
               mButton68.setText(setting.getString(btn68name, ""));
//				mButton99.setText(setting.getString(btn99name, ""));
//				mButton100.setText(setting.getString(btn100name, ""));
//				mButton101.setText(setting.getString(btn101name, ""));
//				mButton102.setText(setting.getString(btn102name, ""));
//				mButton103.setText(setting.getString(btn103name, ""));
//				mButton104.setText(setting.getString(btn104name, ""));

               settingb = getSharedPreferences(custom2data,0);

               cus2name=(TextView)findViewById(R.id.cus2name);
               cus2name.setText(settingb.getString(custom2name,""));
//				 mButton69.setText(settingb.getString(btn69name, ""));
//				 mButton70.setText(settingb.getString(btn70name, ""));
//				 mButton71.setText(settingb.getString(btn71name, ""));
//				 mButton72.setText(settingb.getString(btn72name, ""));
//				 mButton73.setText(settingb.getString(btn73name, ""));
//				 mButton74.setText(settingb.getString(btn74name, ""));
//				 mButton75.setText(settingb.getString(btn75name, ""));
//				 mButton76.setText(settingb.getString(btn76name, ""));
//				 mButton77.setText(settingb.getString(btn77name, ""));
               mButton78.setText(settingb.getString(btn78name, ""));
               mButton79.setText(settingb.getString(btn79name, ""));
               mButton80.setText(settingb.getString(btn80name, ""));
               mButton81.setText(settingb.getString(btn81name, ""));
               mButton82.setText(settingb.getString(btn82name, ""));
               mButton83.setText(settingb.getString(btn83name, ""));
//				 mButton105.setText(settingb.getString(btn105name, ""));
//				 mButton106.setText(settingb.getString(btn106name, ""));
//				 mButton107.setText(settingb.getString(btn107name, ""));
//				 mButton108.setText(settingb.getString(btn108name, ""));
//				 mButton109.setText(settingb.getString(btn109name, ""));
//				 mButton110.setText(settingb.getString(btn110name, ""));

               setting2 = getSharedPreferences(custom3data,0);

               cus3name=(TextView)findViewById(R.id.cus3name);
               cus3name.setText(setting2.getString(custom3name,""));
               // mButton84.setText(setting2.getString(btn84name, ""));
               //mButton85.setText(setting2.getString(btn85name, ""));
               //mButton86.setText(setting2.getString(btn86name, ""));
               //mButton87.setText(setting2.getString(btn87name, ""));
               //mButton88.setText(setting2.getString(btn88name, ""));
               //mButton89.setText(setting2.getString(btn89name, ""));
               //mButton90.setText(setting2.getString(btn90name, ""));
               //mButton91.setText(setting2.getString(btn91name, ""));
               //mButton92.setText(setting2.getString(btn92name, ""));
               mButton93.setText(setting2.getString(btn93name, ""));
               mButton94.setText(setting2.getString(btn94name, ""));
               mButton95.setText(setting2.getString(btn95name, ""));
               mButton96.setText(setting2.getString(btn96name, ""));
               mButton97.setText(setting2.getString(btn97name, ""));
               mButton98.setText(setting2.getString(btn98name, ""));
               //mButton111.setText(setting2.getString(btn111name, ""));
               //mButton112.setText(setting2.getString(btn112name, ""));
               //mButton113.setText(setting2.getString(btn113name, ""));
               //mButton114.setText(setting2.getString(btn114name, ""));
               //mButton115.setText(setting2.getString(btn115name, ""));
               //mButton116.setText(setting2.getString(btn116name, ""));

               airseting = getSharedPreferences(airdata,0);
               mButton17.setText(airseting.getString(btn17name,""));
               mButton18.setText(airseting.getString(btn18name,""));
               mButton19.setText(airseting.getString(btn19name,""));
               mButton20.setText(airseting.getString(btn20name,""));
               mButton21.setText(airseting.getString(btn21name,""));
               mButton22.setText(airseting.getString(btn22name,""));


               lamp = getSharedPreferences(lampdata,0);
               imagebtn1 = (ImageView) findViewById(R.id.imagebtn1);
               imagebtn2 = (ImageView) findViewById(R.id.imagebtn2);
               imagebtn3 = (ImageView) findViewById(R.id.imagebtn3);
               imagebtn4 = (ImageView) findViewById(R.id.imagebtn4);
               imagebtn5 = (ImageView) findViewById(R.id.imagebtn5);
               imagebtn6 = (ImageView) findViewById(R.id.imagebtn6);
               imagebtn7 = (ImageView) findViewById(R.id.imagebtn7);
               imagebtn8 = (ImageView) findViewById(R.id.imagebtn8);
               imagebtn9 = (ImageView) findViewById(R.id.imagebtn9);
               imagebtn10 = (ImageView) findViewById(R.id.imagebtn10);
               imagebtn11 = (ImageView) findViewById(R.id.imagebtn11);
               imagebtn12 = (ImageView) findViewById(R.id.imagebtn12);



               if(lamp.getString(btn1pic,"")==null)
               {
                  imagebtn1.setImageDrawable(null);
               }else{
                  imagebtn1.setImageURI(Uri.parse(lamp.getString(btn1pic,"")));
                  Log.e("uri1", lamp.getString(btn1pic,""));
               }

               if(lamp.getString(btn2pic,"")==null)
               {
                  imagebtn2.setImageDrawable(null);
               }else{
                  imagebtn2.setImageURI(Uri.parse(lamp.getString(btn2pic,"")));
               }

               if(lamp.getString(btn3pic,"")==null)
               {
                  imagebtn3.setImageDrawable(null);
               }else{
                  imagebtn3.setImageURI(Uri.parse(lamp.getString(btn3pic,"")));
               }

               if(lamp.getString(btn4pic,"")==null)
               {
                  imagebtn4.setImageDrawable(null);
               }else{
                  imagebtn4.setImageURI(Uri.parse(lamp.getString(btn4pic,"")));
               }

               if(lamp.getString(btn5pic,"")==null)
               {
                  imagebtn5.setImageDrawable(null);
               }else{
                  imagebtn5.setImageURI(Uri.parse(lamp.getString(btn5pic,"")));
               }

               if(lamp.getString(btn6pic,"")==null)
               {
                  imagebtn6.setImageDrawable(null);
               }else{
                  imagebtn6.setImageURI(Uri.parse(lamp.getString(btn6pic,"")));
               }

               if(lamp.getString(btn7pic,"")==null)
               {
                  imagebtn7.setImageDrawable(null);
               }else{
                  imagebtn7.setImageURI(Uri.parse(lamp.getString(btn7pic,"")));
               }

               if(lamp.getString(btn8pic,"")==null)
               {
                  imagebtn8.setImageDrawable(null);
               }else{
                  imagebtn8.setImageURI(Uri.parse(lamp.getString(btn8pic,"")));
               }

               if(lamp.getString(btn9pic,"")==null)
               {
                  imagebtn9.setImageDrawable(null);
               }else{
                  imagebtn9.setImageURI(Uri.parse(lamp.getString(btn9pic,"")));
               }

               if(lamp.getString(btn10pic,"")==null)
               {
                  imagebtn10.setImageDrawable(null);
               }else{
                  imagebtn10.setImageURI(Uri.parse(lamp.getString(btn10pic,"")));
               }

               if(lamp.getString(btn11pic,"")==null)
               {
                  imagebtn11.setImageDrawable(null);
               }else{
                  imagebtn11.setImageURI(Uri.parse(lamp.getString(btn11pic,"")));
               }

               if(lamp.getString(btn12pic,"")==null)
               {
                  imagebtn12.setImageDrawable(null);
               }else{
                  imagebtn12.setImageURI(Uri.parse(lamp.getString(btn12pic,"")));
               }

               readData(7);
               break;


            //marco4
            case R.id.f4:
               // TODO Auto-generated catch block
               setContentView(R.layout.marcoset3);

               marcoset3name = (EditText) findViewById(R.id.marcoset3name);
               marco3setbtn = (EditText) findViewById(R.id.marco3setbtn);
               mButton01 = (Button) findViewById(R.id.btn01);
               mButton02 = (Button) findViewById(R.id.btn02);
               mButton03 = (Button) findViewById(R.id.btn03);
               mButton04 = (Button) findViewById(R.id.btn04);
               mButton05 = (Button) findViewById(R.id.btn05);
               mButton06 = (Button) findViewById(R.id.btn06);
               mButton07 = (Button) findViewById(R.id.btn07);
               mButton08 = (Button) findViewById(R.id.btn08);
               mButton09 = (Button) findViewById(R.id.btn09);
               mButton10 = (Button) findViewById(R.id.btn10);
               mButton11 = (Button) findViewById(R.id.btn11);
               mButton12 = (Button) findViewById(R.id.btn12);
               mButton13 = (Button) findViewById(R.id.btn13);
               mButton14 = (Button) findViewById(R.id.btn14);
               mButton15 = (Button) findViewById(R.id.btn15);
               mButton16 = (Button) findViewById(R.id.btn16);
               mButton17 = (Button) findViewById(R.id.btn17);
               mButton18 = (Button) findViewById(R.id.btn18);
               mButton19 = (Button) findViewById(R.id.btn19);
               mButton20 = (Button) findViewById(R.id.btn20);
               mButton21 = (Button) findViewById(R.id.btn21);
               mButton22 = (Button) findViewById(R.id.btn22);
               mButton23 = (Button) findViewById(R.id.btn23);
               mButton24 = (Button) findViewById(R.id.btn24);
//				mButton25 = (Button) findViewById(R.id.btn25);
//				mButton26 = (Button) findViewById(R.id.btn26);
//				mButton27 = (Button) findViewById(R.id.btn27);
//				mButton28 = (Button) findViewById(R.id.btn28);
//				mButton29 = (Button) findViewById(R.id.btn29);
//				mButton30 = (Button) findViewById(R.id.btn30);
//				mButton31 = (Button) findViewById(R.id.btn31);
//				mButton32 = (Button) findViewById(R.id.btn32);
               mButton33 = (Button) findViewById(R.id.btn33);
//				mButton34 = (Button) findViewById(R.id.btn34);
               mButton35 = (Button) findViewById(R.id.btn35);
//				mButton36 = (Button) findViewById(R.id.btn36);
//				mButton37 = (Button) findViewById(R.id.btn37);
//				mButton38 = (Button) findViewById(R.id.btn38);
//				mButton39 = (Button) findViewById(R.id.btn39);
//				mButton40 = (Button) findViewById(R.id.btn40);
//				mButton41 = (Button) findViewById(R.id.btn41);
               mButton42 = (Button) findViewById(R.id.btn42);
               mButton43 = (Button) findViewById(R.id.btn43);
               mButton44 = (Button) findViewById(R.id.btn44);
               mButton45 = (Button) findViewById(R.id.btn45);
               mButton46 = (Button) findViewById(R.id.btn46);
               mButton47 = (Button) findViewById(R.id.btn47);
               mButton48 = (Button) findViewById(R.id.btn48);
               mButton49 = (Button) findViewById(R.id.btn49);
               mButton50 = (Button) findViewById(R.id.btn50);
               mButton51 = (Button) findViewById(R.id.btn51);
               mButton52 = (Button) findViewById(R.id.btn52);
               mButton53 = (Button) findViewById(R.id.btn53);
//				mButton54 = (Button) findViewById(R.id.btn54);
//				mButton55 = (Button) findViewById(R.id.btn55);
//				mButton56 = (Button) findViewById(R.id.btn56);
//				mButton57 = (Button) findViewById(R.id.btn57);
//				mButton58 = (Button) findViewById(R.id.btn58);
//				mButton59 = (Button) findViewById(R.id.btn59);
//				mButton60 = (Button) findViewById(R.id.btn60);
//				mButton61 = (Button) findViewById(R.id.btn61);
//				mButton62 = (Button) findViewById(R.id.btn62);
               mButton63 = (Button) findViewById(R.id.btn63);
               mButton64 = (Button) findViewById(R.id.btn64);
               mButton65 = (Button) findViewById(R.id.btn65);
               mButton66 = (Button) findViewById(R.id.btn66);
               mButton67 = (Button) findViewById(R.id.btn67);
               mButton68 = (Button) findViewById(R.id.btn68);
//				mButton69 = (Button) findViewById(R.id.btn69);
//				mButton70 = (Button) findViewById(R.id.btn70);
//				mButton71 = (Button) findViewById(R.id.btn71);
//				mButton72 = (Button) findViewById(R.id.btn72);
//				mButton73 = (Button) findViewById(R.id.btn73);
//				mButton74 = (Button) findViewById(R.id.btn74);
//				mButton75 = (Button) findViewById(R.id.btn75);
//				mButton76 = (Button) findViewById(R.id.btn76);
//				mButton77 = (Button) findViewById(R.id.btn77);
               mButton78 = (Button) findViewById(R.id.btn78);
               mButton79 = (Button) findViewById(R.id.btn79);
               mButton80 = (Button) findViewById(R.id.btn80);
               mButton81 = (Button) findViewById(R.id.btn81);
               mButton82 = (Button) findViewById(R.id.btn82);
               mButton83 = (Button) findViewById(R.id.btn83);
               //mButton84 = (Button) findViewById(R.id.btn84);
               //mButton85 = (Button) findViewById(R.id.btn85);
               //mButton86 = (Button) findViewById(R.id.btn86);
               //mButton87 = (Button) findViewById(R.id.btn87);
               //mButton88 = (Button) findViewById(R.id.btn88);
               //mButton89 = (Button) findViewById(R.id.btn89);
               //mButton90 = (Button) findViewById(R.id.btn90);
               //mButton91 = (Button) findViewById(R.id.btn91);
               //mButton92 = (Button) findViewById(R.id.btn92);
               mButton93 = (Button) findViewById(R.id.btn93);
               mButton94 = (Button) findViewById(R.id.btn94);
               mButton95 = (Button) findViewById(R.id.btn95);
               mButton96 = (Button) findViewById(R.id.btn96);
               mButton97 = (Button) findViewById(R.id.btn97);
               mButton98 = (Button) findViewById(R.id.btn98);
//				mButton99 = (Button) findViewById(R.id.btn99);
//				mButton100 = (Button) findViewById(R.id.btn100);
//				mButton101 = (Button) findViewById(R.id.btn101);
//				mButton102 = (Button) findViewById(R.id.btn102);
//				mButton103 = (Button) findViewById(R.id.btn103);
//				mButton104 = (Button) findViewById(R.id.btn104);
//				mButton105 = (Button) findViewById(R.id.btn105);
//				mButton106 = (Button) findViewById(R.id.btn106);
//				mButton107 = (Button) findViewById(R.id.btn107);
//				mButton108 = (Button) findViewById(R.id.btn108);
//				mButton109 = (Button) findViewById(R.id.btn109);
//				mButton110 = (Button) findViewById(R.id.btn110);
//				mButton111 = (Button) findViewById(R.id.btn111);
//				mButton112 = (Button) findViewById(R.id.btn112);
               mButton153 = (Button) findViewById(R.id.btn153);
               mButton154 = (Button) findViewById(R.id.btn154);
               mButton155 = (Button) findViewById(R.id.btn155);
               mButton156 = (Button) findViewById(R.id.btn156);


               findViewById(R.id.marcoset3ok).setOnClickListener(new ButtonListener());

               findViewById(R.id.s16).setOnClickListener(new ButtonListener());
               findViewById(R.id.s17).setOnClickListener(new ButtonListener());
               findViewById(R.id.s18).setOnClickListener(new ButtonListener());
               findViewById(R.id.s19).setOnClickListener(new ButtonListener());
               findViewById(R.id.s20).setOnClickListener(new ButtonListener());


               lamp = getSharedPreferences(lampdata,0);
               mButton01.setText(lamp.getString(btn01name,""));
               mButton02.setText(lamp.getString(btn02name,""));
               mButton03.setText(lamp.getString(btn03name,""));
               mButton04.setText(lamp.getString(btn04name,""));
               mButton05.setText(lamp.getString(btn05name,""));
               mButton06.setText(lamp.getString(btn06name,""));
               mButton07.setText(lamp.getString(btn07name,""));
               mButton08.setText(lamp.getString(btn08name,""));
               mButton09.setText(lamp.getString(btn09name,""));
               mButton10.setText(lamp.getString(btn10name,""));
               mButton11.setText(lamp.getString(btn11name,""));
               mButton12.setText(lamp.getString(btn12name,""));

               setting = getSharedPreferences(custom1data,0);

               cus1name=(TextView)findViewById(R.id.cus1name);
               cus1name.setText(setting.getString(custom1name,""));
//				mButton54.setText(setting.getString(btn54name, ""));
//				mButton55.setText(setting.getString(btn55name, ""));
//				mButton56.setText(setting.getString(btn56name, ""));
//				mButton57.setText(setting.getString(btn57name, ""));
//				mButton58.setText(setting.getString(btn58name, ""));
//				mButton59.setText(setting.getString(btn59name, ""));
//				mButton60.setText(setting.getString(btn60name, ""));
//				mButton61.setText(setting.getString(btn61name, ""));
//				mButton62.setText(setting.getString(btn62name, ""));
               mButton63.setText(setting.getString(btn63name, ""));
               mButton64.setText(setting.getString(btn64name, ""));
               mButton65.setText(setting.getString(btn65name, ""));
               mButton66.setText(setting.getString(btn66name, ""));
               mButton67.setText(setting.getString(btn67name, ""));
               mButton68.setText(setting.getString(btn68name, ""));
//				mButton99.setText(setting.getString(btn99name, ""));
//				mButton100.setText(setting.getString(btn100name, ""));
//				mButton101.setText(setting.getString(btn101name, ""));
//				mButton102.setText(setting.getString(btn102name, ""));
//				mButton103.setText(setting.getString(btn103name, ""));
//				mButton104.setText(setting.getString(btn104name, ""));

               settingb = getSharedPreferences(custom2data,0);

               cus2name=(TextView)findViewById(R.id.cus2name);
               cus2name.setText(settingb.getString(custom2name,""));
//				 mButton69.setText(settingb.getString(btn69name, ""));
//				 mButton70.setText(settingb.getString(btn70name, ""));
//				 mButton71.setText(settingb.getString(btn71name, ""));
//				 mButton72.setText(settingb.getString(btn72name, ""));
//				 mButton73.setText(settingb.getString(btn73name, ""));
//				 mButton74.setText(settingb.getString(btn74name, ""));
//				 mButton75.setText(settingb.getString(btn75name, ""));
//				 mButton76.setText(settingb.getString(btn76name, ""));
//				 mButton77.setText(settingb.getString(btn77name, ""));
               mButton78.setText(settingb.getString(btn78name, ""));
               mButton79.setText(settingb.getString(btn79name, ""));
               mButton80.setText(settingb.getString(btn80name, ""));
               mButton81.setText(settingb.getString(btn81name, ""));
               mButton82.setText(settingb.getString(btn82name, ""));
               mButton83.setText(settingb.getString(btn83name, ""));
//				 mButton105.setText(settingb.getString(btn105name, ""));
//				 mButton106.setText(settingb.getString(btn106name, ""));
//				 mButton107.setText(settingb.getString(btn107name, ""));
//				 mButton108.setText(settingb.getString(btn108name, ""));
//				 mButton109.setText(settingb.getString(btn109name, ""));
//				 mButton110.setText(settingb.getString(btn110name, ""));

               setting2 = getSharedPreferences(custom3data,0);

               cus3name=(TextView)findViewById(R.id.cus3name);
               cus3name.setText(setting2.getString(custom3name,""));
               // mButton84.setText(setting2.getString(btn84name, ""));
               //mButton85.setText(setting2.getString(btn85name, ""));
               //mButton86.setText(setting2.getString(btn86name, ""));
               //mButton87.setText(setting2.getString(btn87name, ""));
               //mButton88.setText(setting2.getString(btn88name, ""));
               //mButton89.setText(setting2.getString(btn89name, ""));
               //mButton90.setText(setting2.getString(btn90name, ""));
               //mButton91.setText(setting2.getString(btn91name, ""));
               //mButton92.setText(setting2.getString(btn92name, ""));
               mButton93.setText(setting2.getString(btn93name, ""));
               mButton94.setText(setting2.getString(btn94name, ""));
               mButton95.setText(setting2.getString(btn95name, ""));
               mButton96.setText(setting2.getString(btn96name, ""));
               mButton97.setText(setting2.getString(btn97name, ""));
               mButton98.setText(setting2.getString(btn98name, ""));
               //mButton111.setText(setting2.getString(btn111name, ""));
               //mButton112.setText(setting2.getString(btn112name, ""));
               //mButton113.setText(setting2.getString(btn113name, ""));
               //mButton114.setText(setting2.getString(btn114name, ""));
               //mButton115.setText(setting2.getString(btn115name, ""));
               //mButton116.setText(setting2.getString(btn116name, ""));

               airseting = getSharedPreferences(airdata,0);
               mButton17.setText(airseting.getString(btn17name,""));
               mButton18.setText(airseting.getString(btn18name,""));
               mButton19.setText(airseting.getString(btn19name,""));
               mButton20.setText(airseting.getString(btn20name,""));
               mButton21.setText(airseting.getString(btn21name,""));
               mButton22.setText(airseting.getString(btn22name,""));


               lamp = getSharedPreferences(lampdata,0);
               imagebtn1 = (ImageView) findViewById(R.id.imagebtn1);
               imagebtn2 = (ImageView) findViewById(R.id.imagebtn2);
               imagebtn3 = (ImageView) findViewById(R.id.imagebtn3);
               imagebtn4 = (ImageView) findViewById(R.id.imagebtn4);
               imagebtn5 = (ImageView) findViewById(R.id.imagebtn5);
               imagebtn6 = (ImageView) findViewById(R.id.imagebtn6);
               imagebtn7 = (ImageView) findViewById(R.id.imagebtn7);
               imagebtn8 = (ImageView) findViewById(R.id.imagebtn8);
               imagebtn9 = (ImageView) findViewById(R.id.imagebtn9);
               imagebtn10 = (ImageView) findViewById(R.id.imagebtn10);
               imagebtn11 = (ImageView) findViewById(R.id.imagebtn11);
               imagebtn12 = (ImageView) findViewById(R.id.imagebtn12);



               if(lamp.getString(btn1pic,"")==null)
               {
                  imagebtn1.setImageDrawable(null);
               }else{
                  imagebtn1.setImageURI(Uri.parse(lamp.getString(btn1pic,"")));
                  Log.e("uri1", lamp.getString(btn1pic,""));
               }

               if(lamp.getString(btn2pic,"")==null)
               {
                  imagebtn2.setImageDrawable(null);
               }else{
                  imagebtn2.setImageURI(Uri.parse(lamp.getString(btn2pic,"")));
               }

               if(lamp.getString(btn3pic,"")==null)
               {
                  imagebtn3.setImageDrawable(null);
               }else{
                  imagebtn3.setImageURI(Uri.parse(lamp.getString(btn3pic,"")));
               }

               if(lamp.getString(btn4pic,"")==null)
               {
                  imagebtn4.setImageDrawable(null);
               }else{
                  imagebtn4.setImageURI(Uri.parse(lamp.getString(btn4pic,"")));
               }

               if(lamp.getString(btn5pic,"")==null)
               {
                  imagebtn5.setImageDrawable(null);
               }else{
                  imagebtn5.setImageURI(Uri.parse(lamp.getString(btn5pic,"")));
               }

               if(lamp.getString(btn6pic,"")==null)
               {
                  imagebtn6.setImageDrawable(null);
               }else{
                  imagebtn6.setImageURI(Uri.parse(lamp.getString(btn6pic,"")));
               }

               if(lamp.getString(btn7pic,"")==null)
               {
                  imagebtn7.setImageDrawable(null);
               }else{
                  imagebtn7.setImageURI(Uri.parse(lamp.getString(btn7pic,"")));
               }

               if(lamp.getString(btn8pic,"")==null)
               {
                  imagebtn8.setImageDrawable(null);
               }else{
                  imagebtn8.setImageURI(Uri.parse(lamp.getString(btn8pic,"")));
               }

               if(lamp.getString(btn9pic,"")==null)
               {
                  imagebtn9.setImageDrawable(null);
               }else{
                  imagebtn9.setImageURI(Uri.parse(lamp.getString(btn9pic,"")));
               }

               if(lamp.getString(btn10pic,"")==null)
               {
                  imagebtn10.setImageDrawable(null);
               }else{
                  imagebtn10.setImageURI(Uri.parse(lamp.getString(btn10pic,"")));
               }

               if(lamp.getString(btn11pic,"")==null)
               {
                  imagebtn11.setImageDrawable(null);
               }else{
                  imagebtn11.setImageURI(Uri.parse(lamp.getString(btn11pic,"")));
               }

               if(lamp.getString(btn12pic,"")==null)
               {
                  imagebtn12.setImageDrawable(null);
               }else{
                  imagebtn12.setImageURI(Uri.parse(lamp.getString(btn12pic,"")));
               }

               readData(8);
               break;


            //marco5
            case R.id.f5:
               // TODO Auto-generated catch block
               setContentView(R.layout.marcoset4);

               marcoset4name = (EditText) findViewById(R.id.marcoset4name);
               marco4setbtn = (EditText) findViewById(R.id.marco4setbtn);
               mButton01 = (Button) findViewById(R.id.btn01);
               mButton02 = (Button) findViewById(R.id.btn02);
               mButton03 = (Button) findViewById(R.id.btn03);
               mButton04 = (Button) findViewById(R.id.btn04);
               mButton05 = (Button) findViewById(R.id.btn05);
               mButton06 = (Button) findViewById(R.id.btn06);
               mButton07 = (Button) findViewById(R.id.btn07);
               mButton08 = (Button) findViewById(R.id.btn08);
               mButton09 = (Button) findViewById(R.id.btn09);
               mButton10 = (Button) findViewById(R.id.btn10);
               mButton11 = (Button) findViewById(R.id.btn11);
               mButton12 = (Button) findViewById(R.id.btn12);
               mButton13 = (Button) findViewById(R.id.btn13);
               mButton14 = (Button) findViewById(R.id.btn14);
               mButton15 = (Button) findViewById(R.id.btn15);
               mButton16 = (Button) findViewById(R.id.btn16);
               mButton17 = (Button) findViewById(R.id.btn17);
               mButton18 = (Button) findViewById(R.id.btn18);
               mButton19 = (Button) findViewById(R.id.btn19);
               mButton20 = (Button) findViewById(R.id.btn20);
               mButton21 = (Button) findViewById(R.id.btn21);
               mButton22 = (Button) findViewById(R.id.btn22);
               mButton23 = (Button) findViewById(R.id.btn23);
               mButton24 = (Button) findViewById(R.id.btn24);
//				mButton25 = (Button) findViewById(R.id.btn25);
//				mButton26 = (Button) findViewById(R.id.btn26);
//				mButton27 = (Button) findViewById(R.id.btn27);
//				mButton28 = (Button) findViewById(R.id.btn28);
//				mButton29 = (Button) findViewById(R.id.btn29);
//				mButton30 = (Button) findViewById(R.id.btn30);
//				mButton31 = (Button) findViewById(R.id.btn31);
//				mButton32 = (Button) findViewById(R.id.btn32);
               mButton33 = (Button) findViewById(R.id.btn33);
//				mButton34 = (Button) findViewById(R.id.btn34);
               mButton35 = (Button) findViewById(R.id.btn35);
//				mButton36 = (Button) findViewById(R.id.btn36);
//				mButton37 = (Button) findViewById(R.id.btn37);
//				mButton38 = (Button) findViewById(R.id.btn38);
//				mButton39 = (Button) findViewById(R.id.btn39);
//				mButton40 = (Button) findViewById(R.id.btn40);
//				mButton41 = (Button) findViewById(R.id.btn41);
               mButton42 = (Button) findViewById(R.id.btn42);
               mButton43 = (Button) findViewById(R.id.btn43);
               mButton44 = (Button) findViewById(R.id.btn44);
               mButton45 = (Button) findViewById(R.id.btn45);
               mButton46 = (Button) findViewById(R.id.btn46);
               mButton47 = (Button) findViewById(R.id.btn47);
               mButton48 = (Button) findViewById(R.id.btn48);
               mButton49 = (Button) findViewById(R.id.btn49);
               mButton50 = (Button) findViewById(R.id.btn50);
               mButton51 = (Button) findViewById(R.id.btn51);
               mButton52 = (Button) findViewById(R.id.btn52);
               mButton53 = (Button) findViewById(R.id.btn53);
//				mButton54 = (Button) findViewById(R.id.btn54);
//				mButton55 = (Button) findViewById(R.id.btn55);
//				mButton56 = (Button) findViewById(R.id.btn56);
//				mButton57 = (Button) findViewById(R.id.btn57);
//				mButton58 = (Button) findViewById(R.id.btn58);
//				mButton59 = (Button) findViewById(R.id.btn59);
//				mButton60 = (Button) findViewById(R.id.btn60);
//				mButton61 = (Button) findViewById(R.id.btn61);
//				mButton62 = (Button) findViewById(R.id.btn62);
               mButton63 = (Button) findViewById(R.id.btn63);
               mButton64 = (Button) findViewById(R.id.btn64);
               mButton65 = (Button) findViewById(R.id.btn65);
               mButton66 = (Button) findViewById(R.id.btn66);
               mButton67 = (Button) findViewById(R.id.btn67);
               mButton68 = (Button) findViewById(R.id.btn68);
//				mButton69 = (Button) findViewById(R.id.btn69);
//				mButton70 = (Button) findViewById(R.id.btn70);
//				mButton71 = (Button) findViewById(R.id.btn71);
//				mButton72 = (Button) findViewById(R.id.btn72);
//				mButton73 = (Button) findViewById(R.id.btn73);
//				mButton74 = (Button) findViewById(R.id.btn74);
//				mButton75 = (Button) findViewById(R.id.btn75);
//				mButton76 = (Button) findViewById(R.id.btn76);
//				mButton77 = (Button) findViewById(R.id.btn77);
               mButton78 = (Button) findViewById(R.id.btn78);
               mButton79 = (Button) findViewById(R.id.btn79);
               mButton80 = (Button) findViewById(R.id.btn80);
               mButton81 = (Button) findViewById(R.id.btn81);
               mButton82 = (Button) findViewById(R.id.btn82);
               mButton83 = (Button) findViewById(R.id.btn83);
               //mButton84 = (Button) findViewById(R.id.btn84);
               //mButton85 = (Button) findViewById(R.id.btn85);
               //mButton86 = (Button) findViewById(R.id.btn86);
               //mButton87 = (Button) findViewById(R.id.btn87);
               //mButton88 = (Button) findViewById(R.id.btn88);
               //mButton89 = (Button) findViewById(R.id.btn89);
               //mButton90 = (Button) findViewById(R.id.btn90);
               //mButton91 = (Button) findViewById(R.id.btn91);
               //mButton92 = (Button) findViewById(R.id.btn92);
               mButton93 = (Button) findViewById(R.id.btn93);
               mButton94 = (Button) findViewById(R.id.btn94);
               mButton95 = (Button) findViewById(R.id.btn95);
               mButton96 = (Button) findViewById(R.id.btn96);
               mButton97 = (Button) findViewById(R.id.btn97);
               mButton98 = (Button) findViewById(R.id.btn98);
//				mButton99 = (Button) findViewById(R.id.btn99);
//				mButton100 = (Button) findViewById(R.id.btn100);
//				mButton101 = (Button) findViewById(R.id.btn101);
//				mButton102 = (Button) findViewById(R.id.btn102);
//				mButton103 = (Button) findViewById(R.id.btn103);
//				mButton104 = (Button) findViewById(R.id.btn104);
//				mButton105 = (Button) findViewById(R.id.btn105);
//				mButton106 = (Button) findViewById(R.id.btn106);
//				mButton107 = (Button) findViewById(R.id.btn107);
//				mButton108 = (Button) findViewById(R.id.btn108);
//				mButton109 = (Button) findViewById(R.id.btn109);
//				mButton110 = (Button) findViewById(R.id.btn110);
//				mButton111 = (Button) findViewById(R.id.btn111);
//				mButton112 = (Button) findViewById(R.id.btn112);
               mButton153 = (Button) findViewById(R.id.btn153);
               mButton154 = (Button) findViewById(R.id.btn154);
               mButton155 = (Button) findViewById(R.id.btn155);
               mButton156 = (Button) findViewById(R.id.btn156);


               findViewById(R.id.marcoset4ok).setOnClickListener(new ButtonListener());

               findViewById(R.id.s21).setOnClickListener(new ButtonListener());
               findViewById(R.id.s22).setOnClickListener(new ButtonListener());
               findViewById(R.id.s23).setOnClickListener(new ButtonListener());
               findViewById(R.id.s24).setOnClickListener(new ButtonListener());
               findViewById(R.id.s25).setOnClickListener(new ButtonListener());


               lamp = getSharedPreferences(lampdata,0);
               mButton01.setText(lamp.getString(btn01name,""));
               mButton02.setText(lamp.getString(btn02name,""));
               mButton03.setText(lamp.getString(btn03name,""));
               mButton04.setText(lamp.getString(btn04name,""));
               mButton05.setText(lamp.getString(btn05name,""));
               mButton06.setText(lamp.getString(btn06name,""));
               mButton07.setText(lamp.getString(btn07name,""));
               mButton08.setText(lamp.getString(btn08name,""));
               mButton09.setText(lamp.getString(btn09name,""));
               mButton10.setText(lamp.getString(btn10name,""));
               mButton11.setText(lamp.getString(btn11name,""));
               mButton12.setText(lamp.getString(btn12name,""));

               setting = getSharedPreferences(custom1data,0);

               cus1name=(TextView)findViewById(R.id.cus1name);
               cus1name.setText(setting.getString(custom1name,""));
//				mButton54.setText(setting.getString(btn54name, ""));
//				mButton55.setText(setting.getString(btn55name, ""));
//				mButton56.setText(setting.getString(btn56name, ""));
//				mButton57.setText(setting.getString(btn57name, ""));
//				mButton58.setText(setting.getString(btn58name, ""));
//				mButton59.setText(setting.getString(btn59name, ""));
//				mButton60.setText(setting.getString(btn60name, ""));
//				mButton61.setText(setting.getString(btn61name, ""));
//				mButton62.setText(setting.getString(btn62name, ""));
               mButton63.setText(setting.getString(btn63name, ""));
               mButton64.setText(setting.getString(btn64name, ""));
               mButton65.setText(setting.getString(btn65name, ""));
               mButton66.setText(setting.getString(btn66name, ""));
               mButton67.setText(setting.getString(btn67name, ""));
               mButton68.setText(setting.getString(btn68name, ""));
//				mButton99.setText(setting.getString(btn99name, ""));
//				mButton100.setText(setting.getString(btn100name, ""));
//				mButton101.setText(setting.getString(btn101name, ""));
//				mButton102.setText(setting.getString(btn102name, ""));
//				mButton103.setText(setting.getString(btn103name, ""));
//				mButton104.setText(setting.getString(btn104name, ""));

               settingb = getSharedPreferences(custom2data,0);

               cus2name=(TextView)findViewById(R.id.cus2name);
               cus2name.setText(settingb.getString(custom2name,""));
//				 mButton69.setText(settingb.getString(btn69name, ""));
//				 mButton70.setText(settingb.getString(btn70name, ""));
//				 mButton71.setText(settingb.getString(btn71name, ""));
//				 mButton72.setText(settingb.getString(btn72name, ""));
//				 mButton73.setText(settingb.getString(btn73name, ""));
//				 mButton74.setText(settingb.getString(btn74name, ""));
//				 mButton75.setText(settingb.getString(btn75name, ""));
//				 mButton76.setText(settingb.getString(btn76name, ""));
//				 mButton77.setText(settingb.getString(btn77name, ""));
               mButton78.setText(settingb.getString(btn78name, ""));
               mButton79.setText(settingb.getString(btn79name, ""));
               mButton80.setText(settingb.getString(btn80name, ""));
               mButton81.setText(settingb.getString(btn81name, ""));
               mButton82.setText(settingb.getString(btn82name, ""));
               mButton83.setText(settingb.getString(btn83name, ""));
//				 mButton105.setText(settingb.getString(btn105name, ""));
//				 mButton106.setText(settingb.getString(btn106name, ""));
//				 mButton107.setText(settingb.getString(btn107name, ""));
//				 mButton108.setText(settingb.getString(btn108name, ""));
//				 mButton109.setText(settingb.getString(btn109name, ""));
//				 mButton110.setText(settingb.getString(btn110name, ""));

               setting2 = getSharedPreferences(custom3data,0);

               cus3name=(TextView)findViewById(R.id.cus3name);
               cus3name.setText(setting2.getString(custom3name,""));
               // mButton84.setText(setting2.getString(btn84name, ""));
               //mButton85.setText(setting2.getString(btn85name, ""));
               //mButton86.setText(setting2.getString(btn86name, ""));
               //mButton87.setText(setting2.getString(btn87name, ""));
               //mButton88.setText(setting2.getString(btn88name, ""));
               //mButton89.setText(setting2.getString(btn89name, ""));
               //mButton90.setText(setting2.getString(btn90name, ""));
               //mButton91.setText(setting2.getString(btn91name, ""));
               //mButton92.setText(setting2.getString(btn92name, ""));
               mButton93.setText(setting2.getString(btn93name, ""));
               mButton94.setText(setting2.getString(btn94name, ""));
               mButton95.setText(setting2.getString(btn95name, ""));
               mButton96.setText(setting2.getString(btn96name, ""));
               mButton97.setText(setting2.getString(btn97name, ""));
               mButton98.setText(setting2.getString(btn98name, ""));
               //mButton111.setText(setting2.getString(btn111name, ""));
               //mButton112.setText(setting2.getString(btn112name, ""));
               //mButton113.setText(setting2.getString(btn113name, ""));
               //mButton114.setText(setting2.getString(btn114name, ""));
               //mButton115.setText(setting2.getString(btn115name, ""));
               //mButton116.setText(setting2.getString(btn116name, ""));

               airseting = getSharedPreferences(airdata,0);
               mButton17.setText(airseting.getString(btn17name,""));
               mButton18.setText(airseting.getString(btn18name,""));
               mButton19.setText(airseting.getString(btn19name,""));
               mButton20.setText(airseting.getString(btn20name,""));
               mButton21.setText(airseting.getString(btn21name,""));
               mButton22.setText(airseting.getString(btn22name,""));


               lamp = getSharedPreferences(lampdata,0);
               imagebtn1 = (ImageView) findViewById(R.id.imagebtn1);
               imagebtn2 = (ImageView) findViewById(R.id.imagebtn2);
               imagebtn3 = (ImageView) findViewById(R.id.imagebtn3);
               imagebtn4 = (ImageView) findViewById(R.id.imagebtn4);
               imagebtn5 = (ImageView) findViewById(R.id.imagebtn5);
               imagebtn6 = (ImageView) findViewById(R.id.imagebtn6);
               imagebtn7 = (ImageView) findViewById(R.id.imagebtn7);
               imagebtn8 = (ImageView) findViewById(R.id.imagebtn8);
               imagebtn9 = (ImageView) findViewById(R.id.imagebtn9);
               imagebtn10 = (ImageView) findViewById(R.id.imagebtn10);
               imagebtn11 = (ImageView) findViewById(R.id.imagebtn11);
               imagebtn12 = (ImageView) findViewById(R.id.imagebtn12);



               if(lamp.getString(btn1pic,"")==null)
               {
                  imagebtn1.setImageDrawable(null);
               }else{
                  imagebtn1.setImageURI(Uri.parse(lamp.getString(btn1pic,"")));
                  Log.e("uri1", lamp.getString(btn1pic,""));
               }

               if(lamp.getString(btn2pic,"")==null)
               {
                  imagebtn2.setImageDrawable(null);
               }else{
                  imagebtn2.setImageURI(Uri.parse(lamp.getString(btn2pic,"")));
               }

               if(lamp.getString(btn3pic,"")==null)
               {
                  imagebtn3.setImageDrawable(null);
               }else{
                  imagebtn3.setImageURI(Uri.parse(lamp.getString(btn3pic,"")));
               }

               if(lamp.getString(btn4pic,"")==null)
               {
                  imagebtn4.setImageDrawable(null);
               }else{
                  imagebtn4.setImageURI(Uri.parse(lamp.getString(btn4pic,"")));
               }

               if(lamp.getString(btn5pic,"")==null)
               {
                  imagebtn5.setImageDrawable(null);
               }else{
                  imagebtn5.setImageURI(Uri.parse(lamp.getString(btn5pic,"")));
               }

               if(lamp.getString(btn6pic,"")==null)
               {
                  imagebtn6.setImageDrawable(null);
               }else{
                  imagebtn6.setImageURI(Uri.parse(lamp.getString(btn6pic,"")));
               }

               if(lamp.getString(btn7pic,"")==null)
               {
                  imagebtn7.setImageDrawable(null);
               }else{
                  imagebtn7.setImageURI(Uri.parse(lamp.getString(btn7pic,"")));
               }

               if(lamp.getString(btn8pic,"")==null)
               {
                  imagebtn8.setImageDrawable(null);
               }else{
                  imagebtn8.setImageURI(Uri.parse(lamp.getString(btn8pic,"")));
               }

               if(lamp.getString(btn9pic,"")==null)
               {
                  imagebtn9.setImageDrawable(null);
               }else{
                  imagebtn9.setImageURI(Uri.parse(lamp.getString(btn9pic,"")));
               }

               if(lamp.getString(btn10pic,"")==null)
               {
                  imagebtn10.setImageDrawable(null);
               }else{
                  imagebtn10.setImageURI(Uri.parse(lamp.getString(btn10pic,"")));
               }

               if(lamp.getString(btn11pic,"")==null)
               {
                  imagebtn11.setImageDrawable(null);
               }else{
                  imagebtn11.setImageURI(Uri.parse(lamp.getString(btn11pic,"")));
               }

               if(lamp.getString(btn12pic,"")==null)
               {
                  imagebtn12.setImageDrawable(null);
               }else{
                  imagebtn12.setImageURI(Uri.parse(lamp.getString(btn12pic,"")));
               }

               readData(9);
               break;


            //marco6
            case R.id.f6:
               // TODO Auto-generated catch block
               setContentView(R.layout.marcoset5);

               marcoset5name = (EditText) findViewById(R.id.marcoset5name);
               marco5setbtn = (EditText) findViewById(R.id.marco5setbtn);
               mButton01 = (Button) findViewById(R.id.btn01);
               mButton02 = (Button) findViewById(R.id.btn02);
               mButton03 = (Button) findViewById(R.id.btn03);
               mButton04 = (Button) findViewById(R.id.btn04);
               mButton05 = (Button) findViewById(R.id.btn05);
               mButton06 = (Button) findViewById(R.id.btn06);
               mButton07 = (Button) findViewById(R.id.btn07);
               mButton08 = (Button) findViewById(R.id.btn08);
               mButton09 = (Button) findViewById(R.id.btn09);
               mButton10 = (Button) findViewById(R.id.btn10);
               mButton11 = (Button) findViewById(R.id.btn11);
               mButton12 = (Button) findViewById(R.id.btn12);
               mButton13 = (Button) findViewById(R.id.btn13);
               mButton14 = (Button) findViewById(R.id.btn14);
               mButton15 = (Button) findViewById(R.id.btn15);
               mButton16 = (Button) findViewById(R.id.btn16);
               mButton17 = (Button) findViewById(R.id.btn17);
               mButton18 = (Button) findViewById(R.id.btn18);
               mButton19 = (Button) findViewById(R.id.btn19);
               mButton20 = (Button) findViewById(R.id.btn20);
               mButton21 = (Button) findViewById(R.id.btn21);
               mButton22 = (Button) findViewById(R.id.btn22);
               mButton23 = (Button) findViewById(R.id.btn23);
               mButton24 = (Button) findViewById(R.id.btn24);
//				mButton25 = (Button) findViewById(R.id.btn25);
//				mButton26 = (Button) findViewById(R.id.btn26);
//				mButton27 = (Button) findViewById(R.id.btn27);
//				mButton28 = (Button) findViewById(R.id.btn28);
//				mButton29 = (Button) findViewById(R.id.btn29);
//				mButton30 = (Button) findViewById(R.id.btn30);
//				mButton31 = (Button) findViewById(R.id.btn31);
//				mButton32 = (Button) findViewById(R.id.btn32);
               mButton33 = (Button) findViewById(R.id.btn33);
//				mButton34 = (Button) findViewById(R.id.btn34);
               mButton35 = (Button) findViewById(R.id.btn35);
//				mButton36 = (Button) findViewById(R.id.btn36);
//				mButton37 = (Button) findViewById(R.id.btn37);
//				mButton38 = (Button) findViewById(R.id.btn38);
//				mButton39 = (Button) findViewById(R.id.btn39);
//				mButton40 = (Button) findViewById(R.id.btn40);
//				mButton41 = (Button) findViewById(R.id.btn41);
               mButton42 = (Button) findViewById(R.id.btn42);
               mButton43 = (Button) findViewById(R.id.btn43);
               mButton44 = (Button) findViewById(R.id.btn44);
               mButton45 = (Button) findViewById(R.id.btn45);
               mButton46 = (Button) findViewById(R.id.btn46);
               mButton47 = (Button) findViewById(R.id.btn47);
               mButton48 = (Button) findViewById(R.id.btn48);
               mButton49 = (Button) findViewById(R.id.btn49);
               mButton50 = (Button) findViewById(R.id.btn50);
               mButton51 = (Button) findViewById(R.id.btn51);
               mButton52 = (Button) findViewById(R.id.btn52);
               mButton53 = (Button) findViewById(R.id.btn53);
//				mButton54 = (Button) findViewById(R.id.btn54);
//				mButton55 = (Button) findViewById(R.id.btn55);
//				mButton56 = (Button) findViewById(R.id.btn56);
//				mButton57 = (Button) findViewById(R.id.btn57);
//				mButton58 = (Button) findViewById(R.id.btn58);
//				mButton59 = (Button) findViewById(R.id.btn59);
//				mButton60 = (Button) findViewById(R.id.btn60);
//				mButton61 = (Button) findViewById(R.id.btn61);
//				mButton62 = (Button) findViewById(R.id.btn62);
               mButton63 = (Button) findViewById(R.id.btn63);
               mButton64 = (Button) findViewById(R.id.btn64);
               mButton65 = (Button) findViewById(R.id.btn65);
               mButton66 = (Button) findViewById(R.id.btn66);
               mButton67 = (Button) findViewById(R.id.btn67);
               mButton68 = (Button) findViewById(R.id.btn68);
//				mButton69 = (Button) findViewById(R.id.btn69);
//				mButton70 = (Button) findViewById(R.id.btn70);
//				mButton71 = (Button) findViewById(R.id.btn71);
//				mButton72 = (Button) findViewById(R.id.btn72);
//				mButton73 = (Button) findViewById(R.id.btn73);
//				mButton74 = (Button) findViewById(R.id.btn74);
//				mButton75 = (Button) findViewById(R.id.btn75);
//				mButton76 = (Button) findViewById(R.id.btn76);
//				mButton77 = (Button) findViewById(R.id.btn77);
               mButton78 = (Button) findViewById(R.id.btn78);
               mButton79 = (Button) findViewById(R.id.btn79);
               mButton80 = (Button) findViewById(R.id.btn80);
               mButton81 = (Button) findViewById(R.id.btn81);
               mButton82 = (Button) findViewById(R.id.btn82);
               mButton83 = (Button) findViewById(R.id.btn83);
               //mButton84 = (Button) findViewById(R.id.btn84);
               //mButton85 = (Button) findViewById(R.id.btn85);
               //mButton86 = (Button) findViewById(R.id.btn86);
               //mButton87 = (Button) findViewById(R.id.btn87);
               //mButton88 = (Button) findViewById(R.id.btn88);
               //mButton89 = (Button) findViewById(R.id.btn89);
               //mButton90 = (Button) findViewById(R.id.btn90);
               //mButton91 = (Button) findViewById(R.id.btn91);
               //mButton92 = (Button) findViewById(R.id.btn92);
               mButton93 = (Button) findViewById(R.id.btn93);
               mButton94 = (Button) findViewById(R.id.btn94);
               mButton95 = (Button) findViewById(R.id.btn95);
               mButton96 = (Button) findViewById(R.id.btn96);
               mButton97 = (Button) findViewById(R.id.btn97);
               mButton98 = (Button) findViewById(R.id.btn98);
//				mButton99 = (Button) findViewById(R.id.btn99);
//				mButton100 = (Button) findViewById(R.id.btn100);
//				mButton101 = (Button) findViewById(R.id.btn101);
//				mButton102 = (Button) findViewById(R.id.btn102);
//				mButton103 = (Button) findViewById(R.id.btn103);
//				mButton104 = (Button) findViewById(R.id.btn104);
//				mButton105 = (Button) findViewById(R.id.btn105);
//				mButton106 = (Button) findViewById(R.id.btn106);
//				mButton107 = (Button) findViewById(R.id.btn107);
//				mButton108 = (Button) findViewById(R.id.btn108);
//				mButton109 = (Button) findViewById(R.id.btn109);
//				mButton110 = (Button) findViewById(R.id.btn110);
//				mButton111 = (Button) findViewById(R.id.btn111);
//				mButton112 = (Button) findViewById(R.id.btn112);
               mButton153 = (Button) findViewById(R.id.btn153);
               mButton154 = (Button) findViewById(R.id.btn154);
               mButton155 = (Button) findViewById(R.id.btn155);
               mButton156 = (Button) findViewById(R.id.btn156);


               findViewById(R.id.marcoset5ok).setOnClickListener(new ButtonListener());

               findViewById(R.id.s26).setOnClickListener(new ButtonListener());
               findViewById(R.id.s27).setOnClickListener(new ButtonListener());
               findViewById(R.id.s28).setOnClickListener(new ButtonListener());
               findViewById(R.id.s29).setOnClickListener(new ButtonListener());
               findViewById(R.id.s30).setOnClickListener(new ButtonListener());


               lamp = getSharedPreferences(lampdata,0);
               mButton01.setText(lamp.getString(btn01name,""));
               mButton02.setText(lamp.getString(btn02name,""));
               mButton03.setText(lamp.getString(btn03name,""));
               mButton04.setText(lamp.getString(btn04name,""));
               mButton05.setText(lamp.getString(btn05name,""));
               mButton06.setText(lamp.getString(btn06name,""));
               mButton07.setText(lamp.getString(btn07name,""));
               mButton08.setText(lamp.getString(btn08name,""));
               mButton09.setText(lamp.getString(btn09name,""));
               mButton10.setText(lamp.getString(btn10name,""));
               mButton11.setText(lamp.getString(btn11name,""));
               mButton12.setText(lamp.getString(btn12name,""));

               setting = getSharedPreferences(custom1data,0);

               cus1name=(TextView)findViewById(R.id.cus1name);
               cus1name.setText(setting.getString(custom1name,""));
//				mButton54.setText(setting.getString(btn54name, ""));
//				mButton55.setText(setting.getString(btn55name, ""));
//				mButton56.setText(setting.getString(btn56name, ""));
//				mButton57.setText(setting.getString(btn57name, ""));
//				mButton58.setText(setting.getString(btn58name, ""));
//				mButton59.setText(setting.getString(btn59name, ""));
//				mButton60.setText(setting.getString(btn60name, ""));
//				mButton61.setText(setting.getString(btn61name, ""));
//				mButton62.setText(setting.getString(btn62name, ""));
               mButton63.setText(setting.getString(btn63name, ""));
               mButton64.setText(setting.getString(btn64name, ""));
               mButton65.setText(setting.getString(btn65name, ""));
               mButton66.setText(setting.getString(btn66name, ""));
               mButton67.setText(setting.getString(btn67name, ""));
               mButton68.setText(setting.getString(btn68name, ""));
//				mButton99.setText(setting.getString(btn99name, ""));
//				mButton100.setText(setting.getString(btn100name, ""));
//				mButton101.setText(setting.getString(btn101name, ""));
//				mButton102.setText(setting.getString(btn102name, ""));
//				mButton103.setText(setting.getString(btn103name, ""));
//				mButton104.setText(setting.getString(btn104name, ""));

               settingb = getSharedPreferences(custom2data,0);

               cus2name=(TextView)findViewById(R.id.cus2name);
               cus2name.setText(settingb.getString(custom2name,""));
//				 mButton69.setText(settingb.getString(btn69name, ""));
//				 mButton70.setText(settingb.getString(btn70name, ""));
//				 mButton71.setText(settingb.getString(btn71name, ""));
//				 mButton72.setText(settingb.getString(btn72name, ""));
//				 mButton73.setText(settingb.getString(btn73name, ""));
//				 mButton74.setText(settingb.getString(btn74name, ""));
//				 mButton75.setText(settingb.getString(btn75name, ""));
//				 mButton76.setText(settingb.getString(btn76name, ""));
//				 mButton77.setText(settingb.getString(btn77name, ""));
               mButton78.setText(settingb.getString(btn78name, ""));
               mButton79.setText(settingb.getString(btn79name, ""));
               mButton80.setText(settingb.getString(btn80name, ""));
               mButton81.setText(settingb.getString(btn81name, ""));
               mButton82.setText(settingb.getString(btn82name, ""));
               mButton83.setText(settingb.getString(btn83name, ""));
//				 mButton105.setText(settingb.getString(btn105name, ""));
//				 mButton106.setText(settingb.getString(btn106name, ""));
//				 mButton107.setText(settingb.getString(btn107name, ""));
//				 mButton108.setText(settingb.getString(btn108name, ""));
//				 mButton109.setText(settingb.getString(btn109name, ""));
//				 mButton110.setText(settingb.getString(btn110name, ""));

               setting2 = getSharedPreferences(custom3data,0);

               cus3name=(TextView)findViewById(R.id.cus3name);
               cus3name.setText(setting2.getString(custom3name,""));
               // mButton84.setText(setting2.getString(btn84name, ""));
               //mButton85.setText(setting2.getString(btn85name, ""));
               //mButton86.setText(setting2.getString(btn86name, ""));
               //mButton87.setText(setting2.getString(btn87name, ""));
               //mButton88.setText(setting2.getString(btn88name, ""));
               //mButton89.setText(setting2.getString(btn89name, ""));
               //mButton90.setText(setting2.getString(btn90name, ""));
               //mButton91.setText(setting2.getString(btn91name, ""));
               //mButton92.setText(setting2.getString(btn92name, ""));
               mButton93.setText(setting2.getString(btn93name, ""));
               mButton94.setText(setting2.getString(btn94name, ""));
               mButton95.setText(setting2.getString(btn95name, ""));
               mButton96.setText(setting2.getString(btn96name, ""));
               mButton97.setText(setting2.getString(btn97name, ""));
               mButton98.setText(setting2.getString(btn98name, ""));
               //mButton111.setText(setting2.getString(btn111name, ""));
               //mButton112.setText(setting2.getString(btn112name, ""));
               //mButton113.setText(setting2.getString(btn113name, ""));
               //mButton114.setText(setting2.getString(btn114name, ""));
               //mButton115.setText(setting2.getString(btn115name, ""));
               //mButton116.setText(setting2.getString(btn116name, ""));

               airseting = getSharedPreferences(airdata,0);
               mButton17.setText(airseting.getString(btn17name,""));
               mButton18.setText(airseting.getString(btn18name,""));
               mButton19.setText(airseting.getString(btn19name,""));
               mButton20.setText(airseting.getString(btn20name,""));
               mButton21.setText(airseting.getString(btn21name,""));
               mButton22.setText(airseting.getString(btn22name,""));


               lamp = getSharedPreferences(lampdata,0);
               imagebtn1 = (ImageView) findViewById(R.id.imagebtn1);
               imagebtn2 = (ImageView) findViewById(R.id.imagebtn2);
               imagebtn3 = (ImageView) findViewById(R.id.imagebtn3);
               imagebtn4 = (ImageView) findViewById(R.id.imagebtn4);
               imagebtn5 = (ImageView) findViewById(R.id.imagebtn5);
               imagebtn6 = (ImageView) findViewById(R.id.imagebtn6);
               imagebtn7 = (ImageView) findViewById(R.id.imagebtn7);
               imagebtn8 = (ImageView) findViewById(R.id.imagebtn8);
               imagebtn9 = (ImageView) findViewById(R.id.imagebtn9);
               imagebtn10 = (ImageView) findViewById(R.id.imagebtn10);
               imagebtn11 = (ImageView) findViewById(R.id.imagebtn11);
               imagebtn12 = (ImageView) findViewById(R.id.imagebtn12);



               lamp = getSharedPreferences(lampdata,0);

               imagebtn1 = (ImageView) findViewById(R.id.imagebtn1);
               imagebtn2 = (ImageView) findViewById(R.id.imagebtn2);
               imagebtn3 = (ImageView) findViewById(R.id.imagebtn3);
               imagebtn4 = (ImageView) findViewById(R.id.imagebtn4);
               imagebtn5 = (ImageView) findViewById(R.id.imagebtn5);
               imagebtn6 = (ImageView) findViewById(R.id.imagebtn6);
               imagebtn7 = (ImageView) findViewById(R.id.imagebtn7);
               imagebtn8 = (ImageView) findViewById(R.id.imagebtn8);
               imagebtn9 = (ImageView) findViewById(R.id.imagebtn9);
               imagebtn10 = (ImageView) findViewById(R.id.imagebtn10);
               imagebtn11 = (ImageView) findViewById(R.id.imagebtn11);
               imagebtn12 = (ImageView) findViewById(R.id.imagebtn12);



               if(lamp.getString(btn1pic,"")==null)
               {
                  imagebtn1.setImageDrawable(null);
               }else{
                  imagebtn1.setImageURI(Uri.parse(lamp.getString(btn1pic,"")));
                  Log.e("uri1", lamp.getString(btn1pic,""));
               }

               if(lamp.getString(btn2pic,"")==null)
               {
                  imagebtn2.setImageDrawable(null);
               }else{
                  imagebtn2.setImageURI(Uri.parse(lamp.getString(btn2pic,"")));
               }

               if(lamp.getString(btn3pic,"")==null)
               {
                  imagebtn3.setImageDrawable(null);
               }else{
                  imagebtn3.setImageURI(Uri.parse(lamp.getString(btn3pic,"")));
               }

               if(lamp.getString(btn4pic,"")==null)
               {
                  imagebtn4.setImageDrawable(null);
               }else{
                  imagebtn4.setImageURI(Uri.parse(lamp.getString(btn4pic,"")));
               }

               if(lamp.getString(btn5pic,"")==null)
               {
                  imagebtn5.setImageDrawable(null);
               }else{
                  imagebtn5.setImageURI(Uri.parse(lamp.getString(btn5pic,"")));
               }

               if(lamp.getString(btn6pic,"")==null)
               {
                  imagebtn6.setImageDrawable(null);
               }else{
                  imagebtn6.setImageURI(Uri.parse(lamp.getString(btn6pic,"")));
               }

               if(lamp.getString(btn7pic,"")==null)
               {
                  imagebtn7.setImageDrawable(null);
               }else{
                  imagebtn7.setImageURI(Uri.parse(lamp.getString(btn7pic,"")));
               }

               if(lamp.getString(btn8pic,"")==null)
               {
                  imagebtn8.setImageDrawable(null);
               }else{
                  imagebtn8.setImageURI(Uri.parse(lamp.getString(btn8pic,"")));
               }

               if(lamp.getString(btn9pic,"")==null)
               {
                  imagebtn9.setImageDrawable(null);
               }else{
                  imagebtn9.setImageURI(Uri.parse(lamp.getString(btn9pic,"")));
               }

               if(lamp.getString(btn10pic,"")==null)
               {
                  imagebtn10.setImageDrawable(null);
               }else{
                  imagebtn10.setImageURI(Uri.parse(lamp.getString(btn10pic,"")));
               }

               if(lamp.getString(btn11pic,"")==null)
               {
                  imagebtn11.setImageDrawable(null);
               }else{
                  imagebtn11.setImageURI(Uri.parse(lamp.getString(btn11pic,"")));
               }

               if(lamp.getString(btn12pic,"")==null)
               {
                  imagebtn12.setImageDrawable(null);
               }else{
                  imagebtn12.setImageURI(Uri.parse(lamp.getString(btn12pic,"")));
               }

               readData(10);

               break;
         }
         return true;
      }

   }
    public class ButtonListener implements OnClickListener{
		
		 
    	@SuppressWarnings("deprecation")
		public void onClick(View v) {

            //YILINEDIT-voiceRecg
            if(isVoiceRecMode){
                AlertDialog alertDialog;
                switch(v.getId()){
                    case R.id.btn01:
                        lamp = getSharedPreferences(lampdata,0);
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn01name,"").isEmpty()?"此燈":lamp.getString(btn01name,""),"a",true);
                        alertDialog.show();
                        break;
                    case R.id.btn02:
                        lamp = getSharedPreferences(lampdata,0);
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn02name,"").isEmpty()?"此燈":lamp.getString(btn02name,""),"b",true);
                        alertDialog.show();
                        break;
                    case R.id.btn03:
                        lamp = getSharedPreferences(lampdata,0);
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn03name,"").isEmpty()?"此燈":lamp.getString(btn03name,""),"c",true);
                        alertDialog.show();
                        break;
                    case R.id.btn04:
                        lamp = getSharedPreferences(lampdata,0);
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn04name,"").isEmpty()?"此燈":lamp.getString(btn04name,""),"d",true);
                        alertDialog.show();
                        break;
                    case R.id.btn05:
                        lamp = getSharedPreferences(lampdata,0);
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn05name,"").isEmpty()?"此燈":lamp.getString(btn05name,""),"e",true);
                        alertDialog.show();
                        break;
                    case R.id.btn06:
                        lamp = getSharedPreferences(lampdata,0);
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn06name,"").isEmpty()?"此燈":lamp.getString(btn06name,""),"f",true);
                        alertDialog.show();
                        break;
                    case R.id.btn07:
                        lamp = getSharedPreferences(lampdata,0);
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn07name,"").isEmpty()?"此燈":lamp.getString(btn07name,""),"g",true);
                        alertDialog.show();
                        break;
                    case R.id.btn08:
                        lamp = getSharedPreferences(lampdata,0);
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn08name,"").isEmpty()?"此燈":lamp.getString(btn08name,""),"h",true);
                        alertDialog.show();
                        break;
                    case R.id.btn09:
                        lamp = getSharedPreferences(lampdata,0);
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn09name,"").isEmpty()?"此燈":lamp.getString(btn09name,""),"i",true);
                        alertDialog.show();
                        break;
                    case R.id.btn10:
                        lamp = getSharedPreferences(lampdata,0);
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn10name,"").isEmpty()?"此燈":lamp.getString(btn10name,""),"j",true);
                        alertDialog.show();
                        break;
                    case R.id.btn11:
                        lamp = getSharedPreferences(lampdata,0);
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn11name,"").isEmpty()?"此燈":lamp.getString(btn11name,""),"k",true);
                        alertDialog.show();
                        break;
                    case R.id.btn12:
                        lamp = getSharedPreferences(lampdata,0);
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn12name,"").isEmpty()?"此燈":lamp.getString(btn12name,""),"l",true);
                        alertDialog.show();
                        break;
                    case R.id.home1:
                        Toast.makeText(BluetoothChat.this, "請先解除語音學習模式", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.lampseting:
                        Toast.makeText(BluetoothChat.this, "請先解除語音學習模式", Toast.LENGTH_LONG).show();
                        break;
                }
            }else
            switch(v.getId()){
            
            case R.id.save:
				saveData(14);
				break;
				
			case R.id.open:
				init();
                showDialog(DIALOG_MENU);
				break;
            
            
            case R.id.s1 :
    			String delay1 = ("'");
    			marcosetbtn.append(delay1);
    		break;
    	
    		case R.id.s2 :
    			String delay2 = ("(");
    			marcosetbtn.append(delay2);
    			break;
    			
    		case R.id.s3 :
    			String delay3 = (")");
    			marcosetbtn.append(delay3);
    			break;
    			
    		case R.id.s4 :
    			String delay4 = ("*");
    			marcosetbtn.append(delay4);
    			break;
    			
    		case R.id.s5 :
    			String delay5 = ("+");
    			marcosetbtn.append(delay5);
    			break;
    			
    		case R.id.s6 :
    			String delay6 = ("'");
    			marco1setbtn.append(delay6);
    		break;
    	
    		case R.id.s7 :
    			String delay7 = ("(");
    			marco1setbtn.append(delay7);
    			break;
    			
    		case R.id.s8 :
    			String delay8 = (")");
    			marco1setbtn.append(delay8);
    			break;
    			
    		case R.id.s9 :
    			String delay9 = ("*");
    			marco1setbtn.append(delay9);
    			break;
    			
    		case R.id.s10 :
    			String delay10 = ("+");
    			marco1setbtn.append(delay10);
    			break;
    			
    		case R.id.s11 :
    			String delay11 = ("'");
    			marco2setbtn.append(delay11);
    		break;
    	
    		case R.id.s12 :
    			String delay12 = ("(");
    			marco2setbtn.append(delay12);
    			break;
    			
    		case R.id.s13 :
    			String delay13 = (")");
    			marco2setbtn.append(delay13);
    			break;
    			
    		case R.id.s14 :
    			String delay14 = ("*");
    			marco2setbtn.append(delay14);
    			break;
    			
    		case R.id.s15 :
    			String delay15 = ("+");
    			marco2setbtn.append(delay15);
    			break;
    			
    		case R.id.s16 :
    			String delay16 = ("'");
    			marco3setbtn.append(delay16);
    		break;
    	
    		case R.id.s17 :
    			String delay17 = ("(");
    			marco3setbtn.append(delay17);
    			break;
    			
    		case R.id.s18 :
    			String delay18 = (")");
    			marco3setbtn.append(delay18);
    			break;
    			
    		case R.id.s19 :
    			String delay19 = ("*");
    			marco3setbtn.append(delay19);
    			break;
    			
    		case R.id.s20 :
    			String delay20 = ("+");
    			marco3setbtn.append(delay20);
    			break;
    			
    		case R.id.s21 :
    			String delay21 = ("'");
    			marco4setbtn.append(delay21);
    		break;
    	
    		case R.id.s22 :
    			String delay22 = ("(");
    			marco4setbtn.append(delay22);
    			break;
    			
    		case R.id.s23 :
    			String delay23 = (")");
    			marco4setbtn.append(delay23);
    			break;
    			
    		case R.id.s24 :
    			String delay24 = ("*");
    			marco4setbtn.append(delay24);
    			break;
    			
    		case R.id.s25 :
    			String delay25 = ("+");
    			marco4setbtn.append(delay25);
    			break;
    			
    		case R.id.s26 :
    			String delay26 = ("'");
    			marco5setbtn.append(delay26);
    			break;
    			
    		case R.id.s27 :
    			String delay27 = ("(");
    			marco5setbtn.append(delay27);
    			break;
    			
    		case R.id.s28 :
    			String delay28 = (")");
    			marco5setbtn.append(delay28);
    			break;
    			
    		case R.id.s29 :
    			String delay29 = ("*");
    			marco5setbtn.append(delay29);
    			break;
    			
    		case R.id.s30 :
    			String delay30 = ("+");
    			marco5setbtn.append(delay30);
    			break;
    			
    		case R.id.home0 :
    			//showDialog(DIALOG_MESSAGE);
    			showDialog(DIALOG_MENU);
    			break;
    			
    		case R.id.home1 :
    			//showDialog(DIALOG_MESSAGE);
    			showDialog(DIALOG_MENU);
    			break;
    			
    		case R.id.home2 :
    			//showDialog(DIALOG_MESSAGE);
    			showDialog(DIALOG_MENU);
    			break;
    			
    		case R.id.home3 :
    			//showDialog(DIALOG_MESSAGE);
    			showDialog(DIALOG_MENU);
    			break;
    			
    		case R.id.home4 :
    			//showDialog(DIALOG_MESSAGE);
    			showDialog(DIALOG_MENU);
    			break;
    			
    		case R.id.home5 :
    			//showDialog(DIALOG_MESSAGE);
    			showDialog(DIALOG_MENU);
    			break;
    			
    		case R.id.home6 :
    			//showDialog(DIALOG_MESSAGE);
    			showDialog(DIALOG_MENU);
    			break;
    			
    		case R.id.home7 :
    			//showDialog(DIALOG_MESSAGE);
    			showDialog(DIALOG_MENU);
    			break;
    			
    		case R.id.home8 :
    			//showDialog(DIALOG_MESSAGE);
    			showDialog(DIALOG_MENU);
    			break;
    			
    		case R.id.home9 :
    			//showDialog(DIALOG_MESSAGE);
    			showDialog(DIALOG_MENU);
    			break;
    		        
    		case R.id.btn01:
    			String view = ("a3");
                sendmessage(view);
                sendMessageAfter1s("W+j?j");
    			break;
    			
    		case R.id.btn02:
    			String view2 = ("b3");
                sendmessage(view2);
                sendMessageAfter1s("W+k?k");
    			break;	
        		
    		case R.id.btn03:
    			String view3 = ("c3");
                sendmessage(view3);
                sendMessageAfter1s("W+l?l");
    			break;	
    			
    		case R.id.btn04:
    			String view4 = ("d3");
                sendmessage(view4);
                sendMessageAfter1s("W+m?m");
    			break;
    			
    		case R.id.btn05:
    			String view5 = ("e3");
                sendmessage(view5);
                sendMessageAfter1s("W+n?n");
    			break;
    			
    		case R.id.btn06:
    			String view6 = ("f3");
                sendmessage(view6);
                sendMessageAfter1s("W+o?o");
    			break;
    			
    		case R.id.btn07:
    			String view7 = ("g3");
                sendmessage(view7);
                sendMessageAfter1s("W+p?p");
    			break;
    			
    		case R.id.btn08:
    			String view8 = ("h3");
                sendmessage(view8);
                sendMessageAfter1s("W+q?q");
    			break;	
    			
    		case R.id.btn09:
    			String view9 = ("i3");
                sendmessage(view9);
                sendMessageAfter1s("W+r?r");
                break;
    			
    		case R.id.btn10:
    			String view10 = ("j3");
                sendmessage(view10);
                sendMessageAfter1s("W+s?s");
                break;
    			
    		case R.id.btn11:
    			String view11 = ("k3");
                sendmessage(view11);
                sendMessageAfter1s("W+t?t");
                break;
    			
    		case R.id.btn12:
    			String view12 = ("l3");
                sendmessage(view12);
                sendMessageAfter1s("W+u?u");
                break;

    			
    		case R.id.btn13:
    			String view13 = ("Aa");
                String message13 = view13.toString();
                sendmessage(message13);
    			break;
    			
    		case R.id.btn14:
    			String view14 = ("Ab");
                String message14 = view14.toString();
                sendmessage(message14);
    			break;
    			
    		case R.id.btn15:
    			String view15 = ("Ac");
                String message15 = view15.toString();
                sendmessage(message15);
    			break;
    			
    		case R.id.btn16:
    			String view16 = ("Ad");
                String message16 = view16.toString();
                sendmessage(message16);
    			break;
    			
    		case R.id.btn17:
    			String view17 = ("Ae");
                String message17 = view17.toString();
                sendmessage(message17);
    			break;
    			
    		case R.id.btn18:
    			String view18 = ("Af");
                String message18 = view18.toString();
                sendmessage(message18);
    			break;
    			
    		case R.id.btn19:
    			String view19 = ("Ag");
                String message19 = view19.toString();
                sendmessage(message19);
    			break;
    			
    		case R.id.btn20:
    			String view20 = ("Ah");
                String message20 = view20.toString();
                sendmessage(message20);
    			break;
    			
    		case R.id.btn21:
    			String view21 = ("Ai");
                String message21 = view21.toString();
                sendmessage(message21);
    			break;
    			
    		case R.id.btn22:
    			String view22 = ("Aj");
                String message22 = view22.toString();
                sendmessage(message22);
    			break;
    			
    		case R.id.btn23:
    			String view23 = ("Ak");
                String message23 = view23.toString();
                sendmessage(message23);
    			break;
    			
    		case R.id.btn24:
    			String view24 = ("Al");
                String message24 = view24.toString();
                sendmessage(message24);
    			break;
    			
//    		case R.id.btn25:
//    			String view25 = ("Am");
//                String message25 = view25.toString();
//                sendMessage(message25);
//    			break;
//    			
//    		case R.id.btn26:
//    			String view26 = ("An");
//                String message26 = view26.toString();
//                sendMessage(message26);
//    			break;
//    			
//    		case R.id.btn27:
//    			String view27 = ("Ao");
//                String message27 = view27.toString();
//                sendMessage(message27);
//    			break;
//    			
//    		case R.id.btn28:
//    			String view28 = ("Ap");
//                String message28 = view28.toString();
//                sendMessage(message28);
//    			break;
//    			
//    		case R.id.btn29:
//    			String view29 = ("Aq");
//                String message29 = view29.toString();
//                sendMessage(message29);
//    			break;
//    			
//    		case R.id.btn30:
//    			String view30 = ("Ar");
//                String message30 = view30.toString();
//                sendMessage(message30);
//    			break;
//    			
//    		case R.id.btn31:
//    			String view31 = ("As");
//                String message31 = view31.toString();
//                sendMessage(message31);
//    			break;
    			
    		//case R.id.btn32:
    			//String view32 = ("At");
                //String message32 = view32.toString();
                //sendMessage(message32);
    			//break;
    			
    		case R.id.btn33:
    			String view33 = ("Ta");
                String message33 = view33.toString();
                sendmessage(message33);
    			break;
    			
//    		case R.id.btn34:
//    			String view34 = ("Tb");
//                String message34 = view34.toString();
//                sendMessage(message34);
//    			break;
    			
    		case R.id.btn35:
    			String view35 = ("Tc");
                String message35 = view35.toString();
                sendmessage(message35);
    			break;
    			
//    		case R.id.btn36:
//    			String view36 = ("Td");
//                String message36 = view36.toString();
//                sendMessage(message36);
//    			break;
//    			
//    		case R.id.btn37:
//    			String view37 = ("Te");
//                String message37 = view37.toString();
//                sendMessage(message37);
//    			break;
//    			
//    		case R.id.btn38:
//    			String view38 = ("Tf");
//                String message38 = view38.toString();
//                sendMessage(message38);
//    			break;
//    			
//    		case R.id.btn40:
//    			String view40 = ("Th");
//                String message40 = view40.toString();
//                sendMessage(message40);
//    			break;
//    			
//    			//tv home
//    		case R.id.button25:
//    			String view39 = ("Tg");
//    			String message39 = view39.toString();
//                sendMessage(message39);
//    			break;
//    			//tv 靜音
//    		case R.id.button26:
//    			String view41 = ("Ti");
//                String message41 = view41.toString();
//                sendMessage(message41);
//    			break;
    			
    		case R.id.btn42:
    			String view42 = ("Tj");
                String message42 = view42.toString();
                sendmessage(message42);
    			break;
    			
    		case R.id.btn43:
    			String view43 = ("Tk");
                String message43 = view43.toString();
                sendmessage(message43);
    			break;
    			
    		case R.id.btn44:
    			String view44 = ("Tl");
                String message44 = view44.toString();
                sendmessage(message44);
    			break;
    			
    		case R.id.btn45:
    			String view45 = ("Tm");
                String message45 = view45.toString();
                sendmessage(message45);
    			break;
    			
    		case R.id.btn46:
    			String view46 = ("Tn");
                String message46 = view46.toString();
                sendmessage(message46);
    			break;
    			
    		case R.id.btn47:
    			String view47 = ("To");
                String message47 = view47.toString();
                sendmessage(message47);
    			break;
    			
    		case R.id.btn48:
    			String view48 = ("Tp");
                String message48 = view48.toString();
                sendmessage(message48);
    			break;
    			
    		case R.id.btn49:
    			String view49 = ("Tq");
                String message49 = view49.toString();
                sendmessage(message49);
    			break;
    			
    		case R.id.btn50:
    			String view50 = ("Tr");
                String message50 = view50.toString();
                sendmessage(message50);
    			break;
    			
    		case R.id.btn51:
    			String view51 = ("Ts");
                String message51 = view51.toString();
                sendmessage(message51);
    			break;
    			
    		case R.id.btn52:
    			String view52 = ("Tt");
                String message52 = view52.toString();
                sendmessage(message52);
    				break;
    			
    		case R.id.btn53:
    			String view53 = ("Tr");
                String message53 = view53.toString();
                sendmessage(message53);
    				break;
    		
//    		case R.id.btn54:
//    			String view54 = ("Xa");
//                String message54 = view54.toString();
//                sendMessage(message54);
//    				break;
//    			
//    		case R.id.btn55:
//    			String view55 = ("Xb");
//                String message55 = view55.toString();
//                sendMessage(message55);
//    				break;	
//        		
//    		case R.id.btn56:
//    			String view56 = ("Xc");
//                String message56 = view56.toString();
//                sendMessage(message56);
//    				break;	
//    			
//    		case R.id.btn57:
//    			String view57 = ("Xd");
//                String message57 = view57.toString();
//                sendMessage(message57);
//    				break;
//    			
//    		case R.id.btn58:
//    			String view58 = ("Xe");
//                String message58 = view58.toString();
//                sendMessage(message58);
//    				break;
//    			
//    		case R.id.btn59:
//    			String view59 = ("Xf");
//                String message59 = view59.toString();
//                sendMessage(message59);
//    				break;
//    			
//    		case R.id.btn60:
//    			String view60 = ("Xg");
//                String message60 = view60.toString();
//                sendMessage(message60);
//    				break;
//    			
//    		case R.id.btn61:
//    			String view61 = ("Xh");
//                String message61 = view61.toString();
//                sendMessage(message61);
//    				break;	
//    			
//    		case R.id.btn62:
//    			String view62 = ("Xi");
//                String message62 = view62.toString();
//                sendMessage(message62);
//    				break;
    			
    		case R.id.btn63:
    			String view63 = ("Xj");
                String message63 = view63.toString();
                sendmessage(message63);
    				break;
    			
    		case R.id.btn64:
    			String view64 = ("Xk");
                String message64 = view64.toString();
                sendmessage(message64);
    				break;
    			
    		case R.id.btn65:
    			String view65 = ("Xl");
                String message65 = view65.toString();
                sendmessage(message65);
    				break;
    			
    		case R.id.btn66:
    			String view66 = ("Xm");
                String message66 = view66.toString();
                sendmessage(message66);
    				break;
    			
    		case R.id.btn67:
    			String view67 = ("Xn");
                String message67 = view67.toString();
                sendmessage(message67);
    				break;
    			
    		case R.id.btn68:
    			String view68 = ("Xo");
                String message68 = view68.toString();
                sendmessage(message68);
    				break;
    			
//    		case R.id.btn69:
//    			String view69 = ("Ya");
//                String message69 = view69.toString();
//                sendMessage(message69);
//    				break;
//    			
//    		case R.id.btn70:
//    			String view70 = ("Yb");
//                String message70 = view70.toString();
//                sendMessage(message70);
//    				break;
//    			
//    		case R.id.btn71:
//    			String view71 = ("Yc");
//                String message71 = view71.toString();
//                sendMessage(message71);
//    				break;
//    			
//    		case R.id.btn72:
//    			String view72 = ("Yd");
//                String message72 = view72.toString();
//                sendMessage(message72);
//    				break;
//    			
//    		case R.id.btn73:
//    			String view73 = ("Ye");
//                String message73 = view73.toString();
//                sendMessage(message73);
//    				break;
//    			
//    		case R.id.btn74:
//    			String view74 = ("Yf");
//                String message74 = view74.toString();
//                sendMessage(message74);
//    				break;
//    			
//    		case R.id.btn75:
//    			String view75 = ("Yg");
//                String message75 = view75.toString();
//                sendMessage(message75);
//    				break;
//    			
//    		case R.id.btn76:
//    			String view76 = ("Yh");
//                String message76 = view76.toString();
//                sendMessage(message76);
//    				break;
//    			
//    		case R.id.btn77:
//    			String view77 = ("Yi");
//                String message77 = view77.toString();
//                sendMessage(message77);
//    				break;
    			
    		case R.id.btn78:
    			String view78 = ("Yj");
                String message78 = view78.toString();
                sendmessage(message78);
    				break;
    			
    		case R.id.btn79:
    			String view79 = ("Yk");
                String message79 = view79.toString();
                sendmessage(message79);
    				break;
    			
    		case R.id.btn80:
    			String view80 = ("Yl");
                String message80 = view80.toString();
                sendmessage(message80);
    				break;
    			
    		case R.id.btn81:
    			String view81 = ("Ym");
                String message81 = view81.toString();
                sendmessage(message81);
    				break;
    			
    		case R.id.btn82:
    			String view82 = ("Yn");
                String message82 = view82.toString();
                sendmessage(message82);
    				break;
    			
    		case R.id.btn83:
    			String view83 = ("Yo");
                String message83 = view83.toString();
                sendmessage(message83);
    				break;
    				
//    		case R.id.btn84:
//    			String view84 = ("Za");
//                String message84 = view84.toString();
//                sendMessage(message84);
//    				break;
//    				
//    		case R.id.btn85:
//    			String view85 = ("Zb");
//                String message85 = view85.toString();
//                sendMessage(message85);
//    				break;
//    				
//    		case R.id.btn86:
//    			String view86 = ("Zc");
//                String message86 = view86.toString();
//                sendMessage(message86);
//    				break;
//    				
//    		case R.id.btn87:
//    			String view87 = ("Zd");
//                String message87 = view87.toString();
//                sendMessage(message87);
//    				break;
//    				
//    		case R.id.btn88:
//    			String view88 = ("Ze");
//                String message88 = view88.toString();
//                sendMessage(message88);
//    				break;
//    				
//    		case R.id.btn89:
//    			String view89 = ("Zf");
//                String message89 = view89.toString();
//                sendMessage(message89);
//    				break;
//    				
//    		case R.id.btn90:
//    			String view90 = ("Zg");
//                String message90 = view90.toString();
//                sendMessage(message90);
//    				break;
//    				
//    		case R.id.btn91:
//    			String view91 = ("Zh");
//                String message91 = view91.toString();
//                sendMessage(message91);
//    				break;
//    				
//    		case R.id.btn92:
//    			String view92 = ("Zi");
//                String message92 = view92.toString();
//                sendMessage(message92);
//    				break;
    				
    		case R.id.btn93:
    			String view93 = ("Zj");
                String message93 = view93.toString();
                sendmessage(message93);
    				break;
    				
    		case R.id.btn94:
    			String view94 = ("Zk");
                String message94 = view94.toString();
                sendmessage(message94);
    				break;
    				
    		case R.id.btn95:
    			String view95 = ("Zl");
                String message95 = view95.toString();
                sendmessage(message95);
    				break;
    				
    		case R.id.btn96:
    			String view96 = ("Zm");
                String message96 = view96.toString();
                sendmessage(message96);
    				break;
    				
    		case R.id.btn97:
    			String view97 = ("Zn");
                String message97 = view97.toString();
                sendmessage(message97);
    				break;
    				
    		case R.id.btn98:
    			String view98 = ("Zo");
                String message98 = view98.toString();
                sendmessage(message98);
    				break;
    				
//    		case R.id.btn99:
//    			String view99 = ("Xp");
//                String message99 = view99.toString();
//                sendMessage(message99);
//    				break;
//    				
//    		case R.id.btn100:
//    			String view100 = ("Xq");
//                String message100 = view100.toString();
//                sendMessage(message100);
//    				break;
//    				
//    		case R.id.btn101:
//    			String view101 = ("Xr");
//                String message101 = view101.toString();
//                sendMessage(message101);
//    				break;
//    				
//    		case R.id.btn102:
//    			String view102 = ("Xs");
//                String message102 = view102.toString();
//                sendMessage(message102);
//    				break;
//    				
//    		case R.id.btn103:
//    			String view103 = ("Xt");
//                String message103 = view103.toString();
//                sendMessage(message103);
//    				break;
//    				
//    		case R.id.btn104:
//    			String view104 = ("Xu");
//                String message104 = view104.toString();
//                sendMessage(message104);
//    				break;
//    				
//    		case R.id.btn105:
//    			String view105 = ("Yp");
//                String message105 = view105.toString();
//                sendMessage(message105);
//    				break;
//    				
//    		case R.id.btn106:
//    			String view106 = ("Yq");
//                String message106 = view106.toString();
//                sendMessage(message106);
//    				break;
//    				
//    		case R.id.btn107:
//    			String view107 = ("Yr");
//                String message107 = view107.toString();
//                sendMessage(message107);
//    				break;
//    				
//    		case R.id.btn108:
//    			String view108 = ("Ys");
//                String message108 = view108.toString();
//                sendMessage(message108);
//    				break;
//    				
//    		case R.id.btn109:
//    			String view109 = ("Yt");
//                String message109 = view109.toString();
//                sendMessage(message109);
//    				break;
//    				
//    		case R.id.btn110:
//    			String view110 = ("Yu");
//                String message110 = view110.toString();
//                sendMessage(message110);
//    				break;
//    				
//    		case R.id.btn111:
//    			String view111 = ("Zp");
//                String message111 = view111.toString();
//                sendMessage(message111);
//    				break;
//    				
//    		case R.id.btn112:
//    			String view112 = ("Zq");
//                String message112 = view112.toString();
//                sendMessage(message112);
//    				break;
//    				
//    		case R.id.btn113:
//    			String view113 = ("Zr");
//                String message113 = view113.toString();
//                sendMessage(message113);
//    				break;
//    				
//    		case R.id.btn114:
//    			String view114 = ("Zs");
//                String message114 = view114.toString();
//                sendMessage(message114);
//    				break;
//    				
//    		case R.id.btn115:
//    			String view115 = ("Zt");
//                String message115 = view115.toString();
//                sendMessage(message115);
//    				break;
//    				
//    		case R.id.btn116:
//    			String view116 = ("Zu");
//                String message116 = view116.toString();
//                sendMessage(message116);
//    				break;
    				
    			//TV 音量跟頻道	
    		case R.id.btn153:
    			String view153 = ("Tv");
                sendmessage(view153);
    				break;
    				
    		case R.id.btn154:
    			String view154 = ("Tw");
                sendmessage(view154);
    				break;
    				
    		case R.id.btn155:
    			String view155 = ("Tx");
                sendmessage(view155);
    				break;
    				
    		case R.id.btn156:
    			String view156 = ("Ty");
                sendmessage(view156);
    				break;
    				
    		case R.id.lampseting:
    			setContentView(R.layout.lampset);
    			
    			lampeditText1 = (EditText) findViewById(R.id.lampeditText1);
    			lampeditText2 = (EditText) findViewById(R.id.lampeditText2);
    			lampeditText3 = (EditText) findViewById(R.id.lampeditText3);
    			lampeditText4 = (EditText) findViewById(R.id.lampeditText4);
    			lampeditText5 = (EditText) findViewById(R.id.lampeditText5);
    			lampeditText6 = (EditText) findViewById(R.id.lampeditText6);
    			lampeditText7 = (EditText) findViewById(R.id.lampeditText7);
    			lampeditText8 = (EditText) findViewById(R.id.lampeditText8);
    			lampeditText9 = (EditText) findViewById(R.id.lampeditText9);
    			lampeditText10 = (EditText) findViewById(R.id.lampeditText10);
    			lampeditText11 = (EditText) findViewById(R.id.lampeditText11);
    			lampeditText12 = (EditText) findViewById(R.id.lampeditText12);
    			
    			lampeditpic1 = (EditText) findViewById(R.id.lampeditpic1);
    			lampeditpic2 = (EditText) findViewById(R.id.lampeditpic2);
    			lampeditpic3 = (EditText) findViewById(R.id.lampeditpic3);
    			lampeditpic4 = (EditText) findViewById(R.id.lampeditpic4);
    			lampeditpic5 = (EditText) findViewById(R.id.lampeditpic5);
    			lampeditpic6 = (EditText) findViewById(R.id.lampeditpic6);
    			lampeditpic7 = (EditText) findViewById(R.id.lampeditpic7);
    			lampeditpic8 = (EditText) findViewById(R.id.lampeditpic8);
    			lampeditpic9 = (EditText) findViewById(R.id.lampeditpic9);
    			lampeditpic10 = (EditText) findViewById(R.id.lampeditpic10);
    			lampeditpic11 = (EditText) findViewById(R.id.lampeditpic11);
    			lampeditpic12 = (EditText) findViewById(R.id.lampeditpic12);
    			
    			findViewById(R.id.lampok).setOnClickListener(new ButtonListener());
    			
    			findViewById(R.id.lampic1).setOnClickListener(new ButtonListener());
    			findViewById(R.id.lampic2).setOnClickListener(new ButtonListener());
    			findViewById(R.id.lampic3).setOnClickListener(new ButtonListener());
    			findViewById(R.id.lampic4).setOnClickListener(new ButtonListener());
    			findViewById(R.id.lampic5).setOnClickListener(new ButtonListener());
    			findViewById(R.id.lampic6).setOnClickListener(new ButtonListener());
    			findViewById(R.id.lampic7).setOnClickListener(new ButtonListener());
    			findViewById(R.id.lampic8).setOnClickListener(new ButtonListener());
    			findViewById(R.id.lampic9).setOnClickListener(new ButtonListener());
    			findViewById(R.id.lampic10).setOnClickListener(new ButtonListener());
    			findViewById(R.id.lampic11).setOnClickListener(new ButtonListener());
    			findViewById(R.id.lampic12).setOnClickListener(new ButtonListener());
    		
    			
    			readData(1);
    			
    				break;
    				
    		case R.id.curtainset :
    			setContentView(R.layout.curtainset);
    			
    			cur1edit = (EditText) findViewById(R.id.cur1edit);
    			cur2edit = (EditText) findViewById(R.id.cur2edit);
    			cur3edit = (EditText) findViewById(R.id.cur3edit);
    			cur4edit = (EditText) findViewById(R.id.cur4edit);
    			cur5edit = (EditText) findViewById(R.id.cur5edit);
    			cur6edit = (EditText) findViewById(R.id.cur6edit);
    			cur7edit = (EditText) findViewById(R.id.cur7edit);
    			cur8edit = (EditText) findViewById(R.id.cur8edit);
    			cur9edit = (EditText) findViewById(R.id.cur9edit);
    			cur10edit = (EditText) findViewById(R.id.cur10edit);
    			cur11edit = (EditText) findViewById(R.id.cur11edit);
    			cur12edit = (EditText) findViewById(R.id.cur12edit);
    			
    			curok = (Button) findViewById(R.id.curok);
    			curok.setOnClickListener(new ButtonListener());
    			
    			readData(11);
    			
    			break;
    				
    			    			
    				
    		case R.id.custom1set:
    			setContentView(R.layout.setting);
    			
    			custom1edit = (EditText) findViewById(R.id.custom1edit);
    			meditText1 = (EditText) findViewById(R.id.editText1);
    			meditText2 = (EditText) findViewById(R.id.editText2);
    			meditText3 = (EditText) findViewById(R.id.editText3);
    			meditText4 = (EditText) findViewById(R.id.editText4);
    			meditText5 = (EditText) findViewById(R.id.editText5);
    			meditText6 = (EditText) findViewById(R.id.editText6);
    			mButton63 = (Button) findViewById(R.id.btn63);
             	mButton64 = (Button) findViewById(R.id.btn64);
             	mButton65 = (Button) findViewById(R.id.btn65);
             	mButton66 = (Button) findViewById(R.id.btn66);
             	mButton67 = (Button) findViewById(R.id.btn67);
             	mButton68 = (Button) findViewById(R.id.btn68);
//    			meditText7 = (EditText) findViewById(R.id.editText7);
//    			meditText8 = (EditText) findViewById(R.id.editText8);
//    			meditText9 = (EditText) findViewById(R.id.editText9);
//       		meditText10 = (EditText) findViewById(R.id.editText10);
//    			meditText11 = (EditText) findViewById(R.id.editText11);
//      		meditText12 = (EditText) findViewById(R.id.editText12);
//       		meditText13 = (EditText) findViewById(R.id.editText13);
//      		meditText14 = (EditText) findViewById(R.id.editText14);
//      		meditText15 = (EditText) findViewById(R.id.editText15);
//     			meditText46 = (EditText) findViewById(R.id.editText46);
//      		meditText47 = (EditText) findViewById(R.id.editText47);
//     			meditText48 = (EditText) findViewById(R.id.editText48);
//      		meditText49 = (EditText) findViewById(R.id.editText49);
//      		meditText50 = (EditText) findViewById(R.id.editText50);
//      		meditText51 = (EditText) findViewById(R.id.editText51);
    			
      			set00 = (Button) findViewById(R.id.set00);
      			set00.setOnClickListener(new ButtonListener());
      			
      			readData(2);
    		    
      				break;
      				
    		case R.id.custom2set:
    			setContentView(R.layout.setting1);
    			
    			mButton78 = (Button) findViewById(R.id.btn78);
             	mButton79 = (Button) findViewById(R.id.btn79);
             	mButton80 = (Button) findViewById(R.id.btn80);
             	mButton81 = (Button) findViewById(R.id.btn81);
             	mButton82 = (Button) findViewById(R.id.btn82);
             	mButton83 = (Button) findViewById(R.id.btn83);
    			custom2edit = (EditText) findViewById(R.id.custom2edit);
    			meditText16 = (EditText) findViewById(R.id.editText16);
    			meditText17 = (EditText) findViewById(R.id.editText17);
    			meditText18 = (EditText) findViewById(R.id.editText18);
    			meditText19 = (EditText) findViewById(R.id.editText19);
    			meditText20 = (EditText) findViewById(R.id.editText20);
    			meditText21 = (EditText) findViewById(R.id.editText21);
//    			meditText22 = (EditText) findViewById(R.id.editText22);
//    			meditText23 = (EditText) findViewById(R.id.editText23);
//    			meditText24 = (EditText) findViewById(R.id.editText24);
//       		meditText25 = (EditText) findViewById(R.id.editText25);
//    			meditText26 = (EditText) findViewById(R.id.editText26);
//      		meditText27 = (EditText) findViewById(R.id.editText27);
//       		meditText28 = (EditText) findViewById(R.id.editText28);
//     			meditText29 = (EditText) findViewById(R.id.editText29);
//     			meditText30 = (EditText) findViewById(R.id.editText30);
//      		meditText52 = (EditText) findViewById(R.id.editText52);
//      		meditText53 = (EditText) findViewById(R.id.editText53);
//      		meditText54 = (EditText) findViewById(R.id.editText54);
//     			meditText55 = (EditText) findViewById(R.id.editText55);
//     			meditText56 = (EditText) findViewById(R.id.editText56);
//     			meditText57 = (EditText) findViewById(R.id.editText57);
    			
      			set01 = (Button) findViewById(R.id.set01);
      			set01.setOnClickListener(new ButtonListener());
      			
      			readData(3);
    		    
      				break;
      				
    		case R.id.custom3set:
    			setContentView(R.layout.setting2);
    			
    			mButton93 = (Button) findViewById(R.id.btn93);
             	mButton94 = (Button) findViewById(R.id.btn94);
             	mButton95 = (Button) findViewById(R.id.btn95);
             	mButton96 = (Button) findViewById(R.id.btn96);
             	mButton97 = (Button) findViewById(R.id.btn97);
             	mButton98 = (Button) findViewById(R.id.btn98);
    			custom3edit = (EditText) findViewById(R.id.custom3edit);
    			meditText31 = (EditText) findViewById(R.id.editText31);
    			meditText32 = (EditText) findViewById(R.id.editText32);
    			meditText33 = (EditText) findViewById(R.id.editText33);
    			meditText34 = (EditText) findViewById(R.id.editText34);
    			meditText35 = (EditText) findViewById(R.id.editText35);
    			meditText36 = (EditText) findViewById(R.id.editText36);
//    			meditText37 = (EditText) findViewById(R.id.editText37);
//    			meditText38 = (EditText) findViewById(R.id.editText38);
//    			meditText39 = (EditText) findViewById(R.id.editText39);
//       		meditText40 = (EditText) findViewById(R.id.editText40);
//    			meditText41 = (EditText) findViewById(R.id.editText41);
//      		meditText42 = (EditText) findViewById(R.id.editText42);
//       		meditText43 = (EditText) findViewById(R.id.editText43);
//      		meditText44 = (EditText) findViewById(R.id.editText44);
//     			meditText45 = (EditText) findViewById(R.id.editText45);
//     			meditText58 = (EditText) findViewById(R.id.editText58);
//     			meditText59 = (EditText) findViewById(R.id.editText59);
//     			meditText60 = (EditText) findViewById(R.id.editText60);
//     			meditText61 = (EditText) findViewById(R.id.editText61);
//     			meditText62 = (EditText) findViewById(R.id.editText62);
//     			meditText63 = (EditText) findViewById(R.id.editText63);
    			
      			set02 = (Button) findViewById(R.id.set02);
      			set02.setOnClickListener(new ButtonListener());
      			
      			readData(4);
    		    
      				break;
      				
    		case R.id.airset:
    			setContentView(R.layout.airseting);
    			
    			mButton13 = (Button) findViewById(R.id.btn13);
                mButton14 = (Button) findViewById(R.id.btn14);
                mButton15 = (Button) findViewById(R.id.btn15);
                mButton16 = (Button) findViewById(R.id.btn16);
                mButton17 = (Button) findViewById(R.id.btn17);
                mButton18 = (Button) findViewById(R.id.btn18);
                mButton19 = (Button) findViewById(R.id.btn19);
                mButton20 = (Button) findViewById(R.id.btn20);
                mButton21 = (Button) findViewById(R.id.btn21);
                mButton22 = (Button) findViewById(R.id.btn22);
                mButton23 = (Button) findViewById(R.id.btn23);
                mButton24 = (Button) findViewById(R.id.btn24);
    			airok = (Button) findViewById(R.id.airok);
    			
    			meditText52 = (EditText) findViewById(R.id.meditText52);
    			meditText53 = (EditText) findViewById(R.id.meditText53);
    			meditText54 = (EditText) findViewById(R.id.meditText54);
    			meditText55 = (EditText) findViewById(R.id.meditText55);
    			meditText56 = (EditText) findViewById(R.id.meditText56);
    			meditText57 = (EditText) findViewById(R.id.meditText57);
    			
    			airok.setOnClickListener(new ButtonListener());
    			
    			readData(12);
    			
    			break;
      				
      				
      				
      			//marco	
    		case R.id.marcoset1:
    			// TODO Auto-generated catch block
    			setContentView(R.layout.marcoset);

    			marcosetname = (EditText) findViewById(R.id.marcosetname);
    			marcosetbtn = (EditText) findViewById(R.id.marcosetbtn);
    			mButton01 = (Button) findViewById(R.id.btn01);
    			mButton02 = (Button) findViewById(R.id.btn02);
    			mButton03 = (Button) findViewById(R.id.btn03);
    			mButton04 = (Button) findViewById(R.id.btn04);
    			mButton05 = (Button) findViewById(R.id.btn05);
    			mButton06 = (Button) findViewById(R.id.btn06);
    			mButton07 = (Button) findViewById(R.id.btn07);
    			mButton08 = (Button) findViewById(R.id.btn08);
    			mButton09 = (Button) findViewById(R.id.btn09);
    			mButton10 = (Button) findViewById(R.id.btn10);
    			mButton11 = (Button) findViewById(R.id.btn11);
    			mButton12 = (Button) findViewById(R.id.btn12);
    			mButton13 = (Button) findViewById(R.id.btn13);
    			mButton14 = (Button) findViewById(R.id.btn14);
    			mButton15 = (Button) findViewById(R.id.btn15);
    			mButton16 = (Button) findViewById(R.id.btn16);
    			mButton17 = (Button) findViewById(R.id.btn17);
    			mButton18 = (Button) findViewById(R.id.btn18);
    			mButton19 = (Button) findViewById(R.id.btn19);
    			mButton20 = (Button) findViewById(R.id.btn20);
    			mButton21 = (Button) findViewById(R.id.btn21);
    			mButton22 = (Button) findViewById(R.id.btn22);
    			mButton23 = (Button) findViewById(R.id.btn23);
    			mButton24 = (Button) findViewById(R.id.btn24);
//    			mButton25 = (Button) findViewById(R.id.btn25);
//    			mButton26 = (Button) findViewById(R.id.btn26);
//    			mButton27 = (Button) findViewById(R.id.btn27);
//    			mButton28 = (Button) findViewById(R.id.btn28);
//    			mButton29 = (Button) findViewById(R.id.btn29);
//    			mButton30 = (Button) findViewById(R.id.btn30);
//    			mButton31 = (Button) findViewById(R.id.btn31);
//    			mButton32 = (Button) findViewById(R.id.btn32);
    			mButton33 = (Button) findViewById(R.id.btn33);
//    			mButton34 = (Button) findViewById(R.id.btn34);
    			mButton35 = (Button) findViewById(R.id.btn35);
//    			mButton36 = (Button) findViewById(R.id.btn36);
//    			mButton37 = (Button) findViewById(R.id.btn37);
//    			mButton38 = (Button) findViewById(R.id.btn38);
//    			mButton39 = (Button) findViewById(R.id.btn39);
//    			mButton40 = (Button) findViewById(R.id.btn40);
//    			mButton41 = (Button) findViewById(R.id.btn41);
    			mButton42 = (Button) findViewById(R.id.btn42);
    			mButton43 = (Button) findViewById(R.id.btn43);
    			mButton44 = (Button) findViewById(R.id.btn44);
    			mButton45 = (Button) findViewById(R.id.btn45);
    			mButton46 = (Button) findViewById(R.id.btn46);
    			mButton47 = (Button) findViewById(R.id.btn47);
    			mButton48 = (Button) findViewById(R.id.btn48);
    			mButton49 = (Button) findViewById(R.id.btn49);
    			mButton50 = (Button) findViewById(R.id.btn50);
    			mButton51 = (Button) findViewById(R.id.btn51);
    			mButton52 = (Button) findViewById(R.id.btn52);
    			mButton53 = (Button) findViewById(R.id.btn53);
//    			mButton54 = (Button) findViewById(R.id.btn54);
//    			mButton55 = (Button) findViewById(R.id.btn55);
//    			mButton56 = (Button) findViewById(R.id.btn56);
//    			mButton57 = (Button) findViewById(R.id.btn57);
//    			mButton58 = (Button) findViewById(R.id.btn58);
//    			mButton59 = (Button) findViewById(R.id.btn59);
//    			mButton60 = (Button) findViewById(R.id.btn60);
//    			mButton61 = (Button) findViewById(R.id.btn61);
//    			mButton62 = (Button) findViewById(R.id.btn62);
    			mButton63 = (Button) findViewById(R.id.btn63);
    			mButton64 = (Button) findViewById(R.id.btn64);
    			mButton65 = (Button) findViewById(R.id.btn65);
    			mButton66 = (Button) findViewById(R.id.btn66);
    			mButton67 = (Button) findViewById(R.id.btn67);
    			mButton68 = (Button) findViewById(R.id.btn68);
//    			mButton69 = (Button) findViewById(R.id.btn69);
//    			mButton70 = (Button) findViewById(R.id.btn70);
//    			mButton71 = (Button) findViewById(R.id.btn71);
//    			mButton72 = (Button) findViewById(R.id.btn72);
//    			mButton73 = (Button) findViewById(R.id.btn73);
//    			mButton74 = (Button) findViewById(R.id.btn74);
//    			mButton75 = (Button) findViewById(R.id.btn75);
//    			mButton76 = (Button) findViewById(R.id.btn76);
//    			mButton77 = (Button) findViewById(R.id.btn77);
    			mButton78 = (Button) findViewById(R.id.btn78);
    			mButton79 = (Button) findViewById(R.id.btn79);
    			mButton80 = (Button) findViewById(R.id.btn80);
    			mButton81 = (Button) findViewById(R.id.btn81);
    			mButton82 = (Button) findViewById(R.id.btn82);
    			mButton83 = (Button) findViewById(R.id.btn83);
    			//mButton84 = (Button) findViewById(R.id.btn84);
    			//mButton85 = (Button) findViewById(R.id.btn85);
    			//mButton86 = (Button) findViewById(R.id.btn86);
    			//mButton87 = (Button) findViewById(R.id.btn87);
    			//mButton88 = (Button) findViewById(R.id.btn88);
    			//mButton89 = (Button) findViewById(R.id.btn89);
    			//mButton90 = (Button) findViewById(R.id.btn90);
    			//mButton91 = (Button) findViewById(R.id.btn91);
    			//mButton92 = (Button) findViewById(R.id.btn92);
    			mButton93 = (Button) findViewById(R.id.btn93);
    			mButton94 = (Button) findViewById(R.id.btn94);
    			mButton95 = (Button) findViewById(R.id.btn95);
    			mButton96 = (Button) findViewById(R.id.btn96);
    			mButton97 = (Button) findViewById(R.id.btn97);
    			mButton98 = (Button) findViewById(R.id.btn98);
//    			mButton99 = (Button) findViewById(R.id.btn99);
//    			mButton100 = (Button) findViewById(R.id.btn100);
//    			mButton101 = (Button) findViewById(R.id.btn101);
//    			mButton102 = (Button) findViewById(R.id.btn102);
//    			mButton103 = (Button) findViewById(R.id.btn103);
//    			mButton104 = (Button) findViewById(R.id.btn104);
//    			mButton105 = (Button) findViewById(R.id.btn105);
//    			mButton106 = (Button) findViewById(R.id.btn106);
//    			mButton107 = (Button) findViewById(R.id.btn107);
//    			mButton108 = (Button) findViewById(R.id.btn108);
//    			mButton109 = (Button) findViewById(R.id.btn109);
//    			mButton110 = (Button) findViewById(R.id.btn110);
//    			mButton111 = (Button) findViewById(R.id.btn111);
//    			mButton112 = (Button) findViewById(R.id.btn112);
    			mButton153 = (Button) findViewById(R.id.btn153);
    			mButton154 = (Button) findViewById(R.id.btn154);
    			mButton155 = (Button) findViewById(R.id.btn155);
    			mButton156 = (Button) findViewById(R.id.btn156);
    			
    			
    			marcosetok = (Button) findViewById(R.id.marcosetok);
    			s1 = (Button) findViewById(R.id.s1);
    			s2 = (Button) findViewById(R.id.s2);
    			s3 = (Button) findViewById(R.id.s3);
    			s4 = (Button) findViewById(R.id.s4);
    			s5 = (Button) findViewById(R.id.s5);
    			
    			//row1 = (Button) findViewById(R.id.row1);
    			//row2 = (Button) findViewById(R.id.row2);
    			//row3 = (Button) findViewById(R.id.row3);
    			//row4 = (Button) findViewById(R.id.row4);
    			//row5 = (Button) findViewById(R.id.row5);
    			//row6 = (Button) findViewById(R.id.row6);
    			//row7 = (Button) findViewById(R.id.row7);
    			
    			s1.setOnClickListener(new ButtonListener());
    			s2.setOnClickListener(new ButtonListener());
    			s3.setOnClickListener(new ButtonListener());
    			s4.setOnClickListener(new ButtonListener());
    			s5.setOnClickListener(new ButtonListener());
    			
    			//row1.setOnClickListener(new ButtonListener());
    			//row2.setOnClickListener(new ButtonListener());
    			//row3.setOnClickListener(new ButtonListener());
    			//row4.setOnClickListener(new ButtonListener());
    			//row5.setOnClickListener(new ButtonListener());
    			//row6.setOnClickListener(new ButtonListener());
    			//row7.setOnClickListener(new ButtonListener());
    			

    			marcosetok.setOnClickListener(new ButtonListener());

    			lamp = getSharedPreferences(lampdata,0);
    			  mButton01.setText(lamp.getString(btn01name,""));
    			  mButton02.setText(lamp.getString(btn02name,""));
    			  mButton03.setText(lamp.getString(btn03name,""));
    			  mButton04.setText(lamp.getString(btn04name,""));
    			  mButton05.setText(lamp.getString(btn05name,""));
    			  mButton06.setText(lamp.getString(btn06name,""));
    			  mButton07.setText(lamp.getString(btn07name,""));
    			  mButton08.setText(lamp.getString(btn08name,""));
    			  mButton09.setText(lamp.getString(btn09name,""));
    			  mButton10.setText(lamp.getString(btn10name,""));
    			  mButton11.setText(lamp.getString(btn11name,""));
    			  mButton12.setText(lamp.getString(btn12name,""));
    				    
    			setting = getSharedPreferences(custom1data,0);
    			
    			cus1name=(TextView)findViewById(R.id.cus1name);
            	cus1name.setText(setting.getString(custom1name,""));
//    			mButton54.setText(setting.getString(btn54name, ""));
//    			mButton55.setText(setting.getString(btn55name, ""));
//    			mButton56.setText(setting.getString(btn56name, ""));
//    			mButton57.setText(setting.getString(btn57name, ""));
//    			mButton58.setText(setting.getString(btn58name, ""));
//    			mButton59.setText(setting.getString(btn59name, ""));
//    			mButton60.setText(setting.getString(btn60name, ""));
//    			mButton61.setText(setting.getString(btn61name, ""));
//    			mButton62.setText(setting.getString(btn62name, ""));
    			mButton63.setText(setting.getString(btn63name, ""));
    			mButton64.setText(setting.getString(btn64name, ""));
    			mButton65.setText(setting.getString(btn65name, ""));
    			mButton66.setText(setting.getString(btn66name, ""));
    			mButton67.setText(setting.getString(btn67name, ""));
    			mButton68.setText(setting.getString(btn68name, ""));
//    			mButton99.setText(setting.getString(btn99name, ""));
//    			mButton100.setText(setting.getString(btn100name, ""));
//    			mButton101.setText(setting.getString(btn101name, ""));
//    			mButton102.setText(setting.getString(btn102name, ""));
//    			mButton103.setText(setting.getString(btn103name, ""));
//    			mButton104.setText(setting.getString(btn104name, ""));

    			settingb = getSharedPreferences(custom2data,0);
    			
    			cus2name=(TextView)findViewById(R.id.cus2name);
    			cus2name.setText(settingb.getString(custom2name,""));  
//    			 mButton69.setText(settingb.getString(btn69name, ""));
//    			 mButton70.setText(settingb.getString(btn70name, ""));
//    			 mButton71.setText(settingb.getString(btn71name, ""));
//    			 mButton72.setText(settingb.getString(btn72name, ""));
//    			 mButton73.setText(settingb.getString(btn73name, ""));
//    			 mButton74.setText(settingb.getString(btn74name, ""));
//    			 mButton75.setText(settingb.getString(btn75name, ""));
//    			 mButton76.setText(settingb.getString(btn76name, ""));
//    			 mButton77.setText(settingb.getString(btn77name, ""));
    			 mButton78.setText(settingb.getString(btn78name, ""));
    			 mButton79.setText(settingb.getString(btn79name, ""));
    			 mButton80.setText(settingb.getString(btn80name, ""));
    			 mButton81.setText(settingb.getString(btn81name, ""));
    			 mButton82.setText(settingb.getString(btn82name, ""));
    			 mButton83.setText(settingb.getString(btn83name, ""));
//    			 mButton105.setText(settingb.getString(btn105name, ""));
//    			 mButton106.setText(settingb.getString(btn106name, ""));
//    			 mButton107.setText(settingb.getString(btn107name, ""));
//    			 mButton108.setText(settingb.getString(btn108name, ""));
//    			 mButton109.setText(settingb.getString(btn109name, ""));
//    			 mButton110.setText(settingb.getString(btn110name, ""));

    			  setting2 = getSharedPreferences(custom3data,0);
    			  
    			  cus3name=(TextView)findViewById(R.id.cus3name);
              	cus3name.setText(setting2.getString(custom3name,"")); 
    			// mButton84.setText(setting2.getString(btn84name, ""));
    			 //mButton85.setText(setting2.getString(btn85name, ""));
    			 //mButton86.setText(setting2.getString(btn86name, ""));
    			 //mButton87.setText(setting2.getString(btn87name, ""));
    			 //mButton88.setText(setting2.getString(btn88name, ""));
    			 //mButton89.setText(setting2.getString(btn89name, ""));
    			 //mButton90.setText(setting2.getString(btn90name, ""));
    			 //mButton91.setText(setting2.getString(btn91name, ""));
    			 //mButton92.setText(setting2.getString(btn92name, ""));
    			 mButton93.setText(setting2.getString(btn93name, ""));
    			 mButton94.setText(setting2.getString(btn94name, ""));
    			 mButton95.setText(setting2.getString(btn95name, ""));
    			 mButton96.setText(setting2.getString(btn96name, ""));
    			 mButton97.setText(setting2.getString(btn97name, ""));
    			 mButton98.setText(setting2.getString(btn98name, ""));
    			 //mButton111.setText(setting2.getString(btn111name, ""));
    			 //mButton112.setText(setting2.getString(btn112name, ""));
    			 //mButton113.setText(setting2.getString(btn113name, ""));
    			 //mButton114.setText(setting2.getString(btn114name, ""));
    			 //mButton115.setText(setting2.getString(btn115name, ""));
    			 //mButton116.setText(setting2.getString(btn116name, ""));
    			 
    			 airseting = getSharedPreferences(airdata,0);
                 mButton17.setText(airseting.getString(btn17name,""));
   		        mButton18.setText(airseting.getString(btn18name,""));
   		        mButton19.setText(airseting.getString(btn19name,""));
   		        mButton20.setText(airseting.getString(btn20name,""));
   		        mButton21.setText(airseting.getString(btn21name,""));
   		        mButton22.setText(airseting.getString(btn22name,""));

             	
             	lamp = getSharedPreferences(lampdata,0);
  				
  				imagebtn1 = (ImageView) findViewById(R.id.imagebtn1);
  				imagebtn2 = (ImageView) findViewById(R.id.imagebtn2);
  				imagebtn3 = (ImageView) findViewById(R.id.imagebtn3);
  				imagebtn4 = (ImageView) findViewById(R.id.imagebtn4);
  				imagebtn5 = (ImageView) findViewById(R.id.imagebtn5);
  				imagebtn6 = (ImageView) findViewById(R.id.imagebtn6);
  				imagebtn7 = (ImageView) findViewById(R.id.imagebtn7);
  				imagebtn8 = (ImageView) findViewById(R.id.imagebtn8);
  				imagebtn9 = (ImageView) findViewById(R.id.imagebtn9);
  				imagebtn10 = (ImageView) findViewById(R.id.imagebtn10);
  				imagebtn11 = (ImageView) findViewById(R.id.imagebtn11);
  				imagebtn12 = (ImageView) findViewById(R.id.imagebtn12);
  				
  				String stringuri1 = lamp.getString(btn1pic,"");
  				String stringuri2 = lamp.getString(btn2pic,"");
  				String stringuri3 = lamp.getString(btn3pic,"");
  				String stringuri4 = lamp.getString(btn4pic,"");
  				String stringuri5 = lamp.getString(btn5pic,"");
  				String stringuri6 = lamp.getString(btn6pic,"");
  				String stringuri7 = lamp.getString(btn7pic,"");
  				String stringuri8 = lamp.getString(btn8pic,"");
  				String stringuri9 = lamp.getString(btn9pic,"");
  				String stringuri10 = lamp.getString(btn10pic,"");
  				String stringuri11 = lamp.getString(btn11pic,"");
  				String stringuri12 = lamp.getString(btn12pic,"");
  				
  				if(stringuri1==null)
  				{
  				imagebtn1.setImageDrawable(null);
  				}else{
  				imagebtn1.setImageURI(Uri.parse(stringuri1));
  				Log.e("uri1", stringuri1);
  				}
  				
  				if(stringuri2==null)
  				{
  				imagebtn2.setImageDrawable(null);
  				}else{
  				imagebtn2.setImageURI(Uri.parse(stringuri2));
  				}
  				
  				if(stringuri3==null)
  				{
  				imagebtn3.setImageDrawable(null);
  				}else{
  				imagebtn3.setImageURI(Uri.parse(stringuri3));
  				}
  				
  				if(stringuri4==null)
  				{
  				imagebtn4.setImageDrawable(null);
  				}else{
  				imagebtn4.setImageURI(Uri.parse(stringuri4));
  				}
  				
  				if(stringuri5==null)
  				{
  					imagebtn5.setImageDrawable(null);
  				}else{
  				imagebtn5.setImageURI(Uri.parse(stringuri5));
  				}
  				
  				if(stringuri6==null)
  				{
  					imagebtn6.setImageDrawable(null);
  				}else{
  				imagebtn6.setImageURI(Uri.parse(stringuri6));
  				}
  				
  				if(stringuri7==null)
  				{
  					imagebtn7.setImageDrawable(null);
  				}else{
  				imagebtn7.setImageURI(Uri.parse(stringuri7));
  				}
  				
  				if(stringuri8==null)
  				{
  				imagebtn8.setImageDrawable(null);
  				}else{
  				imagebtn8.setImageURI(Uri.parse(stringuri8));
  				}
  				
  				if(stringuri9==null)
  				{
  				imagebtn9.setImageDrawable(null);
  				}else{
  				imagebtn9.setImageURI(Uri.parse(stringuri9));
  				}
  				
  				if(stringuri10==null)
  				{
  				imagebtn10.setImageDrawable(null);
  				}else{
  				imagebtn10.setImageURI(Uri.parse(stringuri10));
  				}
  				
  				if(stringuri11==null)
  				{
  				imagebtn11.setImageDrawable(null);
  				}else{
  				imagebtn11.setImageURI(Uri.parse(stringuri11));
  				}
  				
  				if(stringuri12==null)
  				{
  				imagebtn12.setImageDrawable(null);
  				}else{
  				imagebtn12.setImageURI(Uri.parse(stringuri12));
  				}
    			 
    			readData(5);

    			break;
    			
    			//marco2
    		case R.id.marcoset2:
    			// TODO Auto-generated catch block
    		setContentView(R.layout.marcoset1);

    		marcoset1name = (EditText) findViewById(R.id.marcoset1name);
    		marco1setbtn = (EditText) findViewById(R.id.marco1setbtn);
    		mButton01 = (Button) findViewById(R.id.btn01);
			mButton02 = (Button) findViewById(R.id.btn02);
			mButton03 = (Button) findViewById(R.id.btn03);
			mButton04 = (Button) findViewById(R.id.btn04);
			mButton05 = (Button) findViewById(R.id.btn05);
			mButton06 = (Button) findViewById(R.id.btn06);
			mButton07 = (Button) findViewById(R.id.btn07);
			mButton08 = (Button) findViewById(R.id.btn08);
			mButton09 = (Button) findViewById(R.id.btn09);
			mButton10 = (Button) findViewById(R.id.btn10);
			mButton11 = (Button) findViewById(R.id.btn11);
			mButton12 = (Button) findViewById(R.id.btn12);
			mButton13 = (Button) findViewById(R.id.btn13);
			mButton14 = (Button) findViewById(R.id.btn14);
			mButton15 = (Button) findViewById(R.id.btn15);
			mButton16 = (Button) findViewById(R.id.btn16);
			mButton17 = (Button) findViewById(R.id.btn17);
			mButton18 = (Button) findViewById(R.id.btn18);
			mButton19 = (Button) findViewById(R.id.btn19);
			mButton20 = (Button) findViewById(R.id.btn20);
			mButton21 = (Button) findViewById(R.id.btn21);
			mButton22 = (Button) findViewById(R.id.btn22);
			mButton23 = (Button) findViewById(R.id.btn23);
			mButton24 = (Button) findViewById(R.id.btn24);
//			mButton25 = (Button) findViewById(R.id.btn25);
//			mButton26 = (Button) findViewById(R.id.btn26);
//			mButton27 = (Button) findViewById(R.id.btn27);
//			mButton28 = (Button) findViewById(R.id.btn28);
//			mButton29 = (Button) findViewById(R.id.btn29);
//			mButton30 = (Button) findViewById(R.id.btn30);
//			mButton31 = (Button) findViewById(R.id.btn31);
//			mButton32 = (Button) findViewById(R.id.btn32);
			mButton33 = (Button) findViewById(R.id.btn33);
//			mButton34 = (Button) findViewById(R.id.btn34);
			mButton35 = (Button) findViewById(R.id.btn35);
//			mButton36 = (Button) findViewById(R.id.btn36);
//			mButton37 = (Button) findViewById(R.id.btn37);
//			mButton38 = (Button) findViewById(R.id.btn38);
//			mButton39 = (Button) findViewById(R.id.btn39);
//			mButton40 = (Button) findViewById(R.id.btn40);
//			mButton41 = (Button) findViewById(R.id.btn41);
			mButton42 = (Button) findViewById(R.id.btn42);
			mButton43 = (Button) findViewById(R.id.btn43);
			mButton44 = (Button) findViewById(R.id.btn44);
			mButton45 = (Button) findViewById(R.id.btn45);
			mButton46 = (Button) findViewById(R.id.btn46);
			mButton47 = (Button) findViewById(R.id.btn47);
			mButton48 = (Button) findViewById(R.id.btn48);
			mButton49 = (Button) findViewById(R.id.btn49);
			mButton50 = (Button) findViewById(R.id.btn50);
			mButton51 = (Button) findViewById(R.id.btn51);
			mButton52 = (Button) findViewById(R.id.btn52);
			mButton53 = (Button) findViewById(R.id.btn53);
//			mButton54 = (Button) findViewById(R.id.btn54);
//			mButton55 = (Button) findViewById(R.id.btn55);
//			mButton56 = (Button) findViewById(R.id.btn56);
//			mButton57 = (Button) findViewById(R.id.btn57);
//			mButton58 = (Button) findViewById(R.id.btn58);
//			mButton59 = (Button) findViewById(R.id.btn59);
//			mButton60 = (Button) findViewById(R.id.btn60);
//			mButton61 = (Button) findViewById(R.id.btn61);
//			mButton62 = (Button) findViewById(R.id.btn62);
			mButton63 = (Button) findViewById(R.id.btn63);
			mButton64 = (Button) findViewById(R.id.btn64);
			mButton65 = (Button) findViewById(R.id.btn65);
			mButton66 = (Button) findViewById(R.id.btn66);
			mButton67 = (Button) findViewById(R.id.btn67);
			mButton68 = (Button) findViewById(R.id.btn68);
//			mButton69 = (Button) findViewById(R.id.btn69);
//			mButton70 = (Button) findViewById(R.id.btn70);
//			mButton71 = (Button) findViewById(R.id.btn71);
//			mButton72 = (Button) findViewById(R.id.btn72);
//			mButton73 = (Button) findViewById(R.id.btn73);
//			mButton74 = (Button) findViewById(R.id.btn74);
//			mButton75 = (Button) findViewById(R.id.btn75);
//			mButton76 = (Button) findViewById(R.id.btn76);
//			mButton77 = (Button) findViewById(R.id.btn77);
			mButton78 = (Button) findViewById(R.id.btn78);
			mButton79 = (Button) findViewById(R.id.btn79);
			mButton80 = (Button) findViewById(R.id.btn80);
			mButton81 = (Button) findViewById(R.id.btn81);
			mButton82 = (Button) findViewById(R.id.btn82);
			mButton83 = (Button) findViewById(R.id.btn83);
			//mButton84 = (Button) findViewById(R.id.btn84);
			//mButton85 = (Button) findViewById(R.id.btn85);
			//mButton86 = (Button) findViewById(R.id.btn86);
			//mButton87 = (Button) findViewById(R.id.btn87);
			//mButton88 = (Button) findViewById(R.id.btn88);
			//mButton89 = (Button) findViewById(R.id.btn89);
			//mButton90 = (Button) findViewById(R.id.btn90);
			//mButton91 = (Button) findViewById(R.id.btn91);
			//mButton92 = (Button) findViewById(R.id.btn92);
			mButton93 = (Button) findViewById(R.id.btn93);
			mButton94 = (Button) findViewById(R.id.btn94);
			mButton95 = (Button) findViewById(R.id.btn95);
			mButton96 = (Button) findViewById(R.id.btn96);
			mButton97 = (Button) findViewById(R.id.btn97);
			mButton98 = (Button) findViewById(R.id.btn98);
//			mButton99 = (Button) findViewById(R.id.btn99);
//			mButton100 = (Button) findViewById(R.id.btn100);
//			mButton101 = (Button) findViewById(R.id.btn101);
//			mButton102 = (Button) findViewById(R.id.btn102);
//			mButton103 = (Button) findViewById(R.id.btn103);
//			mButton104 = (Button) findViewById(R.id.btn104);
//			mButton105 = (Button) findViewById(R.id.btn105);
//			mButton106 = (Button) findViewById(R.id.btn106);
//			mButton107 = (Button) findViewById(R.id.btn107);
//			mButton108 = (Button) findViewById(R.id.btn108);
//			mButton109 = (Button) findViewById(R.id.btn109);
//			mButton110 = (Button) findViewById(R.id.btn110);
//			mButton111 = (Button) findViewById(R.id.btn111);
//			mButton112 = (Button) findViewById(R.id.btn112);
			mButton153 = (Button) findViewById(R.id.btn153);
			mButton154 = (Button) findViewById(R.id.btn154);
			mButton155 = (Button) findViewById(R.id.btn155);
			mButton156 = (Button) findViewById(R.id.btn156);
			
			
			s6 = (Button) findViewById(R.id.s6);
			s7 = (Button) findViewById(R.id.s7);
			s8 = (Button) findViewById(R.id.s8);
			s9 = (Button) findViewById(R.id.s9);
			s10 = (Button) findViewById(R.id.s10);
			
			s6.setOnClickListener(new ButtonListener());
			s7.setOnClickListener(new ButtonListener());
			s8.setOnClickListener(new ButtonListener());
			s9.setOnClickListener(new ButtonListener());
			s10.setOnClickListener(new ButtonListener());
		

			findViewById(R.id.marcoset1ok).setOnClickListener(new ButtonListener());
			
			lamp = getSharedPreferences(lampdata,0);
			  mButton01.setText(lamp.getString(btn01name,""));
			  mButton02.setText(lamp.getString(btn02name,""));
			  mButton03.setText(lamp.getString(btn03name,""));
			  mButton04.setText(lamp.getString(btn04name,""));
			  mButton05.setText(lamp.getString(btn05name,""));
			  mButton06.setText(lamp.getString(btn06name,""));
			  mButton07.setText(lamp.getString(btn07name,""));
			  mButton08.setText(lamp.getString(btn08name,""));
			  mButton09.setText(lamp.getString(btn09name,""));
			  mButton10.setText(lamp.getString(btn10name,""));
			  mButton11.setText(lamp.getString(btn11name,""));
			  mButton12.setText(lamp.getString(btn12name,""));
				    
			setting = getSharedPreferences(custom1data,0);
			
			cus1name=(TextView)findViewById(R.id.cus1name);
        	cus1name.setText(setting.getString(custom1name,""));
//			mButton54.setText(setting.getString(btn54name, ""));
//			mButton55.setText(setting.getString(btn55name, ""));
//			mButton56.setText(setting.getString(btn56name, ""));
//			mButton57.setText(setting.getString(btn57name, ""));
//			mButton58.setText(setting.getString(btn58name, ""));
//			mButton59.setText(setting.getString(btn59name, ""));
//			mButton60.setText(setting.getString(btn60name, ""));
//			mButton61.setText(setting.getString(btn61name, ""));
//			mButton62.setText(setting.getString(btn62name, ""));
			mButton63.setText(setting.getString(btn63name, ""));
			mButton64.setText(setting.getString(btn64name, ""));
			mButton65.setText(setting.getString(btn65name, ""));
			mButton66.setText(setting.getString(btn66name, ""));
			mButton67.setText(setting.getString(btn67name, ""));
			mButton68.setText(setting.getString(btn68name, ""));
//			mButton99.setText(setting.getString(btn99name, ""));
//			mButton100.setText(setting.getString(btn100name, ""));
//			mButton101.setText(setting.getString(btn101name, ""));
//			mButton102.setText(setting.getString(btn102name, ""));
//			mButton103.setText(setting.getString(btn103name, ""));
//			mButton104.setText(setting.getString(btn104name, ""));

			settingb = getSharedPreferences(custom2data,0);
			
			cus2name=(TextView)findViewById(R.id.cus2name);
			cus2name.setText(settingb.getString(custom2name,""));  
//			 mButton69.setText(settingb.getString(btn69name, ""));
//			 mButton70.setText(settingb.getString(btn70name, ""));
//			 mButton71.setText(settingb.getString(btn71name, ""));
//			 mButton72.setText(settingb.getString(btn72name, ""));
//			 mButton73.setText(settingb.getString(btn73name, ""));
//			 mButton74.setText(settingb.getString(btn74name, ""));
//			 mButton75.setText(settingb.getString(btn75name, ""));
//			 mButton76.setText(settingb.getString(btn76name, ""));
//			 mButton77.setText(settingb.getString(btn77name, ""));
			 mButton78.setText(settingb.getString(btn78name, ""));
			 mButton79.setText(settingb.getString(btn79name, ""));
			 mButton80.setText(settingb.getString(btn80name, ""));
			 mButton81.setText(settingb.getString(btn81name, ""));
			 mButton82.setText(settingb.getString(btn82name, ""));
			 mButton83.setText(settingb.getString(btn83name, ""));
//			 mButton105.setText(settingb.getString(btn105name, ""));
//			 mButton106.setText(settingb.getString(btn106name, ""));
//			 mButton107.setText(settingb.getString(btn107name, ""));
//			 mButton108.setText(settingb.getString(btn108name, ""));
//			 mButton109.setText(settingb.getString(btn109name, ""));
//			 mButton110.setText(settingb.getString(btn110name, ""));

			  setting2 = getSharedPreferences(custom3data,0);
			  
			  cus3name=(TextView)findViewById(R.id.cus3name);
          	cus3name.setText(setting2.getString(custom3name,"")); 
			// mButton84.setText(setting2.getString(btn84name, ""));
			 //mButton85.setText(setting2.getString(btn85name, ""));
			 //mButton86.setText(setting2.getString(btn86name, ""));
			 //mButton87.setText(setting2.getString(btn87name, ""));
			 //mButton88.setText(setting2.getString(btn88name, ""));
			 //mButton89.setText(setting2.getString(btn89name, ""));
			 //mButton90.setText(setting2.getString(btn90name, ""));
			 //mButton91.setText(setting2.getString(btn91name, ""));
			 //mButton92.setText(setting2.getString(btn92name, ""));
			 mButton93.setText(setting2.getString(btn93name, ""));
			 mButton94.setText(setting2.getString(btn94name, ""));
			 mButton95.setText(setting2.getString(btn95name, ""));
			 mButton96.setText(setting2.getString(btn96name, ""));
			 mButton97.setText(setting2.getString(btn97name, ""));
			 mButton98.setText(setting2.getString(btn98name, ""));
			 //mButton111.setText(setting2.getString(btn111name, ""));
			 //mButton112.setText(setting2.getString(btn112name, ""));
			 //mButton113.setText(setting2.getString(btn113name, ""));
			 //mButton114.setText(setting2.getString(btn114name, ""));
			 //mButton115.setText(setting2.getString(btn115name, ""));
			 //mButton116.setText(setting2.getString(btn116name, ""));
			 
			 airseting = getSharedPreferences(airdata,0);
             mButton17.setText(airseting.getString(btn17name,""));
		        mButton18.setText(airseting.getString(btn18name,""));
		        mButton19.setText(airseting.getString(btn19name,""));
		        mButton20.setText(airseting.getString(btn20name,""));
		        mButton21.setText(airseting.getString(btn21name,""));
		        mButton22.setText(airseting.getString(btn22name,""));

         	
		        lamp = getSharedPreferences(lampdata,0);
				imagebtn1 = (ImageView) findViewById(R.id.imagebtn1);
				imagebtn2 = (ImageView) findViewById(R.id.imagebtn2);
				imagebtn3 = (ImageView) findViewById(R.id.imagebtn3);
				imagebtn4 = (ImageView) findViewById(R.id.imagebtn4);
				imagebtn5 = (ImageView) findViewById(R.id.imagebtn5);
				imagebtn6 = (ImageView) findViewById(R.id.imagebtn6);
				imagebtn7 = (ImageView) findViewById(R.id.imagebtn7);
				imagebtn8 = (ImageView) findViewById(R.id.imagebtn8);
				imagebtn9 = (ImageView) findViewById(R.id.imagebtn9);
				imagebtn10 = (ImageView) findViewById(R.id.imagebtn10);
				imagebtn11 = (ImageView) findViewById(R.id.imagebtn11);
				imagebtn12 = (ImageView) findViewById(R.id.imagebtn12);
				
				
				
				if(lamp.getString(btn1pic,"")==null)
				{
				imagebtn1.setImageDrawable(null);
				}else{
				imagebtn1.setImageURI(Uri.parse(lamp.getString(btn1pic,"")));
				Log.e("uri1", lamp.getString(btn1pic,""));
				}
				
				if(lamp.getString(btn2pic,"")==null)
				{
				imagebtn2.setImageDrawable(null);
				}else{
				imagebtn2.setImageURI(Uri.parse(lamp.getString(btn2pic,"")));
				}
				
				if(lamp.getString(btn3pic,"")==null)
				{
				imagebtn3.setImageDrawable(null);
				}else{
				imagebtn3.setImageURI(Uri.parse(lamp.getString(btn3pic,"")));
				}
				
				if(lamp.getString(btn4pic,"")==null)
				{
				imagebtn4.setImageDrawable(null);
				}else{
				imagebtn4.setImageURI(Uri.parse(lamp.getString(btn4pic,"")));
				}
				
				if(lamp.getString(btn5pic,"")==null)
				{
					imagebtn5.setImageDrawable(null);
				}else{
				imagebtn5.setImageURI(Uri.parse(lamp.getString(btn5pic,"")));
				}
				
				if(lamp.getString(btn6pic,"")==null)
				{
					imagebtn6.setImageDrawable(null);
				}else{
				imagebtn6.setImageURI(Uri.parse(lamp.getString(btn6pic,"")));
				}
				
				if(lamp.getString(btn7pic,"")==null)
				{
					imagebtn7.setImageDrawable(null);
				}else{
				imagebtn7.setImageURI(Uri.parse(lamp.getString(btn7pic,"")));
				}
				
				if(lamp.getString(btn8pic,"")==null)
				{
				imagebtn8.setImageDrawable(null);
				}else{
				imagebtn8.setImageURI(Uri.parse(lamp.getString(btn8pic,"")));
				}
				
				if(lamp.getString(btn9pic,"")==null)
				{
				imagebtn9.setImageDrawable(null);
				}else{
				imagebtn9.setImageURI(Uri.parse(lamp.getString(btn9pic,"")));
				}
				
				if(lamp.getString(btn10pic,"")==null)
				{
				imagebtn10.setImageDrawable(null);
				}else{
				imagebtn10.setImageURI(Uri.parse(lamp.getString(btn10pic,"")));
				}
				
				if(lamp.getString(btn11pic,"")==null)
				{
				imagebtn11.setImageDrawable(null);
				}else{
				imagebtn11.setImageURI(Uri.parse(lamp.getString(btn11pic,"")));
				}
				
				if(lamp.getString(btn12pic,"")==null)
				{
				imagebtn12.setImageDrawable(null);
				}else{
				imagebtn12.setImageURI(Uri.parse(lamp.getString(btn12pic,"")));
				}
			 
			readData(6);
    		break;

    		//marco3
    		case R.id.marcoset3:
    			// TODO Auto-generated catch block
    		setContentView(R.layout.marcoset2);
    		
    		marcoset2name = (EditText) findViewById(R.id.marcoset2name);
    		marco2setbtn = (EditText) findViewById(R.id.marco2setbtn);
    		mButton01 = (Button) findViewById(R.id.btn01);
			mButton02 = (Button) findViewById(R.id.btn02);
			mButton03 = (Button) findViewById(R.id.btn03);
			mButton04 = (Button) findViewById(R.id.btn04);
			mButton05 = (Button) findViewById(R.id.btn05);
			mButton06 = (Button) findViewById(R.id.btn06);
			mButton07 = (Button) findViewById(R.id.btn07);
			mButton08 = (Button) findViewById(R.id.btn08);
			mButton09 = (Button) findViewById(R.id.btn09);
			mButton10 = (Button) findViewById(R.id.btn10);
			mButton11 = (Button) findViewById(R.id.btn11);
			mButton12 = (Button) findViewById(R.id.btn12);
			mButton13 = (Button) findViewById(R.id.btn13);
			mButton14 = (Button) findViewById(R.id.btn14);
			mButton15 = (Button) findViewById(R.id.btn15);
			mButton16 = (Button) findViewById(R.id.btn16);
			mButton17 = (Button) findViewById(R.id.btn17);
			mButton18 = (Button) findViewById(R.id.btn18);
			mButton19 = (Button) findViewById(R.id.btn19);
			mButton20 = (Button) findViewById(R.id.btn20);
			mButton21 = (Button) findViewById(R.id.btn21);
			mButton22 = (Button) findViewById(R.id.btn22);
			mButton23 = (Button) findViewById(R.id.btn23);
			mButton24 = (Button) findViewById(R.id.btn24);
//			mButton25 = (Button) findViewById(R.id.btn25);
//			mButton26 = (Button) findViewById(R.id.btn26);
//			mButton27 = (Button) findViewById(R.id.btn27);
//			mButton28 = (Button) findViewById(R.id.btn28);
//			mButton29 = (Button) findViewById(R.id.btn29);
//			mButton30 = (Button) findViewById(R.id.btn30);
//			mButton31 = (Button) findViewById(R.id.btn31);
//			mButton32 = (Button) findViewById(R.id.btn32);
			mButton33 = (Button) findViewById(R.id.btn33);
//			mButton34 = (Button) findViewById(R.id.btn34);
			mButton35 = (Button) findViewById(R.id.btn35);
//			mButton36 = (Button) findViewById(R.id.btn36);
//			mButton37 = (Button) findViewById(R.id.btn37);
//			mButton38 = (Button) findViewById(R.id.btn38);
//			mButton39 = (Button) findViewById(R.id.btn39);
//			mButton40 = (Button) findViewById(R.id.btn40);
//			mButton41 = (Button) findViewById(R.id.btn41);
			mButton42 = (Button) findViewById(R.id.btn42);
			mButton43 = (Button) findViewById(R.id.btn43);
			mButton44 = (Button) findViewById(R.id.btn44);
			mButton45 = (Button) findViewById(R.id.btn45);
			mButton46 = (Button) findViewById(R.id.btn46);
			mButton47 = (Button) findViewById(R.id.btn47);
			mButton48 = (Button) findViewById(R.id.btn48);
			mButton49 = (Button) findViewById(R.id.btn49);
			mButton50 = (Button) findViewById(R.id.btn50);
			mButton51 = (Button) findViewById(R.id.btn51);
			mButton52 = (Button) findViewById(R.id.btn52);
			mButton53 = (Button) findViewById(R.id.btn53);
//			mButton54 = (Button) findViewById(R.id.btn54);
//			mButton55 = (Button) findViewById(R.id.btn55);
//			mButton56 = (Button) findViewById(R.id.btn56);
//			mButton57 = (Button) findViewById(R.id.btn57);
//			mButton58 = (Button) findViewById(R.id.btn58);
//			mButton59 = (Button) findViewById(R.id.btn59);
//			mButton60 = (Button) findViewById(R.id.btn60);
//			mButton61 = (Button) findViewById(R.id.btn61);
//			mButton62 = (Button) findViewById(R.id.btn62);
			mButton63 = (Button) findViewById(R.id.btn63);
			mButton64 = (Button) findViewById(R.id.btn64);
			mButton65 = (Button) findViewById(R.id.btn65);
			mButton66 = (Button) findViewById(R.id.btn66);
			mButton67 = (Button) findViewById(R.id.btn67);
			mButton68 = (Button) findViewById(R.id.btn68);
//			mButton69 = (Button) findViewById(R.id.btn69);
//			mButton70 = (Button) findViewById(R.id.btn70);
//			mButton71 = (Button) findViewById(R.id.btn71);
//			mButton72 = (Button) findViewById(R.id.btn72);
//			mButton73 = (Button) findViewById(R.id.btn73);
//			mButton74 = (Button) findViewById(R.id.btn74);
//			mButton75 = (Button) findViewById(R.id.btn75);
//			mButton76 = (Button) findViewById(R.id.btn76);
//			mButton77 = (Button) findViewById(R.id.btn77);
			mButton78 = (Button) findViewById(R.id.btn78);
			mButton79 = (Button) findViewById(R.id.btn79);
			mButton80 = (Button) findViewById(R.id.btn80);
			mButton81 = (Button) findViewById(R.id.btn81);
			mButton82 = (Button) findViewById(R.id.btn82);
			mButton83 = (Button) findViewById(R.id.btn83);
			//mButton84 = (Button) findViewById(R.id.btn84);
			//mButton85 = (Button) findViewById(R.id.btn85);
			//mButton86 = (Button) findViewById(R.id.btn86);
			//mButton87 = (Button) findViewById(R.id.btn87);
			//mButton88 = (Button) findViewById(R.id.btn88);
			//mButton89 = (Button) findViewById(R.id.btn89);
			//mButton90 = (Button) findViewById(R.id.btn90);
			//mButton91 = (Button) findViewById(R.id.btn91);
			//mButton92 = (Button) findViewById(R.id.btn92);
			mButton93 = (Button) findViewById(R.id.btn93);
			mButton94 = (Button) findViewById(R.id.btn94);
			mButton95 = (Button) findViewById(R.id.btn95);
			mButton96 = (Button) findViewById(R.id.btn96);
			mButton97 = (Button) findViewById(R.id.btn97);
			mButton98 = (Button) findViewById(R.id.btn98);
//			mButton99 = (Button) findViewById(R.id.btn99);
//			mButton100 = (Button) findViewById(R.id.btn100);
//			mButton101 = (Button) findViewById(R.id.btn101);
//			mButton102 = (Button) findViewById(R.id.btn102);
//			mButton103 = (Button) findViewById(R.id.btn103);
//			mButton104 = (Button) findViewById(R.id.btn104);
//			mButton105 = (Button) findViewById(R.id.btn105);
//			mButton106 = (Button) findViewById(R.id.btn106);
//			mButton107 = (Button) findViewById(R.id.btn107);
//			mButton108 = (Button) findViewById(R.id.btn108);
//			mButton109 = (Button) findViewById(R.id.btn109);
//			mButton110 = (Button) findViewById(R.id.btn110);
//			mButton111 = (Button) findViewById(R.id.btn111);
//			mButton112 = (Button) findViewById(R.id.btn112);
			mButton153 = (Button) findViewById(R.id.btn153);
			mButton154 = (Button) findViewById(R.id.btn154);
			mButton155 = (Button) findViewById(R.id.btn155);
			mButton156 = (Button) findViewById(R.id.btn156);
			
			
			findViewById(R.id.marcoset2ok).setOnClickListener(new ButtonListener());
			
			findViewById(R.id.s11).setOnClickListener(new ButtonListener());
			findViewById(R.id.s12).setOnClickListener(new ButtonListener());
			findViewById(R.id.s13).setOnClickListener(new ButtonListener());
			findViewById(R.id.s14).setOnClickListener(new ButtonListener());
			findViewById(R.id.s15).setOnClickListener(new ButtonListener());
			

			lamp = getSharedPreferences(lampdata,0);
			  mButton01.setText(lamp.getString(btn01name,""));
			  mButton02.setText(lamp.getString(btn02name,""));
			  mButton03.setText(lamp.getString(btn03name,""));
			  mButton04.setText(lamp.getString(btn04name,""));
			  mButton05.setText(lamp.getString(btn05name,""));
			  mButton06.setText(lamp.getString(btn06name,""));
			  mButton07.setText(lamp.getString(btn07name,""));
			  mButton08.setText(lamp.getString(btn08name,""));
			  mButton09.setText(lamp.getString(btn09name,""));
			  mButton10.setText(lamp.getString(btn10name,""));
			  mButton11.setText(lamp.getString(btn11name,""));
			  mButton12.setText(lamp.getString(btn12name,""));
				    
			setting = getSharedPreferences(custom1data,0);
			
			cus1name=(TextView)findViewById(R.id.cus1name);
      	cus1name.setText(setting.getString(custom1name,""));
//			mButton54.setText(setting.getString(btn54name, ""));
//			mButton55.setText(setting.getString(btn55name, ""));
//			mButton56.setText(setting.getString(btn56name, ""));
//			mButton57.setText(setting.getString(btn57name, ""));
//			mButton58.setText(setting.getString(btn58name, ""));
//			mButton59.setText(setting.getString(btn59name, ""));
//			mButton60.setText(setting.getString(btn60name, ""));
//			mButton61.setText(setting.getString(btn61name, ""));
//			mButton62.setText(setting.getString(btn62name, ""));
			mButton63.setText(setting.getString(btn63name, ""));
			mButton64.setText(setting.getString(btn64name, ""));
			mButton65.setText(setting.getString(btn65name, ""));
			mButton66.setText(setting.getString(btn66name, ""));
			mButton67.setText(setting.getString(btn67name, ""));
			mButton68.setText(setting.getString(btn68name, ""));
//			mButton99.setText(setting.getString(btn99name, ""));
//			mButton100.setText(setting.getString(btn100name, ""));
//			mButton101.setText(setting.getString(btn101name, ""));
//			mButton102.setText(setting.getString(btn102name, ""));
//			mButton103.setText(setting.getString(btn103name, ""));
//			mButton104.setText(setting.getString(btn104name, ""));

			settingb = getSharedPreferences(custom2data,0);
			
			cus2name=(TextView)findViewById(R.id.cus2name);
			cus2name.setText(settingb.getString(custom2name,""));  
//			 mButton69.setText(settingb.getString(btn69name, ""));
//			 mButton70.setText(settingb.getString(btn70name, ""));
//			 mButton71.setText(settingb.getString(btn71name, ""));
//			 mButton72.setText(settingb.getString(btn72name, ""));
//			 mButton73.setText(settingb.getString(btn73name, ""));
//			 mButton74.setText(settingb.getString(btn74name, ""));
//			 mButton75.setText(settingb.getString(btn75name, ""));
//			 mButton76.setText(settingb.getString(btn76name, ""));
//			 mButton77.setText(settingb.getString(btn77name, ""));
			 mButton78.setText(settingb.getString(btn78name, ""));
			 mButton79.setText(settingb.getString(btn79name, ""));
			 mButton80.setText(settingb.getString(btn80name, ""));
			 mButton81.setText(settingb.getString(btn81name, ""));
			 mButton82.setText(settingb.getString(btn82name, ""));
			 mButton83.setText(settingb.getString(btn83name, ""));
//			 mButton105.setText(settingb.getString(btn105name, ""));
//			 mButton106.setText(settingb.getString(btn106name, ""));
//			 mButton107.setText(settingb.getString(btn107name, ""));
//			 mButton108.setText(settingb.getString(btn108name, ""));
//			 mButton109.setText(settingb.getString(btn109name, ""));
//			 mButton110.setText(settingb.getString(btn110name, ""));

			  setting2 = getSharedPreferences(custom3data,0);
			  
			  cus3name=(TextView)findViewById(R.id.cus3name);
        	cus3name.setText(setting2.getString(custom3name,"")); 
			// mButton84.setText(setting2.getString(btn84name, ""));
			 //mButton85.setText(setting2.getString(btn85name, ""));
			 //mButton86.setText(setting2.getString(btn86name, ""));
			 //mButton87.setText(setting2.getString(btn87name, ""));
			 //mButton88.setText(setting2.getString(btn88name, ""));
			 //mButton89.setText(setting2.getString(btn89name, ""));
			 //mButton90.setText(setting2.getString(btn90name, ""));
			 //mButton91.setText(setting2.getString(btn91name, ""));
			 //mButton92.setText(setting2.getString(btn92name, ""));
			 mButton93.setText(setting2.getString(btn93name, ""));
			 mButton94.setText(setting2.getString(btn94name, ""));
			 mButton95.setText(setting2.getString(btn95name, ""));
			 mButton96.setText(setting2.getString(btn96name, ""));
			 mButton97.setText(setting2.getString(btn97name, ""));
			 mButton98.setText(setting2.getString(btn98name, ""));
			 //mButton111.setText(setting2.getString(btn111name, ""));
			 //mButton112.setText(setting2.getString(btn112name, ""));
			 //mButton113.setText(setting2.getString(btn113name, ""));
			 //mButton114.setText(setting2.getString(btn114name, ""));
			 //mButton115.setText(setting2.getString(btn115name, ""));
			 //mButton116.setText(setting2.getString(btn116name, ""));
			 
			 airseting = getSharedPreferences(airdata,0);
           mButton17.setText(airseting.getString(btn17name,""));
		        mButton18.setText(airseting.getString(btn18name,""));
		        mButton19.setText(airseting.getString(btn19name,""));
		        mButton20.setText(airseting.getString(btn20name,""));
		        mButton21.setText(airseting.getString(btn21name,""));
		        mButton22.setText(airseting.getString(btn22name,""));

       	
		        lamp = getSharedPreferences(lampdata,0);
				imagebtn1 = (ImageView) findViewById(R.id.imagebtn1);
				imagebtn2 = (ImageView) findViewById(R.id.imagebtn2);
				imagebtn3 = (ImageView) findViewById(R.id.imagebtn3);
				imagebtn4 = (ImageView) findViewById(R.id.imagebtn4);
				imagebtn5 = (ImageView) findViewById(R.id.imagebtn5);
				imagebtn6 = (ImageView) findViewById(R.id.imagebtn6);
				imagebtn7 = (ImageView) findViewById(R.id.imagebtn7);
				imagebtn8 = (ImageView) findViewById(R.id.imagebtn8);
				imagebtn9 = (ImageView) findViewById(R.id.imagebtn9);
				imagebtn10 = (ImageView) findViewById(R.id.imagebtn10);
				imagebtn11 = (ImageView) findViewById(R.id.imagebtn11);
				imagebtn12 = (ImageView) findViewById(R.id.imagebtn12);
				
				
				
				if(lamp.getString(btn1pic,"")==null)
				{
				imagebtn1.setImageDrawable(null);
				}else{
				imagebtn1.setImageURI(Uri.parse(lamp.getString(btn1pic,"")));
				Log.e("uri1", lamp.getString(btn1pic,""));
				}
				
				if(lamp.getString(btn2pic,"")==null)
				{
				imagebtn2.setImageDrawable(null);
				}else{
				imagebtn2.setImageURI(Uri.parse(lamp.getString(btn2pic,"")));
				}
				
				if(lamp.getString(btn3pic,"")==null)
				{
				imagebtn3.setImageDrawable(null);
				}else{
				imagebtn3.setImageURI(Uri.parse(lamp.getString(btn3pic,"")));
				}
				
				if(lamp.getString(btn4pic,"")==null)
				{
				imagebtn4.setImageDrawable(null);
				}else{
				imagebtn4.setImageURI(Uri.parse(lamp.getString(btn4pic,"")));
				}
				
				if(lamp.getString(btn5pic,"")==null)
				{
					imagebtn5.setImageDrawable(null);
				}else{
				imagebtn5.setImageURI(Uri.parse(lamp.getString(btn5pic,"")));
				}
				
				if(lamp.getString(btn6pic,"")==null)
				{
					imagebtn6.setImageDrawable(null);
				}else{
				imagebtn6.setImageURI(Uri.parse(lamp.getString(btn6pic,"")));
				}
				
				if(lamp.getString(btn7pic,"")==null)
				{
					imagebtn7.setImageDrawable(null);
				}else{
				imagebtn7.setImageURI(Uri.parse(lamp.getString(btn7pic,"")));
				}
				
				if(lamp.getString(btn8pic,"")==null)
				{
				imagebtn8.setImageDrawable(null);
				}else{
				imagebtn8.setImageURI(Uri.parse(lamp.getString(btn8pic,"")));
				}
				
				if(lamp.getString(btn9pic,"")==null)
				{
				imagebtn9.setImageDrawable(null);
				}else{
				imagebtn9.setImageURI(Uri.parse(lamp.getString(btn9pic,"")));
				}
				
				if(lamp.getString(btn10pic,"")==null)
				{
				imagebtn10.setImageDrawable(null);
				}else{
				imagebtn10.setImageURI(Uri.parse(lamp.getString(btn10pic,"")));
				}
				
				if(lamp.getString(btn11pic,"")==null)
				{
				imagebtn11.setImageDrawable(null);
				}else{
				imagebtn11.setImageURI(Uri.parse(lamp.getString(btn11pic,"")));
				}
				
				if(lamp.getString(btn12pic,"")==null)
				{
				imagebtn12.setImageDrawable(null);
				}else{
				imagebtn12.setImageURI(Uri.parse(lamp.getString(btn12pic,"")));
				}
				
    		readData(7);
    		break;
      			
    		
    		//marco4
    		case R.id.marcoset4:
    		// TODO Auto-generated catch block
    		setContentView(R.layout.marcoset3);

    		marcoset3name = (EditText) findViewById(R.id.marcoset3name);
    		marco3setbtn = (EditText) findViewById(R.id.marco3setbtn);
    		mButton01 = (Button) findViewById(R.id.btn01);
			mButton02 = (Button) findViewById(R.id.btn02);
			mButton03 = (Button) findViewById(R.id.btn03);
			mButton04 = (Button) findViewById(R.id.btn04);
			mButton05 = (Button) findViewById(R.id.btn05);
			mButton06 = (Button) findViewById(R.id.btn06);
			mButton07 = (Button) findViewById(R.id.btn07);
			mButton08 = (Button) findViewById(R.id.btn08);
			mButton09 = (Button) findViewById(R.id.btn09);
			mButton10 = (Button) findViewById(R.id.btn10);
			mButton11 = (Button) findViewById(R.id.btn11);
			mButton12 = (Button) findViewById(R.id.btn12);
			mButton13 = (Button) findViewById(R.id.btn13);
			mButton14 = (Button) findViewById(R.id.btn14);
			mButton15 = (Button) findViewById(R.id.btn15);
			mButton16 = (Button) findViewById(R.id.btn16);
			mButton17 = (Button) findViewById(R.id.btn17);
			mButton18 = (Button) findViewById(R.id.btn18);
			mButton19 = (Button) findViewById(R.id.btn19);
			mButton20 = (Button) findViewById(R.id.btn20);
			mButton21 = (Button) findViewById(R.id.btn21);
			mButton22 = (Button) findViewById(R.id.btn22);
			mButton23 = (Button) findViewById(R.id.btn23);
			mButton24 = (Button) findViewById(R.id.btn24);
//			mButton25 = (Button) findViewById(R.id.btn25);
//			mButton26 = (Button) findViewById(R.id.btn26);
//			mButton27 = (Button) findViewById(R.id.btn27);
//			mButton28 = (Button) findViewById(R.id.btn28);
//			mButton29 = (Button) findViewById(R.id.btn29);
//			mButton30 = (Button) findViewById(R.id.btn30);
//			mButton31 = (Button) findViewById(R.id.btn31);
//			mButton32 = (Button) findViewById(R.id.btn32);
			mButton33 = (Button) findViewById(R.id.btn33);
//			mButton34 = (Button) findViewById(R.id.btn34);
			mButton35 = (Button) findViewById(R.id.btn35);
//			mButton36 = (Button) findViewById(R.id.btn36);
//			mButton37 = (Button) findViewById(R.id.btn37);
//			mButton38 = (Button) findViewById(R.id.btn38);
//			mButton39 = (Button) findViewById(R.id.btn39);
//			mButton40 = (Button) findViewById(R.id.btn40);
//			mButton41 = (Button) findViewById(R.id.btn41);
			mButton42 = (Button) findViewById(R.id.btn42);
			mButton43 = (Button) findViewById(R.id.btn43);
			mButton44 = (Button) findViewById(R.id.btn44);
			mButton45 = (Button) findViewById(R.id.btn45);
			mButton46 = (Button) findViewById(R.id.btn46);
			mButton47 = (Button) findViewById(R.id.btn47);
			mButton48 = (Button) findViewById(R.id.btn48);
			mButton49 = (Button) findViewById(R.id.btn49);
			mButton50 = (Button) findViewById(R.id.btn50);
			mButton51 = (Button) findViewById(R.id.btn51);
			mButton52 = (Button) findViewById(R.id.btn52);
			mButton53 = (Button) findViewById(R.id.btn53);
//			mButton54 = (Button) findViewById(R.id.btn54);
//			mButton55 = (Button) findViewById(R.id.btn55);
//			mButton56 = (Button) findViewById(R.id.btn56);
//			mButton57 = (Button) findViewById(R.id.btn57);
//			mButton58 = (Button) findViewById(R.id.btn58);
//			mButton59 = (Button) findViewById(R.id.btn59);
//			mButton60 = (Button) findViewById(R.id.btn60);
//			mButton61 = (Button) findViewById(R.id.btn61);
//			mButton62 = (Button) findViewById(R.id.btn62);
			mButton63 = (Button) findViewById(R.id.btn63);
			mButton64 = (Button) findViewById(R.id.btn64);
			mButton65 = (Button) findViewById(R.id.btn65);
			mButton66 = (Button) findViewById(R.id.btn66);
			mButton67 = (Button) findViewById(R.id.btn67);
			mButton68 = (Button) findViewById(R.id.btn68);
//			mButton69 = (Button) findViewById(R.id.btn69);
//			mButton70 = (Button) findViewById(R.id.btn70);
//			mButton71 = (Button) findViewById(R.id.btn71);
//			mButton72 = (Button) findViewById(R.id.btn72);
//			mButton73 = (Button) findViewById(R.id.btn73);
//			mButton74 = (Button) findViewById(R.id.btn74);
//			mButton75 = (Button) findViewById(R.id.btn75);
//			mButton76 = (Button) findViewById(R.id.btn76);
//			mButton77 = (Button) findViewById(R.id.btn77);
			mButton78 = (Button) findViewById(R.id.btn78);
			mButton79 = (Button) findViewById(R.id.btn79);
			mButton80 = (Button) findViewById(R.id.btn80);
			mButton81 = (Button) findViewById(R.id.btn81);
			mButton82 = (Button) findViewById(R.id.btn82);
			mButton83 = (Button) findViewById(R.id.btn83);
			//mButton84 = (Button) findViewById(R.id.btn84);
			//mButton85 = (Button) findViewById(R.id.btn85);
			//mButton86 = (Button) findViewById(R.id.btn86);
			//mButton87 = (Button) findViewById(R.id.btn87);
			//mButton88 = (Button) findViewById(R.id.btn88);
			//mButton89 = (Button) findViewById(R.id.btn89);
			//mButton90 = (Button) findViewById(R.id.btn90);
			//mButton91 = (Button) findViewById(R.id.btn91);
			//mButton92 = (Button) findViewById(R.id.btn92);
			mButton93 = (Button) findViewById(R.id.btn93);
			mButton94 = (Button) findViewById(R.id.btn94);
			mButton95 = (Button) findViewById(R.id.btn95);
			mButton96 = (Button) findViewById(R.id.btn96);
			mButton97 = (Button) findViewById(R.id.btn97);
			mButton98 = (Button) findViewById(R.id.btn98);
//			mButton99 = (Button) findViewById(R.id.btn99);
//			mButton100 = (Button) findViewById(R.id.btn100);
//			mButton101 = (Button) findViewById(R.id.btn101);
//			mButton102 = (Button) findViewById(R.id.btn102);
//			mButton103 = (Button) findViewById(R.id.btn103);
//			mButton104 = (Button) findViewById(R.id.btn104);
//			mButton105 = (Button) findViewById(R.id.btn105);
//			mButton106 = (Button) findViewById(R.id.btn106);
//			mButton107 = (Button) findViewById(R.id.btn107);
//			mButton108 = (Button) findViewById(R.id.btn108);
//			mButton109 = (Button) findViewById(R.id.btn109);
//			mButton110 = (Button) findViewById(R.id.btn110);
//			mButton111 = (Button) findViewById(R.id.btn111);
//			mButton112 = (Button) findViewById(R.id.btn112);
			mButton153 = (Button) findViewById(R.id.btn153);
			mButton154 = (Button) findViewById(R.id.btn154);
			mButton155 = (Button) findViewById(R.id.btn155);
			mButton156 = (Button) findViewById(R.id.btn156);
			
			
			findViewById(R.id.marcoset3ok).setOnClickListener(new ButtonListener());
			
			findViewById(R.id.s16).setOnClickListener(new ButtonListener());
			findViewById(R.id.s17).setOnClickListener(new ButtonListener());
			findViewById(R.id.s18).setOnClickListener(new ButtonListener());
			findViewById(R.id.s19).setOnClickListener(new ButtonListener());
			findViewById(R.id.s20).setOnClickListener(new ButtonListener());
			

			lamp = getSharedPreferences(lampdata,0);
			  mButton01.setText(lamp.getString(btn01name,""));
			  mButton02.setText(lamp.getString(btn02name,""));
			  mButton03.setText(lamp.getString(btn03name,""));
			  mButton04.setText(lamp.getString(btn04name,""));
			  mButton05.setText(lamp.getString(btn05name,""));
			  mButton06.setText(lamp.getString(btn06name,""));
			  mButton07.setText(lamp.getString(btn07name,""));
			  mButton08.setText(lamp.getString(btn08name,""));
			  mButton09.setText(lamp.getString(btn09name,""));
			  mButton10.setText(lamp.getString(btn10name,""));
			  mButton11.setText(lamp.getString(btn11name,""));
			  mButton12.setText(lamp.getString(btn12name,""));
				    
			setting = getSharedPreferences(custom1data,0);
			
			cus1name=(TextView)findViewById(R.id.cus1name);
      	cus1name.setText(setting.getString(custom1name,""));
//			mButton54.setText(setting.getString(btn54name, ""));
//			mButton55.setText(setting.getString(btn55name, ""));
//			mButton56.setText(setting.getString(btn56name, ""));
//			mButton57.setText(setting.getString(btn57name, ""));
//			mButton58.setText(setting.getString(btn58name, ""));
//			mButton59.setText(setting.getString(btn59name, ""));
//			mButton60.setText(setting.getString(btn60name, ""));
//			mButton61.setText(setting.getString(btn61name, ""));
//			mButton62.setText(setting.getString(btn62name, ""));
			mButton63.setText(setting.getString(btn63name, ""));
			mButton64.setText(setting.getString(btn64name, ""));
			mButton65.setText(setting.getString(btn65name, ""));
			mButton66.setText(setting.getString(btn66name, ""));
			mButton67.setText(setting.getString(btn67name, ""));
			mButton68.setText(setting.getString(btn68name, ""));
//			mButton99.setText(setting.getString(btn99name, ""));
//			mButton100.setText(setting.getString(btn100name, ""));
//			mButton101.setText(setting.getString(btn101name, ""));
//			mButton102.setText(setting.getString(btn102name, ""));
//			mButton103.setText(setting.getString(btn103name, ""));
//			mButton104.setText(setting.getString(btn104name, ""));

			settingb = getSharedPreferences(custom2data,0);
			
			cus2name=(TextView)findViewById(R.id.cus2name);
			cus2name.setText(settingb.getString(custom2name,""));  
//			 mButton69.setText(settingb.getString(btn69name, ""));
//			 mButton70.setText(settingb.getString(btn70name, ""));
//			 mButton71.setText(settingb.getString(btn71name, ""));
//			 mButton72.setText(settingb.getString(btn72name, ""));
//			 mButton73.setText(settingb.getString(btn73name, ""));
//			 mButton74.setText(settingb.getString(btn74name, ""));
//			 mButton75.setText(settingb.getString(btn75name, ""));
//			 mButton76.setText(settingb.getString(btn76name, ""));
//			 mButton77.setText(settingb.getString(btn77name, ""));
			 mButton78.setText(settingb.getString(btn78name, ""));
			 mButton79.setText(settingb.getString(btn79name, ""));
			 mButton80.setText(settingb.getString(btn80name, ""));
			 mButton81.setText(settingb.getString(btn81name, ""));
			 mButton82.setText(settingb.getString(btn82name, ""));
			 mButton83.setText(settingb.getString(btn83name, ""));
//			 mButton105.setText(settingb.getString(btn105name, ""));
//			 mButton106.setText(settingb.getString(btn106name, ""));
//			 mButton107.setText(settingb.getString(btn107name, ""));
//			 mButton108.setText(settingb.getString(btn108name, ""));
//			 mButton109.setText(settingb.getString(btn109name, ""));
//			 mButton110.setText(settingb.getString(btn110name, ""));

			  setting2 = getSharedPreferences(custom3data,0);
			  
			  cus3name=(TextView)findViewById(R.id.cus3name);
        	cus3name.setText(setting2.getString(custom3name,"")); 
			// mButton84.setText(setting2.getString(btn84name, ""));
			 //mButton85.setText(setting2.getString(btn85name, ""));
			 //mButton86.setText(setting2.getString(btn86name, ""));
			 //mButton87.setText(setting2.getString(btn87name, ""));
			 //mButton88.setText(setting2.getString(btn88name, ""));
			 //mButton89.setText(setting2.getString(btn89name, ""));
			 //mButton90.setText(setting2.getString(btn90name, ""));
			 //mButton91.setText(setting2.getString(btn91name, ""));
			 //mButton92.setText(setting2.getString(btn92name, ""));
			 mButton93.setText(setting2.getString(btn93name, ""));
			 mButton94.setText(setting2.getString(btn94name, ""));
			 mButton95.setText(setting2.getString(btn95name, ""));
			 mButton96.setText(setting2.getString(btn96name, ""));
			 mButton97.setText(setting2.getString(btn97name, ""));
			 mButton98.setText(setting2.getString(btn98name, ""));
			 //mButton111.setText(setting2.getString(btn111name, ""));
			 //mButton112.setText(setting2.getString(btn112name, ""));
			 //mButton113.setText(setting2.getString(btn113name, ""));
			 //mButton114.setText(setting2.getString(btn114name, ""));
			 //mButton115.setText(setting2.getString(btn115name, ""));
			 //mButton116.setText(setting2.getString(btn116name, ""));
			 
			 airseting = getSharedPreferences(airdata,0);
           mButton17.setText(airseting.getString(btn17name,""));
		        mButton18.setText(airseting.getString(btn18name,""));
		        mButton19.setText(airseting.getString(btn19name,""));
		        mButton20.setText(airseting.getString(btn20name,""));
		        mButton21.setText(airseting.getString(btn21name,""));
		        mButton22.setText(airseting.getString(btn22name,""));

       	
		        lamp = getSharedPreferences(lampdata,0);
				imagebtn1 = (ImageView) findViewById(R.id.imagebtn1);
				imagebtn2 = (ImageView) findViewById(R.id.imagebtn2);
				imagebtn3 = (ImageView) findViewById(R.id.imagebtn3);
				imagebtn4 = (ImageView) findViewById(R.id.imagebtn4);
				imagebtn5 = (ImageView) findViewById(R.id.imagebtn5);
				imagebtn6 = (ImageView) findViewById(R.id.imagebtn6);
				imagebtn7 = (ImageView) findViewById(R.id.imagebtn7);
				imagebtn8 = (ImageView) findViewById(R.id.imagebtn8);
				imagebtn9 = (ImageView) findViewById(R.id.imagebtn9);
				imagebtn10 = (ImageView) findViewById(R.id.imagebtn10);
				imagebtn11 = (ImageView) findViewById(R.id.imagebtn11);
				imagebtn12 = (ImageView) findViewById(R.id.imagebtn12);
				
				
				
				if(lamp.getString(btn1pic,"")==null)
				{
				imagebtn1.setImageDrawable(null);
				}else{
				imagebtn1.setImageURI(Uri.parse(lamp.getString(btn1pic,"")));
				Log.e("uri1", lamp.getString(btn1pic,""));
				}
				
				if(lamp.getString(btn2pic,"")==null)
				{
				imagebtn2.setImageDrawable(null);
				}else{
				imagebtn2.setImageURI(Uri.parse(lamp.getString(btn2pic,"")));
				}
				
				if(lamp.getString(btn3pic,"")==null)
				{
				imagebtn3.setImageDrawable(null);
				}else{
				imagebtn3.setImageURI(Uri.parse(lamp.getString(btn3pic,"")));
				}
				
				if(lamp.getString(btn4pic,"")==null)
				{
				imagebtn4.setImageDrawable(null);
				}else{
				imagebtn4.setImageURI(Uri.parse(lamp.getString(btn4pic,"")));
				}
				
				if(lamp.getString(btn5pic,"")==null)
				{
					imagebtn5.setImageDrawable(null);
				}else{
				imagebtn5.setImageURI(Uri.parse(lamp.getString(btn5pic,"")));
				}
				
				if(lamp.getString(btn6pic,"")==null)
				{
					imagebtn6.setImageDrawable(null);
				}else{
				imagebtn6.setImageURI(Uri.parse(lamp.getString(btn6pic,"")));
				}
				
				if(lamp.getString(btn7pic,"")==null)
				{
					imagebtn7.setImageDrawable(null);
				}else{
				imagebtn7.setImageURI(Uri.parse(lamp.getString(btn7pic,"")));
				}
				
				if(lamp.getString(btn8pic,"")==null)
				{
				imagebtn8.setImageDrawable(null);
				}else{
				imagebtn8.setImageURI(Uri.parse(lamp.getString(btn8pic,"")));
				}
				
				if(lamp.getString(btn9pic,"")==null)
				{
				imagebtn9.setImageDrawable(null);
				}else{
				imagebtn9.setImageURI(Uri.parse(lamp.getString(btn9pic,"")));
				}
				
				if(lamp.getString(btn10pic,"")==null)
				{
				imagebtn10.setImageDrawable(null);
				}else{
				imagebtn10.setImageURI(Uri.parse(lamp.getString(btn10pic,"")));
				}
				
				if(lamp.getString(btn11pic,"")==null)
				{
				imagebtn11.setImageDrawable(null);
				}else{
				imagebtn11.setImageURI(Uri.parse(lamp.getString(btn11pic,"")));
				}
				
				if(lamp.getString(btn12pic,"")==null)
				{
				imagebtn12.setImageDrawable(null);
				}else{
				imagebtn12.setImageURI(Uri.parse(lamp.getString(btn12pic,"")));
				}

    		readData(8);
    		break;
    		
    		
    		//marco5
    		case R.id.marcoset5:
    		// TODO Auto-generated catch block
    		setContentView(R.layout.marcoset4);

    		marcoset4name = (EditText) findViewById(R.id.marcoset4name);
    		marco4setbtn = (EditText) findViewById(R.id.marco4setbtn);
    		mButton01 = (Button) findViewById(R.id.btn01);
			mButton02 = (Button) findViewById(R.id.btn02);
			mButton03 = (Button) findViewById(R.id.btn03);
			mButton04 = (Button) findViewById(R.id.btn04);
			mButton05 = (Button) findViewById(R.id.btn05);
			mButton06 = (Button) findViewById(R.id.btn06);
			mButton07 = (Button) findViewById(R.id.btn07);
			mButton08 = (Button) findViewById(R.id.btn08);
			mButton09 = (Button) findViewById(R.id.btn09);
			mButton10 = (Button) findViewById(R.id.btn10);
			mButton11 = (Button) findViewById(R.id.btn11);
			mButton12 = (Button) findViewById(R.id.btn12);
			mButton13 = (Button) findViewById(R.id.btn13);
			mButton14 = (Button) findViewById(R.id.btn14);
			mButton15 = (Button) findViewById(R.id.btn15);
			mButton16 = (Button) findViewById(R.id.btn16);
			mButton17 = (Button) findViewById(R.id.btn17);
			mButton18 = (Button) findViewById(R.id.btn18);
			mButton19 = (Button) findViewById(R.id.btn19);
			mButton20 = (Button) findViewById(R.id.btn20);
			mButton21 = (Button) findViewById(R.id.btn21);
			mButton22 = (Button) findViewById(R.id.btn22);
			mButton23 = (Button) findViewById(R.id.btn23);
			mButton24 = (Button) findViewById(R.id.btn24);
//			mButton25 = (Button) findViewById(R.id.btn25);
//			mButton26 = (Button) findViewById(R.id.btn26);
//			mButton27 = (Button) findViewById(R.id.btn27);
//			mButton28 = (Button) findViewById(R.id.btn28);
//			mButton29 = (Button) findViewById(R.id.btn29);
//			mButton30 = (Button) findViewById(R.id.btn30);
//			mButton31 = (Button) findViewById(R.id.btn31);
//			mButton32 = (Button) findViewById(R.id.btn32);
			mButton33 = (Button) findViewById(R.id.btn33);
//			mButton34 = (Button) findViewById(R.id.btn34);
			mButton35 = (Button) findViewById(R.id.btn35);
//			mButton36 = (Button) findViewById(R.id.btn36);
//			mButton37 = (Button) findViewById(R.id.btn37);
//			mButton38 = (Button) findViewById(R.id.btn38);
//			mButton39 = (Button) findViewById(R.id.btn39);
//			mButton40 = (Button) findViewById(R.id.btn40);
//			mButton41 = (Button) findViewById(R.id.btn41);
			mButton42 = (Button) findViewById(R.id.btn42);
			mButton43 = (Button) findViewById(R.id.btn43);
			mButton44 = (Button) findViewById(R.id.btn44);
			mButton45 = (Button) findViewById(R.id.btn45);
			mButton46 = (Button) findViewById(R.id.btn46);
			mButton47 = (Button) findViewById(R.id.btn47);
			mButton48 = (Button) findViewById(R.id.btn48);
			mButton49 = (Button) findViewById(R.id.btn49);
			mButton50 = (Button) findViewById(R.id.btn50);
			mButton51 = (Button) findViewById(R.id.btn51);
			mButton52 = (Button) findViewById(R.id.btn52);
			mButton53 = (Button) findViewById(R.id.btn53);
//			mButton54 = (Button) findViewById(R.id.btn54);
//			mButton55 = (Button) findViewById(R.id.btn55);
//			mButton56 = (Button) findViewById(R.id.btn56);
//			mButton57 = (Button) findViewById(R.id.btn57);
//			mButton58 = (Button) findViewById(R.id.btn58);
//			mButton59 = (Button) findViewById(R.id.btn59);
//			mButton60 = (Button) findViewById(R.id.btn60);
//			mButton61 = (Button) findViewById(R.id.btn61);
//			mButton62 = (Button) findViewById(R.id.btn62);
			mButton63 = (Button) findViewById(R.id.btn63);
			mButton64 = (Button) findViewById(R.id.btn64);
			mButton65 = (Button) findViewById(R.id.btn65);
			mButton66 = (Button) findViewById(R.id.btn66);
			mButton67 = (Button) findViewById(R.id.btn67);
			mButton68 = (Button) findViewById(R.id.btn68);
//			mButton69 = (Button) findViewById(R.id.btn69);
//			mButton70 = (Button) findViewById(R.id.btn70);
//			mButton71 = (Button) findViewById(R.id.btn71);
//			mButton72 = (Button) findViewById(R.id.btn72);
//			mButton73 = (Button) findViewById(R.id.btn73);
//			mButton74 = (Button) findViewById(R.id.btn74);
//			mButton75 = (Button) findViewById(R.id.btn75);
//			mButton76 = (Button) findViewById(R.id.btn76);
//			mButton77 = (Button) findViewById(R.id.btn77);
			mButton78 = (Button) findViewById(R.id.btn78);
			mButton79 = (Button) findViewById(R.id.btn79);
			mButton80 = (Button) findViewById(R.id.btn80);
			mButton81 = (Button) findViewById(R.id.btn81);
			mButton82 = (Button) findViewById(R.id.btn82);
			mButton83 = (Button) findViewById(R.id.btn83);
			//mButton84 = (Button) findViewById(R.id.btn84);
			//mButton85 = (Button) findViewById(R.id.btn85);
			//mButton86 = (Button) findViewById(R.id.btn86);
			//mButton87 = (Button) findViewById(R.id.btn87);
			//mButton88 = (Button) findViewById(R.id.btn88);
			//mButton89 = (Button) findViewById(R.id.btn89);
			//mButton90 = (Button) findViewById(R.id.btn90);
			//mButton91 = (Button) findViewById(R.id.btn91);
			//mButton92 = (Button) findViewById(R.id.btn92);
			mButton93 = (Button) findViewById(R.id.btn93);
			mButton94 = (Button) findViewById(R.id.btn94);
			mButton95 = (Button) findViewById(R.id.btn95);
			mButton96 = (Button) findViewById(R.id.btn96);
			mButton97 = (Button) findViewById(R.id.btn97);
			mButton98 = (Button) findViewById(R.id.btn98);
//			mButton99 = (Button) findViewById(R.id.btn99);
//			mButton100 = (Button) findViewById(R.id.btn100);
//			mButton101 = (Button) findViewById(R.id.btn101);
//			mButton102 = (Button) findViewById(R.id.btn102);
//			mButton103 = (Button) findViewById(R.id.btn103);
//			mButton104 = (Button) findViewById(R.id.btn104);
//			mButton105 = (Button) findViewById(R.id.btn105);
//			mButton106 = (Button) findViewById(R.id.btn106);
//			mButton107 = (Button) findViewById(R.id.btn107);
//			mButton108 = (Button) findViewById(R.id.btn108);
//			mButton109 = (Button) findViewById(R.id.btn109);
//			mButton110 = (Button) findViewById(R.id.btn110);
//			mButton111 = (Button) findViewById(R.id.btn111);
//			mButton112 = (Button) findViewById(R.id.btn112);
			mButton153 = (Button) findViewById(R.id.btn153);
			mButton154 = (Button) findViewById(R.id.btn154);
			mButton155 = (Button) findViewById(R.id.btn155);
			mButton156 = (Button) findViewById(R.id.btn156);
			
			
			findViewById(R.id.marcoset4ok).setOnClickListener(new ButtonListener());
			
			findViewById(R.id.s21).setOnClickListener(new ButtonListener());
			findViewById(R.id.s22).setOnClickListener(new ButtonListener());
			findViewById(R.id.s23).setOnClickListener(new ButtonListener());
			findViewById(R.id.s24).setOnClickListener(new ButtonListener());
			findViewById(R.id.s25).setOnClickListener(new ButtonListener());
			

			lamp = getSharedPreferences(lampdata,0);
			  mButton01.setText(lamp.getString(btn01name,""));
			  mButton02.setText(lamp.getString(btn02name,""));
			  mButton03.setText(lamp.getString(btn03name,""));
			  mButton04.setText(lamp.getString(btn04name,""));
			  mButton05.setText(lamp.getString(btn05name,""));
			  mButton06.setText(lamp.getString(btn06name,""));
			  mButton07.setText(lamp.getString(btn07name,""));
			  mButton08.setText(lamp.getString(btn08name,""));
			  mButton09.setText(lamp.getString(btn09name,""));
			  mButton10.setText(lamp.getString(btn10name,""));
			  mButton11.setText(lamp.getString(btn11name,""));
			  mButton12.setText(lamp.getString(btn12name,""));
				    
			setting = getSharedPreferences(custom1data,0);
			
			cus1name=(TextView)findViewById(R.id.cus1name);
      	cus1name.setText(setting.getString(custom1name,""));
//			mButton54.setText(setting.getString(btn54name, ""));
//			mButton55.setText(setting.getString(btn55name, ""));
//			mButton56.setText(setting.getString(btn56name, ""));
//			mButton57.setText(setting.getString(btn57name, ""));
//			mButton58.setText(setting.getString(btn58name, ""));
//			mButton59.setText(setting.getString(btn59name, ""));
//			mButton60.setText(setting.getString(btn60name, ""));
//			mButton61.setText(setting.getString(btn61name, ""));
//			mButton62.setText(setting.getString(btn62name, ""));
			mButton63.setText(setting.getString(btn63name, ""));
			mButton64.setText(setting.getString(btn64name, ""));
			mButton65.setText(setting.getString(btn65name, ""));
			mButton66.setText(setting.getString(btn66name, ""));
			mButton67.setText(setting.getString(btn67name, ""));
			mButton68.setText(setting.getString(btn68name, ""));
//			mButton99.setText(setting.getString(btn99name, ""));
//			mButton100.setText(setting.getString(btn100name, ""));
//			mButton101.setText(setting.getString(btn101name, ""));
//			mButton102.setText(setting.getString(btn102name, ""));
//			mButton103.setText(setting.getString(btn103name, ""));
//			mButton104.setText(setting.getString(btn104name, ""));

			settingb = getSharedPreferences(custom2data,0);
			
			cus2name=(TextView)findViewById(R.id.cus2name);
			cus2name.setText(settingb.getString(custom2name,""));  
//			 mButton69.setText(settingb.getString(btn69name, ""));
//			 mButton70.setText(settingb.getString(btn70name, ""));
//			 mButton71.setText(settingb.getString(btn71name, ""));
//			 mButton72.setText(settingb.getString(btn72name, ""));
//			 mButton73.setText(settingb.getString(btn73name, ""));
//			 mButton74.setText(settingb.getString(btn74name, ""));
//			 mButton75.setText(settingb.getString(btn75name, ""));
//			 mButton76.setText(settingb.getString(btn76name, ""));
//			 mButton77.setText(settingb.getString(btn77name, ""));
			 mButton78.setText(settingb.getString(btn78name, ""));
			 mButton79.setText(settingb.getString(btn79name, ""));
			 mButton80.setText(settingb.getString(btn80name, ""));
			 mButton81.setText(settingb.getString(btn81name, ""));
			 mButton82.setText(settingb.getString(btn82name, ""));
			 mButton83.setText(settingb.getString(btn83name, ""));
//			 mButton105.setText(settingb.getString(btn105name, ""));
//			 mButton106.setText(settingb.getString(btn106name, ""));
//			 mButton107.setText(settingb.getString(btn107name, ""));
//			 mButton108.setText(settingb.getString(btn108name, ""));
//			 mButton109.setText(settingb.getString(btn109name, ""));
//			 mButton110.setText(settingb.getString(btn110name, ""));

			  setting2 = getSharedPreferences(custom3data,0);
			  
			  cus3name=(TextView)findViewById(R.id.cus3name);
        	cus3name.setText(setting2.getString(custom3name,"")); 
			// mButton84.setText(setting2.getString(btn84name, ""));
			 //mButton85.setText(setting2.getString(btn85name, ""));
			 //mButton86.setText(setting2.getString(btn86name, ""));
			 //mButton87.setText(setting2.getString(btn87name, ""));
			 //mButton88.setText(setting2.getString(btn88name, ""));
			 //mButton89.setText(setting2.getString(btn89name, ""));
			 //mButton90.setText(setting2.getString(btn90name, ""));
			 //mButton91.setText(setting2.getString(btn91name, ""));
			 //mButton92.setText(setting2.getString(btn92name, ""));
			 mButton93.setText(setting2.getString(btn93name, ""));
			 mButton94.setText(setting2.getString(btn94name, ""));
			 mButton95.setText(setting2.getString(btn95name, ""));
			 mButton96.setText(setting2.getString(btn96name, ""));
			 mButton97.setText(setting2.getString(btn97name, ""));
			 mButton98.setText(setting2.getString(btn98name, ""));
			 //mButton111.setText(setting2.getString(btn111name, ""));
			 //mButton112.setText(setting2.getString(btn112name, ""));
			 //mButton113.setText(setting2.getString(btn113name, ""));
			 //mButton114.setText(setting2.getString(btn114name, ""));
			 //mButton115.setText(setting2.getString(btn115name, ""));
			 //mButton116.setText(setting2.getString(btn116name, ""));
			 
			 airseting = getSharedPreferences(airdata,0);
           mButton17.setText(airseting.getString(btn17name,""));
		        mButton18.setText(airseting.getString(btn18name,""));
		        mButton19.setText(airseting.getString(btn19name,""));
		        mButton20.setText(airseting.getString(btn20name,""));
		        mButton21.setText(airseting.getString(btn21name,""));
		        mButton22.setText(airseting.getString(btn22name,""));

       	
		        lamp = getSharedPreferences(lampdata,0);
				imagebtn1 = (ImageView) findViewById(R.id.imagebtn1);
				imagebtn2 = (ImageView) findViewById(R.id.imagebtn2);
				imagebtn3 = (ImageView) findViewById(R.id.imagebtn3);
				imagebtn4 = (ImageView) findViewById(R.id.imagebtn4);
				imagebtn5 = (ImageView) findViewById(R.id.imagebtn5);
				imagebtn6 = (ImageView) findViewById(R.id.imagebtn6);
				imagebtn7 = (ImageView) findViewById(R.id.imagebtn7);
				imagebtn8 = (ImageView) findViewById(R.id.imagebtn8);
				imagebtn9 = (ImageView) findViewById(R.id.imagebtn9);
				imagebtn10 = (ImageView) findViewById(R.id.imagebtn10);
				imagebtn11 = (ImageView) findViewById(R.id.imagebtn11);
				imagebtn12 = (ImageView) findViewById(R.id.imagebtn12);
				
				
				
				if(lamp.getString(btn1pic,"")==null)
				{
				imagebtn1.setImageDrawable(null);
				}else{
				imagebtn1.setImageURI(Uri.parse(lamp.getString(btn1pic,"")));
				Log.e("uri1", lamp.getString(btn1pic,""));
				}
				
				if(lamp.getString(btn2pic,"")==null)
				{
				imagebtn2.setImageDrawable(null);
				}else{
				imagebtn2.setImageURI(Uri.parse(lamp.getString(btn2pic,"")));
				}
				
				if(lamp.getString(btn3pic,"")==null)
				{
				imagebtn3.setImageDrawable(null);
				}else{
				imagebtn3.setImageURI(Uri.parse(lamp.getString(btn3pic,"")));
				}
				
				if(lamp.getString(btn4pic,"")==null)
				{
				imagebtn4.setImageDrawable(null);
				}else{
				imagebtn4.setImageURI(Uri.parse(lamp.getString(btn4pic,"")));
				}
				
				if(lamp.getString(btn5pic,"")==null)
				{
					imagebtn5.setImageDrawable(null);
				}else{
				imagebtn5.setImageURI(Uri.parse(lamp.getString(btn5pic,"")));
				}
				
				if(lamp.getString(btn6pic,"")==null)
				{
					imagebtn6.setImageDrawable(null);
				}else{
				imagebtn6.setImageURI(Uri.parse(lamp.getString(btn6pic,"")));
				}
				
				if(lamp.getString(btn7pic,"")==null)
				{
					imagebtn7.setImageDrawable(null);
				}else{
				imagebtn7.setImageURI(Uri.parse(lamp.getString(btn7pic,"")));
				}
				
				if(lamp.getString(btn8pic,"")==null)
				{
				imagebtn8.setImageDrawable(null);
				}else{
				imagebtn8.setImageURI(Uri.parse(lamp.getString(btn8pic,"")));
				}
				
				if(lamp.getString(btn9pic,"")==null)
				{
				imagebtn9.setImageDrawable(null);
				}else{
				imagebtn9.setImageURI(Uri.parse(lamp.getString(btn9pic,"")));
				}
				
				if(lamp.getString(btn10pic,"")==null)
				{
				imagebtn10.setImageDrawable(null);
				}else{
				imagebtn10.setImageURI(Uri.parse(lamp.getString(btn10pic,"")));
				}
				
				if(lamp.getString(btn11pic,"")==null)
				{
				imagebtn11.setImageDrawable(null);
				}else{
				imagebtn11.setImageURI(Uri.parse(lamp.getString(btn11pic,"")));
				}
				
				if(lamp.getString(btn12pic,"")==null)
				{
				imagebtn12.setImageDrawable(null);
				}else{
				imagebtn12.setImageURI(Uri.parse(lamp.getString(btn12pic,"")));
				}

    		readData(9);
    		break;
    		
    		
    		//marco6
    		case R.id.marcoset6:
    		// TODO Auto-generated catch block
    		setContentView(R.layout.marcoset5);

    		marcoset5name = (EditText) findViewById(R.id.marcoset5name);
    		marco5setbtn = (EditText) findViewById(R.id.marco5setbtn);
    		mButton01 = (Button) findViewById(R.id.btn01);
			mButton02 = (Button) findViewById(R.id.btn02);
			mButton03 = (Button) findViewById(R.id.btn03);
			mButton04 = (Button) findViewById(R.id.btn04);
			mButton05 = (Button) findViewById(R.id.btn05);
			mButton06 = (Button) findViewById(R.id.btn06);
			mButton07 = (Button) findViewById(R.id.btn07);
			mButton08 = (Button) findViewById(R.id.btn08);
			mButton09 = (Button) findViewById(R.id.btn09);
			mButton10 = (Button) findViewById(R.id.btn10);
			mButton11 = (Button) findViewById(R.id.btn11);
			mButton12 = (Button) findViewById(R.id.btn12);
			mButton13 = (Button) findViewById(R.id.btn13);
			mButton14 = (Button) findViewById(R.id.btn14);
			mButton15 = (Button) findViewById(R.id.btn15);
			mButton16 = (Button) findViewById(R.id.btn16);
			mButton17 = (Button) findViewById(R.id.btn17);
			mButton18 = (Button) findViewById(R.id.btn18);
			mButton19 = (Button) findViewById(R.id.btn19);
			mButton20 = (Button) findViewById(R.id.btn20);
			mButton21 = (Button) findViewById(R.id.btn21);
			mButton22 = (Button) findViewById(R.id.btn22);
			mButton23 = (Button) findViewById(R.id.btn23);
			mButton24 = (Button) findViewById(R.id.btn24);
//			mButton25 = (Button) findViewById(R.id.btn25);
//			mButton26 = (Button) findViewById(R.id.btn26);
//			mButton27 = (Button) findViewById(R.id.btn27);
//			mButton28 = (Button) findViewById(R.id.btn28);
//			mButton29 = (Button) findViewById(R.id.btn29);
//			mButton30 = (Button) findViewById(R.id.btn30);
//			mButton31 = (Button) findViewById(R.id.btn31);
//			mButton32 = (Button) findViewById(R.id.btn32);
			mButton33 = (Button) findViewById(R.id.btn33);
//			mButton34 = (Button) findViewById(R.id.btn34);
			mButton35 = (Button) findViewById(R.id.btn35);
//			mButton36 = (Button) findViewById(R.id.btn36);
//			mButton37 = (Button) findViewById(R.id.btn37);
//			mButton38 = (Button) findViewById(R.id.btn38);
//			mButton39 = (Button) findViewById(R.id.btn39);
//			mButton40 = (Button) findViewById(R.id.btn40);
//			mButton41 = (Button) findViewById(R.id.btn41);
			mButton42 = (Button) findViewById(R.id.btn42);
			mButton43 = (Button) findViewById(R.id.btn43);
			mButton44 = (Button) findViewById(R.id.btn44);
			mButton45 = (Button) findViewById(R.id.btn45);
			mButton46 = (Button) findViewById(R.id.btn46);
			mButton47 = (Button) findViewById(R.id.btn47);
			mButton48 = (Button) findViewById(R.id.btn48);
			mButton49 = (Button) findViewById(R.id.btn49);
			mButton50 = (Button) findViewById(R.id.btn50);
			mButton51 = (Button) findViewById(R.id.btn51);
			mButton52 = (Button) findViewById(R.id.btn52);
			mButton53 = (Button) findViewById(R.id.btn53);
//			mButton54 = (Button) findViewById(R.id.btn54);
//			mButton55 = (Button) findViewById(R.id.btn55);
//			mButton56 = (Button) findViewById(R.id.btn56);
//			mButton57 = (Button) findViewById(R.id.btn57);
//			mButton58 = (Button) findViewById(R.id.btn58);
//			mButton59 = (Button) findViewById(R.id.btn59);
//			mButton60 = (Button) findViewById(R.id.btn60);
//			mButton61 = (Button) findViewById(R.id.btn61);
//			mButton62 = (Button) findViewById(R.id.btn62);
			mButton63 = (Button) findViewById(R.id.btn63);
			mButton64 = (Button) findViewById(R.id.btn64);
			mButton65 = (Button) findViewById(R.id.btn65);
			mButton66 = (Button) findViewById(R.id.btn66);
			mButton67 = (Button) findViewById(R.id.btn67);
			mButton68 = (Button) findViewById(R.id.btn68);
//			mButton69 = (Button) findViewById(R.id.btn69);
//			mButton70 = (Button) findViewById(R.id.btn70);
//			mButton71 = (Button) findViewById(R.id.btn71);
//			mButton72 = (Button) findViewById(R.id.btn72);
//			mButton73 = (Button) findViewById(R.id.btn73);
//			mButton74 = (Button) findViewById(R.id.btn74);
//			mButton75 = (Button) findViewById(R.id.btn75);
//			mButton76 = (Button) findViewById(R.id.btn76);
//			mButton77 = (Button) findViewById(R.id.btn77);
			mButton78 = (Button) findViewById(R.id.btn78);
			mButton79 = (Button) findViewById(R.id.btn79);
			mButton80 = (Button) findViewById(R.id.btn80);
			mButton81 = (Button) findViewById(R.id.btn81);
			mButton82 = (Button) findViewById(R.id.btn82);
			mButton83 = (Button) findViewById(R.id.btn83);
			//mButton84 = (Button) findViewById(R.id.btn84);
			//mButton85 = (Button) findViewById(R.id.btn85);
			//mButton86 = (Button) findViewById(R.id.btn86);
			//mButton87 = (Button) findViewById(R.id.btn87);
			//mButton88 = (Button) findViewById(R.id.btn88);
			//mButton89 = (Button) findViewById(R.id.btn89);
			//mButton90 = (Button) findViewById(R.id.btn90);
			//mButton91 = (Button) findViewById(R.id.btn91);
			//mButton92 = (Button) findViewById(R.id.btn92);
			mButton93 = (Button) findViewById(R.id.btn93);
			mButton94 = (Button) findViewById(R.id.btn94);
			mButton95 = (Button) findViewById(R.id.btn95);
			mButton96 = (Button) findViewById(R.id.btn96);
			mButton97 = (Button) findViewById(R.id.btn97);
			mButton98 = (Button) findViewById(R.id.btn98);
//			mButton99 = (Button) findViewById(R.id.btn99);
//			mButton100 = (Button) findViewById(R.id.btn100);
//			mButton101 = (Button) findViewById(R.id.btn101);
//			mButton102 = (Button) findViewById(R.id.btn102);
//			mButton103 = (Button) findViewById(R.id.btn103);
//			mButton104 = (Button) findViewById(R.id.btn104);
//			mButton105 = (Button) findViewById(R.id.btn105);
//			mButton106 = (Button) findViewById(R.id.btn106);
//			mButton107 = (Button) findViewById(R.id.btn107);
//			mButton108 = (Button) findViewById(R.id.btn108);
//			mButton109 = (Button) findViewById(R.id.btn109);
//			mButton110 = (Button) findViewById(R.id.btn110);
//			mButton111 = (Button) findViewById(R.id.btn111);
//			mButton112 = (Button) findViewById(R.id.btn112);
			mButton153 = (Button) findViewById(R.id.btn153);
			mButton154 = (Button) findViewById(R.id.btn154);
			mButton155 = (Button) findViewById(R.id.btn155);
			mButton156 = (Button) findViewById(R.id.btn156);
			
			
			findViewById(R.id.marcoset5ok).setOnClickListener(new ButtonListener());
			
			findViewById(R.id.s26).setOnClickListener(new ButtonListener());
			findViewById(R.id.s27).setOnClickListener(new ButtonListener());
			findViewById(R.id.s28).setOnClickListener(new ButtonListener());
			findViewById(R.id.s29).setOnClickListener(new ButtonListener());
			findViewById(R.id.s30).setOnClickListener(new ButtonListener());
			

			lamp = getSharedPreferences(lampdata,0);
			  mButton01.setText(lamp.getString(btn01name,""));
			  mButton02.setText(lamp.getString(btn02name,""));
			  mButton03.setText(lamp.getString(btn03name,""));
			  mButton04.setText(lamp.getString(btn04name,""));
			  mButton05.setText(lamp.getString(btn05name,""));
			  mButton06.setText(lamp.getString(btn06name,""));
			  mButton07.setText(lamp.getString(btn07name,""));
			  mButton08.setText(lamp.getString(btn08name,""));
			  mButton09.setText(lamp.getString(btn09name,""));
			  mButton10.setText(lamp.getString(btn10name,""));
			  mButton11.setText(lamp.getString(btn11name,""));
			  mButton12.setText(lamp.getString(btn12name,""));
				    
			setting = getSharedPreferences(custom1data,0);
			
			cus1name=(TextView)findViewById(R.id.cus1name);
      	cus1name.setText(setting.getString(custom1name,""));
//			mButton54.setText(setting.getString(btn54name, ""));
//			mButton55.setText(setting.getString(btn55name, ""));
//			mButton56.setText(setting.getString(btn56name, ""));
//			mButton57.setText(setting.getString(btn57name, ""));
//			mButton58.setText(setting.getString(btn58name, ""));
//			mButton59.setText(setting.getString(btn59name, ""));
//			mButton60.setText(setting.getString(btn60name, ""));
//			mButton61.setText(setting.getString(btn61name, ""));
//			mButton62.setText(setting.getString(btn62name, ""));
			mButton63.setText(setting.getString(btn63name, ""));
			mButton64.setText(setting.getString(btn64name, ""));
			mButton65.setText(setting.getString(btn65name, ""));
			mButton66.setText(setting.getString(btn66name, ""));
			mButton67.setText(setting.getString(btn67name, ""));
			mButton68.setText(setting.getString(btn68name, ""));
//			mButton99.setText(setting.getString(btn99name, ""));
//			mButton100.setText(setting.getString(btn100name, ""));
//			mButton101.setText(setting.getString(btn101name, ""));
//			mButton102.setText(setting.getString(btn102name, ""));
//			mButton103.setText(setting.getString(btn103name, ""));
//			mButton104.setText(setting.getString(btn104name, ""));

			settingb = getSharedPreferences(custom2data,0);
			
			cus2name=(TextView)findViewById(R.id.cus2name);
			cus2name.setText(settingb.getString(custom2name,""));  
//			 mButton69.setText(settingb.getString(btn69name, ""));
//			 mButton70.setText(settingb.getString(btn70name, ""));
//			 mButton71.setText(settingb.getString(btn71name, ""));
//			 mButton72.setText(settingb.getString(btn72name, ""));
//			 mButton73.setText(settingb.getString(btn73name, ""));
//			 mButton74.setText(settingb.getString(btn74name, ""));
//			 mButton75.setText(settingb.getString(btn75name, ""));
//			 mButton76.setText(settingb.getString(btn76name, ""));
//			 mButton77.setText(settingb.getString(btn77name, ""));
			 mButton78.setText(settingb.getString(btn78name, ""));
			 mButton79.setText(settingb.getString(btn79name, ""));
			 mButton80.setText(settingb.getString(btn80name, ""));
			 mButton81.setText(settingb.getString(btn81name, ""));
			 mButton82.setText(settingb.getString(btn82name, ""));
			 mButton83.setText(settingb.getString(btn83name, ""));
//			 mButton105.setText(settingb.getString(btn105name, ""));
//			 mButton106.setText(settingb.getString(btn106name, ""));
//			 mButton107.setText(settingb.getString(btn107name, ""));
//			 mButton108.setText(settingb.getString(btn108name, ""));
//			 mButton109.setText(settingb.getString(btn109name, ""));
//			 mButton110.setText(settingb.getString(btn110name, ""));

			  setting2 = getSharedPreferences(custom3data,0);
			  
			  cus3name=(TextView)findViewById(R.id.cus3name);
        	cus3name.setText(setting2.getString(custom3name,"")); 
			// mButton84.setText(setting2.getString(btn84name, ""));
			 //mButton85.setText(setting2.getString(btn85name, ""));
			 //mButton86.setText(setting2.getString(btn86name, ""));
			 //mButton87.setText(setting2.getString(btn87name, ""));
			 //mButton88.setText(setting2.getString(btn88name, ""));
			 //mButton89.setText(setting2.getString(btn89name, ""));
			 //mButton90.setText(setting2.getString(btn90name, ""));
			 //mButton91.setText(setting2.getString(btn91name, ""));
			 //mButton92.setText(setting2.getString(btn92name, ""));
			 mButton93.setText(setting2.getString(btn93name, ""));
			 mButton94.setText(setting2.getString(btn94name, ""));
			 mButton95.setText(setting2.getString(btn95name, ""));
			 mButton96.setText(setting2.getString(btn96name, ""));
			 mButton97.setText(setting2.getString(btn97name, ""));
			 mButton98.setText(setting2.getString(btn98name, ""));
			 //mButton111.setText(setting2.getString(btn111name, ""));
			 //mButton112.setText(setting2.getString(btn112name, ""));
			 //mButton113.setText(setting2.getString(btn113name, ""));
			 //mButton114.setText(setting2.getString(btn114name, ""));
			 //mButton115.setText(setting2.getString(btn115name, ""));
			 //mButton116.setText(setting2.getString(btn116name, ""));
			 
			 airseting = getSharedPreferences(airdata,0);
           mButton17.setText(airseting.getString(btn17name,""));
		        mButton18.setText(airseting.getString(btn18name,""));
		        mButton19.setText(airseting.getString(btn19name,""));
		        mButton20.setText(airseting.getString(btn20name,""));
		        mButton21.setText(airseting.getString(btn21name,""));
		        mButton22.setText(airseting.getString(btn22name,""));

       	
		        lamp = getSharedPreferences(lampdata,0);
				imagebtn1 = (ImageView) findViewById(R.id.imagebtn1);
				imagebtn2 = (ImageView) findViewById(R.id.imagebtn2);
				imagebtn3 = (ImageView) findViewById(R.id.imagebtn3);
				imagebtn4 = (ImageView) findViewById(R.id.imagebtn4);
				imagebtn5 = (ImageView) findViewById(R.id.imagebtn5);
				imagebtn6 = (ImageView) findViewById(R.id.imagebtn6);
				imagebtn7 = (ImageView) findViewById(R.id.imagebtn7);
				imagebtn8 = (ImageView) findViewById(R.id.imagebtn8);
				imagebtn9 = (ImageView) findViewById(R.id.imagebtn9);
				imagebtn10 = (ImageView) findViewById(R.id.imagebtn10);
				imagebtn11 = (ImageView) findViewById(R.id.imagebtn11);
				imagebtn12 = (ImageView) findViewById(R.id.imagebtn12);
			 

         	
         	lamp = getSharedPreferences(lampdata,0);
				
				imagebtn1 = (ImageView) findViewById(R.id.imagebtn1);
				imagebtn2 = (ImageView) findViewById(R.id.imagebtn2);
				imagebtn3 = (ImageView) findViewById(R.id.imagebtn3);
				imagebtn4 = (ImageView) findViewById(R.id.imagebtn4);
				imagebtn5 = (ImageView) findViewById(R.id.imagebtn5);
				imagebtn6 = (ImageView) findViewById(R.id.imagebtn6);
				imagebtn7 = (ImageView) findViewById(R.id.imagebtn7);
				imagebtn8 = (ImageView) findViewById(R.id.imagebtn8);
				imagebtn9 = (ImageView) findViewById(R.id.imagebtn9);
				imagebtn10 = (ImageView) findViewById(R.id.imagebtn10);
				imagebtn11 = (ImageView) findViewById(R.id.imagebtn11);
				imagebtn12 = (ImageView) findViewById(R.id.imagebtn12);
				
				
				
				if(lamp.getString(btn1pic,"")==null)
				{
				imagebtn1.setImageDrawable(null);
				}else{
				imagebtn1.setImageURI(Uri.parse(lamp.getString(btn1pic,"")));
				Log.e("uri1", lamp.getString(btn1pic,""));
				}
				
				if(lamp.getString(btn2pic,"")==null)
				{
				imagebtn2.setImageDrawable(null);
				}else{
				imagebtn2.setImageURI(Uri.parse(lamp.getString(btn2pic,"")));
				}
				
				if(lamp.getString(btn3pic,"")==null)
				{
				imagebtn3.setImageDrawable(null);
				}else{
				imagebtn3.setImageURI(Uri.parse(lamp.getString(btn3pic,"")));
				}
				
				if(lamp.getString(btn4pic,"")==null)
				{
				imagebtn4.setImageDrawable(null);
				}else{
				imagebtn4.setImageURI(Uri.parse(lamp.getString(btn4pic,"")));
				}
				
				if(lamp.getString(btn5pic,"")==null)
				{
					imagebtn5.setImageDrawable(null);
				}else{
				imagebtn5.setImageURI(Uri.parse(lamp.getString(btn5pic,"")));
				}
				
				if(lamp.getString(btn6pic,"")==null)
				{
					imagebtn6.setImageDrawable(null);
				}else{
				imagebtn6.setImageURI(Uri.parse(lamp.getString(btn6pic,"")));
				}
				
				if(lamp.getString(btn7pic,"")==null)
				{
					imagebtn7.setImageDrawable(null);
				}else{
				imagebtn7.setImageURI(Uri.parse(lamp.getString(btn7pic,"")));
				}
				
				if(lamp.getString(btn8pic,"")==null)
				{
				imagebtn8.setImageDrawable(null);
				}else{
				imagebtn8.setImageURI(Uri.parse(lamp.getString(btn8pic,"")));
				}
				
				if(lamp.getString(btn9pic,"")==null)
				{
				imagebtn9.setImageDrawable(null);
				}else{
				imagebtn9.setImageURI(Uri.parse(lamp.getString(btn9pic,"")));
				}
				
				if(lamp.getString(btn10pic,"")==null)
				{
				imagebtn10.setImageDrawable(null);
				}else{
				imagebtn10.setImageURI(Uri.parse(lamp.getString(btn10pic,"")));
				}
				
				if(lamp.getString(btn11pic,"")==null)
				{
				imagebtn11.setImageDrawable(null);
				}else{
				imagebtn11.setImageURI(Uri.parse(lamp.getString(btn11pic,"")));
				}
				
				if(lamp.getString(btn12pic,"")==null)
				{
				imagebtn12.setImageDrawable(null);
				}else{
				imagebtn12.setImageURI(Uri.parse(lamp.getString(btn12pic,"")));
				}

    		readData(10);

    		break;

    		
    			
    			case R.id.marco1 :
    			
    			marcoset = getSharedPreferences(marcosetdata,0);
    			String a = marcoset.getString(marcobtn,"");
    			sendmessage("@!"+a+"@");
    			
    			    		break;
    			    			
    			case R.id.marco2 :
    				
    				marco1set = getSharedPreferences(marcoset1data,0);
        			String b = marco1set.getString(marco1btn,"");
        			sendmessage("@"+"\""+b+"@");
        			
    						break;
	                 
    			case R.id.marco3 :
    				
    				marco2set = getSharedPreferences(marcoset2data,0);
        			String c = marco2set.getString(marco2btn,"");
        			sendmessage("@#"+c+"@");
    			
		    				break;
		    			
    			case R.id.marco4 :
    				
    				marco3set = getSharedPreferences(marcoset3data,0);
        			String d = marco3set.getString(marco3btn,"");
        			sendmessage("@$"+d+"@");
        		
    		    			break;
    		    			
    			case R.id.marco5 :
    				
    				marco4set = getSharedPreferences(marcoset4data,0);
        			String e = marco4set.getString(marco4btn,"");
        			sendmessage("@%"+e+"@");
    				
    		    			break;
    		    			
    			case R.id.marco6 :
    				
    				marco5set = getSharedPreferences(marcoset5data,0);
        			String f = marco5set.getString(marco5btn,"");
        			sendmessage("@&"+f+"@");
        			
    				break;

                case R.id.f1 :

                    marcoset = getSharedPreferences(marcosetdata,0);
                    //YILINEDIT

                    sendMacro(marcoset.getString(marcobtn, ""), handler_sendMacro);
                    //sendMessage("Ma");

                    break;

                case R.id.f2 :

                    marco1set = getSharedPreferences(marcoset1data,0);
                    //String b = marco1set.getString(marco1btn,"");
                    sendMacro(marco1set.getString(marco1btn, ""), handler_sendMacro);

                    break;

                case R.id.f3 :

                    marco2set = getSharedPreferences(marcoset2data,0);
                    //String c = marco2set.getString(marco2btn,"");
                    sendMacro(marco2set.getString(marco2btn, ""), handler_sendMacro);

                    break;

                case R.id.f4 :

                    marco3set = getSharedPreferences(marcoset3data,0);
                    //String d = marco3set.getString(marco3btn,"");
                    sendMacro(marco3set.getString(marco3btn, ""), handler_sendMacro);

                    break;

                case R.id.f5 :

                    marco4set = getSharedPreferences(marcoset4data,0);
                    //String e = marco4set.getString(marco4btn,"");
                    sendMacro(marco4set.getString(marco4btn, ""), handler_sendMacro);

                    break;

                case R.id.f6 :

                    marco5set = getSharedPreferences(marcoset5data,0);
                    //String f = marco5set.getString(marco5btn,"");
                    sendMacro(marco5set.getString(marco5btn, ""), handler_sendMacro);

                    break;
    									
    			     	
    			
        			case R.id.lampok :
    				
        				saveData(1);
        				Intent intentlamp = new Intent();
        				intentlamp.setClass(BluetoothChat.this, BluetoothChat.class);
        				finish();
        				startActivity(intentlamp);
        				
        				
    				
        						break;
    				
        			case R.id.curok :
    				
        				saveData(11);
        				Intent intentcur = new Intent();
        				intentcur.setClass(BluetoothChat.this, BluetoothChat.class);
        				finish();
        				startActivity(intentcur);
    				
        						break;
    				
    				
        			case R.id.set00 :
        			
        				saveData(2);
        				Intent intent = new Intent();
        				intent.setClass(BluetoothChat.this, BluetoothChat.class);
                    	finish();
                    	startActivity(intent);

         		    		break;
     		    
        			case R.id.set01 :
        			
        				saveData(3);
        				Intent intent1 = new Intent();
        				intent1.setClass(BluetoothChat.this, BluetoothChat.class);
        				finish();
        				startActivity(intent1);

         		    		break;
         		    	
    				case R.id.set02 :
        			
        			saveData(4);
        			Intent intent2 = new Intent();
                    intent2.setClass(BluetoothChat.this, BluetoothChat.class);
                    finish();
                    startActivity(intent2);

         		    	break;
       		
    			case R.id.marcosetok :
    				
    				saveData(5);

    				Intent intent3 = new Intent();
                    intent3.setClass(BluetoothChat.this, BluetoothChat.class);
                    finish();
                    startActivity(intent3);

    				break;
         		    	
    			case R.id.marcoset1ok :
    				
    				saveData(6);
    				Intent intent4 = new Intent();
                    intent4.setClass(BluetoothChat.this, BluetoothChat.class);
                    finish();
                    startActivity(intent4);
                    
                    break;
                    
    			case R.id.marcoset2ok :
    				
    				saveData(7);
    				Intent intent5 = new Intent();
                    intent5.setClass(BluetoothChat.this, BluetoothChat.class);
                    finish();
                    startActivity(intent5);
                    
                    break;
                    
    			case R.id.marcoset3ok :
    				
    				saveData(8);
    				Intent intent6 = new Intent();
                    intent6.setClass(BluetoothChat.this, BluetoothChat.class);
                    finish();
                    startActivity(intent6);
                    
                    break;
                    
    			case R.id.marcoset4ok :
    				
    				saveData(9);
    				Intent intent7 = new Intent();
                    intent7.setClass(BluetoothChat.this, BluetoothChat.class);
                    finish();
                    startActivity(intent7);
                    
                    break;
                    
    			case R.id.marcoset5ok :
    				
    				saveData(10);
    				Intent intent8 = new Intent();
                    intent8.setClass(BluetoothChat.this, BluetoothChat.class);
                    finish();
                    startActivity(intent8);
                    
                    break;
                    
    			case R.id.airok :
    				saveData(13);
    				Intent intent9 = new Intent();
                    intent9.setClass(BluetoothChat.this, BluetoothChat.class);
                    finish();
                    startActivity(intent9);
    				break;
                    
                    
    			case R.id.window2 :
    				String window2 = ("W+qa1");
                    sendmessage(window2);
    				break;
    				
    			case R.id.window3 :
    				String window3 = ("W+qb1");
                    sendmessage(window3);
    				break;
    				
    			case R.id.window4 :
    				String window4 = ("W+qc1");
                    sendmessage(window4);	
    				break;
    				
    			case R.id.door1 :
    				String door1 = ("W+sa1");
                    sendmessage(door1);	
    				break;
    				
    			case R.id.door2 :
    				String door2 = ("W+sb1");
                    sendmessage(door2);	
    				break;
    				
    			case R.id.door3 :
    				String door3 = ("W+sc1");
                    sendmessage(door3);	
    				break;
    			
    			case R.id.lampic1 :
    				lampeditpic1.setText("");
    				Intent lampic1 = new Intent();
    		        //開啟Pictures畫面Type設定為image
    				lampic1.setType("image/*");
    		        //使用Intent.ACTION_GET_CONTENT這個Action                                            
    				//會開啟選取圖檔視窗讓您選取手機內圖檔
    				lampic1.setAction(Intent.ACTION_GET_CONTENT); 
    		        //取得相片後返回本畫面
    		            startActivityForResult(lampic1,LAMP_PIC1);
    		            setResult(RESULT_OK, lampic1);
    		            
    				break;
    				
    			case R.id.lampic2 :
    				lampeditpic2.setText("");
    				Intent lampic2 = new Intent();
    		        //開啟Pictures畫面Type設定為image
    				lampic2.setType("image/*");
    		        //使用Intent.ACTION_GET_CONTENT這個Action                                            
    				//會開啟選取圖檔視窗讓您選取手機內圖檔
    				lampic2.setAction(Intent.ACTION_GET_CONTENT); 
    		        //取得相片後返回本畫面
    		            startActivityForResult(lampic2,LAMP_PIC2);
    		            setResult(RESULT_OK, lampic2);
    				break;
    				
    			case R.id.lampic3 :
    				lampeditpic3.setText("");
    				Intent lampic3 = new Intent();
    		        //開啟Pictures畫面Type設定為image
    				lampic3.setType("image/*");
    		        //使用Intent.ACTION_GET_CONTENT這個Action                                            
    				//會開啟選取圖檔視窗讓您選取手機內圖檔
    				lampic3.setAction(Intent.ACTION_GET_CONTENT); 
    		        //取得相片後返回本畫面
    		            startActivityForResult(lampic3,LAMP_PIC3);
    		            setResult(RESULT_OK, lampic3);
    				break;
    				
    			case R.id.lampic4 :
    				lampeditpic4.setText("");
    				Intent lampic4 = new Intent();
    		        //開啟Pictures畫面Type設定為image
    				lampic4.setType("image/*");
    		        //使用Intent.ACTION_GET_CONTENT這個Action                                            
    				//會開啟選取圖檔視窗讓您選取手機內圖檔
    				lampic4.setAction(Intent.ACTION_GET_CONTENT); 
    		        //取得相片後返回本畫面
    		            startActivityForResult(lampic4,LAMP_PIC4);
    		            setResult(RESULT_OK, lampic4);
    				break;
    				
    			case R.id.lampic5 :
    				lampeditpic5.setText("");
    				Intent lampic5 = new Intent();
    		        //開啟Pictures畫面Type設定為image
    				lampic5.setType("image/*");
    		        //使用Intent.ACTION_GET_CONTENT這個Action                                            
    				//會開啟選取圖檔視窗讓您選取手機內圖檔
    				lampic5.setAction(Intent.ACTION_GET_CONTENT); 
    		        //取得相片後返回本畫面
    		            startActivityForResult(lampic5,LAMP_PIC5);
    		            setResult(RESULT_OK, lampic5);
    				break;
    				
    			case R.id.lampic6 :
    				lampeditpic6.setText("");
    				Intent lampic6 = new Intent();
    		        //開啟Pictures畫面Type設定為image
    				lampic6.setType("image/*");
    		        //使用Intent.ACTION_GET_CONTENT這個Action                                            
    				//會開啟選取圖檔視窗讓您選取手機內圖檔
    				lampic6.setAction(Intent.ACTION_GET_CONTENT); 
    		        //取得相片後返回本畫面
    		            startActivityForResult(lampic6,LAMP_PIC6);
    		            setResult(RESULT_OK, lampic6);
    				break;
    				
    			case R.id.lampic7 :
    				lampeditpic7.setText("");
    				Intent lampic7 = new Intent();
    		        //開啟Pictures畫面Type設定為image
    				lampic7.setType("image/*");
    		        //使用Intent.ACTION_GET_CONTENT這個Action                                            
    				//會開啟選取圖檔視窗讓您選取手機內圖檔
    				lampic7.setAction(Intent.ACTION_GET_CONTENT); 
    		        //取得相片後返回本畫面
    		            startActivityForResult(lampic7,LAMP_PIC7);
    		            setResult(RESULT_OK, lampic7);
    				break;
    				
    			case R.id.lampic8 :
    				lampeditpic8.setText("");
    				Intent lampic8 = new Intent();
    		        //開啟Pictures畫面Type設定為image
    				lampic8.setType("image/*");
    		        //使用Intent.ACTION_GET_CONTENT這個Action                                            
    				//會開啟選取圖檔視窗讓您選取手機內圖檔
    				lampic8.setAction(Intent.ACTION_GET_CONTENT); 
    		        //取得相片後返回本畫面
    		            startActivityForResult(lampic8,LAMP_PIC8);
    		            setResult(RESULT_OK, lampic8);
    				break;
    				
    			case R.id.lampic9 :
    				lampeditpic9.setText("");
    				Intent lampic9 = new Intent();
    		        //開啟Pictures畫面Type設定為image
    				lampic9.setType("image/*");
    		        //使用Intent.ACTION_GET_CONTENT這個Action                                            
    				//會開啟選取圖檔視窗讓您選取手機內圖檔
    				lampic9.setAction(Intent.ACTION_GET_CONTENT); 
    		        //取得相片後返回本畫面
    		            startActivityForResult(lampic9,LAMP_PIC9);
    		            setResult(RESULT_OK, lampic9);
    				break;
    				
    			case R.id.lampic10 :
    				lampeditpic10.setText("");
    				Intent lampic10 = new Intent();
    		        //開啟Pictures畫面Type設定為image
    				lampic10.setType("image/*");
    		        //使用Intent.ACTION_GET_CONTENT這個Action                                            
    				//會開啟選取圖檔視窗讓您選取手機內圖檔
    				lampic10.setAction(Intent.ACTION_GET_CONTENT); 
    		        //取得相片後返回本畫面
    		            startActivityForResult(lampic10,LAMP_PIC10);
    		            setResult(RESULT_OK, lampic10);
    				break;
    				
    			case R.id.lampic11 :
    				lampeditpic11.setText("");
    				Intent lampic11 = new Intent();
    		        //開啟Pictures畫面Type設定為image
    				lampic11.setType("image/*");
    		        //使用Intent.ACTION_GET_CONTENT這個Action                                            
    				//會開啟選取圖檔視窗讓您選取手機內圖檔
    				lampic11.setAction(Intent.ACTION_GET_CONTENT); 
    		        //取得相片後返回本畫面
    		            startActivityForResult(lampic11,LAMP_PIC11);
    		            setResult(RESULT_OK, lampic11);
    				break;
    				
    			case R.id.lampic12 :
    				lampeditpic12.setText("");
    				Intent lampic12 = new Intent();
    		        //開啟Pictures畫面Type設定為image
    				lampic12.setType("image/*");
    		        //使用Intent.ACTION_GET_CONTENT這個Action                                            
    				//會開啟選取圖檔視窗讓您選取手機內圖檔
    				lampic12.setAction(Intent.ACTION_GET_CONTENT); 
    		        //取得相片後返回本畫面
    		            startActivityForResult(lampic12,LAMP_PIC12);
    		            setResult(RESULT_OK, lampic12);
    				break;
    				
    			case R.id.firstset :
    				setContentView(R.layout.firstset);
    				
    				f1edit = (EditText) findViewById(R.id.f1edit);
    				f2edit = (EditText) findViewById(R.id.f2edit);
    				f3edit = (EditText) findViewById(R.id.f3edit);
    				f4edit = (EditText) findViewById(R.id.f4edit);
    				f5edit = (EditText) findViewById(R.id.f5edit);
    				f6edit = (EditText) findViewById(R.id.f6edit);
    				findViewById(R.id.firstok).setOnClickListener(new ButtonListener());
    				
    				marcoset = getSharedPreferences(marcosetdata,0);
      				marco1set = getSharedPreferences(marcoset1data,0);
      				marco2set = getSharedPreferences(marcoset2data,0);
      				marco3set = getSharedPreferences(marcoset3data,0);
      				marco4set = getSharedPreferences(marcoset4data,0);
      				marco5set = getSharedPreferences(marcoset5data,0);
      				
      				f1edit.setText(marcoset.getString(marconame,""));
      				f2edit.setText(marco1set.getString(marco1name,""));
      				f3edit.setText(marco2set.getString(marco2name,""));
      				f4edit.setText(marco3set.getString(marco3name,""));
      				f5edit.setText(marco4set.getString(marco4name,""));
      				f6edit.setText(marco5set.getString(marco5name,""));
    				
    				break;
    				
    			case R.id.firstok :
    				saveData(12);
    				Intent intent10 = new Intent();
                    intent10.setClass(BluetoothChat.this, BluetoothChat.class);
                    finish();
                    startActivity(intent10);
    				break;
    				
    			case R.id.saveOpenCam :
    				Log.e(TAG, "R.id.saveOpenCam");
    				saveData(15);
    				Toast.makeText(BluetoothChat.this, "saved", Toast.LENGTH_LONG).show();
    				viewPager.setCurrentItem(0);
    			    break;
                case R.id.open_gcm:
                    saveData(16);
                    gcm_init();
                    break;
            			}
    				}
       	    	}
	//marco1 
	public void ClickHandler(View v){
		// TODO Auto-generated catch block
		
	switch(v.getId()){

        case R.id.marcosetok :
			saveData(5);
			Intent intent3 = new Intent();
            intent3.setClass(BluetoothChat.this, BluetoothChat.class);
            finish();

			startActivity(intent3);
		break;
		
	
		case R.id.s1 :
			String delay1 = ("'");
			marcosetbtn.append(delay1);
			break;
	
		case R.id.s2 :
			String delay2 = ("(");
			marcosetbtn.append(delay2);
			break;
			
		case R.id.s3 :
			String delay3 = (")");
			marcosetbtn.append(delay3);
			break;
			
		case R.id.s4 :
			String delay4 = ("*");
			marcosetbtn.append(delay4);
			break;
			
		case R.id.s5 :
			String delay5 = ("+");
			marcosetbtn.append(delay5);
			break;
	
		case R.id.btn01:
				
		if (count1 == 0 )
		{
		String view = ("a4");
		mButton01.setTextColor(Color.parseColor("#FF0000"));
		mButton01.setBackgroundResource(R.drawable.set_after_9);
		marcosetbtn.append(view);
		count1 = 1;
		}
        else if(count1 == 1)
        {
        mButton01.setTextColor(Color.parseColor("#ffffff"));
        mButton01.setBackgroundResource(R.drawable.set_befor_9);
        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("a4", "a5"));
        count1 = 2;
        }
        else if (count1 == 2)
        {
        	String btn01 = marcosetbtn.getText().toString();
        	
        	if(btn01.contains("a5'"))
        	{
        	marcosetbtn.setText(btn01.replaceFirst("a5'", ""));
            count1 = 0;
        	}
        	else if(btn01.contains("a5("))
        	{
        	marcosetbtn.setText(btn01.replaceFirst("a5(", ""));
            count1 = 0;
        	}
        	else if(btn01.contains("a5)"))
        	{
        	marcosetbtn.setText(btn01.replaceFirst("a5)", ""));
            count1 = 0;
        	}
        	else if(btn01.contains("a5*"))
        	{
        	marcosetbtn.setText(btn01.replaceFirst("a5*", ""));
            count1 = 0;
        	}
        	else if(btn01.contains("a5+"))
        	{
        	marcosetbtn.setText(btn01.replaceFirst("a5+", ""));
            count1 = 0;
        	}
        	else if(btn01.contains("a5"))
        	{
            marcosetbtn.setText(btn01.replaceFirst("a5", ""));        	
            count1 = 0;
        	}
        }
		
		break;
		
	case R.id.btn02:
		
		if (count2 == 0 )
		{
		mButton02.setTextColor(Color.parseColor("#FF0000"));
		mButton02.setBackgroundResource(R.drawable.set_after_9);
		marcosetbtn.append("b4");
		count2 = 1;
		}
        else if(count2 == 1)
        {
        mButton02.setTextColor(Color.parseColor("#ffffff"));
		mButton02.setBackgroundResource(R.drawable.set_befor_9);
        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("b4", "b5"));
        count2 = 2;
        }
        else if (count2 == 2)
        {
        	String btn02 = marcosetbtn.getText().toString();
        	
        	if(btn02.contains("b54"))
        	{
        	marcosetbtn.setText(btn02.replaceFirst("b54", ""));
        	count2 = 0;
        	}
        	else if(btn02.contains("b55"))
        	{
        	marcosetbtn.setText(btn02.replaceFirst("b55", ""));
        	count2 = 0;
        	}
        	else if(btn02.contains("b56"))
        	{
        	marcosetbtn.setText(btn02.replaceFirst("b56", ""));
        	count2 = 0;
        	}
        	else if(btn02.contains("b57"))
        	{
        	marcosetbtn.setText(btn02.replaceFirst("b57", ""));
        	count2 = 0;
        	}
        	else if(btn02.contains("b58"))
        	{
        	marcosetbtn.setText(btn02.replaceFirst("b58", ""));
        	count2 = 0;
        	}
        	else if(btn02.contains("b5"))
        	{
            marcosetbtn.setText(btn02.replaceFirst("b5", ""));        	
            count2 = 0;
        	}
        }
		break;	
		
	case R.id.btn03:
		if (count3 == 0 )
		{
		mButton03.setTextColor(Color.parseColor("#FF0000"));
		mButton03.setBackgroundResource(R.drawable.set_after_9);
		marcosetbtn.append("c4");
		count3 = 1;
		}
        else if(count3 == 1)
        {
        mButton03.setTextColor(Color.parseColor("#ffffff"));
		mButton03.setBackgroundResource(R.drawable.set_befor_9);
        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("c4", "c5"));
        count3 = 2;
        }
        else if (count3 == 2)
        {
        	
        	if(marcosetbtn.getText().toString().contains("c54"))
        	{
        	marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("c54", ""));
        	count3 = 0;
        	}
        	else if(marcosetbtn.getText().toString().contains("c55"))
        	{
        	marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("c55", ""));
        	count3 = 0;
        	}
        	else if(marcosetbtn.getText().toString().contains("c56"))
        	{
        	marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("c56", ""));
        	count3 = 0;
        	}
        	else if(marcosetbtn.getText().toString().contains("c57"))
        	{
        	marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("c57", ""));
        	count3 = 0;
        	}
        	else if(marcosetbtn.getText().toString().contains("c58"))
        	{
        	marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("c58", ""));
        	count3 = 0;
        	}
        	else if(marcosetbtn.getText().toString().contains("c5"))
        	{
            marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("c5", ""));        	
            count3 = 0;
        	}
        }
		break;	
		
	case R.id.btn04:
		if (count4 == 0 )
		{
		mButton04.setTextColor(Color.parseColor("#FF0000"));
		mButton04.setBackgroundResource(R.drawable.set_after_9);
		marcosetbtn.append("d4");
		count4 = 1;
		}
        else if(count4 == 1)
        {
        mButton04.setTextColor(Color.parseColor("#ffffff"));
		mButton04.setBackgroundResource(R.drawable.set_befor_9);
        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("d4", "d5"));
        count4 = 2;
        }
        else if (count4 == 2)
        {
        	
        	if(marcosetbtn.getText().toString().contains("d54"))
        	{
        	marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("d54", ""));
        	count4 = 0;
        	}
        	else if(marcosetbtn.getText().toString().contains("d55"))
        	{
        	marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("d55", ""));
        	count4 = 0;
        	}
        	else if(marcosetbtn.getText().toString().contains("d56"))
        	{
        	marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("d56", ""));
        	count4 = 0;
        	}
        	else if(marcosetbtn.getText().toString().contains("d57"))
        	{
        	marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("d57", ""));
        	count4 = 0;
        	}
        	else if(marcosetbtn.getText().toString().contains("d58"))
        	{
        	marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("d58", ""));
        	count4 = 0;
        	}
        	else if(marcosetbtn.getText().toString().contains("d5"))
        	{
            marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("d5", ""));        	
            count4 = 0;
        	}
        }
		break;	
		
	case R.id.btn05:
		if (count5 == 0 )
		{
		String view5 = ("e4");
		mButton05.setTextColor(Color.parseColor("#FF0000"));
		mButton05.setBackgroundResource(R.drawable.set_after_9);
		marcosetbtn.append(view5);
		count5 = 1;
		}
		else if(count5 == 1)
		{
		mButton05.setTextColor(Color.parseColor("#ffffff"));
		mButton05.setBackgroundResource(R.drawable.set_befor_9);
        String btn05 = marcosetbtn.getText().toString();
        String newa = btn05.replaceFirst("e4", "e5");
        marcosetbtn.setText(newa);
        count5 = 2;
    	}
		else if (count5 == 2)
        {
        String btn05 = marcosetbtn.getText().toString();
        String newa = btn05.replaceFirst("e5", "");
        marcosetbtn.setText(newa);
        	count5 = 0;
        }
		break;	
		
	case R.id.btn06:
		if (count6 == 0 )
		{
		String view6 = ("f4");
		mButton06.setTextColor(Color.parseColor("#FF0000"));
		mButton06.setBackgroundResource(R.drawable.set_after_9);
		marcosetbtn.append(view6);
		count6 = 1;
		}
		else if(count6 == 1)
		{
		mButton06.setTextColor(Color.parseColor("#ffffff"));
		mButton06.setBackgroundResource(R.drawable.set_befor_9);
        String btn06 = marcosetbtn.getText().toString();
        String newa = btn06.replaceFirst("f4", "f5");
        marcosetbtn.setText(newa);
        count6 = 2;
    	}
		else if (count6 == 2)
        {
        String btn06 = marcosetbtn.getText().toString();
        String newa = btn06.replaceFirst("f5", "");
        marcosetbtn.setText(newa);
        	count6 = 0;
        }
		break;
		
	case R.id.btn07:
		if (count7 == 0 )
		{
		String view7 = ("g4");
		mButton07.setTextColor(Color.parseColor("#FF0000"));
		mButton07.setBackgroundResource(R.drawable.set_after_9);
		marcosetbtn.append(view7);
		count7 = 1;
		}
		else if(count7 == 1)
		{
		mButton07.setTextColor(Color.parseColor("#ffffff"));
		mButton07.setBackgroundResource(R.drawable.set_befor_9);
        String btn07 = marcosetbtn.getText().toString();
        String newa = btn07.replaceFirst("g4", "g5");
        marcosetbtn.setText(newa);
        count7 = 2;
    	}
		else if (count7 == 2)
        {
        String btn07 = marcosetbtn.getText().toString();
        String newa = btn07.replaceFirst("g5", "");
        marcosetbtn.setText(newa);
        	count7 = 0;
        }
		break;
		
	case R.id.btn08:
		if (count8 == 0 )
		{
		String view8 = ("h4");
		mButton08.setTextColor(Color.parseColor("#FF0000"));
		mButton08.setBackgroundResource(R.drawable.set_after_9);
		marcosetbtn.append(view8);
		count8 = 1;
		}
		else if(count8 == 1)
		{
		mButton08.setTextColor(Color.parseColor("#ffffff"));
		mButton08.setBackgroundResource(R.drawable.set_befor_9);
        String btn08 = marcosetbtn.getText().toString();
        String newa = btn08.replaceFirst("h4", "h5");
        marcosetbtn.setText(newa);
        count8 = 2;
    	}
		else if (count8 == 2)
        {
        String btn08 = marcosetbtn.getText().toString();
        String newa = btn08.replaceFirst("h5", "");
        marcosetbtn.setText(newa);
        	count8 = 0;
        }
		break;	
		
	case R.id.btn09:
		if (count9 == 0 )
		{
		String view9 = ("i4");
		mButton09.setTextColor(Color.parseColor("#FF0000"));
		mButton09.setBackgroundResource(R.drawable.set_after_9);
		marcosetbtn.append(view9);
		count9 = 1;
		}
		else if(count9 == 1)
		{
		mButton09.setTextColor(Color.parseColor("#ffffff"));
		mButton09.setBackgroundResource(R.drawable.set_befor_9);
        String btn09 = marcosetbtn.getText().toString();
        String newa = btn09.replaceFirst("i4", "i5");
        marcosetbtn.setText(newa);
        count9 = 2;
    	}
		else if (count9 == 2)
        {
        String btn09 = marcosetbtn.getText().toString();
        String newa = btn09.replaceFirst("i5", "");
        marcosetbtn.setText(newa);
        	count9 = 0;
        }
		break;
		
	case R.id.btn10:
		if (count10 == 0 )
		{
		String view10 = ("j4");
		mButton10.setTextColor(Color.parseColor("#FF0000"));
		mButton10.setBackgroundResource(R.drawable.set_after_9);
		marcosetbtn.append(view10);
		count10 = 1;
		}
		else if(count10 == 1)
		{
		mButton10.setTextColor(Color.parseColor("#ffffff"));
		mButton10.setBackgroundResource(R.drawable.set_befor_9);
        String btn10 = marcosetbtn.getText().toString();
        String newa = btn10.replaceFirst("j4", "j5");
        marcosetbtn.setText(newa);
        count10 = 2;
    	}
		else if (count10 == 2)
        {
        String btn10 = marcosetbtn.getText().toString();
        String newa = btn10.replaceFirst("j5", "");
        marcosetbtn.setText(newa);
        	count10 = 0;
        }
		break;
		
	case R.id.btn11:
		if (count11 == 0 )
		{
		String view11 = ("k4");
		mButton11.setTextColor(Color.parseColor("#FF0000"));
		mButton11.setBackgroundResource(R.drawable.set_after_9);
		marcosetbtn.append(view11);
		count11 = 1;
		}
		else if(count11 == 1)
		{
		mButton11.setTextColor(Color.parseColor("#ffffff"));
		mButton11.setBackgroundResource(R.drawable.set_befor_9);
        String btn11 = marcosetbtn.getText().toString();
        String newa = btn11.replaceFirst("k4", "k5");
        marcosetbtn.setText(newa);
        count11 = 2;
    	}
		else if (count11 == 2)
        {
        String btn11 = marcosetbtn.getText().toString();
        String newa = btn11.replaceFirst("k5", "");
        marcosetbtn.setText(newa);
        	count11 = 0;
        }
		break;
		
	case R.id.btn12:
		if (count12 == 0 )
		{
		String view12 = ("l4");
		mButton12.setTextColor(Color.parseColor("#FF0000"));
		mButton12.setBackgroundResource(R.drawable.set_after_9);
		marcosetbtn.append(view12);
		count12 = 1;
		}
		else if(count12 == 1)
		{
		mButton12.setTextColor(Color.parseColor("#ffffff"));
		mButton12.setBackgroundResource(R.drawable.set_befor_9);
        String btn12 = marcosetbtn.getText().toString();
        String newa = btn12.replaceFirst("l4", "l5");
        marcosetbtn.setText(newa);
        count12 = 2;
    	}
		else if (count12 == 2)
        {
        String btn12 = marcosetbtn.getText().toString();
        String newa = btn12.replaceFirst("l5", "");
        marcosetbtn.setText(newa);
        	count12 = 0;
        }
		break;
		
	case R.id.btn13:
		String view13 = ("Aa");
		mButton13.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view13);
		
		break;
		
	case R.id.btn14:
		String view14 = ("Ab");
		mButton14.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view14);
        
		break;
		
	case R.id.btn15:
		String view15 = ("Ac");
		mButton15.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view15);
		break;
		
	case R.id.btn16:
		String view16 = ("Ad");
		mButton16.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view16);
		break;
		
	case R.id.btn17:
		String view17 = ("Ae");
		mButton17.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view17);
		break;
		
	case R.id.btn18:
		String view18 = ("Af");
		mButton18.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view18);
		break;
		
	case R.id.btn19:
		String view19 = ("Ag");
		mButton19.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view19);
		break;
		
	case R.id.btn20:
		String view20 = ("Ah");
		mButton20.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view20);
		break;
		
	case R.id.btn21:
		String view21 = ("Ai");
		mButton21.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view21);
		break;
		
	case R.id.btn22:
		String view22 = ("Aj");
		mButton22.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view22);
		break;
		
	case R.id.btn23:
		String view23 = ("Ak");
		mButton23.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view23);
		break;
		
	case R.id.btn24:
		String view24 = ("Al");
		mButton24.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view24);
		break;
		
//	case R.id.btn25:
//		String view25 = ("Am");
//		mButton25.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view25);
//		break;
//		
//	case R.id.btn26:
//		String view26 = ("An");
//		mButton26.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view26);
//		break;
//		
//	case R.id.btn27:
//		String view27 = ("Ao");
//		mButton27.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view27);
//		break;
//		
//	case R.id.btn28:
//		String view28 = ("Ap");
//		mButton28.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view28);
//		break;
//		
//	case R.id.btn29:
//		String view29 = ("Aq");
//		mButton29.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view29);
//		break;
//		
//	case R.id.btn30:
//		String view30 = ("Ar");
//		mButton30.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view30);
//		break;
//		
//	case R.id.btn31:
//		String view31 = ("As");
//		mButton31.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view31);
//		break;
//		
	//case R.id.btn32:
		//String view32 = ("At");
		//mButton32.setTextColor(Color.parseColor("#FF0000"));
		//marcosetbtn.append(view32);
		//break;
		
	case R.id.btn33:
		String view33 = ("Ta");
		mButton33.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view33);
		break;
		
//	case R.id.btn34:
//		String view34 = ("Tb");
//		mButton34.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view34);
//		break;
		
	case R.id.btn35:
		String view35 = ("Tc");
		mButton35.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view35);
		break;
		
//	case R.id.btn36:
//		String view36 = ("Td");
//		mButton36.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view36);
//		break;
//		
//	case R.id.btn37:
//		String view37 = ("Te");
//		mButton37.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view37);
//		break;
//		
//	case R.id.btn38:
//		String view38 = ("Tf");
//		mButton38.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view38);
//		break;
//		
//	case R.id.btn39:
//		String view39 = ("Tg");
//		mButton39.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view39);
//		break;
//		
//	case R.id.btn40:
//		String view40 = ("Th");
//		mButton40.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view40);
//		break;
//		
//	case R.id.btn41:
//		String view41 = ("Ti");
//		mButton41.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view41);
//		break;
		
	case R.id.btn42:
		String view42 = ("Tj");
		mButton42.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view42);
		break;
		
	case R.id.btn43:
		String view43 = ("Tk");
		mButton43.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view43);
		break;
		
	case R.id.btn44:
		String view44 = ("Tl");
		mButton44.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view44);
		break;
		
	case R.id.btn45:
		String view45 = ("Tm");
		mButton45.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view45);
		break;
		
	case R.id.btn46:
		String view46 = ("Tn");
		mButton46.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view46);
		break;
		
	case R.id.btn47:
		String view47 = ("To");
		mButton47.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view47);
		break;
		
	case R.id.btn48:
		String view48 = ("Tp");
		mButton48.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view48);
		break;
		
	case R.id.btn49:
		String view49 = ("Tq");
		mButton49.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view49);
		break;
		
	case R.id.btn50:
		String view50 = ("Tr");
		mButton50.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view50);
		break;
		
	case R.id.btn51:
		String view51 = ("Ts");
		mButton51.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view51);
		break;
		
	case R.id.btn52:
		String view52 = ("Tt");
		mButton52.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view52);
			break;
		
	case R.id.btn53:
		String view53 = ("Tu");
		mButton53.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view53);
			break;
	
//	case R.id.btn54:
//		String view54 = ("Xa");
//		mButton54.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view54);
//			break;
//		
//	case R.id.btn55:
//		String view55 = ("Xb");
//		mButton55.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view55);
//			break;	
//		
//	case R.id.btn56:
//		String view56 = ("Xc");
//		mButton56.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view56);
//			break;	
//		
//	case R.id.btn57:
//		String view57 = ("Xd");
//		mButton57.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view57);
//			break;
//		
//	case R.id.btn58:
//		String view58 = ("Xe");
//		mButton58.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view58);
//			break;
//		
//	case R.id.btn59:
//		String view59 = ("Xf");
//		mButton59.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view59);
//			break;
//		
//	case R.id.btn60:
//		String view60 = ("Xg");
//		mButton60.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view60);
//			break;
//		
//	case R.id.btn61:
//		String view61 = ("Xh");
//		mButton61.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view61);
//			break;	
//		
//	case R.id.btn62:
//		String view62 = ("Xi");
//		mButton62.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view62);
//			break;
		
	case R.id.btn63:
		String view63 = ("Xj");
		mButton63.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view63);
			break;
		
	case R.id.btn64:
		String view64 = ("Xk");
		mButton64.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view64);
			break;
		
	case R.id.btn65:
		String view65 = ("Xl");
		mButton65.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view65);
			break;
		
	case R.id.btn66:
		String view66 = ("Xm");
		mButton66.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view66);
			break;
		
	case R.id.btn67:
		String view67 = ("Xn");
		mButton67.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view67);
			break;
		
	case R.id.btn68:
		String view68 = ("Xo");
		mButton68.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view68);
			break;
		
//	case R.id.btn69:
//		String view69 = ("Ya");
//		mButton69.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view69);
//			break;
//		
//	case R.id.btn70:
//		String view70 = ("Yb");
//		mButton70.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view70);
//			break;
//		
//	case R.id.btn71:
//		String view71 = ("Yc");
//		mButton71.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view71);
//			break;
//		
//	case R.id.btn72:
//		String view72 = ("Yd");
//		mButton72.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view72);
//			break;
//		
//	case R.id.btn73:
//		String view73 = ("Ye");
//		mButton73.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view73);
//			break;
//		
//	case R.id.btn74:
//		String view74 = ("Yf");
//		mButton74.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view74);
//			break;
//		
//	case R.id.btn75:
//		String view75 = ("Yg");
//		mButton75.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view75);
//			break;
//		
//	case R.id.btn76:
//		String view76 = ("Yh");
//		mButton76.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view76);
//			break;
//		
//	case R.id.btn77:
//		String view77 = ("Yi");
//		mButton77.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view77);
//			break;
		
	case R.id.btn78:
		String view78 = ("Yj");
		mButton78.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view78);
			break;
		
	case R.id.btn79:
		String view79 = ("Yk");
		mButton79.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view79);
			break;
		
	case R.id.btn80:
		String view80 = ("Yl");
		mButton80.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view80);
			break;
		
	case R.id.btn81:
		String view81 = ("Ym");
		mButton81.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view81);
			break;
		
	case R.id.btn82:
		String view82 = ("Yn");
		mButton82.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view82);
			break;
		
	case R.id.btn83:
		String view83 = ("Yo");
		mButton83.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view83);
			break;
			
//	case R.id.btn84:
//		String view84 = ("Za");
//		mButton84.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view84);
//			break;
//			
//	case R.id.btn85:
//		String view85 = ("Zb");
//		mButton85.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view85);
//			break;
//			
//	case R.id.btn86:
//		String view86 = ("Zc");
//		mButton86.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view86);
//			break;
//			
//	case R.id.btn87:
//		String view87 = ("Zd");
//		mButton87.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view87);
//			break;
//			
//	case R.id.btn88:
//		String view88 = ("Ze");
//		mButton88.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view88);
//			break;
//			
//	case R.id.btn89:
//		String view89 = ("Zf");
//		mButton89.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view89);
//			break;
//			
//	case R.id.btn90:
//		String view90 = ("Zg");
//		mButton90.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view90);
//			break;
//			
//	case R.id.btn91:
//		String view91 = ("Zh");
//		mButton91.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view91);
//			break;
//			
//	case R.id.btn92:
//		String view92 = ("Zi");
//		mButton92.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view92);
//			break;
			
	case R.id.btn93:
		String view93 = ("Zj");
		mButton93.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view93);
			break;
			
	case R.id.btn94:
		String view94 = ("Zk");
		mButton94.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view94);
			break;
			
	case R.id.btn95:
		String view95 = ("Zl");
		mButton95.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view95);
			break;
			
	case R.id.btn96:
		String view96 = ("Zm");
		mButton96.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view96);
			break;
			
	case R.id.btn97:
		String view97 = ("Zn");
		mButton97.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view97);
			break;
			
	case R.id.btn98:
		String view98 = ("Zo");
		mButton98.setTextColor(Color.parseColor("#FF0000"));
		marcosetbtn.append(view98);
			break;
			
//	case R.id.btn99:
//		String view99 = ("Xp");
//		mButton99.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view99);
//			break;
//			
//	case R.id.btn100:
//		String view100 = ("Xq");
//		mButton100.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view100);
//			break;
//			
//	case R.id.btn101:
//		String view101 = ("Xr");
//		mButton101.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view101);
//			break;
//			
//	case R.id.btn102:
//		String view102 = ("Xs");
//		mButton102.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view102);
//			break;
//			
//	case R.id.btn103:
//		String view103 = ("Xt");
//		mButton103.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view103);
//			break;
//			
//	case R.id.btn104:
//		String view104 = ("Xu");
//		mButton104.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view104);
//			break;
//			
//	case R.id.btn105:
//		String view105 = ("Yp");
//		mButton105.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view105);
//			break;
//			
//	case R.id.btn106:
//		String view106 = ("Yq");
//		mButton106.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view106);
//			break;
//			
//	case R.id.btn107:
//		String view107 = ("Yr");
//		mButton107.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view107);
//			break;
//			
//	case R.id.btn108:
//		String view108 = ("Ys");
//		mButton10.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view108);
//			break;
//			
//	case R.id.btn109:
//		String view109 = ("Yt");
//		mButton109.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view109);
//			break;
//			
//	case R.id.btn110:
//		String view110 = ("Yu");
//		mButton110.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view110);
//			break;
//			
//	case R.id.btn111:
//		String view111 = ("Zp");
//		mButton111.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view111);
//			break;
//			
//	case R.id.btn112:
//		String view112 = ("Zq");
//		mButton112.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view112);
//			break;
//			
//	case R.id.btn113:
//		String view113 = ("Zr");
//		mButton113.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view113);
//			break;
//			
//	case R.id.btn114:
//		String view114 = ("Zs");
//		mButton114.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view114);
//			break;
//			
//	case R.id.btn115:
//		String view115 = ("Zt");
//		mButton115.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view115);
//			break;
//			
//	case R.id.btn116:
//		String view116 = ("Zt");
//		mButton116.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view116);
//			break;
			
	case R.id.btn153:
		String view153 = ("Tv");
		marcosetbtn.append(view153);
			break;
			
	case R.id.btn154:
		String view154 = ("Tw");
		marcosetbtn.append(view154);
			break;
			
	case R.id.btn155:
		String view155 = ("Tx");
		marcosetbtn.append(view155);
			break;
			
	case R.id.btn156:
		String view156 = ("Ty");
		marcosetbtn.append(view156);
			break;
			
	case R.id.window2:
		String window2 = ("W+qa1");
		marcosetbtn.append(window2);
		break;
		
	case R.id.window3:
		String window3 = ("W+qb1");
		marcosetbtn.append(window3);
		break;
		
	case R.id.window4:
		String window4 = ("W+qc1");
		marcosetbtn.append(window4);
		break;
			
	case R.id.door1 :
		String door1 = ("W+sa1");
		marcosetbtn.append(door1);	
		break;
		
	case R.id.door2 :
		String door2 = ("W+sb1");
		marcosetbtn.append(door2);	
		break;
		
	case R.id.door3 :
		String door3 = ("W+sc1");
		marcosetbtn.append(door3);	
		break;
		
		//tv home
//	case R.id.button25:
//		String view39 = ("Tg");
//		String message39 = view39.toString();
//		marcosetbtn.append(message39);
//		break;
//		//tv 靜音
//	case R.id.button26:
//		String view41 = ("Ti");
//        String message41 = view41.toString();
//        marcosetbtn.append(message41);
//		break;
	
		}
	}
	
	//marco2 click
public void Click1Handler(View v){
	// TODO Auto-generated catch block
		
	switch(v.getId()){
	
	case R.id.marcoset1ok :
		
		saveData(6);
		Intent intent4 = new Intent();
        intent4.setClass(BluetoothChat.this, BluetoothChat.class);
        finish();
        startActivity(intent4);
        
        break;
	
	case R.id.s6 :
		String delay6 = ("'");
		marco1setbtn.append(delay6);
	break;

	case R.id.s7 :
		String delay7 = ("(");
		marco1setbtn.append(delay7);
		break;
		
	case R.id.s8 :
		String delay8 = (")");
		marco1setbtn.append(delay8);
		break;
		
	case R.id.s9 :
		String delay9 = ("*");
		marco1setbtn.append(delay9);
		break;
		
	case R.id.s10 :
		String delay10 = ("+");
		marco1setbtn.append(delay10);
		break;

	case R.id.btn01:
		
		if (count1 == 0 )
		{
		String view = ("a4");
		mButton01.setTextColor(Color.parseColor("#FF0000"));
		mButton01.setBackgroundResource(R.drawable.set_after_9);
		marco1setbtn.append(view);
		count1 = 1;
		}
        else if(count1 == 1)
        {
        mButton01.setTextColor(Color.parseColor("#ffffff"));
		mButton01.setBackgroundResource(R.drawable.set_befor_9);
        String btn01 = marco1setbtn.getText().toString();
        String newa = btn01.replaceFirst("a4", "a5");
        marco1setbtn.setText(newa);
        count1 = 2;
        }
        else if (count1 == 2)
        {
        	String btn01 = marcosetbtn.getText().toString();
            String newa = btn01.replaceFirst("a5", "");
            marco1setbtn.setText(newa);
        	count1 = 0;
        }
		
		break;
		
	case R.id.btn02:
		
		if (count2 == 0 )
		{
		String view2 = ("b4");
		mButton02.setTextColor(Color.parseColor("#FF0000"));
		mButton02.setBackgroundResource(R.drawable.set_after_9);
		marco1setbtn.append(view2);
		count2 = 1;
		}
		else if(count2 == 1)
		{
		mButton02.setTextColor(Color.parseColor("#ffffff"));
		mButton02.setBackgroundResource(R.drawable.set_befor_9);
        String btn02 = marco1setbtn.getText().toString();
        String newa = btn02.replaceFirst("b4", "b5");
        marco1setbtn.setText(newa);
        count2 = 2;
    	}
		else if (count2 == 2)
        {
        String btn02 = marco1setbtn.getText().toString();
        String newa = btn02.replaceFirst("b5", "");
        marco1setbtn.setText(newa);
        	count2 = 0;
        }
		break;	
		
	case R.id.btn03:
		if (count3 == 0 )
		{
		String view3 = ("c4");
		mButton03.setTextColor(Color.parseColor("#FF0000"));
		mButton03.setBackgroundResource(R.drawable.set_after_9);
		marco1setbtn.append(view3);
		count3 = 1;
		}
		else if(count3 == 1)
		{
		mButton03.setTextColor(Color.parseColor("#ffffff"));
		mButton03.setBackgroundResource(R.drawable.set_befor_9);
        String btn03 = marco1setbtn.getText().toString();
        String newa = btn03.replaceFirst("c4", "c5");
        marco1setbtn.setText(newa);
        count3 = 2;
    	}
		else if (count3 == 2)
        {
        String btn03 = marco1setbtn.getText().toString();
        String newa = btn03.replaceFirst("c5", "");
        marco1setbtn.setText(newa);
        	count3 = 0;
        }
		break;	
		
	case R.id.btn04:
		if (count4 == 0 )
		{
		String view4 = ("d4");
		mButton04.setTextColor(Color.parseColor("#FF0000"));
		mButton04.setBackgroundResource(R.drawable.set_after_9);
		marco1setbtn.append(view4);
		count4 = 1;
		}
		else if(count4 == 1)
		{
		mButton04.setTextColor(Color.parseColor("#ffffff"));
		mButton04.setBackgroundResource(R.drawable.set_befor_9);
        String btn04 = marco1setbtn.getText().toString();
        String newa = btn04.replaceFirst("d4", "d5");
        marco1setbtn.setText(newa);
        count4 = 2;
    	}
		else if (count4 == 2)
        {
        String btn04 = marco1setbtn.getText().toString();
        String newa = btn04.replaceFirst("d5", "");
        marco1setbtn.setText(newa);
        	count4 = 0;
        }
		break;	
		
	case R.id.btn05:
		if (count5 == 0 )
		{
		String view5 = ("e4");
		mButton05.setTextColor(Color.parseColor("#FF0000"));
		mButton05.setBackgroundResource(R.drawable.set_after_9);
		marco1setbtn.append(view5);
		count5 = 1;
		}
		else if(count5 == 1)
		{
		mButton05.setTextColor(Color.parseColor("#ffffff"));
		mButton05.setBackgroundResource(R.drawable.set_befor_9);
        String btn05 = marco1setbtn.getText().toString();
        String newa = btn05.replaceFirst("e4", "e5");
        marco1setbtn.setText(newa);
        count5 = 2;
    	}
		else if (count5 == 2)
        {
        String btn05 = marco1setbtn.getText().toString();
        String newa = btn05.replaceFirst("e5", "");
        marco1setbtn.setText(newa);
        	count5 = 0;
        }
		break;	
		
	case R.id.btn06:
		if (count6 == 0 )
		{
		String view6 = ("f4");
		mButton06.setTextColor(Color.parseColor("#FF0000"));
		mButton06.setBackgroundResource(R.drawable.set_after_9);
		marco1setbtn.append(view6);
		count6 = 1;
		}
		else if(count6 == 1)
		{
		mButton06.setTextColor(Color.parseColor("#ffffff"));
		mButton06.setBackgroundResource(R.drawable.set_befor_9);
        String btn06 = marco1setbtn.getText().toString();
        String newa = btn06.replaceFirst("f4", "f5");
        marco1setbtn.setText(newa);
        count6 = 2;
    	}
		else if (count6 == 2)
        {
        String btn06 = marco1setbtn.getText().toString();
        String newa = btn06.replaceFirst("f5", "");
        marco1setbtn.setText(newa);
        	count6 = 0;
        }
		break;
		
	case R.id.btn07:
		if (count7 == 0 )
		{
		String view7 = ("g4");
		mButton07.setTextColor(Color.parseColor("#FF0000"));
		mButton07.setBackgroundResource(R.drawable.set_after_9);
		marco1setbtn.append(view7);
		count7 = 1;
		}
		else if(count7 == 1)
		{
		mButton07.setTextColor(Color.parseColor("#ffffff"));
		mButton07.setBackgroundResource(R.drawable.set_befor_9);
        String btn07 = marco1setbtn.getText().toString();
        String newa = btn07.replaceFirst("g4", "g5");
        marco1setbtn.setText(newa);
        count7 = 2;
    	}
		else if (count7 == 2)
        {
        String btn07 = marco1setbtn.getText().toString();
        String newa = btn07.replaceFirst("g5", "");
        marco1setbtn.setText(newa);
        	count7 = 0;
        }
		break;
		
	case R.id.btn08:
		if (count8 == 0 )
		{
		String view8 = ("h4");
		mButton08.setTextColor(Color.parseColor("#FF0000"));
		mButton08.setBackgroundResource(R.drawable.set_after_9);
		marco1setbtn.append(view8);
		count8 = 1;
		}
		else if(count8 == 1)
		{
		mButton08.setTextColor(Color.parseColor("#ffffff"));
		mButton08.setBackgroundResource(R.drawable.set_befor_9);
        String btn08 = marco1setbtn.getText().toString();
        String newa = btn08.replaceFirst("h4", "h5");
        marco1setbtn.setText(newa);
        count8 = 2;
    	}
		else if (count8 == 2)
        {
        String btn08 = marco1setbtn.getText().toString();
        String newa = btn08.replaceFirst("h5", "");
        marco1setbtn.setText(newa);
        	count8 = 0;
        }
		break;	
		
	case R.id.btn09:
		if (count9 == 0 )
		{
		String view9 = ("i4");
		mButton09.setTextColor(Color.parseColor("#FF0000"));
		mButton09.setBackgroundResource(R.drawable.set_after_9);
		marco1setbtn.append(view9);
		count9 = 1;
		}
		else if(count9 == 1)
		{
		mButton09.setTextColor(Color.parseColor("#ffffff"));
		mButton09.setBackgroundResource(R.drawable.set_befor_9);
        String btn09 = marco1setbtn.getText().toString();
        String newa = btn09.replaceFirst("i4", "i5");
        marco1setbtn.setText(newa);
        count9 = 2;
    	}
		else if (count9 == 2)
        {
        String btn09 = marco1setbtn.getText().toString();
        String newa = btn09.replaceFirst("i5", "");
        marco1setbtn.setText(newa);
        	count9 = 0;
        }
		break;
		
	case R.id.btn10:
		if (count10 == 0 )
		{
		String view10 = ("j4");
		mButton10.setTextColor(Color.parseColor("#FF0000"));
		mButton10.setBackgroundResource(R.drawable.set_after_9);
		marco1setbtn.append(view10);
		count10 = 1;
		}
		else if(count10 == 1)
		{
		mButton10.setTextColor(Color.parseColor("#ffffff"));
		mButton10.setBackgroundResource(R.drawable.set_befor_9);
        String btn10 = marco1setbtn.getText().toString();
        String newa = btn10.replaceFirst("j4", "j5");
        marco1setbtn.setText(newa);
        count10 = 2;
    	}
		else if (count10 == 2)
        {
        String btn10 = marco1setbtn.getText().toString();
        String newa = btn10.replaceFirst("j5", "");
        marco1setbtn.setText(newa);
        	count10 = 0;
        }
		break;
		
	case R.id.btn11:
		if (count11 == 0 )
		{
		String view11 = ("k4");
		mButton11.setTextColor(Color.parseColor("#FF0000"));
		mButton11.setBackgroundResource(R.drawable.set_after_9);
		marco1setbtn.append(view11);
		count11 = 1;
		}
		else if(count11 == 1)
		{
		mButton11.setTextColor(Color.parseColor("#ffffff"));
		mButton11.setBackgroundResource(R.drawable.set_befor_9);
        String btn11 = marco1setbtn.getText().toString();
        String newa = btn11.replaceFirst("k4", "k5");
        marco1setbtn.setText(newa);
        count11 = 2;
    	}
		else if (count11 == 2)
        {
        String btn11 = marco1setbtn.getText().toString();
        String newa = btn11.replaceFirst("k5", "");
        marco1setbtn.setText(newa);
        	count11 = 0;
        }
		break;
		
	case R.id.btn12:
		if (count12 == 0 )
		{
		String view12 = ("l4");
		mButton12.setTextColor(Color.parseColor("#FF0000"));
		mButton12.setBackgroundResource(R.drawable.set_after_9);
		marco1setbtn.append(view12);
		count12 = 1;
		}
		else if(count12 == 1)
		{
		mButton12.setTextColor(Color.parseColor("#ffffff"));
		mButton12.setBackgroundResource(R.drawable.set_befor_9);
        String btn12 = marco1setbtn.getText().toString();
        String newa = btn12.replaceFirst("l4", "l5");
        marco1setbtn.setText(newa);
        count12 = 2;
    	}
		else if (count12 == 2)
        {
        String btn12 = marco1setbtn.getText().toString();
        String newa = btn12.replaceFirst("l5", "");
        marco1setbtn.setText(newa);
        	count12 = 0;
        }
		break;
	
case R.id.btn13:
	String view13 = ("Aa");
	mButton13.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view13);
	
	break;
	
case R.id.btn14:
	String view14 = ("Ab");
	mButton14.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view14);
	break;
	
case R.id.btn15:
	String view15 = ("Ac");
	mButton15.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view15);
	break;
	
case R.id.btn16:
	String view16 = ("Ad");
	mButton16.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view16);
	break;
	
case R.id.btn17:
	String view17 = ("Ae");
	mButton17.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view17);
	break;
	
case R.id.btn18:
	String view18 = ("Af");
	mButton18.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view18);
	break;
	
case R.id.btn19:
	String view19 = ("Ag");
	mButton19.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view19);
	break;
	
case R.id.btn20:
	String view20 = ("Ah");
	mButton20.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view20);
	break;
	
case R.id.btn21:
	String view21 = ("Ai");
	mButton21.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view21);
	break;
	
case R.id.btn22:
	String view22 = ("Aj");
	mButton22.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view22);
	break;
	
case R.id.btn23:
	String view23 = ("Ak");
	mButton23.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view23);
	break;
	
case R.id.btn24:
	String view24 = ("Al");
	mButton24.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view24);
	break;
	
//case R.id.btn25:
//	String view25 = ("Am");
//	mButton25.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view25);
//	break;
//	
//case R.id.btn26:
//	String view26 = ("An");
//	mButton26.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view26);
//	break;
//	
//case R.id.btn27:
//	String view27 = ("Ao");
//	mButton27.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view27);
//	break;
//	
//case R.id.btn28:
//	String view28 = ("Ap");
//	mButton28.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view28);
//	break;
//	
//case R.id.btn29:
//	String view29 = ("Aq");
//	mButton29.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view29);
//	break;
//	
//case R.id.btn30:
//	String view30 = ("Ar");
//	mButton30.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view30);
//	break;
//	
//case R.id.btn31:
//	String view31 = ("As");
//	mButton31.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view31);
//	break;
//	
//case R.id.btn32:
	//String view32 = ("At");
	//mButton32.setTextColor(Color.parseColor("#FF0000"));
	//marco1setbtn.append(view32);
	//break;
	
case R.id.btn33:
	String view33 = ("Ta");
	mButton33.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view33);
	break;
	
//case R.id.btn34:
//	String view34 = ("Tb");
//	mButton34.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view34);
//	break;
//	
case R.id.btn35:
	String view35 = ("Tc");
	mButton35.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view35);
	break;
//	
//case R.id.btn36:
//	String view36 = ("Td");
//	mButton36.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view36);
//	break;
//	
//case R.id.btn37:
//	String view37 = ("Te");
//	mButton37.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view37);
//	break;
//	
//case R.id.btn38:
//	String view38 = ("Tf");
//	mButton38.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view38);
//	break;
//	
//case R.id.btn39:
//	String view39 = ("Tg");
//	mButton39.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view39);
//	break;
//	
//case R.id.btn40:
//	String view40 = ("Th");
//	mButton40.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view40);
//	break;
//	
//case R.id.btn41:
	//String view41 = ("Ti");
	//mButton41.setTextColor(Color.parseColor("#FF0000"));
	//marco1setbtn.append(view41);
	//break;
	
case R.id.btn42:
	String view42 = ("Tj");
	mButton42.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view42);
	break;
	
case R.id.btn43:
	String view43 = ("Tk");
	mButton43.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view43);
	break;
	
case R.id.btn44:
	String view44 = ("Tl");
	mButton44.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view44);
	break;
	
case R.id.btn45:
	String view45 = ("Tm");
	mButton45.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view45);
	break;
	
case R.id.btn46:
	String view46 = ("Tn");
	mButton46.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view46);
	break;
	
case R.id.btn47:
	String view47 = ("To");
	mButton47.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view47);
	break;
	
case R.id.btn48:
	String view48 = ("Tp");
	mButton48.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view48);
	break;
	
case R.id.btn49:
	String view49 = ("Tq");
	mButton49.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view49);
	break;
	
case R.id.btn50:
	String view50 = ("Tr");
	mButton50.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view50);
	break;
	
case R.id.btn51:
	String view51 = ("Ts");
	mButton51.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view51);
	break;
	
case R.id.btn52:
	String view52 = ("Tt");
	mButton52.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view52);
		break;
	
case R.id.btn53:
	String view53 = ("Tu");
	mButton53.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view53);
		break;

//case R.id.btn54:
//	String view54 = ("Xa");
//	mButton54.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view54);
//		break;
//	
//case R.id.btn55:
//	String view55 = ("Xb");
//	mButton55.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view55);
//		break;	
//	
//case R.id.btn56:
//	String view56 = ("Xc");
//	mButton56.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view56);
//		break;	
//	
//case R.id.btn57:
//	String view57 = ("Xd");
//	mButton57.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view57);
//		break;
//	
//case R.id.btn58:
//	String view58 = ("Xe");
//	mButton58.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view58);
//		break;
//	
//case R.id.btn59:
//	String view59 = ("Xf");
//	mButton59.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view59);
//		break;
//	
//case R.id.btn60:
//	String view60 = ("Xg");
//	mButton60.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view60);
//		break;
//	
//case R.id.btn61:
//	String view61 = ("Xh");
//	mButton61.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view61);
//		break;	
//	
//case R.id.btn62:
//	String view62 = ("Xi");
//	mButton62.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view62);
//		break;
//	
case R.id.btn63:
	String view63 = ("Xj");
	mButton63.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view63);
		break;
	
case R.id.btn64:
	String view64 = ("Xk");
	mButton64.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view64);
		break;
	
case R.id.btn65:
	String view65 = ("Xl");
	mButton65.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view65);
		break;
	
case R.id.btn66:
	String view66 = ("Xm");
	mButton66.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view66);
		break;
	
case R.id.btn67:
	String view67 = ("Xn");
	mButton67.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view67);
		break;
	
case R.id.btn68:
	String view68 = ("Xo");
	mButton68.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view68);
		break;
	
//case R.id.btn69:
//	String view69 = ("Ya");
//	mButton69.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view69);
//		break;
//	
//case R.id.btn70:
//	String view70 = ("Yb");
//	mButton70.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view70);
//		break;
//	
//case R.id.btn71:
//	String view71 = ("Yc");
//	mButton71.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view71);
//		break;
//	
//case R.id.btn72:
//	String view72 = ("Yd");
//	mButton72.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view72);
//		break;
//	
//case R.id.btn73:
//	String view73 = ("Ye");
//	mButton73.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view73);
//		break;
//	
//case R.id.btn74:
//	String view74 = ("Yf");
//	mButton74.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view74);
//		break;
//	
//case R.id.btn75:
//	String view75 = ("Yg");
//	mButton75.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view75);
//		break;
//	
//case R.id.btn76:
//	String view76 = ("Yh");
//	mButton76.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view76);
//		break;
//	
//case R.id.btn77:
//	String view77 = ("Yi");
//	mButton77.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view77);
//		break;
	
case R.id.btn78:
	String view78 = ("Yj");
	mButton78.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view78);
		break;
	
case R.id.btn79:
	String view79 = ("Yk");
	mButton79.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view79);
		break;
	
case R.id.btn80:
	String view80 = ("Yl");
	mButton80.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view80);
		break;
	
case R.id.btn81:
	String view81 = ("Ym");
	mButton81.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view81);
		break;
	
case R.id.btn82:
	String view82 = ("Yn");
	mButton82.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view82);
		break;
	
case R.id.btn83:
	String view83 = ("Yo");
	mButton83.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view83);
		break;
		
//case R.id.btn84:
//	String view84 = ("Za");
//	mButton84.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view84);
//		break;
//		
//case R.id.btn85:
//	String view85 = ("Zb");
//	mButton85.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view85);
//		break;
//		
//case R.id.btn86:
//	String view86 = ("Zc");
//	mButton86.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view86);
//		break;
//		
//case R.id.btn87:
//	String view87 = ("Zd");
//	mButton87.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view87);
//		break;
//		
//case R.id.btn88:
//	String view88 = ("Ze");
//	mButton88.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view88);
//		break;
//		
//case R.id.btn89:
//	String view89 = ("Zf");
//	mButton89.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view89);
//		break;
//		
//case R.id.btn90:
//	String view90 = ("Zg");
//	mButton90.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view90);
//		break;
//		
//case R.id.btn91:
//	String view91 = ("Zh");
//	mButton91.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view91);
//		break;
//		
//case R.id.btn92:
//	String view92 = ("Zi");
//	mButton92.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view92);
//		break;
		
case R.id.btn93:
	String view93 = ("Zj");
	mButton93.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view93);
		break;
		
case R.id.btn94:
	String view94 = ("Zk");
	mButton94.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view94);
		break;
		
case R.id.btn95:
	String view95 = ("Zl");
	mButton95.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view95);
		break;
		
case R.id.btn96:
	String view96 = ("Zm");
	mButton96.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view96);
		break;
		
case R.id.btn97:
	String view97 = ("Zn");
	mButton97.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view97);
		break;
		
case R.id.btn98:
	String view98 = ("Zo");
	mButton98.setTextColor(Color.parseColor("#FF0000"));
	marco1setbtn.append(view98);
		break;
		
//case R.id.btn99:
//	String view99 = ("Xp");
//	mButton99.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view99);
//		break;
//		
//case R.id.btn100:
//	String view100 = ("Xq");
//	mButton100.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view100);
//		break;
//		
//case R.id.btn101:
//	String view101 = ("Xr");
//	mButton101.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view101);
//		break;
//		
//case R.id.btn102:
//	String view102 = ("Xs");
//	mButton102.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view102);
//		break;
//		
//case R.id.btn103:
//	String view103 = ("Xt");
//	mButton103.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view103);
//		break;
//		
//case R.id.btn104:
//	String view104 = ("Xu");
//	mButton104.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view104);
//		break;
//		
//case R.id.btn105:
//	String view105 = ("Yp");
//	mButton105.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view105);
//		break;
//		
//case R.id.btn106:
//	String view106 = ("Yq");
//	mButton106.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view106);
//		break;
//		
//case R.id.btn107:
//	String view107 = ("Yr");
//	mButton107.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view107);
//		break;
//		
//case R.id.btn108:
//	String view108 = ("Ys");
//	mButton10.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view108);
//		break;
//		
//case R.id.btn109:
//	String view109 = ("Yt");
//	mButton109.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view109);
//		break;
//		
//case R.id.btn110:
//	String view110 = ("Yu");
//	mButton110.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view110);
//		break;
//		
//case R.id.btn111:
//	String view111 = ("Zp");
//	mButton111.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view111);
//		break;
//		
//case R.id.btn112:
//	String view112 = ("Zq");
//	mButton112.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view112);
//		break;
//		
//case R.id.btn113:
//	String view113 = ("Zr");
//	mButton113.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view113);
//		break;
//		
//case R.id.btn114:
//	String view114 = ("Zs");
//	mButton114.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view114);
//		break;
//		
//case R.id.btn115:
//	String view115 = ("Zt");
//	mButton115.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view115);
//		break;
//		
//case R.id.btn116:
//	String view116 = ("Zt");
//	mButton116.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view116);
//		break;
		
case R.id.btn153:
	String view153 = ("Tv");
	marco1setbtn.append(view153);
		break;
		
case R.id.btn154:
	String view154 = ("Tw");
	marco1setbtn.append(view154);
		break;
		
case R.id.btn155:
	String view155 = ("Tx");
	marco1setbtn.append(view155);
		break;
		
case R.id.btn156:
	String view156 = ("Ty");
	marco1setbtn.append(view156);
		break;
		
case R.id.window2:
	String window2 = ("W+qa1");
	marco1setbtn.append(window2);
	break;
	
case R.id.window3:
	String window3 = ("W+qb1");
	marco1setbtn.append(window3);
	break;
	
case R.id.window4:
	String window4 = ("W+qc1");
	marco1setbtn.append(window4);
	break;
			
case R.id.door1 :
	String door1 = ("W+sa1");
	marco1setbtn.append(door1);	
	break;
	
case R.id.door2 :
	String door2 = ("W+sb1");
	marco1setbtn.append(door2);	
	break;
	
case R.id.door3 :
	String door3 = ("W+sc1");
	marco1setbtn.append(door3);	
	break;
	
	//tv home
//case R.id.button25:
//	String view39 = ("Tg");
//	String message39 = view39.toString();
//	marco1setbtn.append(message39);
//	break;
//	//tv 靜音
//case R.id.button26:
//	String view41 = ("Ti");
//    String message41 = view41.toString();
//    marco1setbtn.append(message41);
//	break;
	
		}
	}
	
//marco3
public void Click2Handler(View v){
	// TODO Auto-generated catch block
	
switch(v.getId()){

case R.id.marcoset2ok :
	
	saveData(7);
	Intent intent5 = new Intent();
    intent5.setClass(BluetoothChat.this, BluetoothChat.class);
    finish();
    startActivity(intent5);
    
    break;

case R.id.s11 :
	String delay11 = ("'");
	marco2setbtn.append(delay11);
break;

case R.id.s12 :
	String delay12 = ("(");
	marco2setbtn.append(delay12);
	break;
	
case R.id.s13 :
	String delay13 = (")");
	marco2setbtn.append(delay13);
	break;
	
case R.id.s14 :
	String delay14 = ("*");
	marco2setbtn.append(delay14);
	break;
	
case R.id.s15 :
	String delay15 = ("+");
	marco2setbtn.append(delay15);
	break;

case R.id.btn01:
	
	if (count1 == 0 )
	{
	String view = ("a4");
	mButton01.setTextColor(Color.parseColor("#FF0000"));
	mButton01.setBackgroundResource(R.drawable.set_after_9);
	marco2setbtn.append(view);
	count1 = 1;
	}
    else if(count1 == 1)
    {
    mButton01.setTextColor(Color.parseColor("#ffffff"));
	mButton01.setBackgroundResource(R.drawable.set_befor_9);
    String btn01 = marco2setbtn.getText().toString();
    String newa = btn01.replaceFirst("a4", "a5");
    marco2setbtn.setText(newa);
    count1 = 2;
    }
    else if (count1 == 2)
    {
    	String btn01 = marco2setbtn.getText().toString();
        String newa = btn01.replaceFirst("a5", "");
        marco2setbtn.setText(newa);
    	count1 = 0;
    }
	
	break;
	
case R.id.btn02:
	
	if (count2 == 0 )
	{
	String view2 = ("b4");
	mButton02.setTextColor(Color.parseColor("#FF0000"));
	mButton02.setBackgroundResource(R.drawable.set_after_9);
	marco2setbtn.append(view2);
	count2 = 1;
	}
	else if(count2 == 1)
	{
	mButton02.setTextColor(Color.parseColor("#ffffff"));
	mButton02.setBackgroundResource(R.drawable.set_befor_9);
    String btn02 = marco2setbtn.getText().toString();
    String newa = btn02.replaceFirst("b4", "b5");
    marco2setbtn.setText(newa);
    count2 = 2;
	}
	else if (count2 == 2)
    {
    String btn02 = marco2setbtn.getText().toString();
    String newa = btn02.replaceFirst("b5", "");
    marco2setbtn.setText(newa);
    	count2 = 0;
    }
	break;	
	
case R.id.btn03:
	if (count3 == 0 )
	{
	String view3 = ("c4");
	mButton03.setTextColor(Color.parseColor("#FF0000"));
	mButton03.setBackgroundResource(R.drawable.set_after_9);
	marco2setbtn.append(view3);
	count3 = 1;
	}
	else if(count3 == 1)
	{
	mButton03.setTextColor(Color.parseColor("#ffffff"));
	mButton03.setBackgroundResource(R.drawable.set_befor_9);
    String btn03 = marco2setbtn.getText().toString();
    String newa = btn03.replaceFirst("c4", "c5");
    marco2setbtn.setText(newa);
    count3 = 2;
	}
	else if (count3 == 2)
    {
    String btn03 = marco2setbtn.getText().toString();
    String newa = btn03.replaceFirst("c5", "");
    marco2setbtn.setText(newa);
    	count3 = 0;
    }
	break;	
	
case R.id.btn04:
	if (count4 == 0 )
	{
	String view4 = ("d4");
	mButton04.setTextColor(Color.parseColor("#FF0000"));
	mButton04.setBackgroundResource(R.drawable.set_after_9);
	marco2setbtn.append(view4);
	count4 = 1;
	}
	else if(count4 == 1)
	{
	mButton04.setTextColor(Color.parseColor("#ffffff"));
	mButton04.setBackgroundResource(R.drawable.set_befor_9);
    String btn04 = marco2setbtn.getText().toString();
    String newa = btn04.replaceFirst("d4", "d5");
    marco2setbtn.setText(newa);
    count4 = 2;
	}
	else if (count4 == 2)
    {
    String btn04 = marco2setbtn.getText().toString();
    String newa = btn04.replaceFirst("d5", "");
    marco2setbtn.setText(newa);
    	count4 = 0;
    }
	break;	
	
case R.id.btn05:
	if (count5 == 0 )
	{
	String view5 = ("e4");
	mButton05.setTextColor(Color.parseColor("#FF0000"));
	mButton05.setBackgroundResource(R.drawable.set_after_9);
	marco2setbtn.append(view5);
	count5 = 1;
	}
	else if(count5 == 1)
	{
	mButton05.setTextColor(Color.parseColor("#ffffff"));
	mButton05.setBackgroundResource(R.drawable.set_befor_9);
    String btn05 = marco2setbtn.getText().toString();
    String newa = btn05.replaceFirst("e4", "e5");
    marco2setbtn.setText(newa);
    count5 = 2;
	}
	else if (count5 == 2)
    {
    String btn05 = marco2setbtn.getText().toString();
    String newa = btn05.replaceFirst("e5", "");
    marco2setbtn.setText(newa);
    	count5 = 0;
    }
	break;	
	
case R.id.btn06:
	if (count6 == 0 )
	{
	String view6 = ("f4");
	mButton06.setTextColor(Color.parseColor("#FF0000"));
	mButton06.setBackgroundResource(R.drawable.set_after_9);
	marco2setbtn.append(view6);
	count6 = 1;
	}
	else if(count6 == 1)
	{
	mButton06.setTextColor(Color.parseColor("#ffffff"));
	mButton06.setBackgroundResource(R.drawable.set_befor_9);
    String btn06 = marco2setbtn.getText().toString();
    String newa = btn06.replaceFirst("f4", "f5");
    marco2setbtn.setText(newa);
    count6 = 2;
	}
	else if (count6 == 2)
    {
    String btn06 = marco2setbtn.getText().toString();
    String newa = btn06.replaceFirst("f5", "");
    marco2setbtn.setText(newa);
    	count6 = 0;
    }
	break;
	
case R.id.btn07:
	if (count7 == 0 )
	{
	String view7 = ("g4");
	mButton07.setTextColor(Color.parseColor("#FF0000"));
	mButton07.setBackgroundResource(R.drawable.set_after_9);
	marco2setbtn.append(view7);
	count7 = 1;
	}
	else if(count7 == 1)
	{
	mButton07.setTextColor(Color.parseColor("#ffffff"));
	mButton07.setBackgroundResource(R.drawable.set_befor_9);
    String btn07 = marco2setbtn.getText().toString();
    String newa = btn07.replaceFirst("g4", "g5");
    marco2setbtn.setText(newa);
    count7 = 2;
	}
	else if (count7 == 2)
    {
    String btn07 = marco2setbtn.getText().toString();
    String newa = btn07.replaceFirst("g5", "");
    marco2setbtn.setText(newa);
    	count7 = 0;
    }
	break;
	
case R.id.btn08:
	if (count8 == 0 )
	{
	String view8 = ("h4");
	mButton08.setTextColor(Color.parseColor("#FF0000"));
	mButton08.setBackgroundResource(R.drawable.set_after_9);
	marco2setbtn.append(view8);
	count8 = 1;
	}
	else if(count8 == 1)
	{
	mButton08.setTextColor(Color.parseColor("#ffffff"));
	mButton08.setBackgroundResource(R.drawable.set_befor_9);
    String btn08 = marco2setbtn.getText().toString();
    String newa = btn08.replaceFirst("h4", "h5");
    marco2setbtn.setText(newa);
    count8 = 2;
	}
	else if (count8 == 2)
    {
    String btn08 = marco2setbtn.getText().toString();
    String newa = btn08.replaceFirst("h5", "");
    marco2setbtn.setText(newa);
    	count8 = 0;
    }
	break;	
	
case R.id.btn09:
	if (count9 == 0 )
	{
	String view9 = ("i4");
	mButton09.setTextColor(Color.parseColor("#FF0000"));
	mButton09.setBackgroundResource(R.drawable.set_after_9);
	marco2setbtn.append(view9);
	count9 = 1;
	}
	else if(count9 == 1)
	{
	mButton09.setTextColor(Color.parseColor("#ffffff"));
	mButton09.setBackgroundResource(R.drawable.set_befor_9);
    String btn09 = marco2setbtn.getText().toString();
    String newa = btn09.replaceFirst("i4", "i5");
    marco2setbtn.setText(newa);
    count9 = 2;
	}
	else if (count9 == 2)
    {
    String btn09 = marco2setbtn.getText().toString();
    String newa = btn09.replaceFirst("i5", "");
    marco2setbtn.setText(newa);
    	count9 = 0;
    }
	break;
	
case R.id.btn10:
	if (count10 == 0 )
	{
	String view10 = ("j4");
	mButton10.setTextColor(Color.parseColor("#FF0000"));
	mButton10.setBackgroundResource(R.drawable.set_after_9);
	marco2setbtn.append(view10);
	count10 = 1;
	}
	else if(count10 == 1)
	{
	mButton10.setTextColor(Color.parseColor("#ffffff"));
	mButton10.setBackgroundResource(R.drawable.set_befor_9);
    String btn10 = marco2setbtn.getText().toString();
    String newa = btn10.replaceFirst("j4", "j5");
    marco2setbtn.setText(newa);
    count10 = 2;
	}
	else if (count10 == 2)
    {
    String btn10 = marco2setbtn.getText().toString();
    String newa = btn10.replaceFirst("j5", "");
    marco2setbtn.setText(newa);
    	count10 = 0;
    }
	break;
	
case R.id.btn11:
	if (count11 == 0 )
	{
	String view11 = ("k4");
	mButton11.setTextColor(Color.parseColor("#FF0000"));
	mButton11.setBackgroundResource(R.drawable.set_after_9);
	marco2setbtn.append(view11);
	count11 = 1;
	}
	else if(count11 == 1)
	{
	mButton11.setTextColor(Color.parseColor("#ffffff"));
	mButton11.setBackgroundResource(R.drawable.set_befor_9);
    String btn11 = marco2setbtn.getText().toString();
    String newa = btn11.replaceFirst("k4", "k5");
    marco2setbtn.setText(newa);
    count11 = 2;
	}
	else if (count11 == 2)
    {
    String btn11 = marco2setbtn.getText().toString();
    String newa = btn11.replaceFirst("k5", "");
    marco2setbtn.setText(newa);
    	count11 = 0;
    }
	break;
	
case R.id.btn12:
	if (count12 == 0 )
	{
	String view12 = ("l4");
	mButton12.setTextColor(Color.parseColor("#FF0000"));
	mButton12.setBackgroundResource(R.drawable.set_after_9);
	marco2setbtn.append(view12);
	count12 = 1;
	}
	else if(count12 == 1)
	{
	mButton12.setTextColor(Color.parseColor("#ffffff"));
	mButton12.setBackgroundResource(R.drawable.set_befor_9);
    String btn12 = marco2setbtn.getText().toString();
    String newa = btn12.replaceFirst("l4", "l5");
    marco2setbtn.setText(newa);
    count12 = 2;
	}
	else if (count12 == 2)
    {
    String btn12 = marco2setbtn.getText().toString();
    String newa = btn12.replaceFirst("l5", "");
    marco2setbtn.setText(newa);
    	count12 = 0;
    }
	break;

case R.id.btn13:
String view13 = ("Aa");
mButton13.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view13);

break;

case R.id.btn14:
String view14 = ("Ab");
mButton14.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view14);

break;

case R.id.btn15:
String view15 = ("Ac");
mButton15.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view15);
break;

case R.id.btn16:
String view16 = ("Ad");
mButton16.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view16);
break;

case R.id.btn17:
String view17 = ("Ae");
mButton17.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view17);
break;

case R.id.btn18:
String view18 = ("Af");
mButton18.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view18);
break;

case R.id.btn19:
String view19 = ("Ag");
mButton19.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view19);
break;

case R.id.btn20:
String view20 = ("Ah");
mButton20.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view20);
break;

case R.id.btn21:
String view21 = ("Ai");
mButton21.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view21);
break;

case R.id.btn22:
String view22 = ("Aj");
mButton22.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view22);
break;

case R.id.btn23:
String view23 = ("Ak");
mButton23.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view23);
break;

case R.id.btn24:
String view24 = ("Al");
mButton24.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view24);
break;

//case R.id.btn25:
//String view25 = ("Am");
//mButton25.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view25);
//break;
//
//case R.id.btn26:
//String view26 = ("An");
//mButton26.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view26);
//break;
//
//case R.id.btn27:
//String view27 = ("Ao");
//mButton27.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view27);
//break;
//
//case R.id.btn28:
//String view28 = ("Ap");
//mButton28.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view28);
//break;
//
//case R.id.btn29:
//String view29 = ("Aq");
//mButton29.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view29);
//break;
//
//case R.id.btn30:
//String view30 = ("Ar");
//mButton30.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view30);
//break;
//
//case R.id.btn31:
//String view31 = ("As");
//mButton31.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view31);
//break;
//
//case R.id.btn32:
//String view32 = ("At");
//mButton32.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view32);
//break;

case R.id.btn33:
String view33 = ("Ta");
mButton33.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view33);
break;

//case R.id.btn34:
//String view34 = ("Tb");
//mButton34.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view34);
//break;

case R.id.btn35:
String view35 = ("Tc");
mButton35.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view35);
break;

//case R.id.btn36:
//String view36 = ("Td");
//mButton36.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view36);
//break;
//
//case R.id.btn37:
//String view37 = ("Te");
//mButton37.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view37);
//break;
//
//case R.id.btn38:
//String view38 = ("Tf");
//mButton38.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view38);
//break;
//
//case R.id.btn39:
//String view39 = ("Tg");
//mButton39.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view39);
//break;
//
//case R.id.btn40:
//String view40 = ("Th");
//mButton40.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view40);
//break;
//
//case R.id.btn41:
//String view41 = ("Ti");
//mButton41.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view41);
//break;

case R.id.btn42:
String view42 = ("Tj");
mButton42.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view42);
break;

case R.id.btn43:
String view43 = ("Tk");
mButton43.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view43);
break;

case R.id.btn44:
String view44 = ("Tl");
mButton44.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view44);
break;

case R.id.btn45:
String view45 = ("Tm");
mButton45.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view45);
break;

case R.id.btn46:
String view46 = ("Tn");
mButton46.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view46);
break;

case R.id.btn47:
String view47 = ("To");
mButton47.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view47);
break;

case R.id.btn48:
String view48 = ("Tp");
mButton48.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view48);
break;

case R.id.btn49:
String view49 = ("Tq");
mButton49.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view49);
break;

case R.id.btn50:
String view50 = ("Tr");
mButton50.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view50);
break;

case R.id.btn51:
String view51 = ("Ts");
mButton51.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view51);
break;

case R.id.btn52:
String view52 = ("Tt");
mButton52.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view52);
	break;

case R.id.btn53:
String view53 = ("Tu");
mButton53.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view53);
	break;

//case R.id.btn54:
//String view54 = ("Xa");
//mButton54.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view54);
//	break;
//
//case R.id.btn55:
//String view55 = ("Xb");
//mButton55.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view55);
//	break;	
//
//case R.id.btn56:
//String view56 = ("Xc");
//mButton56.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view56);
//	break;	
//
//case R.id.btn57:
//String view57 = ("Xd");
//mButton57.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view57);
//	break;
//
//case R.id.btn58:
//String view58 = ("Xe");
//mButton58.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view58);
//	break;
//
//case R.id.btn59:
//String view59 = ("Xf");
//mButton59.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view59);
//	break;
//
//case R.id.btn60:
//String view60 = ("Xg");
//mButton60.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view60);
//	break;
//
//case R.id.btn61:
//String view61 = ("Xh");
//mButton61.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view61);
//	break;	
//
//case R.id.btn62:
//String view62 = ("Xi");
//mButton62.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view62);
//	break;

case R.id.btn63:
String view63 = ("Xj");
mButton63.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view63);
	break;

case R.id.btn64:
String view64 = ("Xk");
mButton64.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view64);
	break;

case R.id.btn65:
String view65 = ("Xl");
mButton65.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view65);
	break;

case R.id.btn66:
String view66 = ("Xm");
mButton66.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view66);
	break;

case R.id.btn67:
String view67 = ("Xn");
mButton67.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view67);
	break;

case R.id.btn68:
String view68 = ("Xo");
mButton68.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view68);
	break;

//case R.id.btn69:
//String view69 = ("Ya");
//mButton69.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view69);
//	break;
//
//case R.id.btn70:
//String view70 = ("Yb");
//mButton70.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view70);
//	break;
//
//case R.id.btn71:
//String view71 = ("Yc");
//mButton71.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view71);
//	break;
//
//case R.id.btn72:
//String view72 = ("Yd");
//mButton72.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view72);
//	break;
//
//case R.id.btn73:
//String view73 = ("Ye");
//mButton73.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view73);
//	break;
//
//case R.id.btn74:
//String view74 = ("Yf");
//mButton74.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view74);
//	break;
//
//case R.id.btn75:
//String view75 = ("Yg");
//mButton75.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view75);
//	break;
//
//case R.id.btn76:
//String view76 = ("Yh");
//mButton76.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view76);
//	break;
//
//case R.id.btn77:
//String view77 = ("Yi");
//mButton77.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view77);
//	break;

case R.id.btn78:
String view78 = ("Yj");
mButton78.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view78);
	break;

case R.id.btn79:
String view79 = ("Yk");
mButton79.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view79);
	break;

case R.id.btn80:
String view80 = ("Yl");
mButton80.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view80);
	break;

case R.id.btn81:
String view81 = ("Ym");
mButton81.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view81);
	break;

case R.id.btn82:
String view82 = ("Yn");
mButton82.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view82);
	break;

case R.id.btn83:
String view83 = ("Yo");
mButton83.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view83);
	break;
	
//case R.id.btn84:
//String view84 = ("Za");
//mButton84.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view84);
//	break;
//	
//case R.id.btn85:
//String view85 = ("Zb");
//mButton85.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view85);
//	break;
//	
//case R.id.btn86:
//String view86 = ("Zc");
//mButton86.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view86);
//	break;
//	
//case R.id.btn87:
//String view87 = ("Zd");
//mButton87.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view87);
//	break;
//	
//case R.id.btn88:
//String view88 = ("Ze");
//mButton88.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view88);
//	break;
//	
//case R.id.btn89:
//String view89 = ("Zf");
//mButton89.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view89);
//	break;
//	
//case R.id.btn90:
//String view90 = ("Zg");
//mButton90.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view90);
//	break;
//	
//case R.id.btn91:
//String view91 = ("Zh");
//mButton91.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view91);
//	break;
//	
//case R.id.btn92:
//String view92 = ("Zi");
//mButton92.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view92);
//	break;
	
case R.id.btn93:
String view93 = ("Zj");
mButton93.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view93);
	break;
	
case R.id.btn94:
String view94 = ("Zk");
mButton94.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view94);
	break;
	
case R.id.btn95:
String view95 = ("Zl");
mButton95.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view95);
	break;
	
case R.id.btn96:
String view96 = ("Zm");
mButton96.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view96);
	break;
	
case R.id.btn97:
String view97 = ("Zn");
mButton97.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view97);
	break;
	
case R.id.btn98:
String view98 = ("Zo");
mButton98.setTextColor(Color.parseColor("#FF0000"));
marco2setbtn.append(view98);
	break;
	
//case R.id.btn99:
//String view99 = ("Xp");
//mButton99.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view99);
//	break;
//	
//case R.id.btn100:
//String view100 = ("Xq");
//mButton100.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view100);
//	break;
//	
//case R.id.btn101:
//String view101 = ("Xr");
//mButton101.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view101);
//	break;
//	
//case R.id.btn102:
//String view102 = ("Xs");
//mButton102.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view102);
//	break;
//	
//case R.id.btn103:
//String view103 = ("Xt");
//mButton103.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view103);
//	break;
//	
//case R.id.btn104:
//String view104 = ("Xu");
//mButton104.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view104);
//	break;
//	
//case R.id.btn105:
//String view105 = ("Yp");
//mButton105.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view105);
//	break;
//	
//case R.id.btn106:
//String view106 = ("Yq");
//mButton106.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view106);
//	break;
//	
//case R.id.btn107:
//String view107 = ("Yr");
//mButton107.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view107);
//	break;
//	
//case R.id.btn108:
//String view108 = ("Ys");
//mButton10.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view108);
//	break;
//	
//case R.id.btn109:
//String view109 = ("Yt");
//mButton109.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view109);
//	break;
//	
//case R.id.btn110:
//String view110 = ("Yu");
//mButton110.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view110);
//	break;
//	
//case R.id.btn111:
//String view111 = ("Zp");
//mButton111.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view111);
//	break;
//	
//case R.id.btn112:
//String view112 = ("Zq");
//mButton112.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view112);
//	break;
//	
//case R.id.btn113:
//String view113 = ("Zr");
//mButton113.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view113);
//	break;
//	
//case R.id.btn114:
//String view114 = ("Zs");
//mButton114.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view114);
//	break;
//	
//case R.id.btn115:
//String view115 = ("Zt");
//mButton115.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view115);
//	break;
//	
//case R.id.btn116:
//String view116 = ("Zt");
//mButton116.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view116);
//	break;
	
case R.id.btn153:
	String view153 = ("Tv");
	marco2setbtn.append(view153);
		break;
		
case R.id.btn154:
	String view154 = ("Tw");
	marco2setbtn.append(view154);
		break;
		
case R.id.btn155:
	String view155 = ("Tx");
	marco2setbtn.append(view155);
		break;
		
case R.id.btn156:
	String view156 = ("Ty");
	marco2setbtn.append(view156);
		break;
	
case R.id.window2:
	String window2 = ("W+qa1");
	marco2setbtn.append(window2);
	break;
	
case R.id.window3:
	String window3 = ("W+qb1");
	marco2setbtn.append(window3);
	break;
	
case R.id.window4:
	String window4 = ("W+qc1");
	marco2setbtn.append(window4);
	break;
	
case R.id.door1 :
	String door1 = ("W+sa1");
	marco2setbtn.append(door1);	
	break;
	
case R.id.door2 :
	String door2 = ("W+sb1");
	marco2setbtn.append(door2);	
	break;
	
case R.id.door3 :
	String door3 = ("W+sc1");
	marco2setbtn.append(door3);	
	break;
	
	//tv home
//case R.id.button25:
//	String view39 = ("Tg");
//	String message39 = view39.toString();
//	marco2setbtn.append(message39);
//	break;
//	//tv 靜音
//case R.id.button26:
//	String view41 = ("Ti");
//    String message41 = view41.toString();
//    marco2setbtn.append(message41);
//	break;

	}
}
//marco4
public void Click3Handler(View v){
	// TODO Auto-generated catch block
switch(v.getId()){

case R.id.marcoset3ok :
	
	saveData(8);
	Intent intent6 = new Intent();
    intent6.setClass(BluetoothChat.this, BluetoothChat.class);
    finish();
    startActivity(intent6);
    
    break;

case R.id.s16 :
	String delay16 = ("'");
	marco3setbtn.append(delay16);
break;

case R.id.s17 :
	String delay17 = ("(");
	marco3setbtn.append(delay17);
	break;
	
case R.id.s18 :
	String delay18 = (")");
	marco3setbtn.append(delay18);
	break;
	
case R.id.s19 :
	String delay19 = ("*");
	marco3setbtn.append(delay19);
	break;
	
case R.id.s20 :
	String delay20 = ("+");
	marco3setbtn.append(delay20);
	break;

case R.id.btn01:
	
	if (count1 == 0 )
	{
	String marco4btn1 = ("a4");
	mButton01.setTextColor(Color.parseColor("#FF0000"));
	mButton01.setBackgroundResource(R.drawable.set_after_9);
	marco3setbtn.append(marco4btn1);
	count1 = 1;
	}
    else if(count1 == 1)
    {
    mButton01.setTextColor(Color.parseColor("#ffffff"));
	mButton01.setBackgroundResource(R.drawable.set_befor_9);
    String btn01 = marco3setbtn.getText().toString();
    String newa = btn01.replaceFirst("a4", "a5");
    marco3setbtn.setText(newa);
    count1 = 2;
    }
    else if (count1 == 2)
    {
    	String btn01 = marco3setbtn.getText().toString();
        String newa = btn01.replaceFirst("a5", "");
        marco3setbtn.setText(newa);
    	count1 = 0;
    }
	
	break;
	
case R.id.btn02:
	
	if (count2 == 0 )
	{
	String marco4btn2 = ("b4");
	mButton02.setTextColor(Color.parseColor("#FF0000"));
	mButton02.setBackgroundResource(R.drawable.set_after_9);
	marco3setbtn.append(marco4btn2);
	count2 = 1;
	}
	else if(count2 == 1)
	{
	mButton02.setTextColor(Color.parseColor("#ffffff"));
	mButton02.setBackgroundResource(R.drawable.set_befor_9);
    String btn02 = marco3setbtn.getText().toString();
    String newa = btn02.replaceFirst("b4", "b5");
    marco3setbtn.setText(newa);
    count2 = 2;
	}
	else if (count2 == 2)
    {
    String btn02 = marco3setbtn.getText().toString();
    String newa = btn02.replaceFirst("b5", "");
    marco3setbtn.setText(newa);
    	count2 = 0;
    }
	break;	
	
case R.id.btn03:
	if (count3 == 0 )
	{
	String marco4btn3 = ("c4");
	mButton03.setTextColor(Color.parseColor("#FF0000"));
	mButton03.setBackgroundResource(R.drawable.set_after_9);
	marco3setbtn.append(marco4btn3);
	count3 = 1;
	}
	else if(count3 == 1)
	{
	mButton03.setTextColor(Color.parseColor("#ffffff"));
	mButton03.setBackgroundResource(R.drawable.set_befor_9);
    String btn03 = marco3setbtn.getText().toString();
    String newa = btn03.replaceFirst("c4", "c5");
    marco3setbtn.setText(newa);
    count3 = 2;
	}
	else if (count3 == 2)
    {
    String btn03 = marco3setbtn.getText().toString();
    String newa = btn03.replaceFirst("c5", "");
    marco3setbtn.setText(newa);
    	count3 = 0;
    }
	break;	
	
case R.id.btn04:
	if (count4 == 0 )
	{
	String view4 = ("d4");
	mButton04.setTextColor(Color.parseColor("#FF0000"));
	mButton04.setBackgroundResource(R.drawable.set_after_9);
	marco3setbtn.append(view4);
	count4 = 1;
	}
	else if(count4 == 1)
	{
	mButton04.setTextColor(Color.parseColor("#ffffff"));
	mButton04.setBackgroundResource(R.drawable.set_befor_9);
    String btn04 = marco3setbtn.getText().toString();
    String newa = btn04.replaceFirst("d4", "d5");
    marco3setbtn.setText(newa);
    count4 = 2;
	}
	else if (count4 == 2)
    {
    String btn04 = marco3setbtn.getText().toString();
    String newa = btn04.replaceFirst("d5", "");
    marco3setbtn.setText(newa);
    	count4 = 0;
    }
	break;	
	
case R.id.btn05:
	if (count5 == 0 )
	{
	String view5 = ("e4");
	mButton05.setTextColor(Color.parseColor("#FF0000"));
	mButton05.setBackgroundResource(R.drawable.set_after_9);
	marco3setbtn.append(view5);
	count5 = 1;
	}
	else if(count5 == 1)
	{
	mButton05.setTextColor(Color.parseColor("#ffffff"));
	mButton05.setBackgroundResource(R.drawable.set_befor_9);
    String btn05 = marco3setbtn.getText().toString();
    String newa = btn05.replaceFirst("e4", "e5");
    marco3setbtn.setText(newa);
    count5 = 2;
	}
	else if (count5 == 2)
    {
    String btn05 = marco3setbtn.getText().toString();
    String newa = btn05.replaceFirst("e5", "");
    marco3setbtn.setText(newa);
    	count5 = 0;
    }
	break;	
	
case R.id.btn06:
	if (count6 == 0 )
	{
	String view6 = ("f4");
	mButton06.setTextColor(Color.parseColor("#FF0000"));
	mButton06.setBackgroundResource(R.drawable.set_after_9);
	marco3setbtn.append(view6);
	count6 = 1;
	}
	else if(count6 == 1)
	{
	mButton06.setTextColor(Color.parseColor("#ffffff"));
	mButton06.setBackgroundResource(R.drawable.set_befor_9);
    String btn06 = marco3setbtn.getText().toString();
    String newa = btn06.replaceFirst("f4", "f5");
    marco3setbtn.setText(newa);
    count6 = 2;
	}
	else if (count6 == 2)
    {
    String btn06 = marco3setbtn.getText().toString();
    String newa = btn06.replaceFirst("f5", "");
    marco3setbtn.setText(newa);
    	count6 = 0;
    }
	break;
	
case R.id.btn07:
	if (count7 == 0 )
	{
	String view7 = ("g4");
	mButton07.setTextColor(Color.parseColor("#FF0000"));
	mButton07.setBackgroundResource(R.drawable.set_after_9);
	marco3setbtn.append(view7);
	count7 = 1;
	}
	else if(count7 == 1)
	{
	mButton07.setTextColor(Color.parseColor("#ffffff"));
	mButton07.setBackgroundResource(R.drawable.set_befor_9);
    String btn07 = marco3setbtn.getText().toString();
    String newa = btn07.replaceFirst("g4", "g5");
    marco3setbtn.setText(newa);
    count7 = 2;
	}
	else if (count7 == 2)
    {
    String btn07 = marco3setbtn.getText().toString();
    String newa = btn07.replaceFirst("g5", "");
    marco3setbtn.setText(newa);
    	count7 = 0;
    }
	break;
	
case R.id.btn08:
	if (count8 == 0 )
	{
	String view8 = ("h4");
	mButton08.setTextColor(Color.parseColor("#FF0000"));
	mButton08.setBackgroundResource(R.drawable.set_after_9);
	marco3setbtn.append(view8);
	count8 = 1;
	}
	else if(count8 == 1)
	{
	mButton08.setTextColor(Color.parseColor("#ffffff"));
	mButton08.setBackgroundResource(R.drawable.set_befor_9);
    String btn08 = marco3setbtn.getText().toString();
    String newa = btn08.replaceFirst("h4", "h5");
    marco3setbtn.setText(newa);
    count8 = 2;
	}
	else if (count8 == 2)
    {
    String btn08 = marco3setbtn.getText().toString();
    String newa = btn08.replaceFirst("h5", "");
    marco3setbtn.setText(newa);
    	count8 = 0;
    }
	break;	
	
case R.id.btn09:
	if (count9 == 0 )
	{
	String view9 = ("i4");
	mButton09.setTextColor(Color.parseColor("#FF0000"));
	mButton09.setBackgroundResource(R.drawable.set_after_9);
	marco3setbtn.append(view9);
	count9 = 1;
	}
	else if(count9 == 1)
	{
	mButton09.setTextColor(Color.parseColor("#ffffff"));
	mButton09.setBackgroundResource(R.drawable.set_befor_9);
    String btn09 = marco3setbtn.getText().toString();
    String newa = btn09.replaceFirst("i4", "i5");
    marco3setbtn.setText(newa);
    count9 = 2;
	}
	else if (count9 == 2)
    {
    String btn09 = marco3setbtn.getText().toString();
    String newa = btn09.replaceFirst("i5", "");
    marco3setbtn.setText(newa);
    	count9 = 0;
    }
	break;
	
case R.id.btn10:
	if (count10 == 0 )
	{
	String view10 = ("j4");
	mButton10.setTextColor(Color.parseColor("#FF0000"));
	mButton10.setBackgroundResource(R.drawable.set_after_9);
	marco3setbtn.append(view10);
	count10 = 1;
	}
	else if(count10 == 1)
	{
	mButton10.setTextColor(Color.parseColor("#ffffff"));
	mButton10.setBackgroundResource(R.drawable.set_befor_9);
    String btn10 = marco3setbtn.getText().toString();
    String newa = btn10.replaceFirst("j4", "j5");
    marco3setbtn.setText(newa);
    count10 = 2;
	}
	else if (count10 == 2)
    {
    String btn10 = marco3setbtn.getText().toString();
    String newa = btn10.replaceFirst("j5", "");
    marco3setbtn.setText(newa);
    	count10 = 0;
    }
	break;
	
case R.id.btn11:
	if (count11 == 0 )
	{
	String view11 = ("k4");
	mButton11.setTextColor(Color.parseColor("#FF0000"));
	mButton11.setBackgroundResource(R.drawable.set_after_9);
	marco3setbtn.append(view11);
	count11 = 1;
	}
	else if(count11 == 1)
	{
	mButton11.setTextColor(Color.parseColor("#ffffff"));
	mButton11.setBackgroundResource(R.drawable.set_befor_9);
    String btn11 = marco3setbtn.getText().toString();
    String newa = btn11.replaceFirst("k4", "k5");
    marco3setbtn.setText(newa);
    count11 = 2;
	}
	else if (count11 == 2)
    {
    String btn11 = marco3setbtn.getText().toString();
    String newa = btn11.replaceFirst("k5", "");
    marco3setbtn.setText(newa);
    	count11 = 0;
    }
	break;
	
case R.id.btn12:
	if (count12 == 0 )
	{
	String view12 = ("l4");
	mButton12.setTextColor(Color.parseColor("#FF0000"));
	mButton12.setBackgroundResource(R.drawable.set_after_9);
	marco3setbtn.append(view12);
	count12 = 1;
	}
	else if(count12 == 1)
	{
	mButton12.setTextColor(Color.parseColor("#ffffff"));
	mButton12.setBackgroundResource(R.drawable.set_befor_9);
    String btn12 = marco3setbtn.getText().toString();
    String newa = btn12.replaceFirst("l4", "l5");
    marco3setbtn.setText(newa);
    count12 = 2;
	}
	else if (count12 == 2)
    {
    String btn12 = marco3setbtn.getText().toString();
    String newa = btn12.replaceFirst("l5", "");
    marco3setbtn.setText(newa);
    	count12 = 0;
    }
	break;

case R.id.btn13:
String view13 = ("Aa");
mButton13.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view13);

break;

case R.id.btn14:
String view14 = ("Ab");
mButton14.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view14);

break;

case R.id.btn15:
String view15 = ("Ac");
mButton15.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view15);
break;

case R.id.btn16:
String view16 = ("Ad");
mButton16.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view16);
break;

case R.id.btn17:
String view17 = ("Ae");
mButton17.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view17);
break;

case R.id.btn18:
String view18 = ("Af");
mButton18.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view18);
break;

case R.id.btn19:
String view19 = ("Ag");
mButton19.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view19);
break;

case R.id.btn20:
String view20 = ("Ah");
mButton20.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view20);
break;

case R.id.btn21:
String view21 = ("Ai");
mButton21.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view21);
break;

case R.id.btn22:
String view22 = ("Aj");
mButton22.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view22);
break;

case R.id.btn23:
String view23 = ("Ak");
mButton23.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view23);
break;

case R.id.btn24:
String view24 = ("Al");
mButton24.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view24);
break;

//case R.id.btn25:
//String view25 = ("Am");
//mButton25.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view25);
//break;
//
//case R.id.btn26:
//String view26 = ("An");
//mButton26.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view26);
//break;
//
//case R.id.btn27:
//String view27 = ("Ao");
//mButton27.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view27);
//break;
//
//case R.id.btn28:
//String view28 = ("Ap");
//mButton28.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view28);
//break;

//case R.id.btn29:
//String view29 = ("Aq");
//mButton29.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view29);
//break;
//
//case R.id.btn30:
//String view30 = ("Ar");
//mButton30.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view30);
//break;
//
//case R.id.btn31:
//String view31 = ("As");
//mButton31.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view31);
//break;

//case R.id.btn32:
//String view32 = ("At");
//mButton32.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view32);
//break;

case R.id.btn33:
String view33 = ("Ta");
mButton33.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view33);
break;

//case R.id.btn34:
//String view34 = ("Tb");
//mButton34.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view34);
//break;

case R.id.btn35:
String view35 = ("Tc");
mButton35.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view35);
break;

//case R.id.btn36:
//String view36 = ("Td");
//mButton36.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view36);
//break;
//
//case R.id.btn37:
//String view37 = ("Te");
//mButton37.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view37);
//break;
//
//case R.id.btn38:
//String view38 = ("Tf");
//mButton38.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view38);
//break;
//
//case R.id.btn39:
//String view39 = ("Tg");
//mButton39.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view39);
//break;
//
//case R.id.btn40:
//String view40 = ("Th");
//mButton40.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view40);
//break;
//
//case R.id.btn41:
//String view41 = ("Ti");
//mButton41.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view41);
//break;

case R.id.btn42:
String view42 = ("Tj");
mButton42.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view42);
break;

case R.id.btn43:
String view43 = ("Tk");
mButton43.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view43);
break;

case R.id.btn44:
String view44 = ("Tl");
mButton44.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view44);
break;

case R.id.btn45:
String view45 = ("Tm");
mButton45.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view45);
break;

case R.id.btn46:
String view46 = ("Tn");
mButton46.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view46);
break;

case R.id.btn47:
String view47 = ("To");
mButton47.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view47);
break;

case R.id.btn48:
String view48 = ("Tp");
mButton48.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view48);
break;

case R.id.btn49:
String view49 = ("Tq");
mButton49.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view49);
break;

case R.id.btn50:
String view50 = ("Tr");
mButton50.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view50);
break;

case R.id.btn51:
String view51 = ("Ts");
mButton51.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view51);
break;

case R.id.btn52:
String view52 = ("Tt");
mButton52.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view52);
	break;

case R.id.btn53:
String view53 = ("Tu");
mButton53.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view53);
	break;

//case R.id.btn54:
//String view54 = ("Xa");
//mButton54.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view54);
//	break;
//
//case R.id.btn55:
//String view55 = ("Xb");
//mButton55.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view55);
//	break;	
//
//case R.id.btn56:
//String view56 = ("Xc");
//mButton56.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view56);
//	break;	
//
//case R.id.btn57:
//String view57 = ("Xd");
//mButton57.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view57);
//	break;
//
//case R.id.btn58:
//String view58 = ("Xe");
//mButton58.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view58);
//	break;
//
//case R.id.btn59:
//String view59 = ("Xf");
//mButton59.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view59);
//	break;
//
//case R.id.btn60:
//String view60 = ("Xg");
//mButton60.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view60);
//	break;
//
//case R.id.btn61:
//String view61 = ("Xh");
//mButton61.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view61);
//	break;	
//
//case R.id.btn62:
//String view62 = ("Xi");
//mButton62.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view62);
//	break;

case R.id.btn63:
String view63 = ("Xj");
mButton63.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view63);
	break;

case R.id.btn64:
String view64 = ("Xk");
mButton64.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view64);
	break;

case R.id.btn65:
String view65 = ("Xl");
mButton65.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view65);
	break;

case R.id.btn66:
String view66 = ("Xm");
mButton66.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view66);
	break;

case R.id.btn67:
String view67 = ("Xn");
mButton67.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view67);
	break;

case R.id.btn68:
String view68 = ("Xo");
mButton68.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view68);
	break;

//case R.id.btn69:
//String view69 = ("Ya");
//mButton69.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view69);
//	break;
//
//case R.id.btn70:
//String view70 = ("Yb");
//mButton70.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view70);
//	break;
//
//case R.id.btn71:
//String view71 = ("Yc");
//mButton71.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view71);
//	break;
//
//case R.id.btn72:
//String view72 = ("Yd");
//mButton72.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view72);
//	break;
//
//case R.id.btn73:
//String view73 = ("Ye");
//mButton73.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view73);
//	break;
//
//case R.id.btn74:
//String view74 = ("Yf");
//mButton74.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view74);
//	break;
//
//case R.id.btn75:
//String view75 = ("Yg");
//mButton75.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view75);
//	break;
//
//case R.id.btn76:
//String view76 = ("Yh");
//mButton76.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view76);
//	break;
//
//case R.id.btn77:
//String view77 = ("Yi");
//mButton77.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view77);
//	break;

case R.id.btn78:
String view78 = ("Yj");
mButton78.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view78);
	break;

case R.id.btn79:
String view79 = ("Yk");
mButton79.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view79);
	break;

case R.id.btn80:
String view80 = ("Yl");
mButton80.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view80);
	break;

case R.id.btn81:
String view81 = ("Ym");
mButton81.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view81);
	break;

case R.id.btn82:
String view82 = ("Yn");
mButton82.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view82);
	break;

case R.id.btn83:
String view83 = ("Yo");
mButton83.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view83);
	break;
	
//case R.id.btn84:
//String view84 = ("Za");
//mButton84.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view84);
//	break;
//	
//case R.id.btn85:
//String view85 = ("Zb");
//mButton85.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view85);
//	break;
//	
//case R.id.btn86:
//String view86 = ("Zc");
//mButton86.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view86);
//	break;
//	
//case R.id.btn87:
//String view87 = ("Zd");
//mButton87.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view87);
//	break;
//	
//case R.id.btn88:
//String view88 = ("Ze");
//mButton88.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view88);
//	break;
//	
//case R.id.btn89:
//String view89 = ("Zf");
//mButton89.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view89);
//	break;
//	
//case R.id.btn90:
//String view90 = ("Zg");
//mButton90.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view90);
//	break;
//	
//case R.id.btn91:
//String view91 = ("Zh");
//mButton91.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view91);
//	break;
//	
//case R.id.btn92:
//String view92 = ("Zi");
//mButton92.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view92);
//	break;
	
case R.id.btn93:
String view93 = ("Zj");
mButton93.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view93);
	break;
	
case R.id.btn94:
String view94 = ("Zk");
mButton94.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view94);
	break;
	
case R.id.btn95:
String view95 = ("Zl");
mButton95.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view95);
	break;
	
case R.id.btn96:
String view96 = ("Zm");
mButton96.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view96);
	break;
	
case R.id.btn97:
String view97 = ("Zn");
mButton97.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view97);
	break;
	
case R.id.btn98:
String view98 = ("Zo");
mButton98.setTextColor(Color.parseColor("#FF0000"));
marco3setbtn.append(view98);
	break;
	
//case R.id.btn99:
//String view99 = ("Xp");
//mButton99.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view99);
//	break;
//	
//case R.id.btn100:
//String view100 = ("Xq");
//mButton100.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view100);
//	break;
	
//case R.id.btn101:
//String view101 = ("Xr");
//mButton101.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view101);
//	break;
//	
//case R.id.btn102:
//String view102 = ("Xs");
//mButton102.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view102);
//	break;
//	
//case R.id.btn103:
//String view103 = ("Xt");
//mButton103.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view103);
//	break;
//	
//case R.id.btn104:
//String view104 = ("Xu");
//mButton104.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view104);
//	break;
//	
//case R.id.btn105:
//String view105 = ("Yp");
//mButton105.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view105);
//	break;
//	
//case R.id.btn106:
//String view106 = ("Yq");
//mButton106.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view106);
//	break;
//	
//case R.id.btn107:
//String view107 = ("Yr");
//mButton107.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view107);
//	break;
//	
//case R.id.btn108:
//String view108 = ("Ys");
//mButton10.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view108);
//	break;
//	
//case R.id.btn109:
//String view109 = ("Yt");
//mButton109.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view109);
//	break;
//	
//case R.id.btn110:
//String view110 = ("Yu");
//mButton110.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view110);
//	break;
	
//case R.id.btn111:
//String view111 = ("Zp");
//mButton111.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view111);
//	break;
//	
//case R.id.btn112:
//String view112 = ("Zq");
//mButton112.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view112);
//	break;
//	
//case R.id.btn113:
//String view113 = ("Zr");
//mButton113.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view113);
//	break;
//	
//case R.id.btn114:
//	String view114 = ("Zs");
//	mButton114.setTextColor(Color.parseColor("#FF0000"));
//	marco3setbtn.append(view114);
//	break;
//	
//case R.id.btn115:
//	String view115 = ("Zt");
//	mButton115.setTextColor(Color.parseColor("#FF0000"));
//	marco3setbtn.append(view115);
//	break;
//	
//case R.id.btn116:
//	String view116 = ("Zt");
//	mButton116.setTextColor(Color.parseColor("#FF0000"));
//	marco3setbtn.append(view116);
//	break;
	
case R.id.btn153:
	String view153 = ("Tv");
	marco3setbtn.append(view153);
		break;
		
case R.id.btn154:
	String view154 = ("Tw");
	marco3setbtn.append(view154);
		break;
		
case R.id.btn155:
	String view155 = ("Tx");
	marco3setbtn.append(view155);
		break;
		
case R.id.btn156:
	String view156 = ("Ty");
	marco3setbtn.append(view156);
		break;
	
case R.id.window2:
	String window2 = ("W+qa1");
	marco3setbtn.append(window2);
	break;
	
case R.id.window3:
	String window3 = ("W+qb1");
	marco3setbtn.append(window3);
	break;
	
case R.id.window4:
	String window4 = ("W+qc1");
	marco3setbtn.append(window4);
	break;

case R.id.door1 :
	String door1 = ("W+sa1");
	marco3setbtn.append(door1);	
	break;
	
case R.id.door2 :
	String door2 = ("W+sb1");
	marco3setbtn.append(door2);	
	break;
	
case R.id.door3 :
	String door3 = ("W+sc1");
	marco3setbtn.append(door3);	
	break;
	
	//tv home
//case R.id.button25:
//	String view39 = ("Tg");
//	String message39 = view39.toString();
//	marco3setbtn.append(message39);
//	break;
//	//tv 靜音
//case R.id.button26:
//	String view41 = ("Ti");
//    String message41 = view41.toString();
//    marco3setbtn.append(message41);
//	break;
	
	}
}
	//marco5
public void Click4Handler(View v){
	// TODO Auto-generated catch block
	
	
switch(v.getId()){

case R.id.marcoset4ok :
	
	saveData(9);
	Intent intent7 = new Intent();
    intent7.setClass(BluetoothChat.this, BluetoothChat.class);
    finish();
    startActivity(intent7);
    
    break;

case R.id.s21 :
	String delay21 = ("'");
	marco4setbtn.append(delay21);
break;

case R.id.s22 :
	String delay22 = ("(");
	marco4setbtn.append(delay22);
	break;
	
case R.id.s23 :
	String delay23 = (")");
	marco4setbtn.append(delay23);
	break;
	
case R.id.s24 :
	String delay24 = ("*");
	marco4setbtn.append(delay24);
	break;
	
case R.id.s25 :
	String delay25 = ("+");
	marco4setbtn.append(delay25);
	break;

case R.id.btn01:
	
	if (count1 == 0 )
	{
	String view = ("a4");
	mButton01.setTextColor(Color.parseColor("#FF0000"));
	mButton01.setBackgroundResource(R.drawable.set_after_9);
	marco4setbtn.append(view);
	count1 = 1;
	}
    else if(count1 == 1)
    {
    mButton01.setTextColor(Color.parseColor("#ffffff"));
	mButton01.setBackgroundResource(R.drawable.set_befor_9);
    String btn01 = marco4setbtn.getText().toString();
    String newa = btn01.replaceFirst("a4", "a5");
    marco4setbtn.setText(newa);
    count1 = 2;
    }
    else if (count1 == 2)
    {
    	String btn01 = marco4setbtn.getText().toString();
        String newa = btn01.replaceFirst("a5", "");
        marco4setbtn.setText(newa);
    	count1 = 0;
    }
	
	break;
	
case R.id.btn02:
	
	if (count2 == 0 )
	{
	String view2 = ("b4");
	mButton02.setTextColor(Color.parseColor("#FF0000"));
	mButton02.setBackgroundResource(R.drawable.set_after_9);
	marco4setbtn.append(view2);
	count2 = 1;
	}
	else if(count2 == 1)
	{
	mButton02.setTextColor(Color.parseColor("#ffffff"));
	mButton02.setBackgroundResource(R.drawable.set_befor_9);
    String btn02 = marco4setbtn.getText().toString();
    String newa = btn02.replaceFirst("b4", "b5");
    marco4setbtn.setText(newa);
    count2 = 2;
	}
	else if (count2 == 2)
    {
    String btn02 = marco4setbtn.getText().toString();
    String newa = btn02.replaceFirst("b5", "");
    marco4setbtn.setText(newa);
    	count2 = 0;
    }
	break;	
	
case R.id.btn03:
	if (count3 == 0 )
	{
	String view3 = ("c4");
	mButton03.setTextColor(Color.parseColor("#FF0000"));
	mButton03.setBackgroundResource(R.drawable.set_after_9);
	marco4setbtn.append(view3);
	count3 = 1;
	}
	else if(count3 == 1)
	{
	mButton03.setTextColor(Color.parseColor("#ffffff"));
	mButton03.setBackgroundResource(R.drawable.set_befor_9);
    String btn03 = marco4setbtn.getText().toString();
    String newa = btn03.replaceFirst("c4", "c5");
    marco4setbtn.setText(newa);
    count3 = 2;
	}
	else if (count3 == 2)
    {
    String btn03 = marco4setbtn.getText().toString();
    String newa = btn03.replaceFirst("c5", "");
    marco4setbtn.setText(newa);
    	count3 = 0;
    }
	break;	
	
case R.id.btn04:
	if (count4 == 0 )
	{
	String view4 = ("d4");
	mButton04.setTextColor(Color.parseColor("#FF0000"));
	mButton04.setBackgroundResource(R.drawable.set_after_9);
	marco4setbtn.append(view4);
	count4 = 1;
	}
	else if(count4 == 1)
	{
	mButton04.setTextColor(Color.parseColor("#ffffff"));
	mButton04.setBackgroundResource(R.drawable.set_befor_9);
    String btn04 = marco4setbtn.getText().toString();
    String newa = btn04.replaceFirst("d4", "d5");
    marco4setbtn.setText(newa);
    count4 = 2;
	}
	else if (count4 == 2)
    {
    String btn04 = marco4setbtn.getText().toString();
    String newa = btn04.replaceFirst("d5", "");
    marco4setbtn.setText(newa);
    	count4 = 0;
    }
	break;	
	
case R.id.btn05:
	if (count5 == 0 )
	{
	String view5 = ("e4");
	mButton05.setTextColor(Color.parseColor("#FF0000"));
	mButton05.setBackgroundResource(R.drawable.set_after_9);
	marco4setbtn.append(view5);
	count5 = 1;
	}
	else if(count5 == 1)
	{
	mButton05.setTextColor(Color.parseColor("#ffffff"));
	mButton05.setBackgroundResource(R.drawable.set_befor_9);
    String btn05 = marco4setbtn.getText().toString();
    String newa = btn05.replaceFirst("e4", "e5");
    marco4setbtn.setText(newa);
    count5 = 2;
	}
	else if (count5 == 2)
    {
    String btn05 = marco4setbtn.getText().toString();
    String newa = btn05.replaceFirst("e5", "");
    marco4setbtn.setText(newa);
    	count5 = 0;
    }
	break;	
	
case R.id.btn06:
	if (count6 == 0 )
	{
	String view6 = ("f4");
	mButton06.setTextColor(Color.parseColor("#FF0000"));
	mButton06.setBackgroundResource(R.drawable.set_after_9);
	marco4setbtn.append(view6);
	count6 = 1;
	}
	else if(count6 == 1)
	{
	mButton06.setTextColor(Color.parseColor("#ffffff"));
	mButton06.setBackgroundResource(R.drawable.set_befor_9);
    String btn06 = marco4setbtn.getText().toString();
    String newa = btn06.replaceFirst("f4", "f5");
    marco4setbtn.setText(newa);
    count6 = 2;
	}
	else if (count6 == 2)
    {
    String btn06 = marco4setbtn.getText().toString();
    String newa = btn06.replaceFirst("f5", "");
    marco4setbtn.setText(newa);
    	count6 = 0;
    }
	break;
	
case R.id.btn07:
	if (count7 == 0 )
	{
	String view7 = ("g4");
	mButton07.setTextColor(Color.parseColor("#FF0000"));
	mButton07.setBackgroundResource(R.drawable.set_after_9);
	marco4setbtn.append(view7);
	count7 = 1;
	}
	else if(count7 == 1)
	{
	mButton07.setTextColor(Color.parseColor("#ffffff"));
	mButton07.setBackgroundResource(R.drawable.set_befor_9);
    String btn07 = marco4setbtn.getText().toString();
    String newa = btn07.replaceFirst("g4", "g5");
    marco4setbtn.setText(newa);
    count7 = 2;
	}
	else if (count7 == 2)
    {
    String btn07 = marco4setbtn.getText().toString();
    String newa = btn07.replaceFirst("g5", "");
    marco4setbtn.setText(newa);
    	count7 = 0;
    }
	break;
	
case R.id.btn08:
	if (count8 == 0 )
	{
	String view8 = ("h4");
	mButton08.setTextColor(Color.parseColor("#FF0000"));
	mButton08.setBackgroundResource(R.drawable.set_after_9);
	marco4setbtn.append(view8);
	count8 = 1;
	}
	else if(count8 == 1)
	{
	mButton08.setTextColor(Color.parseColor("#ffffff"));
	mButton08.setBackgroundResource(R.drawable.set_befor_9);
    String btn08 = marco4setbtn.getText().toString();
    String newa = btn08.replaceFirst("h4", "h5");
    marco4setbtn.setText(newa);
    count8 = 2;
	}
	else if (count8 == 2)
    {
    String btn08 = marco4setbtn.getText().toString();
    String newa = btn08.replaceFirst("h5", "");
    marco4setbtn.setText(newa);
    	count8 = 0;
    }
	break;	
	
case R.id.btn09:
	if (count9 == 0 )
	{
	String view9 = ("i4");
	mButton09.setTextColor(Color.parseColor("#FF0000"));
	mButton09.setBackgroundResource(R.drawable.set_after_9);
	marco4setbtn.append(view9);
	count9 = 1;
	}
	else if(count9 == 1)
	{
	mButton09.setTextColor(Color.parseColor("#ffffff"));
	mButton09.setBackgroundResource(R.drawable.set_befor_9);
    String btn09 = marco4setbtn.getText().toString();
    String newa = btn09.replaceFirst("i4", "i5");
    marco4setbtn.setText(newa);
    count9 = 2;
	}
	else if (count9 == 2)
    {
    String btn09 = marco4setbtn.getText().toString();
    String newa = btn09.replaceFirst("i5", "");
    marco4setbtn.setText(newa);
    	count9 = 0;
    }
	break;
	
case R.id.btn10:
	if (count10 == 0 )
	{
	String view10 = ("j4");
	mButton10.setTextColor(Color.parseColor("#FF0000"));
	mButton10.setBackgroundResource(R.drawable.set_after_9);
	marco4setbtn.append(view10);
	count10 = 1;
	}
	else if(count10 == 1)
	{
	mButton10.setTextColor(Color.parseColor("#ffffff"));
	mButton10.setBackgroundResource(R.drawable.set_befor_9);
    String btn10 = marcosetbtn.getText().toString();
    String newa = btn10.replaceFirst("j4", "j5");
    marco4setbtn.setText(newa);
    count10 = 2;
	}
	else if (count10 == 2)
    {
    String btn10 = marco4setbtn.getText().toString();
    String newa = btn10.replaceFirst("j5", "");
    marco4setbtn.setText(newa);
    	count10 = 0;
    }
	break;
	
case R.id.btn11:
	if (count11 == 0 )
	{
	String view11 = ("k4");
	mButton11.setTextColor(Color.parseColor("#FF0000"));
	mButton11.setBackgroundResource(R.drawable.set_after_9);
	marco4setbtn.append(view11);
	count11 = 1;
	}
	else if(count11 == 1)
	{
	mButton11.setTextColor(Color.parseColor("#ffffff"));
	mButton11.setBackgroundResource(R.drawable.set_befor_9);
    String btn11 = marco4setbtn.getText().toString();
    String newa = btn11.replaceFirst("k4", "k5");
    marco4setbtn.setText(newa);
    count11 = 2;
	}
	else if (count11 == 2)
    {
    String btn11 = marco4setbtn.getText().toString();
    String newa = btn11.replaceFirst("k5", "");
    marco4setbtn.setText(newa);
    	count11 = 0;
    }
	break;
	
case R.id.btn12:
	if (count12 == 0 )
	{
	String view12 = ("l4");
	mButton12.setTextColor(Color.parseColor("#FF0000"));
	mButton12.setBackgroundResource(R.drawable.set_after_9);
	marco4setbtn.append(view12);
	count12 = 1;
	}
	else if(count12 == 1)
	{
	mButton12.setTextColor(Color.parseColor("#ffffff"));
	mButton12.setBackgroundResource(R.drawable.set_befor_9);
    String btn12 = marco4setbtn.getText().toString();
    String newa = btn12.replaceFirst("l4", "l5");
    marco4setbtn.setText(newa);
    count12 = 2;
	}
	else if (count12 == 2)
    {
    String btn12 = marco4setbtn.getText().toString();
    String newa = btn12.replaceFirst("l5", "");
    marco4setbtn.setText(newa);
    	count12 = 0;
    }
	break;

case R.id.btn13:
String view13 = ("Aa");
mButton13.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view13);

break;

case R.id.btn14:
String view14 = ("Ab");
mButton14.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view14);

break;

case R.id.btn15:
String view15 = ("Ac");
mButton15.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view15);
break;

case R.id.btn16:
String view16 = ("Ad");
mButton16.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view16);
break;

case R.id.btn17:
String view17 = ("Ae");
mButton17.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view17);
break;

case R.id.btn18:
String view18 = ("Af");
mButton18.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view18);
break;

case R.id.btn19:
String view19 = ("Ag");
mButton19.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view19);
break;

case R.id.btn20:
String view20 = ("Ah");
mButton20.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view20);
break;

case R.id.btn21:
String view21 = ("Ai");
mButton21.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view21);
break;

case R.id.btn22:
String view22 = ("Aj");
mButton22.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view22);
break;

case R.id.btn23:
String view23 = ("Ak");
mButton23.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view23);
break;

case R.id.btn24:
String view24 = ("Al");
mButton24.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view24);
break;

//case R.id.btn25:
//String view25 = ("Am");
//mButton25.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view25);
//break;
//
//case R.id.btn26:
//String view26 = ("An");
//mButton26.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view26);
//break;
//
//case R.id.btn27:
//String view27 = ("Ao");
//mButton27.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view27);
//break;
//
//case R.id.btn28:
//String view28 = ("Ap");
//mButton28.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view28);
//break;
//
//case R.id.btn29:
//String view29 = ("Aq");
//mButton29.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view29);
//break;
//
//case R.id.btn30:
//String view30 = ("Ar");
//mButton30.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view30);
//break;
//
//case R.id.btn31:
//String view31 = ("As");
//mButton31.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view31);
//break;

//case R.id.btn32:
//String view32 = ("At");
//mButton32.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view32);
//break;

case R.id.btn33:
String view33 = ("Ta");
mButton33.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view33);
break;

//case R.id.btn34:
//String view34 = ("Tb");
//mButton34.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view34);
//break;

case R.id.btn35:
String view35 = ("Tc");
mButton35.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view35);
break;

//case R.id.btn36:
//String view36 = ("Td");
//mButton36.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view36);
//break;
//
//case R.id.btn37:
//String view37 = ("Te");
//mButton37.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view37);
//break;
//
//case R.id.btn38:
//String view38 = ("Tf");
//mButton38.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view38);
//break;

//case R.id.btn39:
//String view39 = ("Tg");
//mButton39.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view39);
//break;

//case R.id.btn40:
//String view40 = ("Th");
//mButton40.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view40);
//break;

//case R.id.btn41:
//String view41 = ("Ti");
//mButton41.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view41);
//break;

case R.id.btn42:
String view42 = ("Tj");
mButton42.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view42);
break;

case R.id.btn43:
String view43 = ("Tk");
mButton43.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view43);
break;

case R.id.btn44:
String view44 = ("Tl");
mButton44.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view44);
break;

case R.id.btn45:
String view45 = ("Tm");
mButton45.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view45);
break;

case R.id.btn46:
String view46 = ("Tn");
mButton46.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view46);
break;

case R.id.btn47:
String view47 = ("To");
mButton47.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view47);
break;

case R.id.btn48:
String view48 = ("Tp");
mButton48.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view48);
break;

case R.id.btn49:
String view49 = ("Tq");
mButton49.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view49);
break;

case R.id.btn50:
String view50 = ("Tr");
mButton50.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view50);
break;

case R.id.btn51:
String view51 = ("Ts");
mButton51.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view51);
break;

case R.id.btn52:
String view52 = ("Tt");
mButton52.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view52);
	break;

case R.id.btn53:
String view53 = ("Tu");
mButton53.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view53);
	break;

//case R.id.btn54:
//String view54 = ("Xa");
//mButton54.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view54);
//	break;
//
//case R.id.btn55:
//String view55 = ("Xb");
//mButton55.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view55);
//	break;	
//
//case R.id.btn56:
//String view56 = ("Xc");
//mButton56.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view56);
//	break;	
//
//case R.id.btn57:
//String view57 = ("Xd");
//mButton57.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view57);
//	break;
//
//case R.id.btn58:
//String view58 = ("Xe");
//mButton58.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view58);
//	break;
//
//case R.id.btn59:
//String view59 = ("Xf");
//mButton59.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view59);
//	break;
//
//case R.id.btn60:
//String view60 = ("Xg");
//mButton60.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view60);
//	break;
//
//case R.id.btn61:
//String view61 = ("Xh");
//mButton61.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view61);
//	break;	
//
//case R.id.btn62:
//String view62 = ("Xi");
//mButton62.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view62);
//	break;

case R.id.btn63:
String view63 = ("Xj");
mButton63.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view63);
	break;

case R.id.btn64:
String view64 = ("Xk");
mButton64.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view64);
	break;

case R.id.btn65:
String view65 = ("Xl");
mButton65.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view65);
	break;

case R.id.btn66:
String view66 = ("Xm");
mButton66.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view66);
	break;

case R.id.btn67:
String view67 = ("Xn");
mButton67.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view67);
	break;

case R.id.btn68:
String view68 = ("Xo");
mButton68.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view68);
	break;

//case R.id.btn69:
//String view69 = ("Ya");
//mButton69.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view69);
//	break;
//
//case R.id.btn70:
//String view70 = ("Yb");
//mButton70.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view70);
//	break;
//
//case R.id.btn71:
//String view71 = ("Yc");
//mButton71.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view71);
//	break;
//
//case R.id.btn72:
//String view72 = ("Yd");
//mButton72.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view72);
//	break;
//
//case R.id.btn73:
//String view73 = ("Ye");
//mButton73.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view73);
//	break;
//
//case R.id.btn74:
//String view74 = ("Yf");
//mButton74.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view74);
//	break;
//
//case R.id.btn75:
//String view75 = ("Yg");
//mButton75.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view75);
//	break;
//
//case R.id.btn76:
//String view76 = ("Yh");
//mButton76.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view76);
//	break;
//
//case R.id.btn77:
//String view77 = ("Yi");
//mButton77.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view77);
//	break;

case R.id.btn78:
String view78 = ("Yj");
mButton78.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view78);
	break;

case R.id.btn79:
String view79 = ("Yk");
mButton79.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view79);
	break;

case R.id.btn80:
String view80 = ("Yl");
mButton80.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view80);
	break;

case R.id.btn81:
String view81 = ("Ym");
mButton81.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view81);
	break;

case R.id.btn82:
String view82 = ("Yn");
mButton82.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view82);
	break;

case R.id.btn83:
String view83 = ("Yo");
mButton83.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view83);
	break;
	
//case R.id.btn84:
//String view84 = ("Za");
//mButton84.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view84);
//	break;
//	
//case R.id.btn85:
//String view85 = ("Zb");
//mButton85.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view85);
//	break;
//	
//case R.id.btn86:
//String view86 = ("Zc");
//mButton86.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view86);
//	break;
//	
//case R.id.btn87:
//String view87 = ("Zd");
//mButton87.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view87);
//	break;
//	
//case R.id.btn88:
//String view88 = ("Ze");
//mButton88.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view88);
//	break;
//	
//case R.id.btn89:
//String view89 = ("Zf");
//mButton89.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view89);
//	break;
//	
//case R.id.btn90:
//String view90 = ("Zg");
//mButton90.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view90);
//	break;
//	
//case R.id.btn91:
//String view91 = ("Zh");
//mButton91.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view91);
//	break;
//	
//case R.id.btn92:
//String view92 = ("Zi");
//mButton92.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view92);
//	break;
	
case R.id.btn93:
String view93 = ("Zj");
mButton93.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view93);
	break;
	
case R.id.btn94:
String view94 = ("Zk");
mButton94.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view94);
	break;
	
case R.id.btn95:
String view95 = ("Zl");
mButton95.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view95);
	break;
	
case R.id.btn96:
String view96 = ("Zm");
mButton96.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view96);
	break;
	
case R.id.btn97:
String view97 = ("Zn");
mButton97.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view97);
	break;
	
case R.id.btn98:
String view98 = ("Zo");
mButton98.setTextColor(Color.parseColor("#FF0000"));
marco4setbtn.append(view98);
	break;
	
//case R.id.btn99:
//String view99 = ("Xp");
//mButton99.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view99);
//	break;
//	
//case R.id.btn100:
//String view100 = ("Xq");
//mButton100.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view100);
//	break;
//	
//case R.id.btn101:
//String view101 = ("Xr");
//mButton101.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view101);
//	break;
//	
//case R.id.btn102:
//String view102 = ("Xs");
//mButton102.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view102);
//	break;
//	
//case R.id.btn103:
//String view103 = ("Xt");
//mButton103.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view103);
//	break;
//	
//case R.id.btn104:
//String view104 = ("Xu");
//mButton104.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view104);
//	break;
//	
//case R.id.btn105:
//String view105 = ("Yp");
//mButton105.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view105);
//	break;
//	
//case R.id.btn106:
//String view106 = ("Yq");
//mButton106.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view106);
//	break;
//	
//case R.id.btn107:
//String view107 = ("Yr");
//mButton107.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view107);
//	break;
//	
//case R.id.btn108:
//String view108 = ("Ys");
//mButton10.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view108);
//	break;
//	
//case R.id.btn109:
//String view109 = ("Yt");
//mButton109.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view109);
//	break;
//	
//case R.id.btn110:
//String view110 = ("Yu");
//mButton110.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view110);
//	break;
	
//case R.id.btn111:
//String view111 = ("Zp");
//mButton111.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view111);
//	break;
//	
//case R.id.btn112:
//String view112 = ("Zq");
//mButton112.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view112);
//	break;
//	
//case R.id.btn113:
//String view113 = ("Zr");
//mButton113.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view113);
//	break;
//	
//case R.id.btn114:
//String view114 = ("Zs");
//mButton114.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view114);
//	break;
//	
//case R.id.btn115:
//String view115 = ("Zt");
//mButton115.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view115);
//	break;
//	
//case R.id.btn116:
//String view116 = ("Zt");
//mButton116.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view116);
//	break;
	
case R.id.btn153:
	String view153 = ("Tv");
	marco4setbtn.append(view153);
		break;
		
case R.id.btn154:
	String view154 = ("Tw");
	marco4setbtn.append(view154);
		break;
		
case R.id.btn155:
	String view155 = ("Tx");
	marco4setbtn.append(view155);
		break;
		
case R.id.btn156:
	String view156 = ("Ty");
	marco4setbtn.append(view156);
		break;
	
case R.id.window2:
	String window2 = ("W+qa1");
	marco4setbtn.append(window2);
	break;
	
case R.id.window3:
	String window3 = ("W+qb1");
	marco4setbtn.append(window3);
	break;
	
case R.id.window4:
	String window4 = ("W+qc1");
	marco4setbtn.append(window4);
	break;
	
case R.id.door1 :
	String door1 = ("W+sa1");
	marco4setbtn.append(door1);	
	break;
	
case R.id.door2 :
	String door2 = ("W+sb1");
	marco4setbtn.append(door2);	
	break;
	
case R.id.door3 :
	String door3 = ("W+sc1");
	marco4setbtn.append(door3);	
	break;
	
	//tv home
//case R.id.button25:
//	String view39 = ("Tg");
//	String message39 = view39.toString();
//	marco4setbtn.append(message39);
//	break;
//	//tv 靜音
//case R.id.button26:
//	String view41 = ("Ti");
//    String message41 = view41.toString();
//    marco4setbtn.append(message41);
//	break;

	}
}

//marco6
public void Click5Handler(View v){
	// TODO Auto-generated catch block
	
	
switch(v.getId()){

case R.id.marcoset5ok :
	
	saveData(10);
	Intent intent8 = new Intent();
    intent8.setClass(BluetoothChat.this, BluetoothChat.class);
    finish();
    startActivity(intent8);
    
    break;

case R.id.s26 :
	String delay21 = ("'");
	marco5setbtn.append(delay21);
break;

case R.id.s27 :
	String delay22 = ("(");
	marco5setbtn.append(delay22);
	break;
	
case R.id.s28 :
	String delay23 = (")");
	marco5setbtn.append(delay23);
	break;
	
case R.id.s29 :
	String delay24 = ("*");
	marco5setbtn.append(delay24);
	break;
	
case R.id.s30 :
	String delay25 = ("+");
	marco5setbtn.append(delay25);
	break;

case R.id.btn01:
	
	if (count1 == 0 )
	{
	String view = ("a4");
	mButton01.setTextColor(Color.parseColor("#FF0000"));
	mButton01.setBackgroundResource(R.drawable.set_after_9);
	marco5setbtn.append(view);
	count1 = 1;
	}
  else if(count1 == 1)
  {
  mButton01.setTextColor(Color.parseColor("#ffffff"));
	mButton01.setBackgroundResource(R.drawable.set_befor_9);
  String btn01 = marco5setbtn.getText().toString();
  String newa = btn01.replaceFirst("a4", "a5");
  marco5setbtn.setText(newa);
  count1 = 2;
  }
  else if (count1 == 2)
  {
  	String btn01 = marco5setbtn.getText().toString();
      String newa = btn01.replaceFirst("a5", "");
      marco5setbtn.setText(newa);
  	count1 = 0;
  }
	
	break;
	
case R.id.btn02:
	
	if (count2 == 0 )
	{
	String view2 = ("b4");
	mButton02.setTextColor(Color.parseColor("#FF0000"));
	mButton02.setBackgroundResource(R.drawable.set_after_9);
	marco5setbtn.append(view2);
	count2 = 1;
	}
	else if(count2 == 1)
	{
	mButton02.setTextColor(Color.parseColor("#ffffff"));
	mButton02.setBackgroundResource(R.drawable.set_befor_9);
  String btn02 = marco5setbtn.getText().toString();
  String newa = btn02.replaceFirst("b4", "b5");
  marco5setbtn.setText(newa);
  count2 = 2;
	}
	else if (count2 == 2)
  {
  String btn02 = marco5setbtn.getText().toString();
  String newa = btn02.replaceFirst("b5", "");
  marco5setbtn.setText(newa);
  	count2 = 0;
  }
	break;	
	
case R.id.btn03:
	if (count3 == 0 )
	{
	String view3 = ("c4");
	mButton03.setTextColor(Color.parseColor("#FF0000"));
	mButton03.setBackgroundResource(R.drawable.set_after_9);
	marco5setbtn.append(view3);
	count3 = 1;
	}
	else if(count3 == 1)
	{
	mButton03.setTextColor(Color.parseColor("#ffffff"));
	mButton03.setBackgroundResource(R.drawable.set_befor_9);
  String btn03 = marco5setbtn.getText().toString();
  String newa = btn03.replaceFirst("c4", "c5");
  marco5setbtn.setText(newa);
  count3 = 2;
	}
	else if (count3 == 2)
  {
  String btn03 = marco5setbtn.getText().toString();
  String newa = btn03.replaceFirst("c5", "");
  marco5setbtn.setText(newa);
  	count3 = 0;
  }
	break;	
	
case R.id.btn04:
	if (count4 == 0 )
	{
	String view4 = ("d4");
	mButton04.setTextColor(Color.parseColor("#FF0000"));
	mButton04.setBackgroundResource(R.drawable.set_after_9);
	marco5setbtn.append(view4);
	count4 = 1;
	}
	else if(count4 == 1)
	{
	mButton04.setTextColor(Color.parseColor("#ffffff"));
	mButton04.setBackgroundResource(R.drawable.set_befor_9);
  String btn04 = marco5setbtn.getText().toString();
  String newa = btn04.replaceFirst("d4", "d5");
  marco5setbtn.setText(newa);
  count4 = 2;
	}
	else if (count4 == 2)
  {
  String btn04 = marco5setbtn.getText().toString();
  String newa = btn04.replaceFirst("d5", "");
  marco5setbtn.setText(newa);
  	count4 = 0;
  }
	break;	
	
case R.id.btn05:
	if (count5 == 0 )
	{
	String view5 = ("e4");
	mButton05.setTextColor(Color.parseColor("#FF0000"));
	mButton05.setBackgroundResource(R.drawable.set_after_9);
	marco5setbtn.append(view5);
	count5 = 1;
	}
	else if(count5 == 1)
	{
	mButton05.setTextColor(Color.parseColor("#ffffff"));
	mButton05.setBackgroundResource(R.drawable.set_befor_9);
  String btn05 = marco5setbtn.getText().toString();
  String newa = btn05.replaceFirst("e4", "e5");
  marco5setbtn.setText(newa);
  count5 = 2;
	}
	else if (count5 == 2)
  {
  String btn05 = marco5setbtn.getText().toString();
  String newa = btn05.replaceFirst("e5", "");
  marco5setbtn.setText(newa);
  	count5 = 0;
  }
	break;	
	
case R.id.btn06:
	if (count6 == 0 )
	{
	String view6 = ("f4");
	mButton06.setTextColor(Color.parseColor("#FF0000"));
	mButton06.setBackgroundResource(R.drawable.set_after_9);
	marco5setbtn.append(view6);
	count6 = 1;
	}
	else if(count6 == 1)
	{
	mButton06.setTextColor(Color.parseColor("#ffffff"));
	mButton06.setBackgroundResource(R.drawable.set_befor_9);
  String btn06 = marco5setbtn.getText().toString();
  String newa = btn06.replaceFirst("f4", "f5");
  marco5setbtn.setText(newa);
  count6 = 2;
	}
	else if (count6 == 2)
  {
  String btn06 = marco5setbtn.getText().toString();
  String newa = btn06.replaceFirst("f5", "");
  marco5setbtn.setText(newa);
  	count6 = 0;
  }
	break;
	
case R.id.btn07:
	if (count7 == 0 )
	{
	String view7 = ("g4");
	mButton07.setTextColor(Color.parseColor("#FF0000"));
	mButton07.setBackgroundResource(R.drawable.set_after_9);
	marco5setbtn.append(view7);
	count7 = 1;
	}
	else if(count7 == 1)
	{
	mButton07.setTextColor(Color.parseColor("#ffffff"));
	mButton07.setBackgroundResource(R.drawable.set_befor_9);
  String btn07 = marco5setbtn.getText().toString();
  String newa = btn07.replaceFirst("g4", "g5");
  marco5setbtn.setText(newa);
  count7 = 2;
	}
	else if (count7 == 2)
  {
  String btn07 = marco5setbtn.getText().toString();
  String newa = btn07.replaceFirst("g5", "");
  marco5setbtn.setText(newa);
  	count7 = 0;
  }
	break;
	
case R.id.btn08:
	if (count8 == 0 )
	{
	String view8 = ("h4");
	mButton08.setTextColor(Color.parseColor("#FF0000"));
	mButton08.setBackgroundResource(R.drawable.set_after_9);
	marco5setbtn.append(view8);
	count8 = 1;
	}
	else if(count8 == 1)
	{
	mButton08.setTextColor(Color.parseColor("#ffffff"));
	mButton08.setBackgroundResource(R.drawable.set_befor_9);
  String btn08 = marco5setbtn.getText().toString();
  String newa = btn08.replaceFirst("h4", "h5");
  marco5setbtn.setText(newa);
  count8 = 2;
	}
	else if (count8 == 2)
  {
  String btn08 = marco5setbtn.getText().toString();
  String newa = btn08.replaceFirst("h5", "");
  marco5setbtn.setText(newa);
  	count8 = 0;
  }
	break;	
	
case R.id.btn09:
	if (count9 == 0 )
	{
	String view9 = ("i4");
	mButton09.setTextColor(Color.parseColor("#FF0000"));
	mButton09.setBackgroundResource(R.drawable.set_after_9);
	marco5setbtn.append(view9);
	count9 = 1;
	}
	else if(count9 == 1)
	{
	mButton09.setTextColor(Color.parseColor("#ffffff"));
	mButton09.setBackgroundResource(R.drawable.set_befor_9);
  String btn09 = marco5setbtn.getText().toString();
  String newa = btn09.replaceFirst("i4", "i5");
  marco5setbtn.setText(newa);
  count9 = 2;
	}
	else if (count9 == 2)
  {
  String btn09 = marco5setbtn.getText().toString();
  String newa = btn09.replaceFirst("i5", "");
  marco5setbtn.setText(newa);
  	count9 = 0;
  }
	break;
	
case R.id.btn10:
	if (count10 == 0 )
	{
	String view10 = ("j4");
	mButton10.setTextColor(Color.parseColor("#FF0000"));
	mButton10.setBackgroundResource(R.drawable.set_after_9);
	marco5setbtn.append(view10);
	count10 = 1;
	}
	else if(count10 == 1)
	{
	mButton10.setTextColor(Color.parseColor("#ffffff"));
	mButton10.setBackgroundResource(R.drawable.set_befor_9);
  String btn10 = marco5setbtn.getText().toString();
  String newa = btn10.replaceFirst("j4", "j5");
  marco5setbtn.setText(newa);
  count10 = 2;
	}
	else if (count10 == 2)
  {
  String btn10 = marco5setbtn.getText().toString();
  String newa = btn10.replaceFirst("j5", "");
  marco5setbtn.setText(newa);
  	count10 = 0;
  }
	break;
	
case R.id.btn11:
	if (count11 == 0 )
	{
	String view11 = ("k4");
	mButton11.setTextColor(Color.parseColor("#FF0000"));
	mButton11.setBackgroundResource(R.drawable.set_after_9);
	marco5setbtn.append(view11);
	count11 = 1;
	}
	else if(count11 == 1)
	{
	mButton11.setTextColor(Color.parseColor("#ffffff"));
	mButton11.setBackgroundResource(R.drawable.set_befor_9);
  String btn11 = marco5setbtn.getText().toString();
  String newa = btn11.replaceFirst("k4", "k5");
  marco5setbtn.setText(newa);
  count11 = 2;
	}
	else if (count11 == 2)
  {
  String btn11 = marco5setbtn.getText().toString();
  String newa = btn11.replaceFirst("k5", "");
  marco5setbtn.setText(newa);
  	count11 = 0;
  }
	break;
	
case R.id.btn12:
	if (count12 == 0 )
	{
	String view12 = ("l4");
	mButton12.setTextColor(Color.parseColor("#FF0000"));
	mButton12.setBackgroundResource(R.drawable.set_after_9);
	marco5setbtn.append(view12);
	count12 = 1;
	}
	else if(count12 == 1)
	{
	mButton12.setTextColor(Color.parseColor("#ffffff"));
	mButton12.setBackgroundResource(R.drawable.set_befor_9);
  String btn12 = marco5setbtn.getText().toString();
  String newa = btn12.replaceFirst("l4", "l5");
  marco5setbtn.setText(newa);
  count12 = 2;
	}
	else if (count12 == 2)
  {
  String btn12 = marco5setbtn.getText().toString();
  String newa = btn12.replaceFirst("l5", "");
  marco5setbtn.setText(newa);
  	count12 = 0;
  }
	break;

case R.id.btn13:
String view13 = ("Aa");
mButton13.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view13);

break;

case R.id.btn14:
String view14 = ("Ab");
mButton14.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view14);

break;

case R.id.btn15:
String view15 = ("Ac");
mButton15.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view15);
break;

case R.id.btn16:
String view16 = ("Ad");
mButton16.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view16);
break;

case R.id.btn17:
String view17 = ("Ae");
mButton17.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view17);
break;

case R.id.btn18:
String view18 = ("Af");
mButton18.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view18);
break;

case R.id.btn19:
String view19 = ("Ag");
mButton19.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view19);
break;

case R.id.btn20:
String view20 = ("Ah");
mButton20.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view20);
break;

case R.id.btn21:
String view21 = ("Ai");
mButton21.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view21);
break;

case R.id.btn22:
String view22 = ("Aj");
mButton22.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view22);
break;

case R.id.btn23:
String view23 = ("Ak");
mButton23.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view23);
break;

case R.id.btn24:
String view24 = ("Al");
mButton24.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view24);
break;

//case R.id.btn25:
//String view25 = ("Am");
//mButton25.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view25);
//break;

//case R.id.btn26:
//String view26 = ("An");
//mButton26.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view26);
//break;
//
//case R.id.btn27:
//String view27 = ("Ao");
//mButton27.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view27);
//break;

//case R.id.btn28:
//String view28 = ("Ap");
//mButton28.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view28);
//break;
//
//case R.id.btn29:
//String view29 = ("Aq");
//mButton29.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view29);
//break;
//
//case R.id.btn30:
//String view30 = ("Ar");
//mButton30.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view30);
//break;
//
//case R.id.btn31:
//String view31 = ("As");
//mButton31.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view31);
//break;

//case R.id.btn32:
//String view32 = ("At");
//mButton32.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view32);
//break;

case R.id.btn33:
String view33 = ("Ta");
mButton33.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view33);
break;

//case R.id.btn34:
//String view34 = ("Tb");
//mButton34.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view34);
//break;

case R.id.btn35:
String view35 = ("Tc");
mButton35.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view35);
break;

//case R.id.btn36:
//String view36 = ("Td");
//mButton36.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view36);
//break;
//
//case R.id.btn37:
//String view37 = ("Te");
//mButton37.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view37);
//break;
//
//case R.id.btn38:
//String view38 = ("Tf");
//mButton38.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view38);
//break;
//
//case R.id.btn39:
//String view39 = ("Tg");
//mButton39.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view39);
//break;
//
//case R.id.btn40:
//String view40 = ("Th");
//mButton40.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view40);
//break;
//
//case R.id.btn41:
//String view41 = ("Ti");
//mButton41.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view41);
//break;

case R.id.btn42:
String view42 = ("Tj");
mButton42.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view42);
break;

case R.id.btn43:
String view43 = ("Tk");
mButton43.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view43);
break;

case R.id.btn44:
String view44 = ("Tl");
mButton44.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view44);
break;

case R.id.btn45:
String view45 = ("Tm");
mButton45.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view45);
break;

case R.id.btn46:
String view46 = ("Tn");
mButton46.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view46);
break;

case R.id.btn47:
String view47 = ("To");
mButton47.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view47);
break;

case R.id.btn48:
String view48 = ("Tp");
mButton48.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view48);
break;

case R.id.btn49:
String view49 = ("Tq");
mButton49.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view49);
break;

case R.id.btn50:
String view50 = ("Tr");
mButton50.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view50);
break;

case R.id.btn51:
String view51 = ("Ts");
mButton51.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view51);
break;

case R.id.btn52:
String view52 = ("Tt");
mButton52.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view52);
	break;

case R.id.btn53:
String view53 = ("Tu");
mButton53.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view53);
	break;

//case R.id.btn54:
//String view54 = ("Xa");
//mButton54.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view54);
//	break;
//
//case R.id.btn55:
//String view55 = ("Xb");
//mButton55.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view55);
//	break;	
//
//case R.id.btn56:
//String view56 = ("Xc");
//mButton56.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view56);
//	break;	
//
//case R.id.btn57:
//String view57 = ("Xd");
//mButton57.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view57);
//	break;
//
//case R.id.btn58:
//String view58 = ("Xe");
//mButton58.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view58);
//	break;
//
//case R.id.btn59:
//String view59 = ("Xf");
//mButton59.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view59);
//	break;
//
//case R.id.btn60:
//String view60 = ("Xg");
//mButton60.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view60);
//	break;
//
//case R.id.btn61:
//String view61 = ("Xh");
//mButton61.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view61);
//	break;	
//
//case R.id.btn62:
//String view62 = ("Xi");
//mButton62.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view62);
//	break;

case R.id.btn63:
String view63 = ("Xj");
mButton63.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view63);
	break;

case R.id.btn64:
String view64 = ("Xk");
mButton64.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view64);
	break;

case R.id.btn65:
String view65 = ("Xl");
mButton65.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view65);
	break;

case R.id.btn66:
String view66 = ("Xm");
mButton66.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view66);
	break;

case R.id.btn67:
String view67 = ("Xn");
mButton67.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view67);
	break;

case R.id.btn68:
String view68 = ("Xo");
mButton68.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view68);
	break;

//case R.id.btn69:
//String view69 = ("Ya");
//mButton69.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view69);
//	break;
//
//case R.id.btn70:
//String view70 = ("Yb");
//mButton70.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view70);
//	break;
//
//case R.id.btn71:
//String view71 = ("Yc");
//mButton71.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view71);
//	break;
//
//case R.id.btn72:
//String view72 = ("Yd");
//mButton72.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view72);
//	break;
//
//case R.id.btn73:
//String view73 = ("Ye");
//mButton73.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view73);
//	break;
//
//case R.id.btn74:
//String view74 = ("Yf");
//mButton74.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view74);
//	break;
//
//case R.id.btn75:
//String view75 = ("Yg");
//mButton75.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view75);
//	break;
//
//case R.id.btn76:
//String view76 = ("Yh");
//mButton76.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view76);
//	break;
//
//case R.id.btn77:
//String view77 = ("Yi");
//mButton77.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view77);
//	break;

case R.id.btn78:
String view78 = ("Yj");
mButton78.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view78);
	break;

case R.id.btn79:
String view79 = ("Yk");
mButton79.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view79);
	break;

case R.id.btn80:
String view80 = ("Yl");
mButton80.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view80);
	break;

case R.id.btn81:
String view81 = ("Ym");
mButton81.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view81);
	break;

case R.id.btn82:
String view82 = ("Yn");
mButton82.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view82);
	break;

case R.id.btn83:
String view83 = ("Yo");
mButton83.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view83);
	break;
	
//case R.id.btn84:
//String view84 = ("Xa");
//mButton84.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view84);
//	break;
//	
//case R.id.btn85:
//String view85 = ("Xb");
//mButton85.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view85);
//	break;
//	
//case R.id.btn86:
//String view86 = ("Xc");
//mButton86.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view86);
//	break;
//	
//case R.id.btn87:
//String view87 = ("Xd");
//mButton87.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view87);
//	break;
//	
//case R.id.btn88:
//String view88 = ("Xe");
//mButton88.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view88);
//	break;
//	
//case R.id.btn89:
//String view89 = ("Xf");
//mButton89.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view89);
//	break;
//	
//case R.id.btn90:
//String view90 = ("Xg");
//mButton90.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view90);
//	break;
//	
//case R.id.btn91:
//String view91 = ("Xh");
//mButton91.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view91);
//	break;
//	
//case R.id.btn92:
//String view92 = ("Xi");
//mButton92.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view92);
//	break;
	
case R.id.btn93:
String view93 = ("Xj");
mButton93.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view93);
	break;
	
case R.id.btn94:
String view94 = ("Xk");
mButton94.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view94);
	break;
	
case R.id.btn95:
String view95 = ("Xl");
mButton95.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view95);
	break;
	
case R.id.btn96:
String view96 = ("Xm");
mButton96.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view96);
	break;
	
case R.id.btn97:
String view97 = ("Xn");
mButton97.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view97);
	break;
	
case R.id.btn98:
String view98 = ("Xo");
mButton98.setTextColor(Color.parseColor("#FF0000"));
marco5setbtn.append(view98);
	break;
	
//case R.id.btn99:
//String view99 = ("Xp");
//mButton99.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view99);
//	break;
//	
//case R.id.btn100:
//String view100 = ("Xq");
//mButton100.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view100);
//	break;
//	
//case R.id.btn101:
//String view101 = ("Xr");
//mButton101.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view101);
//	break;
//	
//case R.id.btn102:
//String view102 = ("Xs");
//mButton102.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view102);
//	break;
//	
//case R.id.btn103:
//String view103 = ("Xt");
//mButton103.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view103);
//	break;
//	
//case R.id.btn104:
//String view104 = ("Xu");
//mButton104.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view104);
//	break;
//	
//case R.id.btn105:
//String view105 = ("Yp");
//mButton105.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view105);
//	break;
//	
//case R.id.btn106:
//String view106 = ("Yq");
//mButton106.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view106);
//	break;
//	
//case R.id.btn107:
//String view107 = ("Yr");
//mButton107.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view107);
//	break;
//	
//case R.id.btn108:
//String view108 = ("Ys");
//mButton10.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view108);
//	break;
//	
//case R.id.btn109:
//String view109 = ("Yt");
//mButton109.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view109);
//	break;
//	
//case R.id.btn110:
//String view110 = ("Yu");
//mButton110.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view110);
//	break;
	
//case R.id.btn111:
//String view111 = ("Zp");
//mButton111.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view111);
//	break;
//	
//case R.id.btn112:
//String view112 = ("Zq");
//mButton112.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view112);
//	break;
//	
//case R.id.btn113:
//String view113 = ("Zr");
//mButton113.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view113);
//	break;
//	
//case R.id.btn114:
//String view114 = ("Zs");
//mButton114.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view114);
//	break;
//	
//case R.id.btn115:
//String view115 = ("Zt");
//mButton115.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view115);
//	break;
//	
//case R.id.btn116:
//String view116 = ("Zt");
//mButton116.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view116);
//	break;
	
case R.id.btn153:
	String view153 = ("Tv");
	marco5setbtn.append(view153);
		break;
		
case R.id.btn154:
	String view154 = ("Tw");
	marco5setbtn.append(view154);
		break;
		
case R.id.btn155:
	String view155 = ("Tx");
	marco5setbtn.append(view155);
		break;
		
case R.id.btn156:
	String view156 = ("Ty");
	marco5setbtn.append(view156);
		break;
	
case R.id.window2:
	String window2 = ("W+qa1");
	marco5setbtn.append(window2);
	break;
	
case R.id.window3:
	String window3 = ("W+qb1");
	marco5setbtn.append(window3);
	break;
	
case R.id.window4:
	String window4 = ("W+qc1");
	marco5setbtn.append(window4);
	break;

case R.id.door1 :
	String door1 = ("W+sa1");
	marco5setbtn.append(door1);	
	break;
	
case R.id.door2 :
	String door2 = ("W+sb1");
	marco5setbtn.append(door2);	
	break;
	
case R.id.door3 :
	String door3 = ("W+sc1");
	marco5setbtn.append(door3);	
	break;
		
	//tv home
//case R.id.button25:
//	String view39 = ("Tg");
//	String message39 = view39.toString();
//	marco5setbtn.append(message39);
//	break;
//	//tv 靜音
//case R.id.button26:
//	String view41 = ("Ti");
//    String message41 = view41.toString();
//    marco5setbtn.append(message41);
//	break;

	}
}


    @Override
    public void onStart() {
        super.onStart();
        if(D) Log.e(TAG, "++ ON START ++");
        TextView title = (TextView)findViewById(R.id.title_right_text);
        if(socket!=null && socket.isConnected())
            title.setText(R.string.title_connected);
        else title.setText(R.string.title_not_connected);
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if(D) Log.e(TAG, "+ ON RESUME +");

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mChatService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mChatService.getState() == BluetoothChatService.STATE_NONE) {
              // Start the Bluetooth chat services
              mChatService.start();
            }
        }
    }

    public void setupChat() {
        Log.d(TAG, "setupChat()");

    }
    

    @Override
    public synchronized void onPause() {
        super.onPause();
        if(D) Log.e(TAG, "- ON PAUSE -");
    }

    @Override
    public void onStop() {
        super.onStop();

        if(D) Log.e(TAG, "-- ON STOP --");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth chat services
        if (mChatService != null) mChatService.stop();

        if(D) Log.e(TAG, "--- ON DESTROY ---");

    }

    public void ensureDiscoverable() {
        if(D) Log.d(TAG, "ensure discoverable");
        if (mBluetoothAdapter.getScanMode() !=
            BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    public void init() {
        //Toast.makeText(this, "INIT", Toast.LENGTH_SHORT).show();
        socket = null;
		ThreadPoolUtil.getInstance().execute(new Runnable() {//菴ｿ逕ｨ郤ｿ遞区ｱ�			@Override
            public void run() {
                // TODO Auto-generated method stub
                Message m = new Message();
                try {
                        String IP = ip.getText().toString();
                        int PORT = new Integer(port.getText().toString());
                        socket = new Socket(IP, PORT);
                        m.what = 1;
                        handler.sendMessage(m);
                        receievmessage();

                } catch (UnknownHostException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Log.d(TAG,"UnknownHostException");
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    m.what = 2;
                    handler.sendMessage(m);
                    Log.d(TAG, "IOException");
                }

            }
        });

	}
    public void gcm_init() {
        //Toast.makeText(this, "INIT", Toast.LENGTH_SHORT).show();
        IPname = getSharedPreferences(Ipdata,0);
        token = IPname.getString(gcmID, "");
        ThreadPoolUtil.getInstance().execute(new Runnable() {//菴ｿ逕ｨ郤ｿ遞区ｱ�			@Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    String IP = ip_gcm.getText().toString();
                    int PORT = new Integer(port_gcm.getText().toString());
                    socket_gcm = new Socket(IP, PORT);
                    int length =token.length();
                    sendmessage_gcm("token "+token);
                    receievmessage_gcm();
                    socket_gcm.close();
                } catch (UnknownHostException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Message m = new Message();
                    m.what = 2;
                    handler.sendMessage(m);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Message m = new Message();
                    m.what = 2;
                    handler.sendMessage(m);
                }
            }
        });

    }
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,9000).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
    public void sendmessage(String message) {

    	//Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
		try {
			// 蜷第恪蜉｡蝎ｨ遶ｯ蜿鷹�謨ｰ謐ｮ
			DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			//String str = sendContent.getText().toString().trim();

			out.write(message.getBytes("GBK"));
			//Thread.sleep(300);
			Message m = new Message();
			m.what = 1;
            m.obj =message;
			handler.sendMessage(m);
		} catch (Exception e) {
			e.printStackTrace();
			Message m = new Message();
			m.what = 2;
			handler.sendMessage(m);
		}
	}
    public void sendmessage_gcm(String message) {
        //Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        try {
            // 蜷第恪蜉｡蝎ｨ遶ｯ蜿鷹�謨ｰ謐ｮ
            DataOutputStream out = new DataOutputStream(socket_gcm.getOutputStream());
            //String str = sendContent.getText().toString().trim();
            message = message +"\r\n";
            out.write(message.getBytes("GBK"));
            //Thread.sleep(300);
            Message m = new Message();
            m.what = 1;
            handler.sendMessage(m);
        } catch (Exception e) {
            e.printStackTrace();
            Message m = new Message();
            m.what = 2;
            handler.sendMessage(m);
        }
    }
	//謗･蜿玲恪蜉｡蝎ｨ豸域�
	public void receievmessage(){
		try{
			while(true){
				 DataInputStream input = new DataInputStream(socket.getInputStream());    
				 byte[] buffer;
				 buffer = new byte[input.available()];
				 if(buffer.length != 0){
				 // 隸ｻ蜿也ｼ灘�蛹ｺ
				 input.read(buffer);
				 String msg = new String(buffer, "GBK");//豕ｨ諢剰ｽｬ遐�ｼ御ｸ咲┯荳ｭ譁�ｼ壻ｹｱ遐√�
			 	 Message m = new Message();
				 m.what = 3;
				 m.obj = msg;
				 handler.sendMessage(m);
				 }
			}
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
            if(socket!=null){
                Message m = new Message();
                m.what = 2;
                handler.sendMessage(m);
            }

		}
	}
    public void receievmessage_gcm(){
        try{
            while(true){
                DataInputStream input = new DataInputStream(socket_gcm.getInputStream());
                byte[] buffer;
                buffer = new byte[input.available()];
                if(buffer.length != 0){
                    // 隸ｻ蜿也ｼ灘�蛹ｺ
                    input.read(buffer);
                    String msg = new String(buffer, "UTF-8");//豕ｨ諢剰ｽｬ遐�ｼ御ｸ咲┯荳ｭ譁�ｼ壻ｹｱ遐√�
                    Message m = new Message();
                    m.what = 4;
                    m.obj = msg;
                    handler.sendMessage(m);
                }
            }

        }catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            Message m = new Message();
            m.what = 2;
            handler.sendMessage(m);
        }
    }
    

    // The Handler that gets information back from the BluetoothChatService
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
            TextView title = (TextView)findViewById(R.id.title_right_text);
            if(socket!=null && socket.isConnected())
                title.setText(R.string.title_connected);
            else title.setText(R.string.title_not_connected);
            Log.d(TAG,msg.what+"");
			if (msg.what == 1) {
                /*if(msg.obj != null){
                    TextView title2 = (TextView)findViewById(R.id.title_left_text);
                    title2.setText(msg.obj.toString());
                }*/

				//Toast.makeText(MainActivity.this, "逋ｼ騾∵�蜉�, 1).show();
				//sendContent.setText("");
			}
			if (msg.what == 2) {
				Toast.makeText(BluetoothChat.this, "請確認網路連線是否正常", Toast.LENGTH_SHORT).show();
				//init();
			}
			if(msg.what == 3){
				//recContent.setText(msg.obj.toString());
                //Toast.makeText(BluetoothChat.this, msg.obj.toString(), Toast.LENGTH_LONG).show();
                Log.d(TAG, msg.obj + "");
                if(msg.obj.toString().contains("j"))
					if(msg.obj.toString().contains("j000"))
					{
					    findViewById(R.id.btn01).setBackgroundResource(R.drawable.set_9);
					}else {
                        findViewById(R.id.btn01).setBackgroundResource(R.drawable.set_9_invert);
                    }
                if(msg.obj.toString().contains("k"))
					if(msg.obj.toString().contains("k000"))
					{
					    findViewById(R.id.btn02).setBackgroundResource(R.drawable.set_9);
					}else {
                        findViewById(R.id.btn02).setBackgroundResource(R.drawable.set_9_invert);
                    }
                if(msg.obj.toString().contains("l"))
					if (msg.obj.toString().contains("l000"))
					{
					    findViewById(R.id.btn03).setBackgroundResource(R.drawable.set_9);
					}else{
                        findViewById(R.id.btn03).setBackgroundResource(R.drawable.set_9_invert);
                    }
                if(msg.obj.toString().contains("m"))
					if (msg.obj.toString().contains("m000"))
					{
					    findViewById(R.id.btn04).setBackgroundResource(R.drawable.set_9);
					}else {
                        findViewById(R.id.btn04).setBackgroundResource(R.drawable.set_9_invert);
                    }
                if(msg.obj.toString().contains("n"))
					if (msg.obj.toString().contains("n000"))
					{
					    findViewById(R.id.btn05).setBackgroundResource(R.drawable.set_9);
					}else {
                        findViewById(R.id.btn05).setBackgroundResource(R.drawable.set_9_invert);
                    }
                if(msg.obj.toString().contains("o"))
					if (msg.obj.toString().contains("o000"))
					{
					    findViewById(R.id.btn06).setBackgroundResource(R.drawable.set_9);
					}else {
                        findViewById(R.id.btn06).setBackgroundResource(R.drawable.set_9_invert);
                    }
                if(msg.obj.toString().contains("p"))
					if (msg.obj.toString().contains("p000"))
					{
					    findViewById(R.id.btn07).setBackgroundResource(R.drawable.set_9);
					}else {
                        findViewById(R.id.btn07).setBackgroundResource(R.drawable.set_9_invert);
                    }
                if(msg.obj.toString().contains("q"))
					if (msg.obj.toString().contains("q000"))
					{
					    findViewById(R.id.btn08).setBackgroundResource(R.drawable.set_9);
					}else {
                        findViewById(R.id.btn08).setBackgroundResource(R.drawable.set_9_invert);
                    }
                if(msg.obj.toString().contains("r"))
					if (msg.obj.toString().contains("r000"))
					{
					    findViewById(R.id.btn09).setBackgroundResource(R.drawable.set_9);
					}else {
                        findViewById(R.id.btn09).setBackgroundResource(R.drawable.set_9_invert);
                    }
                if(msg.obj.toString().contains("s"))
					if (msg.obj.toString().contains("s000"))
					{
					    findViewById(R.id.btn10).setBackgroundResource(R.drawable.set_9);
					}else {
                        findViewById(R.id.btn10).setBackgroundResource(R.drawable.set_9_invert);
                    }
                if(msg.obj.toString().contains("t"))
					if (msg.obj.toString().contains("t000"))
					{
					    findViewById(R.id.btn11).setBackgroundResource(R.drawable.set_9);
					}else {
                        findViewById(R.id.btn11).setBackgroundResource(R.drawable.set_9_invert);
                    }
                if(msg.obj.toString().contains("u"))
					if (msg.obj.toString().contains("u000"))
					{
					    findViewById(R.id.btn12).setBackgroundResource(R.drawable.set_9);
					}else {
                        findViewById(R.id.btn12).setBackgroundResource(R.drawable.set_9_invert);
                    }

			}
            if(msg.what == 4){
                if(msg.obj.toString().contains("okgcm")){
                    Toast.makeText(BluetoothChat.this, "註冊成功", Toast.LENGTH_SHORT).show();
                }else if(msg.obj.toString().contains("repeatgcm"))
                    Toast.makeText(BluetoothChat.this, "此裝置已註冊過", Toast.LENGTH_SHORT).show();
                else if(msg.obj.toString().contains("failgcm"))
                    Toast.makeText(BluetoothChat.this, "註冊失敗，請聯絡伺服器管理者", Toast.LENGTH_SHORT).show();
            }
		}
	};

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(D) Log.d(TAG, "onActivityResult " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {
        case REQUEST_CONNECT_DEVICE:
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                // Get the device MAC address
                String address = data.getExtras()
                                     .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                // Get the BLuetoothDevice object
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                // Attempt to connect to the device
                mChatService.connect(device);
            }
            break;
        case REQUEST_ENABLE_BT:
            // When the request to enable Bluetooth returns
            if (resultCode == Activity.RESULT_OK) {
                // Bluetooth is now enabled, so set up a chat session
                setupChat();
            } else {
                // User did not enable Bluetooth or an error occured
                Log.d(TAG, "BT not enabled");
                Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                finish();
            }
            break;

        case LAMP_PIC1 :
            if (resultCode == Activity.RESULT_OK) {
               Uri uri1 = data.getData();
               Log.e("uri1", uri1.toString());
               try {
                   //使用ContentProvider通过URI获取原始图片
            	   BitmapFactory.Options opts = new BitmapFactory.Options();
            	   opts.inSampleSize = 4;
            	   Bitmap photo =  BitmapFactory.decodeStream(this.getContentResolver().openInputStream(uri1),null, opts);
                       //为防止原始图片过大导致内存溢出，这里先缩小原图显示，然后释放原始Bitmap占用的内存
                       Bitmap smallBitmap = zoomBitmap(photo, photo.getWidth() / 2, photo.getHeight()/2 );
                       //释放原始图片占用的内存，防止out of memory异常发生
                       photo.recycle();
                       FileOutputStream fos = null;
                       fos = new FileOutputStream("/mnt/sdcard/Lamp1.jpg");
                       String Imagelamp1 = "file:///mnt/sdcard/Lamp1.jpg";
                       smallBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                       lampeditpic1.append(Imagelamp1);
                    	   fos.close();
               } catch (FileNotFoundException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
            }
        	break;

        case LAMP_PIC2 :
            if (resultCode == Activity.RESULT_OK) {
               Uri uri2 = data.getData();
               Log.e("uri2", uri2.toString());
               try {
                   //使用ContentProvider通过URI获取原始图片
            	   BitmapFactory.Options opts = new BitmapFactory.Options();
            	   opts.inSampleSize = 4;
            	   Bitmap photo =  BitmapFactory.decodeStream(this.getContentResolver().openInputStream(uri2),null, opts);
                       //为防止原始图片过大导致内存溢出，这里先缩小原图显示，然后释放原始Bitmap占用的内存
                       Bitmap smallBitmap = zoomBitmap(photo, photo.getWidth() / 2, photo.getHeight()/2 );
                       //释放原始图片占用的内存，防止out of memory异常发生
                       photo.recycle();
                       FileOutputStream fos = null;
                       fos = new FileOutputStream("/mnt/sdcard/Lamp2.jpg");
                       String Imagelamp2 = "file:///mnt/sdcard/Lamp2.jpg";
                       smallBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                       lampeditpic2.append(Imagelamp2);
                    	   fos.close();
               } catch (FileNotFoundException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
            }
        	break;

        case LAMP_PIC3 :
            if (resultCode == Activity.RESULT_OK) {
               Uri uri3 = data.getData();
               Log.e("uri", uri3.toString());
               try {
                   //使用ContentProvider通过URI获取原始图片
            	   BitmapFactory.Options opts = new BitmapFactory.Options();
            	   opts.inSampleSize = 4;
            	   Bitmap photo =  BitmapFactory.decodeStream(this.getContentResolver().openInputStream(uri3),null, opts);
                       //为防止原始图片过大导致内存溢出，这里先缩小原图显示，然后释放原始Bitmap占用的内存
                       Bitmap smallBitmap = zoomBitmap(photo, photo.getWidth() / 2, photo.getHeight()/2 );
                       //释放原始图片占用的内存，防止out of memory异常发生
                       photo.recycle();
                       FileOutputStream fos = null;
                       fos = new FileOutputStream("/mnt/sdcard/Lamp3.jpg");
                       String Imagelamp3 = "file:///mnt/sdcard/Lamp3.jpg";
                       smallBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                       lampeditpic3.append(Imagelamp3);
                    	   fos.close();
               } catch (FileNotFoundException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
            }
        	break;

        case LAMP_PIC4 :
            if (resultCode == Activity.RESULT_OK) {
               Uri uri4 = data.getData();
               Log.e("uri", uri4.toString());
               try {
                   //使用ContentProvider通过URI获取原始图片
            	   BitmapFactory.Options opts = new BitmapFactory.Options();
            	   opts.inSampleSize = 4;
            	   Bitmap photo =  BitmapFactory.decodeStream(this.getContentResolver().openInputStream(uri4),null, opts);
                       //为防止原始图片过大导致内存溢出，这里先缩小原图显示，然后释放原始Bitmap占用的内存
                       Bitmap smallBitmap = zoomBitmap(photo, photo.getWidth() / 2, photo.getHeight()/2 );
                       //释放原始图片占用的内存，防止out of memory异常发生
                       photo.recycle();
                       FileOutputStream fos = null;
                       fos = new FileOutputStream("/mnt/sdcard/Lamp4.jpg");
                       String Imagelamp4 = "file:///mnt/sdcard/Lamp4.jpg";
                       smallBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                       lampeditpic4.append(Imagelamp4);
                    	   fos.close();
               } catch (FileNotFoundException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
            }
        	break;

        case LAMP_PIC5 :
            if (resultCode == Activity.RESULT_OK) {
               Uri uri5 = data.getData();
               Log.e("uri", uri5.toString());
               try {
                   //使用ContentProvider通过URI获取原始图片
            	   BitmapFactory.Options opts = new BitmapFactory.Options();
            	   opts.inSampleSize = 4;
            	   Bitmap photo =  BitmapFactory.decodeStream(this.getContentResolver().openInputStream(uri5),null, opts);
                       //为防止原始图片过大导致内存溢出，这里先缩小原图显示，然后释放原始Bitmap占用的内存
                       Bitmap smallBitmap = zoomBitmap(photo, photo.getWidth() / 2, photo.getHeight()/2 );
                       //释放原始图片占用的内存，防止out of memory异常发生
                       photo.recycle();
                       FileOutputStream fos = null;
                       fos = new FileOutputStream("/mnt/sdcard/Lamp5.jpg");
                       String Imagelamp5 = "file:///mnt/sdcard/Lamp5.jpg";
                       smallBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                       lampeditpic5.append(Imagelamp5);
                    	   fos.close();
               } catch (FileNotFoundException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
            }
        	break;

        case LAMP_PIC6 :
            if (resultCode == Activity.RESULT_OK) {
               Uri uri6 = data.getData();
               Log.e("uri", uri6.toString());
               try {
                   //使用ContentProvider通过URI获取原始图片
            	   BitmapFactory.Options opts = new BitmapFactory.Options();
            	   opts.inSampleSize = 4;
            	   Bitmap photo =  BitmapFactory.decodeStream(this.getContentResolver().openInputStream(uri6),null, opts);
                       //为防止原始图片过大导致内存溢出，这里先缩小原图显示，然后释放原始Bitmap占用的内存
                       Bitmap smallBitmap = zoomBitmap(photo, photo.getWidth() / 2, photo.getHeight()/2 );
                       //释放原始图片占用的内存，防止out of memory异常发生
                       photo.recycle();
                       FileOutputStream fos = null;
                       fos = new FileOutputStream("/mnt/sdcard/Lamp6.jpg");
                       String Imagelamp6 = "file:///mnt/sdcard/Lamp6.jpg";
                       smallBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                       lampeditpic6.append(Imagelamp6);
                    	   fos.close();
               } catch (FileNotFoundException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
            }
        	break;

        case LAMP_PIC7 :
            if (resultCode == Activity.RESULT_OK) {
               Uri uri7 = data.getData();
               Log.e("uri", uri7.toString());
               try {
                   //使用ContentProvider通过URI获取原始图片
            	   BitmapFactory.Options opts = new BitmapFactory.Options();
            	   opts.inSampleSize = 4;
            	   Bitmap photo =  BitmapFactory.decodeStream(this.getContentResolver().openInputStream(uri7),null, opts);
                       //为防止原始图片过大导致内存溢出，这里先缩小原图显示，然后释放原始Bitmap占用的内存
                       Bitmap smallBitmap = zoomBitmap(photo, photo.getWidth() / 2, photo.getHeight()/2 );
                       //释放原始图片占用的内存，防止out of memory异常发生
                       photo.recycle();
                       FileOutputStream fos = null;
                       fos = new FileOutputStream("/mnt/sdcard/Lamp7.jpg");
                       String Imagelamp7 = "file:///mnt/sdcard/Lamp7.jpg";
                       smallBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                       lampeditpic7.append(Imagelamp7);
                    	   fos.close();
               } catch (FileNotFoundException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
            }
        	break;

        case LAMP_PIC8 :
            if (resultCode == Activity.RESULT_OK) {
               Uri uri8 = data.getData();
               Log.e("uri", uri8.toString());
               try {
                   //使用ContentProvider通过URI获取原始图片
            	   BitmapFactory.Options opts = new BitmapFactory.Options();
            	   opts.inSampleSize = 4;
            	   Bitmap photo =  BitmapFactory.decodeStream(this.getContentResolver().openInputStream(uri8),null, opts);
                       //为防止原始图片过大导致内存溢出，这里先缩小原图显示，然后释放原始Bitmap占用的内存
                       Bitmap smallBitmap = zoomBitmap(photo, photo.getWidth() / 2, photo.getHeight()/2 );
                       //释放原始图片占用的内存，防止out of memory异常发生
                       photo.recycle();
                       FileOutputStream fos = null;
                       fos = new FileOutputStream("/mnt/sdcard/Lamp8.jpg");
                       String Imagelamp8 = "file:///mnt/sdcard/Lamp8.jpg";
                       smallBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                       lampeditpic8.append(Imagelamp8);
                    	   fos.close();
               } catch (FileNotFoundException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
            }
        	break;

        case LAMP_PIC9 :
            if (resultCode == Activity.RESULT_OK) {
               Uri uri9 = data.getData();
               Log.e("uri", uri9.toString());
               try {
                   //使用ContentProvider通过URI获取原始图片
            	   BitmapFactory.Options opts = new BitmapFactory.Options();
            	   opts.inSampleSize = 4;
            	   Bitmap photo =  BitmapFactory.decodeStream(this.getContentResolver().openInputStream(uri9),null, opts);
                       //为防止原始图片过大导致内存溢出，这里先缩小原图显示，然后释放原始Bitmap占用的内存
                       Bitmap smallBitmap = zoomBitmap(photo, photo.getWidth() / 2, photo.getHeight()/2 );
                       //释放原始图片占用的内存，防止out of memory异常发生
                       photo.recycle();
                       FileOutputStream fos = null;
                       fos = new FileOutputStream("/mnt/sdcard/Lamp9.jpg");
                       String Imagelamp9 = "file:///mnt/sdcard/Lamp9.jpg";
                       smallBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                       lampeditpic9.append(Imagelamp9);
                    	   fos.close();
               } catch (FileNotFoundException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
            }
        	break;

        case LAMP_PIC10 :
            if (resultCode == Activity.RESULT_OK) {
               Uri uri10 = data.getData();
               Log.e("uri", uri10.toString());
               try {
                   //使用ContentProvider通过URI获取原始图片
            	   BitmapFactory.Options opts = new BitmapFactory.Options();
            	   opts.inSampleSize = 4;
            	   Bitmap photo =  BitmapFactory.decodeStream(this.getContentResolver().openInputStream(uri10),null, opts);
                       //为防止原始图片过大导致内存溢出，这里先缩小原图显示，然后释放原始Bitmap占用的内存
                       Bitmap smallBitmap = zoomBitmap(photo, photo.getWidth() / 2, photo.getHeight()/2 );
                       //释放原始图片占用的内存，防止out of memory异常发生
                       photo.recycle();
                       FileOutputStream fos = null;
                       fos = new FileOutputStream("/mnt/sdcard/Lamp10.jpg");
                       String Imagelamp10 = "file:///mnt/sdcard/Lamp10.jpg";
                       smallBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                       lampeditpic10.append(Imagelamp10);
                    	   fos.close();
               } catch (FileNotFoundException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
            }
        	break;

        case LAMP_PIC11 :
            if (resultCode == Activity.RESULT_OK) {
               Uri uri11 = data.getData();
               Log.e("uri", uri11.toString());
               try {
                   //使用ContentProvider通过URI获取原始图片
            	   BitmapFactory.Options opts = new BitmapFactory.Options();
            	   opts.inSampleSize = 4;
            	   Bitmap photo =  BitmapFactory.decodeStream(this.getContentResolver().openInputStream(uri11),null, opts);
                       //为防止原始图片过大导致内存溢出，这里先缩小原图显示，然后释放原始Bitmap占用的内存
                       Bitmap smallBitmap = zoomBitmap(photo, photo.getWidth() / 2, photo.getHeight()/2 );
                       //释放原始图片占用的内存，防止out of memory异常发生
                       photo.recycle();
                       FileOutputStream fos = null;
                       fos = new FileOutputStream("/mnt/sdcard/Lamp11.jpg");
                       String Imagelamp11 = "file:///mnt/sdcard/Lamp11.jpg";
                       smallBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                       lampeditpic11.append(Imagelamp11);
                    	   fos.close();
               } catch (FileNotFoundException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
            }
        	break;

        case LAMP_PIC12 :
            if (resultCode == Activity.RESULT_OK) {
               Uri uri12 = data.getData();
               Log.e("uri", uri12.toString());
               try {
                   //使用ContentProvider通过URI获取原始图片
            	   BitmapFactory.Options opts = new BitmapFactory.Options();
            	   opts.inSampleSize = 4;
            	   Bitmap photo =  BitmapFactory.decodeStream(this.getContentResolver().openInputStream(uri12),null, opts);
                       //为防止原始图片过大导致内存溢出，这里先缩小原图显示，然后释放原始Bitmap占用的内存
                       Bitmap smallBitmap = zoomBitmap(photo, photo.getWidth() / 2, photo.getHeight()/2 );
                       //释放原始图片占用的内存，防止out of memory异常发生
                       photo.recycle();
                       FileOutputStream fos = null;
                       fos = new FileOutputStream("/mnt/sdcard/Lamp12.jpg");
                       String Imagelamp12 = "file:///mnt/sdcard/Lamp12.jpg";
                       smallBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                       lampeditpic12.append(Imagelamp12);

                    	   fos.close();
               } catch (FileNotFoundException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
            }
        	break;
            //YILINEDIT-voiceRecg
            case RECORD:

                if (resultCode == RESULT_OK) {
                    String resultsString = "";
                    ArrayList<String> results = data.getStringArrayListExtra(
                            RecognizerIntent.EXTRA_RESULTS);
                    for (int i = 0; i< results.size(); i++) {
                        recResult[i] = results.get(i);
                    }
                    //Toast.makeText(this, recResult[0], Toast.LENGTH_LONG).show();
                    if(isVoiceRecMode){
                        Log.d(TAG,"學習 "+recResult[0]+recResult[1]);
                        if(recResult[0]=="" && recResult[0]==null){
                            Toast.makeText(this,
                                    "辨識失敗", Toast.LENGTH_LONG).show();
                        }else{
                            Log.d(TAG,"把結果匯入資料庫 action"+recAction);
                            DBOperation dbo = new DBOperation(getApplicationContext());
                            dbo.insertArray(recResult,recAction);
                            dbo.close();
                        }

                     /*isVoiceRecMode =false;
                     changeMenu="voice_stop";*/
                    }else{
                        if(recResult[0]=="" && recResult[0]==null){
                            Toast.makeText(this,
                                    "辨識失敗", Toast.LENGTH_LONG).show();
                        }else{
                            Log.d(TAG,"尋找資料庫 "+recResult[0]);
                            DBOperation dbo = new DBOperation(getApplicationContext());
                            String voiceResult=dbo.getVoiceResult(recResult[0]);
                            Log.d(TAG,"結果 "+voiceResult);
                            dbo.close();
                            if(voiceResult=="finalResultIsNull" ){
                                Toast.makeText(this,
                                        "辨識失敗", Toast.LENGTH_LONG).show();
                                Log.d(TAG,"失敗 ");
                            }else{
                                sendmessage(voiceResult);
                                Log.d(TAG,"成功 ");
                            }

                        }

                    }
                }
                break;
            case RETRUN_MAIN:
                init();
                break;
    		}

        }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	menu.add(0, st, 0, "開啟學習");
        menu.add(0, scan, 1, "掃描裝置");
        //YILINEDIT-voiceRecg
        menu.add(0,record,2,"語音操作");
        menu.add(0, record_learning, 3, "語音學習");
             return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        
        case st:
            changeMenu = "learning";//YILINEDIT-voiceRecg
        	String view99 = ("Li");
            String message99 = view99.toString();
            sendmessage(message99);
        	return true;
        case close:
            changeMenu = "none";//YILINEDIT-voiceRecg
        	String view100 = ("Cc");
            String message100 = view100.toString();
            sendmessage(message100);
        	return true;
        	
        case scan:
            //sendmessage("W+o?o");
        	viewPager.setCurrentItem(page_ip_config);
        	return true;
        //YILINEDIT-voiceRecg
        case record:
            voiceRec("開始辨識(確認網路良好)");
            break;
        case record_learning:
            Log.e(TAG,"record_learning");
            changeMenu = "voiceLearning";
            Log.e(TAG,"RE");
            viewPager.setCurrentItem(1);
            /*AlertDialog alertDialog = getVoiceAlertDialog("這是一個對話框");
                     alertDialog.show();*/
            isVoiceRecMode=true;
            changeMenu = "voice";
            break;
        case record_learning_stop:
            isVoiceRecMode=false;
            changeMenu = "voice_stop";
            break;
        }
        return false;
    }

    //YILINEDIT-voiceRecg
    public void voiceRec(String title){
        try {
            Intent intent = new Intent(
                    RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(
                    RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(
                    RecognizerIntent.EXTRA_PROMPT,
                    title);

            startActivityForResult(intent, RECORD);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this,
                    "ActivityNotFoundException", Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	menu.clear();
        //YILINEDIT-voiceRecg因為要加語音辨識整個有調整過
        if (changeMenu=="learning") {
            menu.removeItem(st);
            menu.removeItem(record);
            menu.removeItem(record_learning);
            menu.add(0, close, 0, "關閉學習");
            menu.add(0, scan, 1, "掃描裝置");


        } else if(changeMenu=="none"){
            menu.removeItem(close);
            menu.add(0, st, 0, "開啟學習");
            menu.add(0, scan, 1, "掃描裝置");
            menu.add(0,record,2,"語音操作");
            menu.add(0, record_learning, 3, "語音學習");

        }else if(changeMenu=="voice"){

            menu.removeItem(st);
            menu.removeItem(record);
            menu.removeItem(record_learning);
            menu.add(0,record_learning_stop,3,"關閉語音學習");
        }else if(changeMenu=="voice_stop"){
            menu.removeItem(record_learning_stop);
            menu.add(0, st, 0, "開啟學習");
            menu.add(0, scan, 1, "掃描裝置");
            menu.add(0,record,2,"語音操作");
            menu.add(0,record_learning,3,"語音學習");
        }
    	    return super.onPrepareOptionsMenu(menu);
    				}

        
    @Override 
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, 
    float velocityY) { 
    //velocityX表示横向的移动，根据手指移动的方向切换
    if(velocityX < 0){ 
    Toast.makeText(this, "left", Toast.LENGTH_LONG).show(); 
    }else if(velocityX > 0){ 
    Toast.makeText(this, "right", Toast.LENGTH_LONG).show(); 
    } 
    return false; 
    }

	@Override
	public void onGesture(GestureOverlayView arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGestureCancelled(GestureOverlayView overlay, MotionEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGestureEnded(GestureOverlayView overlay, MotionEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGestureStarted(GestureOverlayView overlay, MotionEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override 
	public boolean onTouch(View v, MotionEvent event) { 
	//detector.onTouchEvent(event); 
	return true; 
	}
	//在按下动作时被调用 
	@Override 
	public boolean onDown(MotionEvent e) { 
	return false; 
	} 
	//在长按时被调用 
	@Override 
	public void onLongPress(MotionEvent e) { 
	} 
	   
	//在滚动时调用 
	@Override 
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, 
	float distanceY) { 
	return false; 
	} 
	   
	//在按住时被调用 
	@Override 
	public void onShowPress(MotionEvent e) { 
	} 
	   
	//在抬起时被调用 
	@Override 
	public boolean onSingleTapUp(MotionEvent e) { 

	return false; 
    
			}
	
	public void readData(int i){
		switch(i){
		
		case 1:
		
        lamp = getSharedPreferences(lampdata,0);
        lampeditText1.setText(lamp.getString(btn01name, ""));
        lampeditText2.setText(lamp.getString(btn02name, ""));
        lampeditText3.setText(lamp.getString(btn03name, ""));
        lampeditText4.setText(lamp.getString(btn04name, ""));
        lampeditText5.setText(lamp.getString(btn05name, ""));
        lampeditText6.setText(lamp.getString(btn06name, ""));
        lampeditText7.setText(lamp.getString(btn07name, ""));
        lampeditText8.setText(lamp.getString(btn08name, ""));
        lampeditText9.setText(lamp.getString(btn09name, ""));
        lampeditText10.setText(lamp.getString(btn10name, ""));
        lampeditText11.setText(lamp.getString(btn11name, ""));
        lampeditText12.setText(lamp.getString(btn12name, ""));
        lampeditpic1.setText(lamp.getString(btn1pic, ""));
        lampeditpic2.setText(lamp.getString(btn2pic, ""));
        lampeditpic3.setText(lamp.getString(btn3pic, ""));
        lampeditpic4.setText(lamp.getString(btn4pic, ""));
        lampeditpic5.setText(lamp.getString(btn5pic, ""));
        lampeditpic6.setText(lamp.getString(btn6pic, ""));
        lampeditpic7.setText(lamp.getString(btn7pic, ""));
        lampeditpic8.setText(lamp.getString(btn8pic, ""));
        lampeditpic9.setText(lamp.getString(btn9pic, ""));
        lampeditpic10.setText(lamp.getString(btn10pic, ""));
        lampeditpic11.setText(lamp.getString(btn11pic, ""));
        lampeditpic12.setText(lamp.getString(btn12pic, ""));
		
        break;
        
		case 2:
        
        setting = getSharedPreferences(custom1data,0);
        
        custom1edit.setText(setting.getString(custom1name, ""));
//        meditText1.setText(setting.getString(btn54name, ""));
//        meditText2.setText(setting.getString(btn55name, ""));
//        meditText3.setText(setting.getString(btn56name, ""));
//        meditText4.setText(setting.getString(btn57name, ""));
//        meditText5.setText(setting.getString(btn58name, ""));
//        meditText6.setText(setting.getString(btn59name, ""));
//        meditText7.setText(setting.getString(btn60name, ""));
//        meditText8.setText(setting.getString(btn61name, ""));
//        meditText9.setText(setting.getString(btn62name, ""));
          meditText1.setText(setting.getString(btn63name, ""));
          meditText2.setText(setting.getString(btn64name, ""));
          meditText3.setText(setting.getString(btn65name, ""));
          meditText4.setText(setting.getString(btn66name, ""));
          meditText5.setText(setting.getString(btn67name, ""));
          meditText6.setText(setting.getString(btn68name, ""));
//        meditText46.setText(setting.getString(btn99name, ""));
//        meditText47.setText(setting.getString(btn100name, ""));
//        meditText48.setText(setting.getString(btn101name, ""));
//        meditText49.setText(setting.getString(btn102name, ""));
//        meditText50.setText(setting.getString(btn103name, ""));
//        meditText51.setText(setting.getString(btn104name, ""));
       break;
       
		case 3 :
			
			settingb = getSharedPreferences(custom2data,0);
			
			custom2edit.setText(settingb.getString(custom2name, ""));
//	        meditText16.setText(settingb.getString(btn69name, ""));
//	        meditText17.setText(settingb.getString(btn70name, ""));
//	        meditText18.setText(settingb.getString(btn71name, ""));
//	        meditText19.setText(settingb.getString(btn72name, ""));
//	        meditText20.setText(settingb.getString(btn73name, ""));
//	        meditText21.setText(settingb.getString(btn74name, ""));
//	        meditText22.setText(settingb.getString(btn75name, ""));
//	        meditText23.setText(settingb.getString(btn76name, ""));
//	        meditText24.setText(settingb.getString(btn77name, ""));
	        meditText16.setText(settingb.getString(btn78name, ""));
	        meditText17.setText(settingb.getString(btn79name, ""));
	        meditText18.setText(settingb.getString(btn80name, ""));
	        meditText19.setText(settingb.getString(btn81name, ""));
	        meditText20.setText(settingb.getString(btn82name, ""));
	        meditText21.setText(settingb.getString(btn83name, ""));
//	        meditText52.setText(settingb.getString(btn105name, ""));
//	        meditText53.setText(settingb.getString(btn106name, ""));
//	        meditText54.setText(settingb.getString(btn107name, ""));
//	        meditText55.setText(settingb.getString(btn108name, ""));
//	        meditText56.setText(settingb.getString(btn109name, ""));
//	        meditText57.setText(settingb.getString(btn110name, ""));
			break;
			
		case 4 :
			
			setting2 = getSharedPreferences(custom3data,0);
	        
			custom3edit.setText(setting2.getString(custom3name, ""));
//	        meditText31.setText(setting2.getString(btn84name, ""));
//	        meditText32.setText(setting2.getString(btn85name, ""));
//	        meditText33.setText(setting2.getString(btn86name, ""));
//	        meditText34.setText(setting2.getString(btn87name, ""));
//	        meditText35.setText(setting2.getString(btn88name, ""));
//	        meditText36.setText(setting2.getString(btn89name, ""));
//	        meditText37.setText(setting2.getString(btn90name, ""));
//	        meditText38.setText(setting2.getString(btn91name, ""));
//	        meditText39.setText(setting2.getString(btn92name, ""));
	        meditText31.setText(setting2.getString(btn93name, ""));
	        meditText32.setText(setting2.getString(btn94name, ""));
	        meditText33.setText(setting2.getString(btn95name, ""));
	        meditText34.setText(setting2.getString(btn96name, ""));
	        meditText35.setText(setting2.getString(btn97name, ""));
	        meditText36.setText(setting2.getString(btn98name, ""));
//	        meditText58.setText(setting2.getString(btn111name, ""));
//	        meditText59.setText(setting2.getString(btn112name, ""));
//	        meditText60.setText(setting2.getString(btn113name, ""));
//	        meditText61.setText(setting2.getString(btn114name, ""));
//	        meditText62.setText(setting2.getString(btn115name, ""));
//	        meditText63.setText(setting2.getString(btn116name, ""));
	        
			break;
			
		case 5 :
			
			marcoset = getSharedPreferences(marcosetdata,0);
			marcosetname.setText(marcoset.getString(marconame, ""));
			marcosetbtn.setText(marcoset.getString(marcobtn, ""));
			
			break;
			
		case 6 :
			
			marco1set = getSharedPreferences(marcoset1data,0);
			marcoset1name.setText(marco1set.getString(marco1name, ""));
			marco1setbtn.setText(marco1set.getString(marco1btn, ""));
			
			break;
			
		case 7 :
			
			marco2set = getSharedPreferences(marcoset2data,0);
			marcoset2name.setText(marco2set.getString(marco2name, ""));
			marco2setbtn.setText(marco2set.getString(marco2btn, ""));
			break;
			
		case 8 :
	
			marco3set = getSharedPreferences(marcoset3data,0);
			marcoset3name.setText(marco3set.getString(marco3name, ""));
			marco3setbtn.setText(marco3set.getString(marco3btn, ""));
			break;
	
		case 9 :
	
			marco4set = getSharedPreferences(marcoset4data,0);
			marcoset4name.setText(marco4set.getString(marco4name, ""));
			marco4setbtn.setText(marco4set.getString(marco4btn, ""));
		break;
	
		case 10 :
	
			marco5set = getSharedPreferences(marcoset5data,0);
			marcoset5name.setText(marco5set.getString(marco5name, ""));
			marco5setbtn.setText(marco5set.getString(marco5btn, ""));
	
			break;
			
		case 11 :
			
			curtain = getSharedPreferences(curtaindata,0);
			cur1edit.setText(curtain.getString(curtain1, ""));
			cur2edit.setText(curtain.getString(curtain2, ""));
			cur3edit.setText(curtain.getString(curtain3, ""));
			cur4edit.setText(curtain.getString(curtain4, ""));
			cur5edit.setText(curtain.getString(curtain5, ""));
			cur6edit.setText(curtain.getString(curtain6, ""));
			cur7edit.setText(curtain.getString(curtain7, ""));
			cur8edit.setText(curtain.getString(curtain8, ""));
			cur9edit.setText(curtain.getString(curtain9, ""));
			cur10edit.setText(curtain.getString(curtain10, ""));
			cur11edit.setText(curtain.getString(curtain11, ""));
			cur12edit.setText(curtain.getString(curtain12, ""));
			break;
			
		case 12 :
			airseting = getSharedPreferences(airdata,0);
			meditText52.setText(airseting.getString(btn17name, ""));
	        meditText53.setText(airseting.getString(btn18name, ""));
	        meditText54.setText(airseting.getString(btn19name, ""));
	        meditText55.setText(airseting.getString(btn20name, ""));
	        meditText56.setText(airseting.getString(btn21name, ""));
	        meditText57.setText(airseting.getString(btn22name, ""));
			break;
			
		case 13 :
			IPname = getSharedPreferences(Ipdata,0);
	        ip.setText(IPname.getString(IP, ""));
	        port.setText("8899");
            ip_gcm.setText(IPname.getString(IP_gcm, ""));
            port_gcm.setText("1337");
			break;
       
		case 14:
  			OPenCam = getSharedPreferences(OpenCamData,0);
  			opencam.setText(OPenCam.getString(CAM, ""));
  			break;
			
		}
    }
    public void saveData(int i){
    	switch(i){
    	
    	case 1 :
    	
        lamp = getSharedPreferences(lampdata,0);
        lamp.edit()
        	.putString(btn01name, lampeditText1.getText().toString())
        	.putString(btn02name, lampeditText2.getText().toString())
        	.putString(btn03name, lampeditText3.getText().toString())
        	.putString(btn04name, lampeditText4.getText().toString())
        	.putString(btn05name, lampeditText5.getText().toString())
        	.putString(btn06name, lampeditText6.getText().toString())
        	.putString(btn07name, lampeditText7.getText().toString())
        	.putString(btn08name, lampeditText8.getText().toString())
        	.putString(btn09name, lampeditText9.getText().toString())
        	.putString(btn10name, lampeditText10.getText().toString())
        	.putString(btn11name, lampeditText11.getText().toString())
        	.putString(btn12name, lampeditText12.getText().toString())
        	.putString(btn1pic, lampeditpic1.getText().toString())
        	.putString(btn2pic, lampeditpic2.getText().toString())
        	.putString(btn3pic, lampeditpic3.getText().toString())
        	.putString(btn4pic, lampeditpic4.getText().toString())
        	.putString(btn5pic, lampeditpic5.getText().toString())
        	.putString(btn6pic, lampeditpic6.getText().toString())
        	.putString(btn7pic, lampeditpic7.getText().toString())
        	.putString(btn8pic, lampeditpic8.getText().toString())
        	.putString(btn9pic, lampeditpic9.getText().toString())
        	.putString(btn10pic, lampeditpic10.getText().toString())
        	.putString(btn11pic, lampeditpic11.getText().toString())
        	.putString(btn12pic, lampeditpic12.getText().toString())
        	.commit();
        break;
        
        case 2 :
        setting = getSharedPreferences(custom1data,0);
        setting.edit()
            .putString(custom1name, custom1edit.getText().toString())
//            .putString(btn54name, meditText1.getText().toString())
//            .putString(btn55name, meditText2.getText().toString())
//            .putString(btn56name, meditText3.getText().toString())
//            .putString(btn57name, meditText4.getText().toString())
//            .putString(btn58name, meditText5.getText().toString())
//            .putString(btn59name, meditText6.getText().toString())
//            .putString(btn60name, meditText7.getText().toString())
//            .putString(btn61name, meditText8.getText().toString())
//            .putString(btn62name, meditText9.getText().toString())
              .putString(btn63name, meditText1.getText().toString())
              .putString(btn64name, meditText2.getText().toString())
              .putString(btn65name, meditText3.getText().toString())
              .putString(btn66name, meditText4.getText().toString())
              .putString(btn67name, meditText5.getText().toString())
              .putString(btn68name, meditText6.getText().toString())
//            .putString(btn99name, meditText46.getText().toString())
//            .putString(btn100name, meditText47.getText().toString())
//            .putString(btn101name, meditText48.getText().toString())
//            .putString(btn102name, meditText49.getText().toString())
//            .putString(btn103name, meditText50.getText().toString())
//            .putString(btn104name, meditText51.getText().toString())
            .commit();
        break;
        
        case 3 :
        	
        	settingb = getSharedPreferences(custom2data,0);
        	settingb.edit()
        		.putString(custom2name, custom2edit.getText().toString())
//                .putString(btn69name, meditText16.getText().toString())
//                .putString(btn70name, meditText17.getText().toString())
//                .putString(btn71name, meditText18.getText().toString())
//                .putString(btn72name, meditText19.getText().toString())
//                .putString(btn73name, meditText20.getText().toString())
//                .putString(btn74name, meditText21.getText().toString())
//                .putString(btn75name, meditText22.getText().toString())
//                .putString(btn76name, meditText23.getText().toString())
                  .putString(btn78name, meditText16.getText().toString())
                  .putString(btn79name, meditText17.getText().toString())
                  .putString(btn80name, meditText18.getText().toString())
                  .putString(btn81name, meditText19.getText().toString())
                  .putString(btn82name, meditText20.getText().toString())
                  .putString(btn83name, meditText21.getText().toString())
//                .putString(btn83name, meditText22.getText().toString())
//                .putString(btn105name, meditText52.getText().toString())
//                .putString(btn106name, meditText53.getText().toString())
//                .putString(btn107name, meditText54.getText().toString())
//                .putString(btn108name, meditText55.getText().toString())
//                .putString(btn109name, meditText56.getText().toString())
//                .putString(btn110name, meditText57.getText().toString())
                .commit();
        	 break;
        case 4 :
        	setting2 = getSharedPreferences(custom3data,0);
            setting2.edit()
            	.putString(custom3name, custom3edit.getText().toString())
//                .putString(btn84name, meditText31.getText().toString())
//                .putString(btn85name, meditText32.getText().toString())
//                .putString(btn86name, meditText33.getText().toString())
//                .putString(btn87name, meditText34.getText().toString())
//                .putString(btn88name, meditText35.getText().toString())
//                .putString(btn89name, meditText36.getText().toString())
//                .putString(btn90name, meditText37.getText().toString())
//                .putString(btn91name, meditText38.getText().toString())
//                .putString(btn92name, meditText39.getText().toString())
                  .putString(btn93name, meditText31.getText().toString())
                  .putString(btn94name, meditText32.getText().toString())
                  .putString(btn95name, meditText33.getText().toString())
                  .putString(btn96name, meditText34.getText().toString())
                  .putString(btn97name, meditText35.getText().toString())
                  .putString(btn98name, meditText36.getText().toString())
//                .putString(btn111name, meditText58.getText().toString())
//                .putString(btn112name, meditText59.getText().toString())
//                .putString(btn113name, meditText60.getText().toString())
//                .putString(btn114name, meditText61.getText().toString())
//                .putString(btn115name, meditText62.getText().toString())
//                .putString(btn116name, meditText63.getText().toString())
                .commit();
        	break;
            
        case 5 :
        	marcoset = getSharedPreferences(marcosetdata,0);
        	marcoset.edit()
                .putString(marconame, marcosetname.getText().toString())
                .putString(marcobtn, marcosetbtn.getText().toString())
                .commit();
        	break;
        	
        case 6 :
        	marco1set = getSharedPreferences(marcoset1data,0);
        	marco1set.edit()
                .putString(marco1name, marcoset1name.getText().toString())
                .putString(marco1btn, marco1setbtn.getText().toString())
                .commit();
        	break;
        	
        case 7 :
        	marco2set = getSharedPreferences(marcoset2data,0);
        	marco2set.edit()
                .putString(marco2name, marcoset2name.getText().toString())
                .putString(marco2btn, marco2setbtn.getText().toString())
                .commit();
        	break;
        	
        case 8 :
        	marco3set = getSharedPreferences(marcoset3data,0);
        	marco3set.edit()
                .putString(marco3name, marcoset3name.getText().toString())
                .putString(marco3btn, marco3setbtn.getText().toString())
                .commit();
        	break;
        	
        case 9 :
        	marco4set = getSharedPreferences(marcoset4data,0);
        	marco4set.edit()
                .putString(marco4name, marcoset4name.getText().toString())
                .putString(marco4btn, marco4setbtn.getText().toString())
                .commit();
        	break;
        	
        case 10 :
        	marco5set = getSharedPreferences(marcoset5data,0);
        	marco5set.edit()
                .putString(marco5name, marcoset5name.getText().toString())
                .putString(marco5btn, marco5setbtn.getText().toString())
                .commit();
        	break;
        	
        
        case 11 :
        	
        	curtain = getSharedPreferences(curtaindata,0);
        	curtain.edit()
        		.putString(curtain1, cur1edit.getText().toString())
        		.putString(curtain2, cur2edit.getText().toString())
        		.putString(curtain3, cur3edit.getText().toString())
        		.putString(curtain4, cur4edit.getText().toString())
        		.putString(curtain5, cur5edit.getText().toString())
        		.putString(curtain6, cur6edit.getText().toString())
        		.putString(curtain7, cur7edit.getText().toString())
        		.putString(curtain8, cur8edit.getText().toString())
        		.putString(curtain9, cur9edit.getText().toString())
        		.putString(curtain10, cur10edit.getText().toString())
        		.putString(curtain11, cur11edit.getText().toString())
        		.putString(curtain12, cur12edit.getText().toString())
                .commit();
        	break;
        	
        	
        	 case 12:
        		 marcoset = getSharedPreferences(marcosetdata,0);
 	        	marcoset.edit()
 	                .putString(marconame, f1edit.getText().toString())
 	                .commit();
 	        	
 	        	marco1set = getSharedPreferences(marcoset1data,0);
 	        	marco1set.edit()
 	                .putString(marco1name, f2edit.getText().toString())
 	                .commit();
 	        	
 	        	marco2set = getSharedPreferences(marcoset2data,0);
 	        	marco2set.edit()
 	                .putString(marco2name, f3edit.getText().toString())
 	                .commit();
 	        	
 	        	marco3set = getSharedPreferences(marcoset3data,0);
 	        	marco3set.edit()
 	                .putString(marco3name, f4edit.getText().toString())
 	                .commit();
 	        	
 	        	marco4set = getSharedPreferences(marcoset4data,0);
 	        	marco4set.edit()
 	                .putString(marco4name, f5edit.getText().toString())
                        .commit();

                 marco5set = getSharedPreferences(marcoset5data,0);
 	        	marco5set.edit()
 	                .putString(marco5name, f6edit.getText().toString())
 	                .commit();
        	break;
        	
        	 case 13 :
     	        airseting = getSharedPreferences(airdata,0);
     	        airseting.edit()
           		.putString(btn17name, meditText52.getText().toString())
                        .putString(btn18name, meditText53.getText().toString())
                        .putString(btn19name, meditText54.getText().toString())
                        .putString(btn20name, meditText55.getText().toString())
           		.putString(btn21name, meditText56.getText().toString())
           		.putString(btn22name, meditText57.getText().toString())
                        .commit();
                 break;

            case 14:
                IPname = getSharedPreferences(Ipdata,0);
     	    		IPname.edit()
     	        	.putString(IP, ip.getText().toString())
     	        	.putString(PORT, port.getText().toString())
                            .commit();

            case 15:
                OPenCam = getSharedPreferences(OpenCamData,0);
           		OPenCam.edit()
    	                .putString(CAM, opencam.getText().toString())
    	                .commit();
           	break;
            case 16:
                IPname = getSharedPreferences(Ipdata,0);
                IPname.edit()
                        .putString(IP_gcm, ip_gcm.getText().toString())
                        .putString(PORT_gcm, port_gcm.getText().toString())
                        .commit();
                break;

		} 
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {//捕捉返回鍵
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {   
            ConfirmExit();//按返回鍵，則執行退出確認
            return true;   
        }   
        return super.onKeyDown(keyCode, event);   
    }
    public void ConfirmExit(){//退出確認
        AlertDialog.Builder ad=new AlertDialog.Builder(BluetoothChat.this);
        ad.setTitle("飛鑫遙控");
        ad.setMessage("確定關閉程式?");
        ad.setPositiveButton("是", new DialogInterface.OnClickListener() {//退出按鈕
            public void onClick(DialogInterface dialog, int i) {
                // TODO Auto-generated method stub
            	BluetoothChat.this.finish();//關閉activity
  
            }
        });
        ad.setNegativeButton("否",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int i) {
                //不退出不用執行任何操作
            }
        });
        ad.show();//示對話框
    }

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageSelected(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabChanged(String tabId) {
		// TODO Auto-generated method stub
		
	}
    //YILINEDIT-情境
    void sendMacro(final String macroArray,final Handler handler_sendMacro){
        //YILINEDIT
        new Thread(new Runnable(){
            public void run(){

                int timer=0;
                try{
                    for(int i=0;i<macroArray.length();i+=3){
                        //sendMessage(macroArray.substring(i, i+2));
                        handler_sendMacro.obtainMessage(BluetoothChat.SEND_MESSAGE_MACRO, macroArray.substring(i, i+2)).sendToTarget();
                        Log.e(TAG, macroArray.substring(i, i+2));
                        Log.e(TAG, macroArray.substring(i+2, i+3));

                        if(macroArray.substring(i+2, i+3).compareTo("'")==0){
                            timer =1;
                        }else if(macroArray.substring(i+2, i+3).compareTo("(")==0){
                            timer=2;
                        }else if(macroArray.substring(i+2, i+3).compareTo(")")==0){
                        }else if(macroArray.substring(i+2, i+3).compareTo("*")==0){
                            timer=4;
                        }else if(macroArray.substring(i+2, i+3).compareTo("+")==0){
                            timer=5;
                        }
                        Calendar begin = Calendar.getInstance();
                        int iDiffSec;
                        Log.e(TAG, "等"+timer+"秒");
                        do{
                            Calendar now= Calendar.getInstance();
                            iDiffSec = 60*(now.get(Calendar.MINUTE)-begin.get(Calendar.MINUTE))+(now.get(Calendar.SECOND)-begin.get(Calendar.SECOND));

                        }while(iDiffSec <timer);

                        // Log.e(TAG, "waitForSendNext");

                    }
                }
                catch(StringIndexOutOfBoundsException e){
                    e.getStackTrace();
                }



            }

        }).start();
    }
    void sendMessageAfter1s(final String message){
        //YILINEDIT
        new Thread(new Runnable(){
            public void run(){

                try{
                    Calendar begin = Calendar.getInstance();
                    int iDiffSec;
                        do{
                            Calendar now= Calendar.getInstance();
                            iDiffSec = 60000*(now.get(Calendar.MINUTE)-begin.get(Calendar.MINUTE))+1000*(now.get(Calendar.SECOND)-begin.get(Calendar.SECOND))+(now.get(Calendar.MILLISECOND)-begin.get(Calendar.MILLISECOND));

                        }while(iDiffSec <2000);
                    sendmessage(message);
                }
                catch(StringIndexOutOfBoundsException e){
                    e.getStackTrace();
                }
            }

        }).start();
    }
	/** 缩放Bitmap图片 **/
    public Bitmap zoomBitmap(Bitmap bitmap, int width, int height) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        Matrix matrix = new Matrix();
        float scaleWidth = ((float) width / w);
        float scaleHeight = ((float) height / h);
        matrix.postScale(scaleWidth, scaleHeight);// 利用矩阵进行缩放不会造成内存溢出
        Bitmap newbmp = Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);
        return newbmp;
    }
    @Override
    public boolean handleMessage(Message msg) {
        // TODO Auto-generated method stub
        switch(msg.what){
            case BluetoothChat.SEND_MESSAGE_MACRO:
                String readMessage = msg.obj.toString();
                sendmessage(readMessage);
                break;
        }

        return false;
    }

	}





	



		   
		
	    
	    
